var msInMin = 60 * 1000;
var msInHour = msInMin * 60;
var msInDay = msInHour * 24;

var remainingTime;

function begin(){
	animateTagLine();
}

function animateTagLine(){
	$("#tagline").css("top", "0px");
	$("#tagline").css("opacity", "1.0");
}

function notifyClicked(){
	$("#notifyButton").remove();
	$("#subscriptionForm").css("visibility", "visible");
}

function subscribeClicked(){
	$("#subscriptionForm").remove();
	$("#postSubscribeMsg").css("visibility", "visible");
}

function playVideo(){
	$("#videoFrame .youtubeMask").remove();
	$("#videoFrame").append('<iframe class="main-container" src="//www.youtube.com/embed/OjA0hopSNeI?autoplay=1&theme=light" frameborder="0" allowfullscreen></iframe>');
}