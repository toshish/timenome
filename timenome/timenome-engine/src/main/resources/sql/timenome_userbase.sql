-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.1.49-1ubuntu8.1





DROP TABLE IF EXISTS `timenome_user`.`USER`;
CREATE TABLE  `timenome_user`.`USER` (
  `userid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` tinytext NOT NULL,
  `password_hash` tinytext NOT NULL,
  `apikey` tinytext NOT NULL,
  `secret` tinytext NOT NULL,
  `name` tinytext NOT NULL,
  PRIMARY KEY (`userid`),
  UNIQUE KEY `username` (`username`(128))
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `timenome_user`.`USER`
--


LOCK TABLES `USER` WRITE;
UNLOCK TABLES;
--
-- Definition of table `timenome_user`.`CHART_USER`
--

DROP TABLE IF EXISTS `timenome_user`.`CHART_USER`;
CREATE TABLE  `timenome_user`.`CHART_USER` (
  `chartuserid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `chartusername` tinytext NOT NULL,
  `chartpassword_hash` tinytext NOT NULL,
  `userid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`chartuserid`),
  UNIQUE KEY `chartusername` (`chartusername`(128)),
  KEY `userid_fk_constraint` (`userid`),
  CONSTRAINT `userid_fk_constraint` FOREIGN KEY (`userid`) REFERENCES `USER` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `timenome_user`.`CHART_USER`
--

LOCK TABLES `CHART_USER` WRITE;
UNLOCK TABLES;


--
-- Definition of table `timenome_user`.`PREFERENCES`
--

DROP TABLE IF EXISTS `timenome_user`.`PREFERENCES`;
CREATE TABLE  `timenome_user`.`PREFERENCES` (
  `userid` int(10) unsigned NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` longtext NOT NULL,
  PRIMARY KEY (`userid`, `key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `timenome_user`.`PREFERENCES`
--

LOCK TABLES `PREFERENCES` WRITE;
UNLOCK TABLES;


--
-- Definition of table `timenome_user`.`USER`
--







