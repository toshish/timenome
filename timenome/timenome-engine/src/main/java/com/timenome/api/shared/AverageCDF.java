package com.timenome.api.shared;

import java.util.List;

import com.timenome.api.BuiltInCDF;
import com.timenome.api.Entry;


class AverageCDF extends AbstractConsolidationFunction {

	public AverageCDF() {
		super(BuiltInCDF.AVERAGE);
	}

	@Override
	public Entry<? extends Number> consolidate(List<Entry<? extends Number>> dataToBeConsolidated) {
		Entry<? extends Number> sumEntry = new SummationCDF().consolidate(dataToBeConsolidated);
		double average = sumEntry.getValue().doubleValue() / dataToBeConsolidated.size();
		return new Entry<Number>(average);
	}

}
