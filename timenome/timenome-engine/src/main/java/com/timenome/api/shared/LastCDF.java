package com.timenome.api.shared;

import java.util.List;

import com.timenome.api.BuiltInCDF;
import com.timenome.api.Entry;


class LastCDF extends AbstractConsolidationFunction {

	protected LastCDF() {
		super(BuiltInCDF.LAST);
	}
	
	@Override
	public Entry<? extends Number> consolidate(List<Entry<? extends Number>> dataToBeConsolidated) {
		if(dataToBeConsolidated.size() > 0) {
			return dataToBeConsolidated.get(dataToBeConsolidated.size() - 1);
		}
		return null;
	}
	
}
