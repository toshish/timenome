package com.timenome.api.core;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.carpalsoft.timewarp.schema.storageimpl.Storage;
import com.timenome.api.exceptions.ConnectionException;
import com.timenome.api.user.TimeNomeUserBase;

public class Registry {

	private Map<String, Connection> connectionMap;
	private static Registry registry;

	private Registry() {
		connectionMap = new ConcurrentHashMap<String, Connection>();
	}
	
	public static Registry getRegistry() {
		if(registry == null) {
			registry = new Registry();
		}
		return registry;
	}
	
	public Connection connect(Map<String, String> connectionParams) throws JAXBException, ClassNotFoundException, ConnectionException, InstantiationException, IllegalAccessException{
		if(!connectionParams.containsKey("name")) {
			throw new ConnectionException("'name' key must be present in the map.");
		}
		String connectionName = connectionParams.get("name"); 
		
		JAXBContext jaxbC = JAXBContext.newInstance(Storage.class);
		Unmarshaller unmarshaller = jaxbC.createUnmarshaller();
		Storage storage = (Storage) unmarshaller.unmarshal(Registry.class.getClassLoader().getResourceAsStream("schema/storageImpl.xml"));
		
		List<Storage.StorageImpl> storageImpls = storage.getStorageImpl();
		Class<?> storageImplClass = null;
		for(Storage.StorageImpl storageImpl : storageImpls) {
			if(storageImpl.getName().equalsIgnoreCase(connectionName)) {
				storageImplClass = Class.forName(storageImpl.getConnection());
				break;
			}
		}
		Connection connection = null;
		if(storageImplClass != null) {
			Connector connector = (Connector) storageImplClass.newInstance();
			connection = connector.connect(connectionParams);
		}
		if(connection != null) {
			connectionMap.put(connectionName, connection);
		}
		return connection;
	}

	
}
