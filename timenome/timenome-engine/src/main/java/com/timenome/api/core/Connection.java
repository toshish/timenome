package com.timenome.api.core;

import java.util.Map;

import com.timenome.api.exceptions.DataStoreException;
import com.timenome.api.exceptions.UserAlreadyExists;
import com.timenome.api.exceptions.UserDoesNotExists;

public interface Connection {
	
	public boolean deleteDataStore(String dataStoreName, String username) throws DataStoreException, UserDoesNotExists;
	
	public void close();

	public DataStore createDataStore(String dsName, String username) throws DataStoreException, UserDoesNotExists;

	public DataStore getDataStore(String dsName, String username) throws UserDoesNotExists;

	public Map<String, DataStore> getAllDataStores(String username) throws UserDoesNotExists;
	
	public Map<String, User> getAllUsers();
	
	public User createUser(String username) throws UserAlreadyExists;

	public boolean deleteUser(String username) throws UserDoesNotExists;

	User getUser(String username) throws UserDoesNotExists;

}
