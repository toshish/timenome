package com.timenome.api.logger;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.timenome.api.constant.TimeNomeConstants;

public class TimeNomeLogger extends Logger {
	private FileHandler fileHandler;
	private ConsoleHandler consoleHandler = new ConsoleHandler();

	private static HashMap<Class<?>, TimeNomeLogger> loggerMap = new HashMap<>();

	private TimeNomeLogger(String loggerName) throws SecurityException, IOException {
		super(loggerName, null);
		setLevel(Level.FINEST);

		File file = new File(TimeNomeConstants.TIMENOME_PROPERTIES_FILE);

		Properties logProp = new Properties();
		try {
			Properties prop = new Properties();
			prop.load(new FileReader(file));
			this.fileHandler = new FileHandler(prop.getProperty("timenome.log.file"));
			logProp.load(TimeNomeLogger.class.getClassLoader().getResourceAsStream("conf/timenome-logger.properties"));
			this.fileHandler.setLevel(Level.parse(logProp.getProperty("timenome.logger.file.log.level")));
			consoleHandler.setLevel(Level.parse(logProp.getProperty("timenome.logger.console.log.level")));
		} catch (Exception e) {
			if(this.fileHandler == null) {
				this.fileHandler = new FileHandler();
			}
			this.fileHandler.setLevel(Level.INFO);
			consoleHandler.setLevel(Level.INFO);
			this.warning("Couldn't load configs from logger properties file. Continuing with default logger config.");
			if(this.fileHandler == null) {
				this.fileHandler = new FileHandler("timenome.log");
			}
		} 
		addHandler(fileHandler);
		addHandler(consoleHandler);
	}

	public static TimeNomeLogger getTimeNomeLogger(Class<?> clazz) {
		if(loggerMap.get(clazz) == null) {
			try {
				TimeNomeLogger timeNomeLogger = new TimeNomeLogger(clazz.getCanonicalName());
				loggerMap.put(clazz, timeNomeLogger);
			} catch (SecurityException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return loggerMap.get(clazz);
	}
}
