package com.timenome.api.core;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.TransferQueue;
import java.util.logging.Logger;

import com.timenome.api.BuiltInCDF;
import com.timenome.api.Entry;
import com.timenome.api.constant.TimeNomeConstants;
import com.timenome.api.exceptions.ArchiveException;
import com.timenome.api.execution.DataStoreThread;
import com.timenome.api.logger.TimeNomeLogger;
import com.timenome.api.shared.ConsolidationFunction;

public abstract class DataStore {
	private String name;

	protected Map<String, Archive> archiveMap;
	private TransferQueue<Entry<? extends Number>> dataEntriesQueue;
	
	private static Logger logger = TimeNomeLogger.getTimeNomeLogger(DataStore.class);

	private Map<String, Archive> notConnectedArchivesMap;
	
	private boolean areAllArchivesConnected = false;
	
	private DataStoreThread thread;
	
	protected Map<String, String> attributeMap;
	
	protected DataStore(String dataStoreName) {
		this.name = dataStoreName;
		archiveMap = new ConcurrentHashMap<String, Archive>();
		dataEntriesQueue = new LinkedTransferQueue<Entry<? extends Number>>();
		notConnectedArchivesMap = new ConcurrentHashMap<String, Archive>();
		attributeMap = new ConcurrentHashMap<String, String>();
		connectToAllArchives();
		if(!areAllArchivesConnected) {
			String log = "Couldn't connect one or more archives to datastore " + this + "." + TimeNomeConstants.NEW_LINE;
			for(String key : notConnectedArchivesMap.keySet()) {
				log = log + notConnectedArchivesMap.get(key) + TimeNomeConstants.NEW_LINE;
			}
			logger.warning(log);
		}
		setThread(new DataStoreThread(this));
	}
	
	private void connectToAllArchives() {
		Iterator<String> archiveIterator = archiveMap.keySet().iterator();
		while(archiveIterator.hasNext()) {
			Archive archive = archiveMap.get(archiveIterator.next());
			try {
				archive.connectToDataStore(this);
			} catch (ArchiveException e) {
				notConnectedArchivesMap.put(archive.getArchiveName(), archive);
			}
		}
		if(notConnectedArchivesMap.size() == 0) {
			areAllArchivesConnected = true;
		}
	}
	
	final public String getDataStoreName() {
		return name;
	}
	
	final public boolean addEntry(Entry<? extends Number> entry) {
		return dataEntriesQueue.add(entry);
	}
	
	final public boolean addBulk(List<Entry<? extends Number>> bulkEntry) {
		return dataEntriesQueue.addAll(bulkEntry);
	}
	
	final public void putEntry(Entry<? extends Number> entry) throws InterruptedException {
		dataEntriesQueue.put(entry);
	}
	
	
	/**
	 * 
	 * @return {@link java.util.Map.Entry} of {@link Archive}s in this Data store. 
	 *         List will be empty if no archives present in the Data store.
	 */
	final public Set<java.util.Map.Entry<String, Archive>> getArchives() {
		ArrayList<Archive> archiveList = new ArrayList<Archive>();
		Iterator<String> iter = archiveMap.keySet().iterator(); 
		while(iter.hasNext()) {
			String archiveName = iter.next();
			archiveList.add(archiveMap.get(archiveName));
		}
		return archiveMap.entrySet();
	}
	/**
	 * 
	 * @param archiveName
	 * @return {@link Archive} instance of the archive name passed. 
	 *         {@code null} if archive doesn't exist in this Data store.
	 */
	final public Archive getArchive(String archiveName) {
		return archiveMap.get(archiveName);
	}
	
	public abstract List<Entry<? extends Number>> get();
		
	/**
	 * Registers a new archive with the DataStore. Once registered, put operation on
	 * DataStore will add data to the newly added archive.
	 * 
	 * @param archive
	 * @throws ArchiveException
	 */
	final public void addArchive(Archive archive) throws ArchiveException {
		if (archiveMap.containsKey(archive.getArchiveName())) {
			throw new ArchiveException("Another Archive with the same name exists.");
		}
		addArchiveSpec(archive);
		archiveMap.put(archive.getArchiveName(), archive);
		archive.connectToDataStore(this);
	}
	
	/**
	 * Creates and adds a new archive with the DataStore. Once added, put operation on
	 * DataStore will add data to the newly added archive.
	 * 
	 * @param archiveName
	 * @param storageRange
	 * @param storagePrecision
	 * @param consolidationFunction
	 * @return 
	 * @throws ArchiveException
	 */
	final public Archive addArchive(String archiveName, TimeRangeDef storageRange, TimeRangeDef storagePrecision,
			ConsolidationFunction consolidationFunction) throws ArchiveException {
		if (archiveMap.containsKey(archiveName)) {
			throw new ArchiveException("Another Archive with the same name exists.");
		}
		Archive archive = getConcreteArchive(archiveName, storageRange, storagePrecision, consolidationFunction);
		addArchive(archive);
		return archive;
	}
	
	
	/**
	 * Creates and adds a new archive with the DataStore. Once added, put operation on
	 * DataStore will add data to the newly added archive.
	 * 
	 * @param archiveName
	 * @param storageRange
	 * @param storagePrecision
	 * @param builtInCDF
	 * @throws ArchiveException
	 */
	final public Archive addArchive(String archiveName, TimeRangeDef storageRange, TimeRangeDef storagePrecision,
			BuiltInCDF builtInCDF) throws ArchiveException {
		if(archiveMap.containsKey(archiveName)) {
			throw new ArchiveException("Another Archive with the same name exists.");
		}
		Archive archive = getConcreteArchive(archiveName, storageRange, storagePrecision, builtInCDF);
		addArchive(archive);
		return archive;
	}
	/**
	 * This abstract method should define concrete implementation of adding Archive.
	 * @param archive
	 */
	protected abstract void addArchiveSpec(Archive archive);
	
	protected abstract Archive getConcreteArchive(String archiveName, TimeRangeDef storageRange, TimeRangeDef storagePrecision,
			ConsolidationFunction consolidationFunction);
	protected abstract Archive getConcreteArchive(String archiveName,
			TimeRangeDef storageRange, TimeRangeDef storagePrecision,
			BuiltInCDF builtInCDF);

	final public void removeArchive(String archiveName) throws ArchiveException {
		if (!archiveMap.containsKey(archiveName)) {
			throw new ArchiveException("Archive with the name '" + archiveName + "' cannot be found in data store '" + this.getDataStoreName() + "'");
		}
		if(removeArchiveSpec(archiveName)) {
			archiveMap.remove(archiveName);
		}
		else {
			throw new ArchiveException("Unrecognized error occured while removing archive.");
		}
	}

	protected abstract boolean removeArchiveSpec(String archiveName) throws ArchiveException;

	public final TransferQueue<Entry<? extends Number>> getDataEntriesQueue() {
		return dataEntriesQueue;
	}

	public boolean hasArchive(String archiveName) {
		return this.archiveMap.containsKey(archiveName);
	}
	
	public void stopAllArchives() {
		for(Archive archive : archiveMap.values()) {
			archive.stop();
		}
	}
	
	public void stop() {
		stopAllArchives();
		getThread().markForStop();
	}

	/**
	 * @return the thread
	 */
	public DataStoreThread getThread() {
		return thread;
	}

	/**
	 * @param thread the thread to set
	 */
	private void setThread(DataStoreThread thread) {
		this.thread = thread;
	}

	/**
	 * Set new attribute with 'key' as name, and corresponding value.
	 * 
	 * A new attribute will be created if it doesn't exist already.
	 * Since, name of data store is also one of the default attribute of data store;
	 * calling this function with 'key' as "name", will result in change of data store name.
	 * 
	 * @param key
	 * @param value
	 * @return true if no errors while setting attribute, otherwise false
	 */
	public boolean setAttribute(String key, String value) {
		boolean isSet = false;
		isSet = setAttributeSpec(key, value);
		if(key.equalsIgnoreCase("name") && isSet) {
			this.name = value;
		}
		return isSet;
	}
	
	protected abstract boolean setAttributeSpec(String key, String value);

	
	/**
	 * Removes attribute with name in parameter.
	 * 
	 * If key is given as "name", the method will return false without removing the attribute.
	 * 
	 * @param key
	 * @return true if no errors while removing attribute, otherwise false
	 */
	public boolean removeAttribute(String key) {
		if(key.equalsIgnoreCase("name")) {
			return false;
		}
		return removeAttributeSpec(key);
	}
	
	protected abstract boolean removeAttributeSpec(String key);
	
	public Map<String, String> getAttributes() {
		return attributeMap;
	}
}

