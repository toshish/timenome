package com.timenome.api.core;

import java.util.Map;

import com.timenome.api.exceptions.ConnectionException;

public interface Connector {
		public Connection connect(Map<String, String> connectionParams) throws ConnectionException;

}
