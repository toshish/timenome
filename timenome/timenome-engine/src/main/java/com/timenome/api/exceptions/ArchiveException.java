package com.timenome.api.exceptions;

public class ArchiveException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3513783176579597755L;

	public ArchiveException() {
		// TODO Auto-generated constructor stub
	}

	public ArchiveException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ArchiveException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ArchiveException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ArchiveException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
