package com.timenome.api.core;

import com.timenome.api.TimeUnit;


public class TimeRangeDef {
	private TimeUnit timeUnit;
	private long cnt;

	/**
	 * TimeRange represents the duration/range of time. It is calculated by
	 * multiplying {@code timeUnit} and {@code cnt} <br/>
	 * <b>e.g.</b> timeUnit = Day, cnt = 3<br/>
	 * TimeRange = Day x 3 = 3 days
	 * 
	 * @param timeUnit
	 * @param cnt
	 */
	public TimeRangeDef(TimeUnit timeUnit, long cnt) {
		super();
		this.timeUnit = timeUnit;
		this.cnt = cnt;
	}

	public long getTimeInMillseconds() {
		return cnt * timeUnit.getTimeInMilliseconds();
	}

	public TimeUnit getTimeUnit() {
		return timeUnit;
	}

	public long getCnt() {
		return cnt;
	}

	@Override
	public String toString() {
		return "TimeRangeDef [timeUnit=" + timeUnit + ", cnt=" + cnt + "]";
	}
}
