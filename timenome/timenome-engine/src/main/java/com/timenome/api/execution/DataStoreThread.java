package com.timenome.api.execution;

import java.util.Iterator;
import java.util.concurrent.TransferQueue;
import java.util.logging.Logger;

import com.timenome.api.Entry;
import com.timenome.api.core.Archive;
import com.timenome.api.core.DataStore;
import com.timenome.api.logger.TimeNomeLogger;

public class DataStoreThread extends Thread {

	private DataStore dataStore;
	private TransferQueue<Entry<? extends Number>> dataEntriesQueue;
	private boolean running = false;;
	
	private static Logger logger = TimeNomeLogger.getTimeNomeLogger(DataStoreThread.class);
	
	public DataStoreThread(DataStore dataStore) {
		super(dataStore.getDataStoreName());
		this.dataStore = dataStore;
		this.dataEntriesQueue = dataStore.getDataEntriesQueue();
		this.start();
		running = true;
		logger.info("Thread for dataStore " + dataStore.getDataStoreName() + " started.");
	}

	@Override
	public void run() {
		while(running) {
			Entry<? extends Number> entry = null;
			try {
				entry = dataEntriesQueue.take();
			} catch (InterruptedException e) {
				logger.throwing("DataStoreThread", "run", e);
			}
			if(entry != null) {
				Iterator<java.util.Map.Entry<String, Archive>> iter = dataStore.getArchives().iterator();
				while(iter.hasNext()) {
					java.util.Map.Entry<String, Archive> archiveEntry = iter.next();
					Archive archive = archiveEntry.getValue();
					if(!archive.put(entry)) {
						logger.warning("Couldn't add entry to archive " + archive);
					}
				}
			}
		}
		logger.info("Stopping thread for data store: " + dataStore.getDataStoreName());
	}

	public void markForStop() {
		this.running  = false;
	}


}
