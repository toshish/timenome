package com.timenome.api.exceptions;

public class UserbaseInitializationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2763763196175265832L;

	public UserbaseInitializationException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserbaseInitializationException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public UserbaseInitializationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public UserbaseInitializationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public UserbaseInitializationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
