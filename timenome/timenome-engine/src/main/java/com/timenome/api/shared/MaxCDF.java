package com.timenome.api.shared;

import java.util.Collections;
import java.util.List;

import com.timenome.api.BuiltInCDF;
import com.timenome.api.Entry;


class MaxCDF extends AbstractConsolidationFunction {

	protected MaxCDF() {
		super(BuiltInCDF.MAX);
	}

	@Override
	public Entry<? extends Number> consolidate(List<Entry<? extends Number>> dataToBeConsolidated) {
		Entry<? extends Number> maxEntry = null;
		if(dataToBeConsolidated.size() > 0) {
			List<Entry<? extends Number>> listData = Collections.synchronizedList(dataToBeConsolidated);
			double maxValue = Double.MIN_VALUE;
			synchronized (listData) {
				for(Entry<? extends Number> dataEntry : listData) {
					if(maxValue < (Double) dataEntry.getValue()) {
						maxValue = (Double) dataEntry.getValue();
					}
				}
			}
			Entry<? extends Number> lastEntry = dataToBeConsolidated.get(dataToBeConsolidated.size() - 1);
			maxEntry = new Entry<Number>(lastEntry.getTimeStamp(), maxValue);
		}
		else {
			maxEntry = new Entry<Number>(null);
		}
		return maxEntry;
	}

}
