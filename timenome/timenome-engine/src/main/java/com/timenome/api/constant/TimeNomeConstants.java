package com.timenome.api.constant;

public final class TimeNomeConstants {
	public static String TIMENOME_PROPERTIES_FILE = System.getenv("TIMENOME_PROPERTIES");
	
	public static String NEW_LINE = "\n";
	
	
	
	public static String USER_TABLE = "USER";
	
	public static String TIMENOME_USER_DATABASE = "timenome_user";
			
}
