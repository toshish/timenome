package com.timenome.api.exceptions;

public class ImproperSequencedDataException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6591175973879268263L;

	
	public ImproperSequencedDataException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ImproperSequencedDataException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ImproperSequencedDataException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
