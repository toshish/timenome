package com.timenome.api.exceptions;

public class TimeOutOfRangeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8234236549339504413L;

	public TimeOutOfRangeException() {
		// TODO Auto-generated constructor stub
	}

	public TimeOutOfRangeException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public TimeOutOfRangeException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public TimeOutOfRangeException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public TimeOutOfRangeException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
