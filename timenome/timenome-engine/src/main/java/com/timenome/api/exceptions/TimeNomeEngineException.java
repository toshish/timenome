package com.timenome.api.exceptions;

public class TimeNomeEngineException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1377313961261358788L;

	public TimeNomeEngineException() {
		// TODO Auto-generated constructor stub
	}

	public TimeNomeEngineException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public TimeNomeEngineException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public TimeNomeEngineException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public TimeNomeEngineException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
