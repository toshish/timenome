package com.timenome.api;

public enum BuiltInCDF {
	MAX, MIN, AVERAGE, SUM, LAST, FIRST, RANGE, NO_OPERATION;
}
