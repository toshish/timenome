package com.timenome.api.exceptions;

public class UserDoesNotExists extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8200318414634501492L;

	public UserDoesNotExists() {
		// TODO Auto-generated constructor stub
	}

	public UserDoesNotExists(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public UserDoesNotExists(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public UserDoesNotExists(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public UserDoesNotExists(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
