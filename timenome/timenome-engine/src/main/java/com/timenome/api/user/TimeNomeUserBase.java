/**
 * 
 */
package com.timenome.api.user;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;

import com.timenome.api.core.User;
import com.timenome.api.dbhelper.MySQLDBHelper;
import com.timenome.api.exceptions.TimeNomeEngineException;
import com.timenome.api.exceptions.UserAlreadyExists;
import com.timenome.api.exceptions.UserDoesNotExists;
import com.timenome.api.exceptions.UserbaseInitializationException;
import com.timenome.api.execution.TimeNomeEngine;
import com.timenome.api.logger.TimeNomeLogger;

/**
 * 
 * @author toshish
 *
 */
public class TimeNomeUserBase {
	private static TimeNomeLogger logger = TimeNomeLogger.getTimeNomeLogger(TimeNomeUserBase.class);

	private static HashMap<TimeNomeEngine, TimeNomeUserBase> timeNomeUserBaseMap = new HashMap<TimeNomeEngine, TimeNomeUserBase>();
	private Connection connection;
	private TimeNomeEngine timeNomeEngine;


	/**
	 * @param timeNomeEngine 
	 * @throws TimeNomeEngineException 
	 * 
	 */
	private TimeNomeUserBase(TimeNomeEngine timeNomeEngine) throws UserbaseInitializationException {
		this.connection = MySQLDBHelper.getConnection();
		this.timeNomeEngine = timeNomeEngine;
	}
	
	public static TimeNomeUserBase getTimeNomeUserBase(TimeNomeEngine timeNomeEngine) throws UserbaseInitializationException {
		if(timeNomeUserBaseMap.get(timeNomeEngine) == null) {
			timeNomeUserBaseMap.put(timeNomeEngine, new TimeNomeUserBase(timeNomeEngine));
		}
		return timeNomeUserBaseMap.get(timeNomeEngine);
	}

	/**
	 * @return the connection
	 */
	public Connection getConnection() {
		return connection;
	}

	public User addUser(String username, String password, String name) throws UserAlreadyExists {
		logger.finest("entering addUser");
		User user = null;
		PreparedStatement statement = null;
		SecureRandom random = new SecureRandom();

		try {
			statement =  connection.prepareStatement("INSERT INTO USER (username, password_hash, name, apikey, secret) values (?, ?, ?, ?, ?)");
			statement.setString(1, username);
			statement.setString(2, password);
			statement.setString(3, name);
			statement.setString(4, new BigInteger(130, random).toString(32));
			statement.setString(5, new BigInteger(130, random).toString(32));

		} catch (SQLException e) {
			logger.log(Level.SEVERE, "Incorrect SQL.", e);
		}

		try {
			statement.executeUpdate();
			user = timeNomeEngine.getConnection().createUser(username);
		} catch (SQLException e) {
			logger.severe("Adding an entry to userbase failed. - " + e.getLocalizedMessage());
			throw new UserAlreadyExists("User " + username + " already exists in userbase.", e);
		}
		finally {
			try {
				statement.close();
			} catch (SQLException e) {
				logger.warning("Rolling back the created used in engine failed.\n" + e.getLocalizedMessage());
			}
		}
		return user;
	}

	public boolean removeUser(String username, String authenticationToken) throws UserDoesNotExists {
		logger.finest("entering removeUser");
		boolean isUserDeleted = false;
		PreparedStatement statement = null;
		try {
			statement =  connection.prepareStatement("DELETE FROM USER WHERE username = ? and password_hash = ?");
			statement.setString(1, username);
			statement.setString(2, authenticationToken);
		} catch (SQLException e) {
			logger.log(Level.SEVERE, "Incorrect SQL.", e);
		}

		try {
			if(statement.executeUpdate() < 1) {
				logger.warning("Either incorrect username/password or user doesn't exist. "
						+ "No entry was found, user will not be deleted.");
				throw new UserDoesNotExists("Either incorrect username/password or user doesn't exist.");
			}
			else {
				isUserDeleted = timeNomeEngine.getConnection().deleteUser(username);
			}
		} catch (SQLException e) {
			logger.warning("Removing entry from userbase failed. - " + e.getLocalizedMessage());
		}
		finally {
			try {
				statement.close();
			} catch (SQLException e) {
				logger.warning("Rolling back the created user in engine failed. - " + e.getLocalizedMessage());
			}
		}
		return isUserDeleted;
	}

	public User getUser(String username, String password) {
		logger.finest("entering getUser");
		PreparedStatement statement = null;
		try {
			statement =  connection.prepareStatement("SELECT userid from USER WHERE username = ? and password_hash = ?");
			statement.setString(1, username);
			statement.setString(2, password);
		} catch (SQLException e) {
			logger.log(Level.SEVERE, "Incorrect SQL.", e);
		}

		User user = null;
		ResultSet rs = null;
		try {
			rs = statement.executeQuery();
			if(rs.next()) {
				user = timeNomeEngine.getConnection().getUser(username);
			}
		} catch (SQLException e) {
			logger.severe("Error while executing query. - " + e.getLocalizedMessage());
		} catch (UserDoesNotExists e) {
			logger.warning("Requested user doesn't exists. - " + e.getLocalizedMessage());
		}
		finally {
			try {
				rs.close();
				statement.close();
			} catch (SQLException e) {
				logger.warning("Error while closing sql resouces. - " + e.getLocalizedMessage());
			}
		}
		logger.finest("exiting getUser");
		return user;
	}

	public User getUserByAPICredentials(String key, String secret) {
		logger.finest("entering getUserByAPICredentials");
		PreparedStatement statement = null;
		try {
			statement =  connection.prepareStatement("SELECT userid, username from USER WHERE apikey = ? and secret = ?");
			statement.setString(1, key);
			statement.setString(2, secret);
		} catch (SQLException e) {
			logger.severe("Incorrect SQL. - " + e.getLocalizedMessage());
		}

		User user = null;
		ResultSet rs = null;
		try {
			rs = statement.executeQuery();
			if(rs.next()) {
				String username = rs.getString("username");
				user = timeNomeEngine.getConnection().getUser(username);
			}
		} catch (SQLException e) {
			logger.severe("Error while executing query. - " + e.getLocalizedMessage());
		} catch (UserDoesNotExists e) {
			logger.warning("Requested user doesn't exists. - " + e.getLocalizedMessage());
		}
		finally {
			try {
				rs.close();
				statement.close();
			} catch (SQLException e) {
				logger.warning("Error while closing sql resouces. - " + e.getLocalizedMessage());
			}
		}
		logger.finest("exiting getUserByAPICredentials");
		return user;

	}

	public boolean setPreferences(String username, String password, HashMap<String, String> attribMap) {
		logger.finest("entering setPreferences");
		boolean isPreferencesSet = false;
		int userid = 0;
		PreparedStatement statement = null;
		try {
			statement =  connection.prepareStatement("SELECT userid from USER WHERE username = ? and password_hash = ?");
			statement.setString(1, username);
			statement.setString(2, password);
		} catch (SQLException e) {
			logger.log(Level.SEVERE, "Incorrect SQL.", e);
		}

		ResultSet rs = null;
		try {
			rs = statement.executeQuery();
			if(rs.next()) {
				userid = rs.getInt("userid");
			}
		} catch (SQLException e) {
			logger.severe("Error while executing query. - " + e.getLocalizedMessage());
		}
		finally {
			try {
				rs.close();
				statement.close();
			} catch (SQLException e) {
				logger.warning("Error while closing sql resouces. - " + e.getLocalizedMessage());
			}
		}
		int prefCount = 0;
		for(String key : attribMap.keySet()) {
			statement = null;
			try {
				statement =  connection.prepareStatement("INSERT INTO PREFERENCES values (?, ?, ?)");
				statement.setInt(1, userid);
				statement.setString(2, key);
				statement.setString(3, attribMap.get(key));
			} catch (SQLException e) {
				logger.log(Level.SEVERE, "Incorrect SQL.", e);
				isPreferencesSet = false;
			}
			try {
				int count = statement.executeUpdate();
				if(count < 1) {
					logger.warning("Either user doesn't exists or invalid credentials.");
					isPreferencesSet = false;
				}
				else {
					prefCount++;
				}
			} catch (SQLException e) {
				logger.severe("Error while executing query. - " + e.getLocalizedMessage());
				try {
					statement =  connection.prepareStatement("UPDATE PREFERENCES P SET P.value = ? where P.key = ? and P.userid = ?");
					statement.setString(1, attribMap.get(key));
					statement.setString(2, key);
					statement.setInt(3, userid);
				} catch (SQLException e1) {
					logger.log(Level.SEVERE, "Incorrect SQL.", e);
					isPreferencesSet = false;
				}
				try {
					int count = statement.executeUpdate();
					if(count < 1) {
						logger.warning("Either user doesn't exists or invalid credentials.");
						isPreferencesSet = false;
					}
					else {
						prefCount++;
					}
				} catch(SQLException e1) {
					logger.severe("Error while executing query. - " + e1.getLocalizedMessage());
				}
				isPreferencesSet = false;
			} 
		}
		if(prefCount == attribMap.size()) {
			isPreferencesSet = true;
		}
		logger.finest("exiting setPreferences");
		return isPreferencesSet;
	}

	public HashMap<String, String> getPreferences(String username, String password) {
		HashMap<String, String> prefMap = new HashMap<String, String>();
		PreparedStatement statement = null;
		try {
			statement =  connection.prepareStatement("SELECT P.key, P.value from PREFERENCES P WHERE userid = (SELECT userid FROM USER WHERE username = ? AND password_hash = ?)");
			statement.setString(1, username);
			statement.setString(2, password);
		} catch (SQLException e) {
			logger.log(Level.SEVERE, "Incorrect SQL.", e);
		}
		
		ResultSet rs = null;
		try {
			rs = statement.executeQuery();
			while(rs.next()) {
				prefMap.put(rs.getString("key"), rs.getString("value"));
			}
		} catch (SQLException e) {
			logger.log(Level.SEVERE, "Error while executing query.", e);
		}
		finally {
			try {
				rs.close();
				statement.close();
			} catch (SQLException e) {
				logger.log(Level.SEVERE, "Error while closing sql resouces.", e);
			}
		}
		
		return prefMap;
	}

	public String[] getAPICredentials(String username, String password) {
		logger.finest("entering getAPICredentials");
		String[] keyAndSecret = new String[2];
		PreparedStatement statement = null;
		try {
			statement =  connection.prepareStatement("SELECT apikey, secret FROM USER WHERE username = ? and password_hash = ?");
			statement.setString(1, username);
			statement.setString(2, password);
		} catch (SQLException e) {
			logger.log(Level.SEVERE, "Incorrect SQL.", e);
		}
		try {
			ResultSet rs = statement.executeQuery();
			if(rs.next()) {
				keyAndSecret[0] = rs.getString("apikey");
				keyAndSecret[1] = rs.getString("secret");
			}
		} catch (SQLException e) {
			logger.log(Level.SEVERE, "Error while executing query.", e);
		} 
		logger.finest("exiting getAPICredentials");
		return keyAndSecret;
	}

}
