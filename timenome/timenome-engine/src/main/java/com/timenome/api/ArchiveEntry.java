package com.timenome.api;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.timenome.api.core.Archive;


/**
 * This class represents an archive entry which should be passed in {@code put()} method of {@link Archive} class.
 * @author toshish
 *
 */

public final class ArchiveEntry {

	private List<Entry<? extends Number>> entries;
	
	public ArchiveEntry(Collection<Entry<? extends Number>> entriesList) {
		entries = new ArrayList<Entry<? extends Number>>(entriesList);
//		// Validate the sequence of entries in this list.
//		Iterator<Entry<? extends Number>> iter = entries.iterator();
//		long prevTimeStamp = 0;
//		while(iter.hasNext()) {
//			if(prevTimeStamp > iter.next().getTimeStamp()) {
//				throw new ImproperSequencedDataException("DataStore entries are not properly sequenced with respect to time. Entry for time stamp " +
//						prevTimeStamp + " should be after " + iter.next().getTimeStamp());
//			}
//		}
//		// Loop completed. All good.
//		this.entries = entries;
	}
	
	public ArchiveEntry() {
		entries = new ArrayList<Entry<? extends Number>>();
	}
	
	public List<Entry<? extends Number>> getEntries() {
		return entries;
	}
	
	public boolean putEntry(Entry<? extends Number> entry) {
		return entries.add(entry);
	}
	
	public boolean putEntries(List<Entry<? extends Number>> entries) {
		return entries.addAll(entries);
	}
	
	public int size() {
		return entries.size();
	}
	
	public void clear() {
		entries.clear();
	}
	
	public boolean isEmpty() {
		return entries.isEmpty();
	}

}
