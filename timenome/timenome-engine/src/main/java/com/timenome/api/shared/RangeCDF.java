/**
 * 
 */
package com.timenome.api.shared;

import java.util.List;

import com.timenome.api.BuiltInCDF;
import com.timenome.api.Entry;

/**
 * @author toshish
 *
 */
public class RangeCDF extends AbstractConsolidationFunction {


	protected RangeCDF() {
		super(BuiltInCDF.RANGE);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see com.timenome.api.shared.ConsolidationFunction#consolidate(java.util.List)
	 */
	@Override
	public Entry<? extends Number> consolidate(List<Entry<? extends Number>> dataToBeConsolidated) {
		Entry<? extends Number> rangeEntry = null;
		if(dataToBeConsolidated.size() > 0) {
			Entry<? extends Number> maxEntry = new MaxCDF().consolidate(dataToBeConsolidated);
			Entry<? extends Number> minEntry = new MinCDF().consolidate(dataToBeConsolidated);

			Double rangeValue = (Double)maxEntry.getValue() - (Double)minEntry.getValue();
			rangeEntry = new Entry<Number>(maxEntry.getTimeStamp(), rangeValue);
		}
		else {
			rangeEntry = new Entry<Number>(null);
		}
		return rangeEntry;
	}

}
