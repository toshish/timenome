package com.timenome.api.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import com.timenome.api.ArchiveEntry;
import com.timenome.api.BuiltInCDF;
import com.timenome.api.Entry;
import com.timenome.api.exceptions.ArchiveException;
import com.timenome.api.exceptions.TimeOutOfRangeException;
import com.timenome.api.handler.StreamDataHandler;
import com.timenome.api.shared.ConsolidationFunction;
import com.timenome.api.shared.ConsolidationFunctionFactory;

public abstract class Archive {
	private String archiveName;
	private TimeRangeDef storageRange;
	private TimeRangeDef storagePrecision;
	private ConsolidationFunction consolidationFunction;
	protected int counter = 0;

//	private TransferQueue<Entry<? extends Number>> dataEntriesTransferQueue;
	private DataStore myDataStore;

	private final ArchiveEntry archiveEntry;
	private Timer timer;
	private Map<String, StreamDataHandler> streamDataHandlers = new ConcurrentHashMap<>();

	private Archive(String archiveName, TimeRangeDef storageRange, TimeRangeDef storagePrecision) {
		this.archiveName = archiveName;
		this.storageRange = storageRange;
		this.storagePrecision = storagePrecision;
		this.archiveEntry = new ArchiveEntry();
		this.timer = new Timer(this.archiveName); 
		timer.scheduleAtFixedRate(new ArchiveTimerTask(this), storagePrecision.getTimeInMillseconds(), storagePrecision.getTimeInMillseconds());
//		System.out.println(this + " -- " + hashCode());
	}

	/**
	 * @param archiveName
	 * @param storageRange
	 *            represents for how long the data will be stored
	 * @param storagePrecision
	 *            represents at what interval the data will be stored
	 * @param consolidationFunction
	 *            this function will be called to consolidate data, once data is
	 *            collection is done for timeRange equal to {@code storagePrecision}
	 *            <br/>
	 *            by default {@code BuiltInCDF.AVERAGE} is used as consolidation function
	 */
	protected Archive(String archiveName, TimeRangeDef storageRange, TimeRangeDef storagePrecision,
			ConsolidationFunction consolidationFunction) {
		this(archiveName, storageRange, storagePrecision);

		if(consolidationFunction==null){
			this.consolidationFunction = ConsolidationFunctionFactory.getInstance().getConsolidationFunctionInstance(BuiltInCDF.AVERAGE);
		}else{
			this.consolidationFunction = consolidationFunction;
		}
	}



	/**
	 * @param archiveName
	 * @param storageRange
	 *            represents for how long the data will be stored
	 * @param storagePrecision
	 *            represents at what interval the data will be stored
	 * @param builtInCDF
	 * 			  this built in function will be called to consolidate data, once data is
	 *            collection is done for timeRange equal to {@code storagePrecision}
	 *            <br/>
	 *            by default {@code BuiltInCDF.AVERAGE} is used as consolidation function        
	 */
	public Archive(String archiveName, TimeRangeDef storageRange, TimeRangeDef storagePrecision, BuiltInCDF builtInCDF){
		this(archiveName, storageRange, storagePrecision);

		this.consolidationFunction = ConsolidationFunctionFactory.getInstance().getConsolidationFunctionInstance(builtInCDF);
	}

	final public ConsolidationFunction getConsolidationFunction() {
		return consolidationFunction;
	}

	final public TimeRangeDef getStoragePrecision() {
		return storagePrecision;
	}

	final public TimeRangeDef getStorageRange() {
		return storageRange;
	}

	final public String getArchiveName() {
		return archiveName;
	}

	/**
	 * This method should return the specific archive implementation object of this archive.
	 *  
	 * @return Object containing specific archive implementation.
	 */
	public abstract Object getSpecificArchiveObject();

	/**
	 * This method should be implemented in its one of the overridden versions to put passed archiveEntry
	 * in the archive to the persistent archive source.
	 * @param archiveEntry
	 */
	final public boolean put(Entry<? extends Number> entry) {
//		System.out.println("archive -" + archiveEntry);
		return archiveEntry.putEntry(entry);
	}

	final public boolean put(List<Entry<? extends Number>> entries) {
		return archiveEntry.putEntries(entries);
	}

	public ArchiveEntry getArchiveEntry() {
		return archiveEntry;
	}
	
	/**
	 * 
	 * @param timeStamp
	 * @param number
	 * @return true if entry is successfully stored. false otherwise.
	 */
	protected abstract boolean putSpec(Entry<? extends Number> entry);

	final void connectToDataStore(DataStore dataStore) throws ArchiveException {
//		if(dataEntriesTransferQueue == null) {
//			throw new ArchiveException("dataEntriesTransferQueue is null.");
//		}
		if(dataStore == null) {
			throw new ArchiveException("dataStore is null.");
		}
		this.setMyDataStore(dataStore);
	}

	
	public void addStreamDataHandler(String streamId, StreamDataHandler streamDataHandler) {
		this.streamDataHandlers.put(streamId, streamDataHandler);
	}
	
	public Collection<StreamDataHandler> getStreamDataHandlers() {
		return streamDataHandlers.values();
	}
	
	public void removeStreamDataHandler(String streamId) {
		this.streamDataHandlers.remove(streamId);
	}

	private class ArchiveTimerTask extends TimerTask implements Runnable {
		
		private Archive archive;

		public ArchiveTimerTask(Archive archive) {
			this.archive = archive;
		}
		
		@Override
		public void run() {
			Entry<? extends Number> consolidatedEntry = null;
//			System.out.println(archive + " - " + archive.hashCode());
			ArchiveEntry archiveEntry = archive.getArchiveEntry();
//			System.out.println("timer " + archiveEntry);
			if(!archiveEntry.isEmpty()) {
				consolidatedEntry = consolidationFunction.consolidate(archiveEntry.getEntries());
			}
			else {
				consolidatedEntry = new Entry<Double>(null);
			}
			putSpec(consolidatedEntry);
			callStreamDataHandlers(consolidatedEntry);
			archiveEntry.clear();
		}
		
		private void callStreamDataHandlers(Entry<? extends Number> entry) {
			for(String streamId : streamDataHandlers.keySet()) {
				ArrayList<Entry<? extends Number>> list = new ArrayList<Entry<? extends Number>>();
				list.add(entry);
				streamDataHandlers.get(streamId).onDataAggregated(list);
			}
		}
	}

	protected abstract TimeRangeDef getArchiveStartTime();
	
	public List<Entry<? extends Number>> getData() {
		TimeRangeDef startTime = getArchiveStartTime();
		TimeRangeDef endTime = new TimeRangeDef(com.timenome.api.TimeUnit.MILLI_SECOND, System.currentTimeMillis());
		try {
			return getData(startTime, endTime);
		} catch (TimeOutOfRangeException e) {
			e.printStackTrace();
			System.err.println("This situation should never occur. Range is not defined in this function.");
			// This situation should never occur.
		}
		List<Entry<? extends Number>> data = new ArrayList<>();
		return data;
	}
	
	public List<Entry<? extends Number>> getDataTill(TimeRangeDef endTime) throws TimeOutOfRangeException {
		TimeRangeDef startTime = getArchiveStartTime();
		return getData(startTime, endTime);
	}
	
	public List<Entry<? extends Number>> getDataFrom(TimeRangeDef startTime) throws TimeOutOfRangeException {
		TimeRangeDef endTime = new TimeRangeDef(com.timenome.api.TimeUnit.MILLI_SECOND, System.currentTimeMillis());
		return getData(startTime, endTime);
	}
	
	public List<Entry<? extends Number>> getData(TimeRangeDef startTime, TimeRangeDef endTime) throws TimeOutOfRangeException {
		List<Entry<? extends Number>> data = getDataSpec(startTime, endTime);
		if(data == null) {
			throw new TimeOutOfRangeException();
		}
		return data;
	}
	
	protected abstract List<Entry<? extends Number>> getDataSpec(TimeRangeDef startTime, TimeRangeDef endTime) throws TimeOutOfRangeException;

	/**
	 * @return the myDataStore
	 */
	public DataStore getMyDataStore() {
		return myDataStore;
	}

	/**
	 * @param myDataStore the myDataStore to set
	 */
	private void setMyDataStore(DataStore myDataStore) {
		this.myDataStore = myDataStore;
	}
	
	public void stop() {
		this.timer.cancel();
		this.timer.purge();
	}

	@Override
	public String toString() {
		return "{" + archiveName + "[" + storagePrecision + ", " + consolidationFunction + "]" + "}";
	}
	
}
