package com.timenome.api.shared;

import java.util.List;

import com.timenome.api.BuiltInCDF;
import com.timenome.api.Entry;

public interface ConsolidationFunction {
	public Entry<? extends Number> consolidate(List<Entry<? extends Number>> dataToBeConsolidated);
	
	public BuiltInCDF getCDFValue();
}
