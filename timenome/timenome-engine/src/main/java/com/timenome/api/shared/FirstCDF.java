package com.timenome.api.shared;

import java.util.List;

import com.timenome.api.BuiltInCDF;
import com.timenome.api.Entry;


class FirstCDF extends AbstractConsolidationFunction {

	protected FirstCDF() {
		super(BuiltInCDF.FIRST);
	}
	
	@Override
	public Entry<? extends Number> consolidate(List<Entry<? extends Number>> dataToBeConsolidated) {
		if(dataToBeConsolidated.size() > 0) {
			return dataToBeConsolidated.get(0);
		}
		return null;
	}
	
}
