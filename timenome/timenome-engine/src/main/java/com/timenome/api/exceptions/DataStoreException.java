package com.timenome.api.exceptions;

public class DataStoreException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8744035682826985327L;

	public DataStoreException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DataStoreException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public DataStoreException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public DataStoreException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public DataStoreException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
}
