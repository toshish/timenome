package com.timenome.api.shared;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.timenome.api.BuiltInCDF;
import com.timenome.api.Entry;


class SummationCDF extends AbstractConsolidationFunction {

	protected SummationCDF() {
		super(BuiltInCDF.SUM);
	}

	@Override
	public Entry<? extends Number> consolidate(List<Entry<? extends Number>> dataToBeConsolidated) {
		List<Entry<? extends Number>> listData = Collections.synchronizedList(dataToBeConsolidated);
		double sum = 0;
		synchronized (listData) {
			Iterator<Entry<? extends Number>> iter = listData.iterator();
			while(iter.hasNext()) {
				Entry<? extends Number> entry = iter.next();
				Number value = entry.getValue();
				sum = sum + value.doubleValue();
			}
		}
		
		Entry<? extends Number> lastEntry = dataToBeConsolidated.get(dataToBeConsolidated.size() - 1);
		return new Entry<Number>(lastEntry.getTimeStamp(), sum);
	}
}
