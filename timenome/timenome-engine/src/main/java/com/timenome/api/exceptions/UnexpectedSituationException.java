package com.timenome.api.exceptions;

public class UnexpectedSituationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 191272260666338699L;

	public UnexpectedSituationException() {
		// TODO Auto-generated constructor stub
	}

	public UnexpectedSituationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public UnexpectedSituationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public UnexpectedSituationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public UnexpectedSituationException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
