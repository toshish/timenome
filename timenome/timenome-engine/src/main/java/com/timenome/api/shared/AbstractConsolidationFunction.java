/**
 * 
 */
package com.timenome.api.shared;

import com.timenome.api.BuiltInCDF;

/**
 * @author toshish
 *
 */
public abstract class AbstractConsolidationFunction implements
		ConsolidationFunction {

	BuiltInCDF builtInCDF;
	
	protected AbstractConsolidationFunction(BuiltInCDF builtInCDF) {
		this.builtInCDF = builtInCDF;
	}
	
	@Override
	public BuiltInCDF getCDFValue() {
		return builtInCDF;
	}

	@Override
	public String toString() {
		return "[builtInCDF=" + builtInCDF + "]";
	}
}
