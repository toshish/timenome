package com.timenome.api;

public final class EngineEntry {
	
	private String username;
	private String dataStoreName;
	private Entry<? extends Number> entry;
	
	public EngineEntry(String username, String dataStoreName, Entry<? extends Number> entry) {
		this.setUsername(username);
		this.setDataStoreName(dataStoreName);
		this.setEntry(entry);
	}

	public String getDataStoreName() {
		return dataStoreName;
	}

	private void setDataStoreName(String dataStoreName) {
		this.dataStoreName = dataStoreName;
	}

	public Entry<? extends Number> getEntry() {
		return entry;
	}

	private void setEntry(Entry<? extends Number> entry) {
		this.entry = entry;
	}

	public String getUsername() {
		return username;
	}

	private void setUsername(String username) {
		this.username = username;
	}

}
