package com.timenome.api.handler;

import java.util.List;

import com.timenome.api.Entry;

public interface StreamDataHandler {
	void onDataAggregated(List<Entry<? extends Number>> dataEntries);
}
