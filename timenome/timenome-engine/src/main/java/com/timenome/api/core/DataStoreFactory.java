package com.timenome.api.core;

import java.util.HashMap;
import java.util.List;

public final class DataStoreFactory {
	private static HashMap<Class<?>, DataStoreFactory> dataStoreFactoryMap = new HashMap<Class<?>, DataStoreFactory>();
	private Class<? super DataStore> dataStoreClass;
	private DataStoreFactory(Class<? super DataStore> dataStoreClass) {
		this.setDataStoreClass(dataStoreClass);
	}
	
	public static DataStoreFactory getFactoryFor(Class<? super DataStore> subClassOfDataStore) {
		if(!dataStoreFactoryMap.containsKey(subClassOfDataStore)) {
			dataStoreFactoryMap.put(subClassOfDataStore, new DataStoreFactory(subClassOfDataStore));
		}
		return dataStoreFactoryMap.get(subClassOfDataStore);
	}
	
	public DataStore createNewDataStore(String dataStoreName) {
		return null;
	}
	
	public static List<DataStore> getAllDataStores() {
		return null;
	}

	/**
	 * @return the dataStoreClass
	 */
	public Class<? super DataStore> getDataStoreClass() {
		return dataStoreClass;
	}

	/**
	 * @param dataStoreClass the dataStoreClass to set
	 */
	private void setDataStoreClass(Class<? super DataStore> dataStoreClass) {
		this.dataStoreClass = dataStoreClass;
	}
	
}
