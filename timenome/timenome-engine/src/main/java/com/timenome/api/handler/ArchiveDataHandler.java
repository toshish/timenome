/**
 * 
 */
package com.timenome.api.handler;

import java.util.List;

import com.timenome.api.Entry;
import com.timenome.api.core.Archive;

/**
 * @author toshish
 *
 */
public interface ArchiveDataHandler {
	
	/**
	 * adds the data specified 
	 * @param archive
	 * @param entries
	 * @return
	 */
	
	public boolean addDataToArchive(Archive archive, List<Entry<Number>> entries);
}
