package com.timenome.api;

/**
 * Instance of this class represents a single entity to be put in DataStore.
 * This class allows value to be instance of any sub-class of {@link Number}.
 * It does not have any setter methods. Time stamp and values passed while 
 * calling constructor cannot be changed further.
 * 
 * @author toshish
 *
 * @param <E extends {@link Number}>
 */
public class Entry <E extends Number> {
	private long timeStamp;
	private E value;
	
	/**
	 * Entry created using this constructor will have timeStamp as current system time as returned by {@code System.currentTimeMillis()} function.
	 * @param value
	 */
	public Entry(E value) {
		this.timeStamp = System.currentTimeMillis();
		this.value = value;
	}
	
	/**
	 * Entry with this constructor will have timeStamp as passed in the argument.
	 * @param timeStamp
	 * @param value
	 */
	public Entry(long timeStamp, E value) {
		this.timeStamp = timeStamp;
		this.value = value;
	}
	/**
	 * 
	 * @return time stamp of this entry.
	 */
	public long getTimeStamp() {
		return timeStamp;
	}
	
	/**
	 * 
	 * @return value in this entry.
	 */
	public E getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		return "[timestamp : " + timeStamp + ", value :" + value + "]";
	}
}
