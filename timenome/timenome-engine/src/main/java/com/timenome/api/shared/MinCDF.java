package com.timenome.api.shared;

import java.util.Collections;
import java.util.List;

import com.timenome.api.BuiltInCDF;
import com.timenome.api.Entry;


public class MinCDF extends AbstractConsolidationFunction {

	protected MinCDF() {
		super(BuiltInCDF.MIN);
	}

	@Override
	public Entry<? extends Number> consolidate(List<Entry<? extends Number>> dataToBeConsolidated) {
		Entry<? extends Number> minEntry = null;
		if(dataToBeConsolidated.size() > 0) {
			List<Entry<? extends Number>> listData = Collections.synchronizedList(dataToBeConsolidated);
			double minValue = Double.MAX_VALUE;
			synchronized (listData) {
				for(Entry<? extends Number> dataEntry : listData) {
					if(minValue > (Double) dataEntry.getValue()) {
						minValue = (Double) dataEntry.getValue();
					}
				}
			}
			Entry<? extends Number> lastEntry = dataToBeConsolidated.get(dataToBeConsolidated.size() - 1);
			minEntry = new Entry<Number>(lastEntry.getTimeStamp(), minValue);
		}
		else {
			minEntry = new Entry<Number>(null);
		}
		return minEntry;
	}

}
