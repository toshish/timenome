package com.timenome.api.shared;

import com.timenome.api.BuiltInCDF;

public class ConsolidationFunctionFactory {
	private static ConsolidationFunctionFactory consolidationFunctionFactory = new ConsolidationFunctionFactory();

	public static ConsolidationFunctionFactory getInstance() {
		return consolidationFunctionFactory;
	}

	private ConsolidationFunctionFactory() {

	}

	public ConsolidationFunction getConsolidationFunctionInstance(BuiltInCDF builtInCDF) {
		switch (builtInCDF) {
		case AVERAGE:
			return new AverageCDF();
		case FIRST:
			return new FirstCDF();
		case LAST:
			return new LastCDF();
		case MAX:
			return new MaxCDF();
		case MIN:
			return new MinCDF();
		case SUM:
			return new SummationCDF();
		case RANGE:
			return new RangeCDF();
		default:
			return new AverageCDF();
		}
	}
}
