package com.timenome.api.execution;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.TransferQueue;

import com.timenome.api.EngineEntry;
import com.timenome.api.core.Connection;
import com.timenome.api.core.DataStore;
import com.timenome.api.core.User;
import com.timenome.api.exceptions.TimeNomeEngineException;
import com.timenome.api.exceptions.UserDoesNotExists;
import com.timenome.api.logger.TimeNomeLogger;

public class TimeNomeEngine implements Runnable {
	
	private TransferQueue<EngineEntry> engineEntryQueue;
	private static HashMap<Connection, TimeNomeEngine> timeNomeEngineMap = new HashMap<Connection, TimeNomeEngine>();
	private static HashMap<TimeNomeEngine, Thread> threadMap = new HashMap<TimeNomeEngine, Thread>();
	private boolean running = false;
	private Connection connection;
	private static  Map<String, User> userMap;
	private static TimeNomeLogger logger = TimeNomeLogger.getTimeNomeLogger(TimeNomeEngine.class);
//	private static HashMap<User, HashMap<DataStore, Thread>> userDSThreadMap = new HashMap<>();
	
	public static TimeNomeEngine getTimeNomeEngine(Connection connection) {
		logger.entering(TimeNomeEngine.class.getCanonicalName(), "getTimeNomeEngine");
		TimeNomeEngine timeNomeEngine = timeNomeEngineMap.get(connection);
		if(timeNomeEngine == null) {
			logger.fine("Creating singleton");
			timeNomeEngine = new TimeNomeEngine(connection);
			timeNomeEngineMap.put(connection, timeNomeEngine);
			Thread t = new Thread(timeNomeEngine);
			logger.fine("TimeNome Engine is about to start.");
			t.start();
			threadMap.put(timeNomeEngine, t);
		}
		logger.exiting(TimeNomeEngine.class.getCanonicalName(), "getTimeNomeEngine");
		return timeNomeEngine;
	}
	
	private TimeNomeEngine(Connection connection) {
		userMap  = connection.getAllUsers();
		this.setConnection(connection);
		this.engineEntryQueue = new LinkedTransferQueue<EngineEntry>();
		this.running = true;
	}
	
//	private void initializeDataStores() {
//		logger.info("Initializing DataStores");
//		for(String username : userMap.keySet()) {
//			User user = userMap.get(username);
//			createDataStoreThreadsForUser(user);
//		}
//		logger.info("Initialization of DataStores completed.");
//	}
	
//	private void createDataStoreThreadsForUser(User user) {
//		Map<String, DataStore> dsMap = user.getDataStores();
//		HashMap<DataStore, Thread> dsThreadMap = new HashMap<DataStore, Thread>();
//		for(String dsName : dsMap.keySet()) {
//			dsThreadMap.put(dsMap.get(dsName), new DataStoreThread(dsMap.get(dsName)));
//		}
//		userDSThreadMap.put(user, dsThreadMap);
//	}
	
	
	
	private boolean updateEngineWithNewUser(String userName) {
		try {
			User user = connection.getUser(userName);
			userMap.put(userName, user);
//			createDataStoreThreadsForUser(user);
		} catch (UserDoesNotExists e) {
			logger.warning(e.getMessage());
			return false;
		}
		return true;
	}
	
	
	@Override
	public void run() {
//		initializeDataStores();
		logger.info("The engine has started.");
		
		while(running) {
			try {
				EngineEntry entry = engineEntryQueue.take();
				String username = entry.getUsername();
				if(username != null) {
					if(!userMap.containsKey(username) && updateEngineWithNewUser(username)) {
						logger.info("Engine updated with user " + username);
					}
//					if(userMap.containsKey(username)) {
						User user = userMap.get(username);
						DataStore dataStore = user.getDataStore(entry.getDataStoreName());
						if(dataStore != null) {
							dataStore.putEntry(entry.getEntry());
						}
						else {
							logger.severe("Failed to get DataStore " + entry.getDataStoreName() + " for user " + username);
						}
//					}
//					else {
//						logger.severe("Failed to get authenticated user : " + username);
//					}
					
				}
				else {
					logger.severe("Engine entry without username generated.");
				}
				
				
			} catch (InterruptedException e) {
				new TimeNomeEngineException(TimeNomeEngine.class.getName() + " interrupted during receiving.", e).printStackTrace();
				running = false;
			}
		}
		logger.info("The engine has stopped.");
	}
	
	public void putEntry(EngineEntry entry) throws InterruptedException {
		engineEntryQueue.put(entry);
	}
	
	public void stop() {
		setRunningState(false);
	}
	
	
	private void setRunningState(boolean state) {
		this.running = state;
	}
	
	public boolean isRunning() {
		return this.running;
	}

	/**
	 * @return the connection
	 */
	public Connection getConnection() {
		return connection;
	}

	/**
	 * @param connection the connection to set
	 */
	private void setConnection(Connection connection) {
		this.connection = connection;
	}
}
