package com.timenome.api.core;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public abstract class User {
	
	private String name;
	protected Map<String, DataStore> dataStoreMap;
	
	public User(String name) {
		this.name = name;
		dataStoreMap = new ConcurrentHashMap<String, DataStore>();
		getMyDataStoresSpec();
	}
	
	protected abstract void getMyDataStoresSpec();

	public String getName() {
		return this.name;
	}
	
	public Map<String, DataStore> getDataStores() {
		return dataStoreMap;
	}
	
	public DataStore getDataStore(String dataStore) {
		if(!dataStoreMap.containsKey(dataStore)) {
			getMyDataStoresSpec();
		}
		return dataStoreMap.get(dataStore);
	}
	
	
}
