/**
 * 
 */
package com.timenome.api.dbhelper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;

import org.apache.ibatis.ScriptRunner;

import com.timenome.api.constant.TimeNomeConstants;
import com.timenome.api.exceptions.UserbaseInitializationException;
import com.timenome.api.logger.TimeNomeLogger;
import com.timenome.api.user.TimeNomeUserBase;

/**
 * @author toshish
 *
 */
public final class MySQLDBHelper {

	private static TimeNomeLogger logger = TimeNomeLogger.getTimeNomeLogger(TimeNomeUserBase.class);

	private static Connection connection;
	
	

	public static Connection getConnection() throws UserbaseInitializationException {
		if(connection == null) {
			initConnection();
			if(connection != null) {
				Timer timer = new Timer(true);
				timer.scheduleAtFixedRate(new TimerTask() {
					
					@Override
					public void run() {
						try {
							initConnection();
						} catch (UserbaseInitializationException e) {
							logger.severe("Userbase initialization failed during periodic ping operation.");
							logger.throwing("MySQLDBHelper", "getConnection", e);
						}						
					}
				}, 1800000, 1800000);
			}
		}
		return connection;
	}

	private static void initConnection() throws UserbaseInitializationException {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			throw new UserbaseInitializationException("Mysql driver couldn't be found.", e);
		}

		Properties prop = new Properties();
		try {
			prop.load(new FileReader(new File(System.getenv("TIMENOME_PROPERTIES"))));
		} catch (FileNotFoundException e1) {
			throw new UserbaseInitializationException("Properties file: " + System.getenv("TIMENOME_PROPERTIES") + " couldn't be found.", e1);
		} catch (IOException e1) {
			throw new UserbaseInitializationException("Error while reading properties file: " + System.getenv("TIMENOME_PROPERTIES") + ".", e1);
		}
		String host = prop.getProperty("mysql.db.host");
		String port = prop.getProperty("mysql.db.port");
		String sid = prop.getProperty("mysql.db.sid");
		String url = "jdbc:mysql://" + host + ":" + port + "/" + sid + "?autoReconnect=true";
		String username = prop.getProperty("mysql.db.username");
		String password = prop.getProperty("mysql.db.password");
		boolean isConnectionWithSID = false;
		try {
			MySQLDBHelper.connection = (Connection) DriverManager.getConnection(url, username, password);
			isConnectionWithSID = true;
		} catch (SQLException e) {
			if(!(e instanceof com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException)) {
				throw new UserbaseInitializationException("Not able to connect to mysql database.", e);
			}
			logger.info("Not able to connect to mysql database. Attempting to connect without specific SID.");

			url = "jdbc:mysql://" + host + ":" + port + "?autoReconnect=true";
			try {
				MySQLDBHelper.connection = (Connection) DriverManager.getConnection(url, username, password);
				isConnectionWithSID = false;
			} catch (SQLException e1) {
				logger.severe("Attempt to obtain mysql connection without specific SID failed.");
				throw new UserbaseInitializationException(e1);
			}
			logger.info("Obtained mysql connection without specific SID.");
		}

		if(!isConnectionWithSID) {
			//Create database;
			createDatabase();
			logger.info("Empty user database created.");
			try {
				connection.setCatalog(TimeNomeConstants.TIMENOME_USER_DATABASE);
				logger.info("Mysql connection set with newly created database.");
			} catch (SQLException e) {
				throw new UserbaseInitializationException("Not able to connect to mysql database.", e);
			}

			logger.warning("Attempting to create userbase.");
			createNewUserBase();

		}
		else if(!isUserBaseExist()) {
			logger.warning("Userbase doesn't exist. Attempting to create it.");
			createNewUserBase();
		}
	}
	
	private static void createNewUserBase() throws UserbaseInitializationException {
		PreparedStatement stmt = null;
		// Create userbase
		try {
			//			final File f = new File(TimeNomeUserBase.class.getProtectionDomain().getCodeSource().getLocation().getPath());

			//			logger.info();
			//			String query = IOUtils.toString(new FileReader("sql/timenome_userbase.sql"));
			InputStream is = TimeNomeUserBase.class.getClassLoader().getResourceAsStream("sql/timenome_userbase.sql");

			if(is == null) {
				throw new UserbaseInitializationException("Could find sql/timenome_userbase.sql.");
			}
			ScriptRunner runner = new ScriptRunner(connection, false, true);
			runner.runScript(new BufferedReader(new InputStreamReader(is)));


			//			String query = IOUtils.toString(is);
			//			logger.info(query);
			//			stmt = connection.prepareStatement(query);
			//			stmt.execute();
			logger.info("Userbase creation completed.");
		} catch (FileNotFoundException e) {
			throw new UserbaseInitializationException(e);
		} catch (IOException e) {
			throw new UserbaseInitializationException(e);
		} catch (SQLException e) {
			throw new UserbaseInitializationException(e);
		}
		finally {
			try {
				if(stmt != null) {
					stmt.close();
				}
			} catch (SQLException e) {
				logger.log(Level.SEVERE, "Error while closing sql resouces.", e);
			}
		}
		logger.info("Verifying newly created Userbase.");
		if(!isUserBaseExist()) {
			throw new UserbaseInitializationException("Attempt to create Userbase failed.");
		}
	}

	private static void createDatabase() throws UserbaseInitializationException {
		PreparedStatement statement = null;
		try {
			statement =  connection.prepareStatement("CREATE DATABASE IF NOT EXISTS " + TimeNomeConstants.TIMENOME_USER_DATABASE);
			statement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.SEVERE, "Incorrect SQL.", e);
			throw new UserbaseInitializationException("Incorrect SQL.", e);
		}
	}

	private static boolean isUserBaseExist() throws UserbaseInitializationException {
		boolean isUserBaseExist = false;
		PreparedStatement statement = null;
		try {
			statement =  connection.prepareStatement("SHOW TABLES LIKE ?");
			statement.setString(1, TimeNomeConstants.USER_TABLE);
		} catch (SQLException e) {
			logger.log(Level.SEVERE, "Incorrect SQL.", e);
			throw new UserbaseInitializationException("Incorrect SQL.", e);
		}
		ResultSet rs = null;
		try {
			rs = statement.executeQuery();
			isUserBaseExist = rs.next();
		} catch (SQLException e) {
			logger.log(Level.WARNING, "Error while fetching from userbase.", e);
			isUserBaseExist = false;
		}
		finally {
			try {
				rs.close();
				statement.close();
			} catch (SQLException e) {
				logger.log(Level.SEVERE, "Error while closing sql resouces.", e);
			}
		}
		return isUserBaseExist;
	}



}
