package com.timenome.examples.beambrush;

import com.timenome.BuiltInAggregationFunction;
import com.timenome.DataStore;
import com.timenome.TimeNomeConnection;
import com.timenome.TimeRange;
import com.timenome.TimeUnit;
import com.timenome.factory.TimeNomeConnectionFactory;

public class CreateDataStoreAndArchives {

	public static void main(String[] args) {
		try {
			TimeNomeConnection timeNomeConnection = TimeNomeConnectionFactory.createConnection(TimeNomeConstants.TIMENOME_HOST,
						TimeNomeConstants.TIMENOME_PORT, TimeNomeConstants.API_KEY, TimeNomeConstants.API_SECRET);
			
			DataStore frommeyer = timeNomeConnection.createDataStore("frommeyer");
			System.out.println("Added DataStore: frommeyer");
			DataStore dykes = timeNomeConnection.createDataStore("dykes");
			System.out.println("Added DataStore: dykes");
			DataStore curry = timeNomeConnection.createDataStore("curry");
			System.out.println("Added DataStore: curry");
			
			frommeyer.addArchive("brushing-score", new TimeRange(TimeUnit.DAY, 1), BuiltInAggregationFunction.AVERAGE);
			System.out.println("Added Archive: brushing-score to DataStore: frommeyer");
			frommeyer.addArchive("brushing-min-time", new TimeRange(TimeUnit.SECOND, 6), BuiltInAggregationFunction.MIN);
			System.out.println("Added Archive: brushing-min-time to DataStore: frommeyer");
			dykes.addArchive("brushing-frequency", new TimeRange(TimeUnit.WEEK, 2), BuiltInAggregationFunction.SUM);
			System.out.println("Added Archive: brushing-frequency to DataStore: dykes");
			curry.addArchive("brushing-max-time", new TimeRange(TimeUnit.MONTH, 3), BuiltInAggregationFunction.MAX);
			System.out.println("Added Archive: brushing-max-time to DataStore: curry");
			
		} catch (Exception e) {
			System.out.println("Error: " + e);
			e.printStackTrace();
		}
	}

}
