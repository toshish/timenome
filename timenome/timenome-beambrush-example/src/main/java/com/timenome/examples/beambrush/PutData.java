package com.timenome.examples.beambrush;

import java.util.Random;

import com.timenome.DataStore;
import com.timenome.DataTuple;
import com.timenome.TimeNomeConnection;
import com.timenome.factory.TimeNomeConnectionFactory;

public class PutData {
	public static void main(String[] args) {
		try {
			TimeNomeConnection timeNomeConnection = TimeNomeConnectionFactory.createConnection(TimeNomeConstants.TIMENOME_HOST,
					TimeNomeConstants.TIMENOME_PORT, TimeNomeConstants.API_KEY, TimeNomeConstants.API_SECRET);

			DataStore dsFro = timeNomeConnection.getDataStore("ani");
			
			Random r = new Random();
			int i = 0;
			while(i < 100000) {
				DataTuple<? extends Number> entry = new DataTuple<Double>(r.nextDouble() * 10);
				if(!dsFro.putData(entry)) {
					System.err.println("Error adding entry : " + entry);
				}
				else {
					System.out.println("Added : " + entry);
				}
				Thread.sleep(500);
			}
		} catch (Exception e) {
			System.out.println("Error: " + e);
			e.printStackTrace();
		}
	}

}
