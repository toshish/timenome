package com.timenome.examples.beambrush;

import com.timenome.TimeNomeConnection;
import com.timenome.factory.TimeNomeConnectionFactory;

public class DeleteDataStore {
	public static void main(String[] args) {
		try {
			TimeNomeConnection timeNomeConnection = TimeNomeConnectionFactory.createConnection(TimeNomeConstants.TIMENOME_HOST,
						TimeNomeConstants.TIMENOME_PORT, TimeNomeConstants.API_KEY, TimeNomeConstants.API_SECRET);
			
			if (timeNomeConnection.deleteDataStore("frommeyer")){
				System.out.println("Deleted DataStore: frommeyer");
			}
			if (timeNomeConnection.deleteDataStore("dykes")){
				System.out.println("Deleted DataStore: dykes");
			}
			if (timeNomeConnection.deleteDataStore("curry")){
				System.out.println("Deleted DataStore: curry");
			}
			
		} catch (Exception e) {
			
			System.out.println("Error: " + e);
			e.printStackTrace();
		}
	}
}
