package com.timenome.examples.beambrush;

import java.util.Map;

import com.timenome.Archive;
import com.timenome.DataStore;
import com.timenome.TimeNomeConnection;
import com.timenome.factory.TimeNomeConnectionFactory;

public class GetDataStoreAndArchives {

	public static void main(String[] args) {
		try {
			TimeNomeConnection timeNomeConnection = TimeNomeConnectionFactory.createConnection(TimeNomeConstants.TIMENOME_HOST,
					TimeNomeConstants.TIMENOME_PORT, TimeNomeConstants.API_KEY, TimeNomeConstants.API_SECRET);

			DataStore frommeyer = timeNomeConnection.getDataStore("frommeyer");
			System.out.println("Got DataStore: " + frommeyer.getDataStoreName());
			DataStore dykes = timeNomeConnection.getDataStore("dykes");
			System.out.println("Got DataStore: " + dykes.getDataStoreName());
			DataStore curry = timeNomeConnection.getDataStore("curry");
			System.out.println("Got DataStore: " + curry.getDataStoreName());

			Map<String, Archive> archives = frommeyer.getArchives();
			for(String archiveName : archives.keySet()) {
				System.out.println("frommeyer has archive: " + archives.get(archiveName));
			}
			
			Archive frequencyArchive = dykes.getArchive("brushing-frequency");
			System.out.println("dykes has archive: " + frequencyArchive);

		} catch (Exception e) {
			System.out.println("Error: " + e);
			e.printStackTrace();
		}
	}

}
