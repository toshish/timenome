package com.timenome.examples.beambrush;

import java.util.List;

import com.timenome.Archive;
import com.timenome.DataStore;
import com.timenome.DataTuple;
import com.timenome.TimeNomeConnection;
import com.timenome.factory.TimeNomeConnectionFactory;
import com.timenome.stream.DataStreamReceiver;

public class StreamData {

	public static void main(String[] args) {
		try {
			TimeNomeConnection timeNomeConnection = TimeNomeConnectionFactory.createConnection(TimeNomeConstants.TIMENOME_HOST,
					TimeNomeConstants.TIMENOME_PORT, TimeNomeConstants.API_KEY, TimeNomeConstants.API_SECRET);

			DataStore frommeyer = timeNomeConnection.getDataStore("frommeyer");
			if(frommeyer != null) {
				System.out.println(frommeyer);
				Archive archive = frommeyer.getArchive("brushing-min-time");
				if(archive != null) {
					System.out.println(archive);
					DataStreamReceiver streamReceiver = new DataStreamReceiver() {

						public void onReceived(List<DataTuple<Double>> listOfEntries) {
							for(DataTuple<Double> entry : listOfEntries) {
								System.out.println(entry);
							}

						}
					};

					timeNomeConnection.streamData(frommeyer.getDataStoreName(), "brushing-min-time", streamReceiver);

					Thread.sleep(100000);
				} else {
					System.err.println("Something went wrong. Archive not found");
				}
			} else {
				System.err.println("Something went wrong. DataStore not found");
			}
		} catch (Exception e) {
			System.out.println("Error: " + e);
			e.printStackTrace();
		}
	}

}
