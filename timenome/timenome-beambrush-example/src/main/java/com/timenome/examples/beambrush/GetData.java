package com.timenome.examples.beambrush;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import com.timenome.DataStore;
import com.timenome.DataTuple;
import com.timenome.TimeNomeConnection;
import com.timenome.factory.TimeNomeConnectionFactory;

public class GetData {

	public static void main(String[] args) {
		try {
			TimeNomeConnection timeNomeConnection = TimeNomeConnectionFactory.createConnection(TimeNomeConstants.TIMENOME_HOST,
					TimeNomeConstants.TIMENOME_PORT, TimeNomeConstants.API_KEY, TimeNomeConstants.API_SECRET);

			DataStore frommeyer = timeNomeConnection.getDataStore("frommeyer");
			System.out.println(frommeyer);
			
			Calendar cal = GregorianCalendar.getInstance(TimeZone.getDefault());
			cal.setTimeInMillis(System.currentTimeMillis() - 60 * 60 * 1000);
			System.out.println(cal.getTime());
			Date startTime = cal.getTime();
			Calendar cal1 = GregorianCalendar.getInstance(TimeZone.getDefault());
			cal1.setTimeInMillis(System.currentTimeMillis());
			System.out.println(cal1.getTime());

			Date endTime = cal1.getTime();
			List<DataTuple<Double>> dataList = timeNomeConnection.getData(frommeyer.getDataStoreName(), "brushing-score", startTime, endTime);
			for(DataTuple<? extends Number> entry : dataList) {
				System.out.println("  " + entry.getTimeStamp() + " : " + entry.getValue());
			}
		} catch (Exception e) {
			System.out.println("Error: " + e);
			e.printStackTrace();
		}
	}

}
