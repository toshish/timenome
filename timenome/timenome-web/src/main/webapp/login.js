var emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

var MIN_PWD_LENGTH = 5;
var INPUT_POLLER_INTERVAL = 200;

var signUpModalValidationPoller = null;

var loginEmail = null;
var loginPassword = null;

var signUpEmail = null;
var signUpName = null;
var signUpPassword = null;
var signUpConfirmPassword = null;
var doSignUpButton = null;
var doLoginBtn = null;

var loginSignUpTab = null;
var loginTabLink = null;
var signUpTabLink = null;

var signUpEmailValidated = false;
var signUpNameValidated = false;
var signUpPasswordValidated = false;
var signUpConfirmPasswordValidated = false;

var signUpNameValidationMsg = null;
var signUpEmailValidationMsg = null;
var signUpPasswordValidationMsg = null;
var signUpConfirmPasswordValidationMsg = null;

var signUpEmailPoller = null;
var signUpNamePoller = null;
var signUpPasswordPoller = null;
var signUpConfirmPasswordPoller = null;

var signUpInProgress = false;
var signUpLoading = null;
var loginLoading = null;

var loginFailedMsg = null;
var onSignUpMsg = null;

function onPageLoad(){

	loginSignUpTab = $("#loginSignUpTab");
	loginTabLink = loginSignUpTab.find("[href='#login']");
	signUpTabLink = loginSignUpTab.find("[href='#signUp']");

	loginEmail = $("#loginEmail");
	loginPassword = $("#loginPassword");

	signUpName = $("#signUpName");
	signUpEmail = $("#signUpEmail");
	signUpPassword = $("#signUpPassword");
	signUpConfirmPassword = $("#signUpConfirmPassword");
	doSignUpButton = $("#doSignUpButton");
	doLoginBtn = $("#doLoginBtn");

	signUpNameValidationMsg = $("#signUpNameValidationMsg");
	signUpEmailValidationMsg = $("#signUpEmailValidationMsg");
	signUpPasswordValidationMsg = $("#signUpPasswordValidationMsg");
	signUpConfirmPasswordValidationMsg = $("#signUpConfirmPasswordValidationMsg");
	loginFailedMsg = $("#loginFailedMsg");
	onSignUpMsg = $("#onSignUpMsg");

	signUpLoading = $("#signUpLoading");
	signUpLoading.hide();

	loginLoading = $("#loginLoading");
	loginLoading.hide();


	initLoginSignUpTab();
	addSignupFormListeners();
	addLoginFormListeners();

	var location = window.location.href;
	var locationParts = location.split("#");

	if(locationParts.length == 2 && locationParts[1] === "signUp"){
		showSignUpTab();
	}
	else{
		showLoginTab();
	}
}

function showLoginTab(){
	loginEmail.val('');
	loginPassword.val('');
	loginFailedMsg.hide();

	clearSignUpNamePoller();
	clearSignUpEmailPoller();
	clearSignUpPasswordPoller();
	clearSignUpConfirmPasswordPoller();
	window.clearInterval(signUpModalValidationPoller);
	loginTabLink.tab('show');
}

function showSignUpTab(){
	signUpName.val('');
	signUpNameValidationMsg.hide();

	signUpEmail.val('');
	signUpEmailValidationMsg.hide();

	signUpPassword.val('');
	signUpPasswordValidationMsg.hide();

	signUpConfirmPassword.val('');
	signUpConfirmPasswordValidationMsg.hide();

	onSignUpMsg.text('');

	signUpEmailValidated = false;
	signUpNameValidated = false;
	signUpPasswordValidated = false;
	signUpConfirmPasswordValidated = false;

	doSignUpButton.attr('disabled', true);

	signUpModalValidationPoller = window.setInterval(function(){
		if(signUpNameValidated && signUpEmailValidated && signUpPasswordValidated && signUpConfirmPasswordValidated && !signUpInProgress){
			doSignUpButton.removeAttr('disabled');
		}
		else{
			doSignUpButton.attr('disabled', true);
		}
	}, INPUT_POLLER_INTERVAL);

	signUpTabLink.tab('show');
}

function initLoginSignUpTab(){
	loginTabLink.click(function(e){
		showLoginTab();
	});

	signUpTabLink.click(function(e){
		showSignUpTab();
	});
}

function addLoginFormListeners(){
	function onEnterPress(event){
		if(event.keyCode === 13){
			event.preventDefault();
			login();
		}
	}

	loginEmail.keypress(onEnterPress);
	loginPassword.keypress(onEnterPress);
}

function clearSignUpEmailPoller(){
	if(signUpEmailPoller != null){
		window.clearInterval(signUpEmailPoller);
		signUpEmailPoller = null;
	}
}
function clearSignUpNamePoller(){
	if(signUpNamePoller != null){
		window.clearInterval(signUpNamePoller);
		signUpNamePoller = null;
	}
}

function clearSignUpPasswordPoller(){
	if(signUpPasswordPoller != null){
		window.clearInterval(signUpPasswordPoller);
		signUpPasswordPoller = null;
	}
}

function clearSignUpConfirmPasswordPoller(){
	if(signUpConfirmPasswordPoller != null){
		window.clearInterval(signUpConfirmPasswordPoller);
		signUpConfirmPasswordPoller = null;
	}
}

function addSignupFormListeners(){
	signUpEmail.focus(function(){
		signUpEmailPoller = window.setInterval(validateSignUpMailId, INPUT_POLLER_INTERVAL);
	});
	signUpEmail.blur(function(){
		clearSignUpEmailPoller();
		validateSignUpMailId();
	});

	signUpName.focus(function(){
		signUpNamePoller = window.setInterval(validateSignUpName, INPUT_POLLER_INTERVAL);
	});
	signUpName.blur(function(){
		clearSignUpNamePoller();
		validateSignUpName();
	});

	signUpPassword.focus(function(){
		signUpPasswordPoller = window.setInterval(validateSignUpPassword, INPUT_POLLER_INTERVAL);
	});
	signUpPassword.blur(function(){
		clearSignUpPasswordPoller();
		validateSignUpPassword();
	});

	signUpConfirmPassword.focus(function(){
		signUpConfirmPasswordPoller = window.setInterval(validateSignUpConfirmPassword, INPUT_POLLER_INTERVAL);
	});
	signUpConfirmPassword.blur(function(){
		clearSignUpConfirmPasswordPoller();
		validateSignUpConfirmPassword();
	});
}

function validateSignUpConfirmPassword(){
	if((signUpPassword.val().length !== signUpConfirmPassword.val().length) || (signUpPassword.val()!==signUpConfirmPassword.val()) || signUpPassword.val().length == 0){
		signUpConfirmPasswordValidated = false;
		signUpConfirmPasswordValidationMsg.hide();
	}
	else{
		signUpConfirmPasswordValidated = true;
		signUpConfirmPasswordValidationMsg.show();
	}
}

function validateSignUpPassword(){
	if(signUpPassword.val().length >= MIN_PWD_LENGTH){
		signUpPasswordValidated = true;
		signUpPasswordValidationMsg.hide();
	}
	else{
		signUpPasswordValidated = false;
		signUpPasswordValidationMsg.show();
	}
	validateSignUpConfirmPassword();
}

function validateSignUpName(){
	if(signUpName.val().length > 0){
		signUpNameValidated = true;
		signUpNameValidationMsg.hide();
	}
	else{
		signUpNameValidated = false;
		signUpNameValidationMsg.show();
	}
}

function validateSignUpMailId(){
	var correctEmail = emailRegex.test(signUpEmail.val());

	if(correctEmail){
		signUpEmailValidated = true;
		signUpEmailValidationMsg.hide();
	}
	else{
		signUpEmailValidated = false;
		signUpEmailValidationMsg.show();
	}
}

function login(){
	doLoginBtn.attr('disabled',true);
	loginLoading.show();
	var email = loginEmail.val();
	var password = loginPassword.val();

	var authParams = $.base64.encode(email + ':' + password);
	$.ajax({
		type: "POST",
		url: "rest/userservice/login",
		data: JSON.stringify({}),
		contentType: "application/json; charset=UTF-8",
		dataType: "json",
		headers: {
			"Authorization": authParams
		}
	}).done(function(authenticated){
			doLoginBtn.removeAttr('disabled');
			loginLoading.hide();
			if(authenticated === true){
				window.location.href = "index.html";
			}
			else{
				loginPassword.val('');
				loginFailedMsg.show();
			}
		});
}

function signUp(){
	signUpInProgress = true;
	signUpLoading.show();

	var userName = signUpName.val();
	var email = signUpEmail.val();
	var password = signUpPassword.val();

	var signUpData = {"name": userName};

	var authParams = $.base64.encode(email + ':' + password);
	$.ajax({
		type: "POST",
		url: "rest/userservice/signup",
		data: JSON.stringify(signUpData),
		contentType: "application/json; charset=UTF-8",
		dataType: "json",
		headers: {
			"Authorization": authParams
		}
	}).done(function(data){
			signUpInProgress = false;
			signUpLoading.hide();
			if(data.error !== null){
				onSignUpMsg.removeClass("alert-success");
				onSignUpMsg.addClass("alert-danger");
				onSignUpMsg.text(data.error.errorText);
			}
			else{
				onSignUpMsg.addClass("alert-success");
				onSignUpMsg.removeClass("alert-danger");
				onSignUpMsg.html("Congrats! You have successfully signed up. <br/> A validation email has been to your registered email address for confirmation.");
			}
		});
}