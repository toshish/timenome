[TimeNome]
=========
A Framework for writing event enabled Time Series data analysis applications.

Version
----

1.0 Beta

timenome.js
----------
timenome.js is a library for writing web applications using TimeNome engine as a backend. 

Third Party Libraries dependency
-----------

timenome.js uses a number of open source projects to work properly:

* [ProtoBuf.js] - A libary for serializing and deserializing protobuf messages.
* [ByteBuffer.js] - Provides a full-featured ByteBuffer implementation using typed arrays. Needed by ProtoBuf.js
* [Long.js] - A Long class for representing a 64-bit two's-complement integer value. Needed by ByteBuffer.js
* HTML5 WebSocket

Browser Compatibility
-----------
timenome.js works on all browers which support HTML5 WebSockets. Unfortunately IE-7, IE-8 and IE-9 don't support WebSockets and hence timenome.js will not work on them.

Please check [WebSocket Support] to know if your browser is supported.

We will soon port timenome.js to work on IE-8 and IE-9.

Using timenome.js
--------------
Read the [Concepts] to learn how you can use TimeNome in your system.

To use timenome.js in your web application, copy the HTML content below in your web application. 

```
<script type="text/javascript" src="Long.min.js"></script>
<script type="text/javascript" src="ByteBuffer.min.js"></script>
<script type="text/javascript" src="ProtoBuf.min.js"></script>

<script type="text/javascript" src="timenome.js"></script>
```

You can use timenome.js to create a connection to the TimeNome engine. Using that connection you can get do data manipulation/data definition operations with TimeNome.

Visit [Creating a connection] to get started with timenome.js.

License
----

Code Licensed Under [Attribution-NonCommercial-NoDerivs 3.0 Unported]

[ProtoBuf.js]:https://github.com/dcodeIO/ProtoBuf.js/
[ByteBuffer.js]:https://github.com/dcodeIO/ByteBuffer.js
[Long.js]:https://github.com/dcodeIO/Long.js
[Attribution-NonCommercial-NoDerivs 3.0 Unported]:http://creativecommons.org/licenses/by-nc-nd/3.0/legalcode
[WebSocket Support]:http://caniuse.com/websockets
[Creating a connection]: TimeNome.html#toc6
[TimeNome]: http://www.timenome.com
[Concepts]: http://www.timenome.com/docs/concepts.html
