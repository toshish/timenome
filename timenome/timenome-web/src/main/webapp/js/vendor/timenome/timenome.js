//sudo jsdoc timenome.js -t ~/Dropbox/TimeNome/lib/docstrap-master/template/ -c ~/Dropbox/TimeNome/docs/jsdoc.conf.json -d doc

/**
 * @namespace TimeNome
 */
var timeNome = (function(){

	//Use http://jscompress.com/ to minify
	//Use http://www.textfixer.com/tools/remove-line-breaks.php to remove line breaks from Proto File and parse as String

	var ProtoBuf = dcodeIO.ProtoBuf;
//	var timeNomeProto = ProtoBuf.protoFromFile("protofiles/timenome.proto").build().timenome;
	var timeNomeProto = ProtoBuf.protoFromString('package timenome; option java_package = "com.timenome.proto"; option java_outer_classname = "TimeNomeProtos"; message StatusResponse { required bool isSuccess = 1; optional Error errorObj = 2; } message Error { required int32 errorCode = 1; optional string errorText = 2; } enum BuiltInAggregationFunction { MAX = 0; MIN = 1; AVERAGE = 2; SUM = 3; LAST = 4; FIRST = 5; RANGE = 6; NO_OPERATION = 7; } message DataStore { required string dataStoreName = 1; repeated Archive archive = 2; repeated Attribute attribute = 3; } message Archive { required string archiveName = 1; required BuiltInAggregationFunction aggregationFunction = 2 [default = AVERAGE]; required bool isCounterBased = 3 [default = false]; optional uint64 storagePrecision = 4; optional uint32 counterSize = 5; } message DataEntry { required uint64 timestamp = 1; optional double value = 2; } message Authentication { required string key = 1; required string secret = 2; } message Attribute { required string key = 1; required string value = 2; } message CreateDataStoreRequest { required DataStore dataStore = 1; } message DeleteDataStoreRequest { required string dataStoreName = 1; } message CreateArchiveRequest { required string dataStoreName = 1; required Archive archive = 2; } message DeleteArchiveRequest { required string dataStoreName = 1; required string archiveName = 2; } message PutDataRequest { required string dataStoreName = 1; repeated DataEntry dataEntry = 2; } message PutDataRequestInternal{ required PutDataRequest putDataRequest = 1; required string handback = 2; } message ListAllDataStoreNamesRequest { } message ListAllDataStoreNamesResponse { repeated string dataStoreName = 1; } message GetDataStoreRequest { required string dataStoreName = 1; } message GetDataStoreResponse { required DataStore dataStore = 1; } message GetDataRequest { required string dataStoreName = 1; required string archiveName = 2; required uint64 startTimestamp = 3; required uint64 endTimestamp = 4; } message GetDataResponse { required string dataStoreName = 1; required string archiveName = 2; repeated DataEntry dataEntry = 3; } message PutDataResponse{ required string handback = 1; } message StreamDataRequest { required string dataStoreName = 1; required string archiveName = 2; optional bool isLive = 3 [default = true]; optional uint64 updateFrequency = 4; } message ExecuteRequest { required Authentication authentication = 1; optional CreateDataStoreRequest createDataStoreRequest = 2; optional CreateArchiveRequest createArchiveRequest = 3; optional DeleteDataStoreRequest deleteDataStoreRequest = 4; optional DeleteArchiveRequest deleteArchiveRequest = 5; optional ListAllDataStoreNamesRequest listAllDataStoreNamesRequest = 6; optional GetDataStoreRequest getDataStoreRequest = 7; optional GetDataRequest getDataRequest = 8; optional StreamDataRequest streamDataRequest = 9; } message ExecutionResponse { required StatusResponse statusResponse = 1; optional ListAllDataStoreNamesResponse listAllDataStoreNamesResponse = 2; optional GetDataStoreResponse getDataStoreResponse = 3; optional GetDataResponse getDataResponse = 4; optional PutDataResponse putDataResponse = 5; }').build().timenome;

	var KEEP_WEBSOCKET_OPEN = true;

	var authenticatedObjMap = {};
	var putConnectionMap = {};
	var putConnectionFirstCallBuffer = {};
	var putCallHandbackMap = {};

	var handbackCnt = 0;

	//Endpoints
	var authenticationEndpoint = "/authenticate";

	var createDataStoreEndpoint = "/create/datastore";
	var deleteDataStoreEndpoint = "/delete/datastore";

	var createArchiveEndpoint = "/create/archive";
	var deleteArchiveEndpoint = "/delete/archive";

	var listAllDataStoreNamesEndpoint = "/list/datastorenames";
	var getDataStoreEndpoint = "/get/datastore";

	var getDataEndpoint = "/get/data";
	var streamDataEndpoint = "/stream/data";

	var putDataEndpoint = "/put/data";

	//Data Structures
	var Authentication = timeNomeProto.Authentication;
	var DataEntry = timeNomeProto.DataEntry;
	var DataStore = timeNomeProto.DataStore;
	var Attribute = timeNomeProto.Attribute;
	var Archive = timeNomeProto.Archive;
	var BuiltInAggregationFunction = timeNomeProto.BuiltInAggregationFunction;

	//Requests & Responses
	var ExecuteRequest = timeNomeProto.ExecuteRequest;
	var ExecutionResponse = timeNomeProto.ExecutionResponse;

	var CreateDataStoreRequest = timeNomeProto.CreateDataStoreRequest;
	var DeleteDataStoreRequest = timeNomeProto.DeleteDataStoreRequest;

	var GetDataStoreRequest = timeNomeProto.GetDataStoreRequest;
	/**
	 * @member GetDataStoreResponse
	 *
	 * @property {TimeNomeConnection.DataStore} dataStore A Data Store object containing details of the Data Store and its Archives
	 *
	 */
	var GetDataStoreResponse = timeNomeProto.GetDataStoreResponse;

	var CreateArchiveRequest = timeNomeProto.CreateArchiveRequest;
	var DeleteArchiveRequest = timeNomeProto.DeleteArchiveRequest;

	var ListAllDataStoreNamesRequest = timeNomeProto.ListAllDataStoreNamesRequest;
	/**
	 * @member ListAllDataStoreNamesResponse
	 *
	 * @property {Array.<string>} dataStoreName An array of strings and each string represents a Data Store's name in the system.
	 *
	 */
	var ListAllDataStoreNamesResponse = timeNomeProto.ListAllDataStoreNamesResponse;

	var GetDataRequest = timeNomeProto.GetDataRequest;
	/**
	 * @member GetDataResponse
	 *
	 * @property {string} dataStoreName Name of the Data Store from which the archive's data is fetched
	 * @property {string} archiveName Name of the Archive whose data is fetched
	 * @property {Array.<TimeNomeConnection.DataEntry>} dataEntry Array of timestamp,value tuples stored in the archive.
	 *
	 */
	var GetDataResponse = timeNomeProto.GetDataResponse;
	var StreamDataRequest = timeNomeProto.StreamDataRequest;

	var PutDataRequest = timeNomeProto.PutDataRequest;
	var PutDataRequestInternal = timeNomeProto.PutDataRequestInternal;

	var PROTO_ENCODING_ERROR = 71;
	var PROTO_DECODING_ERROR = 72;

	var CONNECTION_ERROR = 100;

	/**
	 * @member TimeNomeException
	 * @memberOf TimeNome
	 *
	 * @property {string} exceptionType The specific category of the exception
	 * @property {number} exceptionCode A number which corresponds to exceptionType.
	 * @property {string} exceptionMessage A detailed exception string specifying the cause of exception.
	 *
	 */
	function TimeNomeException(type, message, errorCode){
		this.exceptionType = type;
		this.exceptionMessage = message;
		this.exceptionCode = errorCode;
	}

	//Websock connection object
	function WebSockConn(endpointUrl, websockCallbacks){
		if('WebSocket' in window){
			this.websocket = new WebSocket(endpointUrl);
		}
		else if('MozWebSocket' in window){
			this.websocket = new MozWebSocket(endpointUrl);
		}
		else{
			throw {message: "WebSocket is not supported on this browser"};
		}

		this.websocket.binaryType = "arraybuffer";//protobuf doesn't work otherwise
		this.setCallbacks(websockCallbacks);
	}

	WebSockConn.prototype.setCallbacks = function(websockCallbacks){
		if(typeof websockCallbacks == "object"){
			if(typeof websockCallbacks.onOpen === "function"){
				this.websocket.onopen = websockCallbacks.onOpen;
			}

			if(typeof websockCallbacks.onMessage === "function"){
				this.websocket.onmessage = websockCallbacks.onMessage;
			}

			if(typeof websockCallbacks.onClose === "function"){
				this.websocket.onclose = websockCallbacks.onClose;
			}

			if(typeof websockCallbacks.onError === "function"){
				this.websocket.onerror = websockCallbacks.onError;
			}
		}
	};

	WebSockConn.prototype.sendMessage = function(protoObject){
		var encodedMessage = null;
		try{
			encodedMessage = protoObject.encode().toArrayBuffer();
		}
		catch(e){
			// format exception
			var formatException = new TimeNomeException("Format Exception", e.message, PROTO_ENCODING_ERROR);
			throw formatException;
		}

		try{
			this.websocket.send(encodedMessage);
		}
		catch(e){
			// connectivity exception
			var connectivityException = new TimeNomeException("Connectivity Exception", e.message, CONNECTION_ERROR);
			throw connectivityException;
		}
	};

	WebSockConn.prototype.disconnect = function(){
		this.websocket.close();
	};

	//Generic websocket wrapper which doesn't care about handbacks
	function ExecuteCommandConnection(connectionId, endpointUrl, dataToBeSent, successCallback, errorCallback, resultObjKey, keepWebSocketOpen){
		var self = this;

		try{
			WebSockConn.call(this, authenticatedObjMap[connectionId].engineUrl + endpointUrl, {
				onOpen: function(event){
					self.executeCommand(dataToBeSent, errorCallback, keepWebSocketOpen);
				},
				onMessage: function(event){
					self.processResponse(event.data, successCallback, errorCallback, resultObjKey, keepWebSocketOpen);
				},
				onError: function(){
					if(typeof errorCallback === "function"){
						errorCallback(new TimeNomeException("Connectivity Exception", "Failed to connect to " + authenticatedObjMap[connectionId].engineUrl + ". Please recheck the url", CONNECTION_ERROR));
					}
				}
			});
		}
		catch(e){
			if(typeof errorCallback === "function"){
				errorCallback(new TimeNomeException("Connectivity Exception", e.message, CONNECTION_ERROR));
			}
		}

		this.setSuccessCallback = function(callback){
			successCallback = callback;
		};

		this.setResultObjKey = function(resObjKey){
			resultObjKey = resObjKey;
		};
	}

	ExecuteCommandConnection.prototype = Object.create(WebSockConn.prototype);
	ExecuteCommandConnection.prototype.executeCommand = function(dataToBeSent, errorCallback, keepWebSocketOpen){
		try{
			this.sendMessage(dataToBeSent);
		}
		catch(e){
			if(!keepWebSocketOpen){
				this.disconnect();
			}

			if(typeof(errorCallback) === "function"){
				errorCallback(e);
			}
		}
	};
	ExecuteCommandConnection.prototype.processResponse = function(responseData, successCallback, errorCallback, resultObjKey, keepWebSocketOpen){
		if(!keepWebSocketOpen){
			this.disconnect();
		}
		try{
			var executionResponse = ExecutionResponse.decode(responseData);
			var statusResponse = executionResponse.statusResponse;

			if(statusResponse.isSuccess){
				if(typeof(successCallback) === "function"){
					var resultObj = undefined;
					if(typeof resultObjKey === "string"){
						resultObj = executionResponse[resultObjKey];
					}
					successCallback(resultObj);
				}
			}
			else{
				var timeNomeException = new TimeNomeException("Request Exception", statusResponse.errorObj.errorText, statusResponse.errorObj.errorCode);
				if(typeof(errorCallback) === "function"){
					errorCallback(timeNomeException);
				}
			}
		}
		catch(e){
			var exception = new TimeNomeException("Format Exception", e.message, PROTO_DECODING_ERROR);
			if(typeof(errorCallback) === "function"){
				errorCallback(exception);
			}
		}
	};

	/**
	 * @namespace TimeNomeConnection
	 *
	 * @desc Use {@link TimeNome.createConnection} to create a new Connection
	 *
	 */
	function TimeNomeConnection(connectionId){

		var connection = this;

		var connectionInterface = {

			/**
			 * @class CreateDataStoreRequest
			 * @memberOf TimeNomeConnection
			 * @property {string} dataStoreName  The name of the new DataStore you want to create. No two DataStores can have same name.
			 * @property {Array<TimeNomeConnection.CreateArchiveRequest>} [archive=[]] Array of requests for creating set of Archives along with DataStore.
			 *
			 * @example
			 * var archive1 = new myTimeNomeConnection.Archive({
			 *     archiveName: "myArchive1",
			 *     aggregationFunction: myTimeNomeConnection.BuiltInAggregationFunction.SUM,
			 *     isCounterBased: true,
			 *     counterSize: 15
			 * });
			 *
			 * var archive2 = new myTimeNomeConnection.Archive({
			 *     archiveName: "myArchive2",
			 *     aggregationFunction: myTimeNomeConnection.BuiltInAggregationFunction.MIN,
			 *     archive1.storagePrecision: 24 * 60 * 60 * 1000 // 24 hours
			 * });
			 *
			 *  var createDataStoreRequest = new myTimeNomeConnection.CreateDataStoreRequest({
			 *        dataStore: new myTimeNomeConnection.DataStore({
			 *                       dataStoreName: "my data store",
			 *                       archive: [archive1, archive2]
			 *                   })
			 *  });
			 *
			 * @example
			 * var createDataStoreRequest = new myTimeNomeConnection.CreateDataStoreRequest({
			 *        dataStore: new myTimeNomeConnection.DataStore({
			 *                       dataStoreName: "my data store"
			 *                   })
			 *  });
			 * //Check {@link TimeNome.createConnection} to learn how to create a connection
			 *
			 */
			"CreateDataStoreRequest": CreateDataStoreRequest,

			/**
			 * Create a new DataStore. <br/>You can create an empty DataStore with no Archives in it.
			 *
			 * @method createDataStore
			 * @memberOf TimeNomeConnection#
			 *
			 * @param {TimeNomeConnection.CreateDataStoreRequest} createDataStoreRequestObj
			 * @param {function} successCallback If Data Store gets created successfully then this callback is invoked. No parameters are passed to the callback
			 * @param {function} errorCallback If Data Store fails to get created then this callback is invoked. {@link TimeNomeException} is passed as a parameter.
			 *
			 * @example
			 *
			 * //authenticatedConnection can be made using
			 * //{@link TimeNome.createConnection}
			 *
			 *  authenticatedConnection.createDataStore(new authenticatedConnection.CreateDataStoreRequest({
			 *   'dataStore': new authenticatedConnection.DataStore({
			 *		'dataStoreName': 'My new data store'
			 *	})
			 *	},function(){
             *
			 * 	}, function errorCallback(exceptionObj){
			 *  //The exceptionObj is of type {@link TimeNomeException}
			 * });
			 *
			 */
			"createDataStore": connection.createDataStore,

			/**
			 * @class DeleteDataStoreRequest
			 * @memberOf TimeNomeConnection
			 * @property {string} dataStoreName  The name of the DataStore you want to delete.
			 *
			 * @example
			 *  var deleteDataStoreRequest = new myTimeNomeConnection.DeleteDataStoreRequest({
			 *        dataStoreName: "mydatastore"
			 *  });
			 *
			 * //Check {@link TimeNome.createConnection} to learn how to create a connection
			 *
			 */
			"DeleteDataStoreRequest": DeleteDataStoreRequest,

			/**
			 * Deletes an existing DataStore along with all of its Archives and the data in them.
			 *
			 * @method deleteDataStore
			 * @memberOf TimeNomeConnection#
			 *
			 * @param {TimeNomeConnection.DeleteDataStoreRequest} deleteDataStoreRequestObj
			 * @param {function} successCallback If Data Store gets deleted successfully then this callback is invoked. No parameters are passed to the callback
			 * @param {function} errorCallback If Data Store fails to get deleted then this callback is invoked. {@link TimeNomeException} is passed as a parameter.
			 *
			 * @example
			 *
			 * //authenticatedConnection can be made using
			 * //{@link TimeNome.createConnection}
			 *
			 * authenticatedConnection.deleteDataStore(new authenticatedConnection.DeleteDataStoreRequest({
			 *		'dataStoreName': 'My data store'
			 *	},function(){
             *
			 * 	}, function errorCallback(exceptionObj){
			 *     //The exceptionObj is of type {@link TimeNomeException}
			 * });
			 */
			"deleteDataStore": connection.deleteDataStore,

			/**
			 * @class CreateArchiveRequest
			 * @memberOf TimeNomeConnection
			 * @property {string} dataStoreName The name of the DataStore in which the Archive will be created.
			 * @property {TimeNomeConnection.Archive} archive The Archive object containing details of the Archive to be created.
			 *
			 * @example
			 * var createArchiveRequest = new myTimeNomeConnection.CreateArchiveRequest({
			 *     dataStoreName: "myDataStore",
			 *     archive: new myTimeNomeConnection.Archive({
			 *                   archiveName: "myNewArchive",
			 *                   aggregationFunction: myTimeNomeConnection.BuiltInAggregationFunction.AVERAGE,
			 *                   isCounterBased: true,
			 *                   counterSize: 10
			 *              })
			 * });
			 *
			 * //Check {@link TimeNome.createConnection} to learn how to create a connection
			 *
			 */
			"CreateArchiveRequest": CreateArchiveRequest,

			/**
			 * Deletes an existing DataStore along with all of its Archives and the data in them.
			 *
			 * @method createArchive
			 * @memberOf TimeNomeConnection#
			 *
			 * @param {TimeNomeConnection.CreateArchiveRequest} createArchiveRequestObj
			 * @param {function} successCallback If Data Store gets deleted successfully then this callback is invoked. No parameters are passed to the callback
			 * @param {function} errorCallback If Data Store fails to get deleted then this callback is invoked. {@link TimeNomeException} is passed as a parameter.
			 *
			 * @example
			 *
			 * //authenticatedConnection can be made using
			 * //{@link TimeNome.createConnection}
			 *
			 * var createArchiveRequest = new authenticatedConnection.CreateArchiveRequest({
			 *     dataStoreName: "myDataStore",
			 *     archive: new authenticatedConnection.Archive({
			 *                   archiveName: "myNewArchive",
			 *                   aggregationFunction: authenticatedConnection.BuiltInAggregationFunction.AVERAGE,
			 *                   isCounterBased: true,
			 *                   counterSize: 10
			 *              })
			 * });
			 *
			 * authenticatedConnection.createArchive(createArchiveRequest,
			 *  function(){
             *
			 * 	},
			 *    function errorCallback(exceptionObj){
			 *     //The exceptionObj is of type {@link TimeNomeException}
			 *  });
			 */
			"createArchive": connection.createArchive,

			/**
			 * @class DeleteArchiveRequest
			 * @memberOf TimeNomeConnection
			 * @property {string} dataStoreName  The name of the DataStore from which the Archive will be deleted.
			 * @property {string} archiveName The name of the Archive to be deleted.
			 *
			 * @example
			 *  var deleteArchiveRequest = new myTimeNomeConnection.DeleteArchiveRequest({
			 *        dataStoreName: "mydatastore",
			 *        archiveName: "myarchive"
			 *  });
			 *
			 * //Check {@link TimeNome.createConnection} to learn how to create a connection
			 */
			"DeleteArchiveRequest": DeleteArchiveRequest,

			/**
			 * Deletes an existing Archive along with all of its data.
			 *
			 * @method deleteArchive
			 * @memberOf TimeNomeConnection#
			 *
			 * @param {TimeNomeConnection.DeleteArchiveRequest} deleteArchiveRequestObj
			 * @param {function} successCallback If Archive gets deleted successfully then this callback is invoked. No parameters are passed to the callback
			 * @param {function} errorCallback If Archive fails to get deleted then this callback is invoked. {@link TimeNomeException} is passed as a parameter.
			 *
			 * @example
			 *
			 * //authenticatedConnection can be made using
			 * //{@link TimeNome.createConnection}
			 *
			 * authenticatedConnection.deleteArchive(new authenticatedConnection.DeleteArchiveRequest({
			 *		'dataStoreName': 'My data store',
			 *	    'archiveName': 'My archive'
			 *	},function(){
             *
			 * 	}, function errorCallback(exceptionObj){
			 *     //The exceptionObj is of type {@link TimeNomeException}
			 * });
			 */
			"deleteArchive": connection.deleteArchive,

			/**
			 * @class ListAllDataStoreNamesRequest
			 * @memberOf TimeNomeConnection
			 *
			 * @example
			 *  var listAllDataStoreNamesRequest = new myTimeNomeConnection.ListAllDataStoreNamesRequest();
			 *
			 * //Check {@link TimeNome.createConnection} to learn how to create a connection
			 */
			"ListAllDataStoreNamesRequest": ListAllDataStoreNamesRequest,

			/**
			 * Fetches names of all DataStores currently present in the system.
			 *
			 * @method listAllDataStoreNames
			 * @memberOf TimeNomeConnection#
			 *
			 * @param {TimeNomeConnection.ListAllDataStoreNamesRequest} listAllDsNamesRequestObj
			 * @param {function} successCallback If all Data Store names are successfully fetched then this callback is invoked. {@link ListAllDataStoreNamesResponse} is passed as a parameter
			 * @param {function} errorCallback If the call to get all Data Store names fails then this callback is invoked. {@link TimeNomeException} is passed as a parameter.
			 *
			 * @example
			 *
			 * //authenticatedConnection can be made using
			 * //{@link TimeNome.createConnection}
			 *
			 * authenticatedConnection.listAllDataStoreNames(new authenticatedConnection.ListAllDataStoreNamesRequest(),
			 *      function(response){
			 *      //The response is of type {@link ListAllDataStoreNamesResponse}
			 *            var dataStoreNames = response.dataStoreName;
			 *
			 *            for(var i = 0; i < dataStoreNames.length; i++){
			 *				//dataStoreNames[i] is a string representing a Data Store's name
			 *			}
			 *        },
			 *      function errorCallback(exceptionObj){
			 *          //The exceptionObj is of type {@link TimeNomeException}
			 *        });
			 */
			"listAllDataStoreNames": connection.listAllDataStoreNames,

			/**
			 * @class GetDataStoreRequest
			 * @memberOf TimeNomeConnection
			 * @property {string} dataStoreName  The name of the DataStore whose details are needed.
			 *
			 *  @example
			 *  var getDataStoreRequest = new myTimeNomeConnection.GetDataStoreRequest({
			 *        dataStoreName: "mydatastore",
			 *  });
			 *
			 * //Check {@link TimeNome.createConnection} to learn how to create a connection
			 */
			"GetDataStoreRequest": GetDataStoreRequest,

			/**
			 * Returns details of a DataStore.
			 *
			 * @method getDataStore
			 * @memberOf TimeNomeConnection#
			 *
			 * @param {TimeNomeConnection.GetDataStoreRequest} getDataStoreRequest
			 * @param {function} successCallback If the Data Store is successfully fetched then this callback is invoked. {@link GetDataStoreResponse} is passed as a parameter
			 * @param {function} errorCallback If the call to get Data Store fails then this callback is invoked. {@link TimeNomeException} is passed as a parameter.
			 *
			 * @example
			 *
			 * //authenticatedConnection can be made using
			 * //{@link TimeNome.createConnection}
			 *
			 * var getDataStoreRequest = new authenticatedConnection.GetDataStoreRequest({
			 *        dataStoreName: "mydatastore",
			 *  });
			 *
			 * authenticatedConnection.getDataStore(getDataStoreRequest,
			 *      function(response){
			 *        //The response is of type {@link GetDataStoreResponse}
			 *        },
			 *      function errorCallback(exceptionObj){
			 *          //The exceptionObj is of type {@link TimeNomeException}
			 *        });
			 */
			"getDataStore": connection.getDataStore,

			/**
			 * @class DataStore
			 * @desc {@link http://www.timenome.com/docs/concepts.html#dataStore | Learn more about DataStore.}
			 *
			 * @memberOf TimeNomeConnection
			 * @property {string} dataStoreName Name of the data store.
			 * @property {Array.<TimeNomeConnection,Attribute>} set of attributes associated with the data store.
			 * @property {Array.<TimeNomeConnection.Archive>} archive Array of Archives present in the DataStore.
			 *
			 * @example
			 *
			 * var attribute1 = new myTimeNomeConnection.Attribute(
			 *     key: "my-fav-key1",
			 *     value: "my-keys-value"
			 * );
			 *
			 * var attribute2 = new myTimeNomeConnection.Attribute(
			 *     key: "my-fav-key2",
			 *     value: "my-keys-value2"
			 * );
			 *
			 * var dataStore1 = new myTimeNomeConnection.DataStore(
			 *     dataStoreName: "my data store",
			 *     attribute: [attribute1, attribute2]
			 * );
			 *
			 * //Check {@link TimeNome.createConnection} to learn how to create a connection
			 *
			 * @example
			 * var archive1 = new myTimeNomeConnection.Archive({
			 *     archiveName: "myArchive1",
			 *     aggregationFunction: myTimeNomeConnection.BuiltInAggregationFunction.SUM,
			 *     isCounterBased: true,
			 *     counterSize: 15
			 * });
			 *
			 * var archive2 = new myTimeNomeConnection.Archive({
			 *     archiveName: "myArchive2",
			 *     aggregationFunction: myTimeNomeConnection.BuiltInAggregationFunction.MIN,
			 *     archive1.storagePrecision: 24 * 60 * 60 * 1000 // 24 hours
			 * });
			 *
			 * var dataStore = new myTimeNomeConnection.DataStore({
			 *     dataStoreName: "my data store",
			 *     archive: [archive1, archive2]
			 * });
			 *
			 * //Check {@link TimeNome.createConnection} to learn how to create a connection
			 *
			 */
			"DataStore": DataStore,

			/**
			 * @class Attribute
			 * @desc An Attribute is a property(key) value pair which can be associated as Meta Data with a DataStore.
			 *
			 * @memberOf TimeNomeConnection
			 * @property {string} key property name.
			 * @property {string} value The value of the property
			 *
			 * @example
			 *
			 * var attribute = new myTimeNomeConnection.Attribute(
			 *     key: "my-fav-key",
			 *     value: "my-keys-value"
			 * );
			 *
			 * //Check {@link TimeNome.createConnection} to learn how to create a connection
			 */
			"Attribute": Attribute,

			/**
			 * @class Archive
			 * @desc {@link http://www.timenome.com/docs/concepts.html#archive | Learn more about Archive.}
			 *
			 * @memberOf TimeNomeConnection
			 * @property {string} archiveName The name of Archive.<br/> It should be unique among all the Archives for a given DataStore.
			 * @property {TimeNomeConnection.BuiltInAggregationFunction} [aggregationFunction=AVERAGE] The Aggregation Function that should be applied over set of values collected over <code>storagePrecision</code>.
			 * @property {boolean} [isCounterBased=false] If the value is true then <code>counterSize</code> is used to determine when the Aggregation function should be applied. Else <code>storagePrecision</code> is used.
			 * @property {number} storagePrecision The amount of time in milliseconds after which the Archive applies Aggregation Function on data collected. <br>The result of the Aggregation function is stored in Archive.<br>{@link http://www.timenome.com/docs/concepts.html#storagePrecision | Learn more here.}
			 * @property {number} counterSize In case the Archive is counter based then, this param specifies the number of values on which Archive applies Aggregation Function.<br>{@link http://www.timenome.com/docs/concepts.html#counterSize | Learn more here.}
			 *
			 * @example
			 *
			 * var archive1 = new myTimeNomeConnection.Archive();
			 * archive1.archiveName = "my archive based on storage precision";
			 * archive1.aggregationFunction = myTimeNomeConnection.BuiltInAggregationFunction.RANGE;
			 * archive1.isCounterBased = false; //Default value is false, so you can leave this property unset as well
			 * archive1.storagePrecision = 60 * 1000; //60 seconds = 60 * 1000 milliseconds
			 *
			 * //Check {@link TimeNome.createConnection} to learn how to create a connection
			 *
			 * @example
			 * var archive2 = new myTimeNomeConnection.Archive({
			 *     archiveName: "my archive based on counter",
			 *     aggregationFunction: myTimeNomeConnection.BuiltInAggregationFunction.SUM,
			 *     isCounterBased: true,
			 *     counterSize: 40
			 * });
			 *
			 * //Check {@link TimeNome.createConnection} to learn how to create a connection
			 */
			"Archive": Archive,

			/**
			 * @enum BuiltInAggregationFunction
			 * @memberOf TimeNomeConnection
			 *
			 * @property {enum} MAX Stores maximum value from set of values collected over Storage Precision / Counter Size.
			 * @property {enum} MIN Stores minimum value from set of values collected over Storage Precision / Counter Size.
			 * @property {enum} AVERAGE Stores average/mean of the set of values collected over Storage Precision / Counter Size.
			 * @property {enum} RANGE Stores the difference between maximum and minimum of the values from the set of values collected over Storage Precision / Counter Size.
			 * @property {enum} SUM Stores sum of the set of values collected over Storage Precision / Counter Size.
			 * @property {enum} FIRST Stores first value from the set of values collected over Storage Precision / Counter Size.
			 * @property {enum} LAST Stores last value from the set of values collected over Storage Precision / Counter Size.
			 * @property {enum} NO_OPERATION Values are stored as they are.
			 *
			 * @example
			 *
			 * var max = myTimeNomeConnection.BuiltInAggregationFunction.MAX;
			 *
			 * //Check {@link TimeNome.createConnection} to learn how to create a connection
			 */

			"BuiltInAggregationFunction": BuiltInAggregationFunction,

			/**
			 * @class DataEntry
			 * @desc A DataEntry is a tuple of timestamp and value. When a user gets/puts data from/into TimeNome data is returned/entered in form of DataEntry
			 * @memberOf TimeNomeConnection
			 * @property {number} timestamp  Timestamp in milliseconds (measured since Epoch) specifying when the data was produced.
			 * @property {number} value  The value to be stored for the given <code> timestamp </code>
			 *
			 * @example
			 *
			 * var dataEntry = new myTimeNomeConnection.DataEntry({
			 *     timestamp: new Date().getTime(),
			 *     value: 35.78
			 * });
			 *
			 * //Check {@link TimeNome.createConnection} to learn how to create a connection
			 *
			 */
			"DataEntry": DataEntry,

			/**
			 * @class GetDataRequest
			 * @memberOf TimeNomeConnection
			 *
			 * @property {string} dataStoreName Name of DataStore.
			 * @property {string} archiveName Name of Archive from which data is needed.
			 * @property {number} startTimestamp Timestamp since when the data is needed.
			 * @property {number} endTimestamp Timestamp till when the data is needed.
			 *
			 * @example
			 *  var getDataRequest = new myTimeNomeConnection.GetDataRequest({
			 *        dataStoreName: "mydatastore",
			 *        archiveName: "myarchive",
			 *        startTimestamp: new Date().getTime() - 24 * 60 * 60 * 1000, //yesterday's timestamp
			 *        endTimestamp: new Date().getTime()
			 *  });
			 *
			 * //Check {@link TimeNome.createConnection} to learn how to create a connection
			 *
			 */
			"GetDataRequest": GetDataRequest,

			/**
			 * Returns data stored in an Archive.
			 *
			 * @method getData
			 * @memberOf TimeNomeConnection#
			 *
			 * @param {TimeNomeConnection.GetDataRequest} getDataRequest
			 * @param {function} successCallback If the data is successfully fetched then this callback is invoked. {@link GetDataResponse} is passed as a parameter
			 * @param {function} errorCallback If the call to get Data Store fails then this callback is invoked. {@link TimeNomeException} is passed as a parameter.
			 *
			 * @example
			 *
			 * //authenticatedConnection can be made using
			 * //{@link TimeNome.createConnection}
			 *
			 * var getDataRequest = new authenticatedConnection.GetDataRequest({
			 *        dataStoreName: "mydatastore",
			 *        archiveName: "myarchive",
			 *        startTimestamp: new Date().getTime() - 24 * 60 * 60 * 1000, //yesterday's timestamp
			 *        endTimestamp: new Date().getTime()
			 *  });
			 *
			 * authenticatedConnection.getData(getDataRequest,
			 *      function(response){
			 *        //The response is of type {@link GetDataResponse}
			 *        },
			 *      function errorCallback(exceptionObj){
			 *          //The exceptionObj is of type {@link TimeNomeException}
			 *        });
			 */
			"getData": connection.getData,

			/**
			 * @class StreamDataRequest
			 * @memberOf TimeNomeConnection
			 *
			 * @property {string} dataStoreName Name of DataStore.
			 * @property {string} archiveName Name of Archive from which live data stream is needed.
			 *
			 * @example
			 *  var streamDataRequest = new myTimeNomeConnection.StreamDataRequest({
			 *        dataStoreName: "mydatastore",
			 *        archiveName: "myarchive",
			 *  });
			 *
			 * //Check {@link TimeNome.createConnection} to learn how to create a connection
			 */
			"StreamDataRequest": StreamDataRequest,

			/**
			 * Returns live stream data stored in an Archive.
			 *
			 * @method streamData
			 * @memberOf TimeNomeConnection#
			 *
			 * @param {TimeNomeConnection.StreamDataRequest} streamDataRequest
			 * @param {function} streamOpenCallback If the stream successfully opens, then this callback is invoked. {@link StreamHandler} is passed as a parameter
			 * @param {function} onDataCallback After the stream opens, this callback is invoked each time a new set of data points are available from TimeNome engine. {@link GetDataResponse} is passed as a parameter
			 * @param {function} errorCallback If call for opening the stream fails or if streaming fails midway, then this callback is invoked. {@link TimeNomeException} is passed as a parameter.
			 *
			 * @example
			 *
			 * //authenticatedConnection can be made using
			 * //{@link TimeNome.createConnection}
			 *
			 * var streamDataRequest = new authenticatedConnection.StreamDataRequest({
			 *        dataStoreName: "mydatastore",
			 *        archiveName: "myarchive"
			 *  });
			 *
			 * authenticatedConnection.streamData(streamDataRequest,
			 *      function(streamHandler){
			 *        //The streamHandler is of type {@link StreamHandler}
			 *      },
			 *      function(response){
			 *        //The response is of type {@link GetDataResponse}
			 *        },
			 *      function errorCallback(exceptionObj){
			 *          //The exceptionObj is of type {@link TimeNomeException}
			 *        });
			 */
			"streamData": connection.streamData,

			/**
			 * @class PutDataRequest
			 * @memberOf TimeNomeConnection
			 *
			 * @property {string} dataStoreName Name of DataStore in which data is to be added.
			 * @property {Array.<TimeNomeConnection.DataEntry>} dataEntry Set of (timestamp, value) tuples to be added to DataStore.
			 *
			 * @example
			 *
			 * var dataEntry1 = new myTimeNomeConnection.DataEntry({
			 *     timestamp: new Date().getTime(),
			 *     value: 35.78
			 * });
			 *
			 * var dataEntry2 = new myTimeNomeConnection.DataEntry({
			 *     timestamp: new Date().getTime() - 60 * 1000,
			 *     value: 89
			 * });
			 *
			 *  var putDataRequest = new myTimeNomeConnection.PutDataRequest({
			 *        dataStoreName: "mydatastore",
			 *        dataEntry: [dataEntry1, dataEntry2]
			 *  });
			 *
			 * //Check {@link TimeNome.createConnection} to learn how to create a connection
			 */
			"PutDataRequest": PutDataRequest,

			/**
			 * Returns data stored in an Archive.
			 *
			 * @method putData
			 * @memberOf TimeNomeConnection#
			 *
			 * @param {TimeNomeConnection.PutDataRequest} putDataRequest
			 * @param {function} successCallback If data is added to the system successfully then this callback is invoked. No parameters are passed to the callback
			 * @param {function} errorCallback If data fails to get added to the system then this callback is invoked. {@link TimeNomeException} is passed as a parameter.
			 *
			 * @example
			 *
			 * //authenticatedConnection can be made using
			 * //{@link TimeNome.createConnection}
			 *
			 * ar dataEntry1 = new authenticatedConnection.DataEntry({
			 *     timestamp: new Date().getTime(),
			 *     value: 35.78
			 * });
			 *
			 * var dataEntry2 = new authenticatedConnection.DataEntry({
			 *     timestamp: new Date().getTime() - 60 * 1000,
			 *     value: 89
			 * });
			 *
			 *  var putDataRequest = new authenticatedConnection.PutDataRequest({
			 *        dataStoreName: "mydatastore",
			 *        dataEntry: [dataEntry1, dataEntry2]
			 *  });
			 *
			 * authenticatedConnection.putData(putDataRequest,function(){
             *
			 * 	}, function errorCallback(exceptionObj){
			 *     //The exceptionObj is of type {@link TimeNomeException}
			 * });
			 */
			"putData": function(putDataRequest, successCallback, errorCallback){
				if(putConnectionFirstCallBuffer[connectionId] === undefined){
					putConnectionFirstCallBuffer[connectionId] = [];

					putConnectionMap[connectionId] = new ExecuteCommandConnection(connectionId, putDataEndpoint, new ExecuteRequest({
						"authentication": authenticatedObjMap[connectionId].authenticationObj
					}), function(){
						connectionInterface.putData = connection.putDataInternal;

						putConnectionMap[connectionId].setResultObjKey("putDataResponse");
						putCallHandbackMap[connectionId] = {};
						putConnectionMap[connectionId].setSuccessCallback(function(putDataResponse){
							var successCallback = putCallHandbackMap[connectionId][putDataResponse['handback']];
							if(typeof successCallback === "function"){
								successCallback();
							}
							delete putCallHandbackMap[connectionId][putDataResponse['handback']];
						});

						if(putConnectionFirstCallBuffer[connectionId]){
							for(var i = 0; i < putConnectionFirstCallBuffer[connectionId].length; i++){
								var putCallDetails = putConnectionFirstCallBuffer[connectionId][i];
								connectionInterface.putData(putCallDetails.putDataRequest, putCallDetails.successCallback, putCallDetails.errorCallback);
							}
						}

						delete putConnectionFirstCallBuffer[connectionId];

					}, function(exception){
						if(putConnectionFirstCallBuffer[connectionId]){
							for(var i = 0; i < putConnectionFirstCallBuffer[connectionId].length; i++){
								var putCallDetails = putConnectionFirstCallBuffer[connectionId][i];
								putCallDetails.errorCallback(exception);
							}

							delete putConnectionFirstCallBuffer[connectionId];
						}
					}, null, KEEP_WEBSOCKET_OPEN);
				}

				putConnectionFirstCallBuffer[connectionId].push({
					'putDataRequest': putDataRequest,
					'successCallback': successCallback,
					'errorCallback': errorCallback
				});
			},

			"getConnectionId": function(){
				return connectionId;
			},

			/**
			 * Disconnects user session with TimeNome.
			 * <br>
			 * {@link TimeNomeConnection} is very lightweight and hence it should be disconnected only when shutting down the app.
			 * <br>All callbacks are be deleted and hence result of data api calls is not returned to the caller.
			 *
			 * @method closeConnection
			 * @memberOf TimeNomeConnection#
			 *
			 * @example
			 *
			 * myTimeNomeConnection.closeConnection();
			 * //Check {@link TimeNome.createConnection} to learn how to create a connection
			 */
			"closeConnection": function(){
				var putConnection = putConnectionMap[connectionId];

				if(typeof putConnection !== "undefined"){
					putConnection.disconnect();
					//and also tell them websocket is dead
					delete putConnectionFirstCallBuffer[connectionId];

					//and also tell them websocket is dead
					delete putCallHandbackMap[connectionId];

					delete putConnectionMap[connectionId];
				}

				delete authenticatedObjMap[connectionId];
			}
		};

		return connectionInterface;
	}

	TimeNomeConnection.prototype.createDataStore = function(createDataStoreRequestObj, successCallback, errorCallback){
		new ExecuteCommandConnection(this.getConnectionId(), createDataStoreEndpoint, new ExecuteRequest({
			"authentication": authenticatedObjMap[this.getConnectionId()].authenticationObj,
			"createDataStoreRequest": createDataStoreRequestObj
		}), successCallback, errorCallback);
	};

	TimeNomeConnection.prototype.deleteDataStore = function(deleteDataStoreObj, successCallback, errorCallback){
		new ExecuteCommandConnection(this.getConnectionId(), deleteDataStoreEndpoint, new ExecuteRequest({
			"authentication": authenticatedObjMap[this.getConnectionId()].authenticationObj,
			"deleteDataStoreRequest": deleteDataStoreObj
		}), successCallback, errorCallback);
	};

	TimeNomeConnection.prototype.createArchive = function(createArchiveRequestObj, successCallback, errorCallback){
		new ExecuteCommandConnection(this.getConnectionId(), createArchiveEndpoint, new ExecuteRequest({
			"authentication": authenticatedObjMap[this.getConnectionId()].authenticationObj,
			"createArchiveRequest": createArchiveRequestObj
		}), successCallback, errorCallback);
	};

	TimeNomeConnection.prototype.deleteArchive = function(deleteArchiveRequestObj, successCallback, errorCallback){
		new ExecuteCommandConnection(this.getConnectionId(), deleteArchiveEndpoint, new ExecuteRequest({
			"authentication": authenticatedObjMap[this.getConnectionId()].authenticationObj,
			"deleteArchiveRequest": deleteArchiveRequestObj
		}), successCallback, errorCallback);
	};

	TimeNomeConnection.prototype.putDataInternal = function(putDataRequest, successCallback, errorCallback){
		var connection = putConnectionMap[this.getConnectionId()];

		var handback = 'WebHandback' + handbackCnt++;
		putCallHandbackMap[this.getConnectionId()][handback] = successCallback;

		connection.executeCommand(new PutDataRequestInternal({
			'putDataRequest': putDataRequest,
			'handback': handback
		}), errorCallback, KEEP_WEBSOCKET_OPEN);

		//INFO: putDataSuccessCallback is defined in putData of TimeNomeConnection return object
	};

	//For following calls don't do anything if successCallback is not present
	TimeNomeConnection.prototype.listAllDataStoreNames = function(listAllDsNamesRequestObj, successCallback, errorCallback){
		if(typeof successCallback === "function"){
			new ExecuteCommandConnection(this.getConnectionId(), listAllDataStoreNamesEndpoint, new ExecuteRequest({
				"authentication": authenticatedObjMap[this.getConnectionId()].authenticationObj,
				"listAllDataStoreNamesRequest": listAllDsNamesRequestObj
			}), successCallback, errorCallback, "listAllDataStoreNamesResponse");
		}
	};

	TimeNomeConnection.prototype.getDataStore = function(getDataStoreRequest, successCallback, errorCallback){
		if(typeof successCallback === "function"){
			new ExecuteCommandConnection(this.getConnectionId(), getDataStoreEndpoint, new ExecuteRequest({
				"authentication": authenticatedObjMap[this.getConnectionId()].authenticationObj,
				"getDataStoreRequest": getDataStoreRequest
			}), successCallback, errorCallback, "getDataStoreResponse");
		}
	};

	TimeNomeConnection.prototype.getData = function(getDataRequest, successCallback, errorCallback){
		if(typeof successCallback === "function"){
			new ExecuteCommandConnection(this.getConnectionId(), getDataEndpoint, new ExecuteRequest({
				"authentication": authenticatedObjMap[this.getConnectionId()].authenticationObj,
				"getDataRequest": getDataRequest
			}), successCallback, errorCallback, "getDataResponse");
		}
	};

	TimeNomeConnection.prototype.streamData = function(streamDataRequest, streamOpenCallback, onDataCallback, errorCallback){
		var keepStreamOpen = false;
		if(typeof onDataCallback === "function"){
			keepStreamOpen = KEEP_WEBSOCKET_OPEN;
		}

		var executeCmdConnection = new ExecuteCommandConnection(this.getConnectionId(), streamDataEndpoint, new ExecuteRequest({
			"authentication": authenticatedObjMap[this.getConnectionId()].authenticationObj,
			"streamDataRequest": streamDataRequest
		}), function(){

			if(typeof streamOpenCallback === "function"){
				streamOpenCallback(
					/**
					 * @member StreamHandler
					 *
					 * @property {function} close A function which stops streaming of data from engine.
					 *
					 *  @example
					 *
					 * //authenticatedConnection can be made using
					 * //{@link TimeNome.createConnection}
					 *
					 * var streamDataRequest = new authenticatedConnection.StreamDataRequest({
			         *        dataStoreName: "mydatastore",
			         *        archiveName: "myarchive"
			         *  });
					 *
					 * authenticatedConnection.streamData(streamDataRequest,
					 *      function(streamHandler){
					 *      //This is how you can close a stream
			         *        streamHandler.close();
					 *      },
					 *      function(response){
			         *        //The response is of type {@link GetDataResponse}
					 *        },
					 *      function errorCallback(exceptionObj){
			         *          //The exceptionObj is of type {@link TimeNomeException}
					 *        });
					 */{
						close: function(){
							executeCmdConnection.disconnect();
						}
					});
			}

			if(keepStreamOpen){
				executeCmdConnection.setSuccessCallback(onDataCallback);
			}
		}, errorCallback, "getDataResponse", keepStreamOpen);
	};

	//Very first call to be made before calling methods
	function authenticate(url, authenticationObj, successCallback, errorCallback){
		if(typeof url === "string"){
			if(url.charAt(url.length - 1) == "/"){
				url = url.substring(0, url.length - 1);
			}

			var connectionId = "TimeNomeConnection" + (Math.random() * 1000).toString(16);
			authenticatedObjMap[connectionId] = {
				'engineUrl': url,
				'authenticationObj': authenticationObj
			};

			new ExecuteCommandConnection(connectionId, authenticationEndpoint, new ExecuteRequest({
				"authentication": authenticationObj
			}), function(){
				if(typeof successCallback === "function"){
					successCallback(new TimeNomeConnection(connectionId));
				}
			}, errorCallback);
		}
		else{
			errorCallback(new TimeNomeException("Connectivity Exception", "Incorrect url", CONNECTION_ERROR));
		}
	}

	return{
		/**
		 * @class AuthenticationObj
		 * @memberOf TimeNome
		 * @property {string} key Key of the user to be authenticated.
		 * @property {string} secret Secret of the user to be authenticatd
		 * @desc When a new user is registered with TimeNome a key and secret is assigned to him.
		 * <br/>The key and secret is used for authenticating users when they use the data api.
		 *
		 * @example
		 * var authObj = new timeNome.AuthenticationObj({
		 *     "key":  "m7ia7c92fh4pqp9qgjtc52t7h7"
		 *     "secret": "90q26916mpgkvp3etl6or9j2j7"
		 * });
		 *
		 * //Replace the values of key and secret with the ones you get.
		 *
		 */
		"AuthenticationObj": Authentication,

		/**
		 * Creates a new Connection with TimeNome engine. Using this connection any call from {@link TimeNomeConnection} can be made any number of times. <br/>If {@link TimeNome.AuthenticationObj} is not filled with key and secret then this method throws {@link TimeNomeException}.
		 *
		 * @method createConnection
		 * @memberOf TimeNome#
		 *
		 * @param {string} url The url of the server where TimeNome engine is running.
		 * @param {TimeNome.AuthenticationObj} authenticationObj The object containing key and secret for the user to be authenticated.
		 * @param {function} successCallback If the authentication credentials are successfully validated, then {@link TimeNomeConnection} is returned.
		 * @param {function} errorCallback If the authentication credentials fail to validate then {@link TimeNomeException} is returned
		 *
		 * @example
		 * var authObj = new timeNome.AuthenticationObj({
		 *     "key":  "m7ia7c92fh4pqp9qgjtc52t7h7"
		 *     "secret": "90q26916mpgkvp3etl6or9j2j7"
		 * });
		 *
		 * function successCallback(connectionObj){
		 *  //The connectionObj is of type {@link TimeNomeConnection}
		 *  //Using connectionObj you can use data api's.
		 * }
		 *
		 * function errorCallback(exceptionObj){
		 *  //The exceptionObj is of type {@link TimeNomeException}
		 * }
		 *
		 * // ws:// is to be used in the url instead of http://
		 * timeNome.createConnection("ws://localhost:8080/mytimenomeengine/", authObj, successCallback, errorCallback);
		 */
		"createConnection": authenticate
	};
})();
