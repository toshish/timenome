var portlet = angular.module('portlet', []);

portlet.directive('portlet',
	['$route', 'PageUrlService', '$timeout',
		function($route, PageUrlService, $timeout){

			/**
			 * This directive exposes scope.viewConfig as a property to be used by portlet view
			 * using this the scope.viewConfig portlet's parent controller can talk to the portlet view
			 *
			 */

			var portletIdPrefix = "uknportlet_";
			var portletCnt = 0;

			function getNewPortletId(){
				return portletIdPrefix + ++portletCnt;
			}

			return {
				restrict: "A",
				templateUrl: "js/modules/portlet/templates/directives/portlet.html",
				scope: {
					viewConfig: "=portletContentConfig",
					configureView: "=portletViewConfigureFunction",
					portletTitle: "=portletTitle"
				},
				link: function(scope, element, attrs){

					var portletId = angular.isString(attrs.portlet) ? attrs.portlet:getNewPortletId();
					var fullSizePortletPageLink = PageUrlService.createFullSizePortletPageLink($route.current[PAGE_NAME], portletId);

					if(angular.isDefined(attrs.onLoadCallback)){
						scope.$parent[attrs.onLoadCallback](portletId);
					}

					scope.portletBodyStyle = {};
					scope.minimized = false;

					element.find("[fullscreen-new-window-link]").attr("href", fullSizePortletPageLink);

					scope.hideable = angular.isDefined(attrs.portletMinimizePolicy) ? (attrs.portletMinimizePolicy === "hide"):true;

					scope.$watch('$parent.' + attrs['portletContentConfig'] + '.templateUrl', function(){
						scope.portletTemplateUrl = scope.viewConfig.templateUrl;
					});

					//On Minimize
					scope.minimize = function(){
						scope.minimized = !scope.minimized;

						if(scope.minimized === true){
							scope.portletBodyStyle.height = "0px"; //to override custom css defined by users
							if("destroy" === attrs.portletMinimizePolicy){
								scope.portletTemplateUrl = "";
							}
						}
						else{
							delete scope.portletBodyStyle.height;
							if("destroy" === attrs.portletMinimizePolicy){
								//Extra time so that animation completes and chart takes full height.
								//This can't be done via callback because animation is css controlled
								$timeout(function(){
									scope.portletTemplateUrl = scope.viewConfig.templateUrl;
								}, 500);
							}
						}
					};
				}
			};
		}]
);
