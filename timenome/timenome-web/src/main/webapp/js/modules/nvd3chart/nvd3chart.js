var nvd3chart = angular.module('nvd3chart', []);

nvd3chart.directive('nvd3chart',
	[function(){

		var chartIdPrefix = "nvd3Chart_";
		var chartCnt = 0;

		var chartTypeMap = {
			"lineChart": {
				nvd3ChartType: "lineChart",
				createChart: createLineChart
			},
			"zoomableLineChart": {
				nvd3ChartType: "lineWithFocusChart",
				createChart: createZoomableLineChart
			}
		};

		var d3ColorRange = d3.scale.category10().range();

		function getNewChartId(){
			return chartIdPrefix + ++chartCnt;
		}

		function updateChart(chartId, chartObj, data){
			if(chartObj !== null){
				d3.select(chartId)
					.datum(data)
					.transition().duration(NVD3_CHART_UPDATE_TRANSITION_DURATION)
					.call(chartObj);
			}
		}

		function createLineChart(useInteractiveGuideline){
			useInteractiveGuideline = angular.isDefined(useInteractiveGuideline)? useInteractiveGuideline: true;

			var chart = nv.models[this.nvd3ChartType]().useInteractiveGuideline(useInteractiveGuideline);

			chart = chart.x(function(d){
				return d.timestamp.toNumber();
			}).y(function(d){
				return d.value;
			});

			//Color range
			chart = chart.color(d3ColorRange);

			//Abscissa
			chart.xAxis.tickFormat(function(d){
				return d3.time.format('%X')(new Date(d));
			});

			chart.yAxis.tickFormat(d3.format(',.2f'));

			return chart;
		}

		function createZoomableLineChart(){
			var chart = nv.models[this.nvd3ChartType]();
			chart = chart.x(function(d){
				return d.timestamp.toNumber();
			}).y(function(d){
				return d.value;
			});

			//Color range
			chart = chart.color(d3ColorRange);

			//Abscissa
			chart.xAxis.tickFormat(function(d){
				return d3.time.format('%d %b, %H:%M:%S')(new Date(d));
			});

			chart.x2Axis.tickFormat(function(d){
				return d3.time.format('%d %b %y')(new Date(d));
			});

			chart.yAxis.tickFormat(d3.format(',.2f'));
			chart.y2Axis.tickFormat(d3.format(',.2f'));

			return chart;
		}

		function getNvd3ChartType(chartType){
			if(angular.isDefined(chartType) && angular.isString(chartType) && angular.isDefined(chartTypeMap[chartType])){
				return chartTypeMap[chartType];
			}
			else{
				return chartTypeMap[LINE_CHART];
			}
		}

		return {
			restrict: "A",
			templateUrl: "js/modules/nvd3chart/templates/directives/nvd3chart.html",
			scope: {
				chartData: "=chartData"
			},
			link: function(scope, element, attrs){
				element.attr("id", getNewChartId());

				var chartSelector = "#" + element.attr("id") + " svg";

				var nvd3ChartCreator = getNvd3ChartType(attrs.chartType);
				var chart = nvd3ChartCreator.createChart();

				//Register graph object with nvd3
				nv.addGraph(function(){
					return chart;
				});

				//Add Watch on chart-update
				if(angular.isDefined(attrs.onUpdateWatch)){
					scope.$watch('$parent.' + attrs.onUpdateWatch, function(newVal, oldVal){
						updateChart(chartSelector, chart, scope.chartData);

						if(newVal === oldVal){
							nv.utils.windowResize(chart.update);
						}
					});
				}
			}
		};

	}]
);
