var nvd3chart = angular.module('amchart', []);

nvd3chart.directive('amchart',
	['SettingsService', function(SettingsService){

		var chartIdPrefix = "amchart_";
		var chartCnt = 0;

		var chartTypeMap = {
			"lineChart": {
				amchartType: "serial",
				createChart: createSerialChart,
				createGraph: function(title, valueAxis, valueField){
					var graph = new AmCharts.AmGraph();
					graph.bullet = "round";
					graph.type="line";
					graph.hideBulletsCount = 70;
					graph.title = title;
					graph.valueAxis = valueAxis;
					graph.valueField = valueField;
					return graph;
				},
				stackValues: false
			},
			"columnChart": {
				amchartType: "serial",
				createChart: createSerialChart,
				createGraph: function(title, valueAxis, valueField){
					var graph = new AmCharts.AmGraph();
					graph.fillAlphas = 0.8;
					graph.type="column";
					graph.hideBulletsCount = 70;
					graph.title = title;
					graph.valueAxis = valueAxis;
					graph.valueField = valueField;
					return graph;
				},
				stackValues: false
			},
			"stackedColumnChart": {
				amchartType: "serial",
				createChart: createSerialChart,
				createGraph: function(title, valueAxis, valueField){
					var graph = new AmCharts.AmGraph();
					graph.fillAlphas = 0.8;
					graph.type="column";
					graph.hideBulletsCount = 70;
					graph.title = title;
					graph.valueAxis = valueAxis;
					graph.valueField = valueField;
					return graph;
				},
				stackValues: true
			}
		};

		var colorArray = ['#FF6600', '#B0DE09', '#0D8ECF', '#2A0CD0', '#CD0D74', '#CC0000', '#00CC00', '#0000CC', '#DDDDDD', '#999999', '#333333', '#990000'];
		var categoryAxis24Hr = [
			{period: 'fff', format: 'JJ:NN:SS'},
			{period: 'ss', format: 'JJ:NN:SS'},
			{period: 'mm', format: 'JJ:NN'},
			{period: 'hh', format: 'JJ:NN'},
			{period: 'DD', format: 'MMM DD'},
			{period: 'WW', format: 'MMM DD'},
			{period: 'MM', format: 'MMM'},
			{period: 'YYYY', format: 'YYYY'}
		];
		var categoryAxis12Hr = [
			{period: 'fff', format: 'LL:NN:SS A'},
			{period: 'ss', format: 'LL:NN:SS A'},
			{period: 'mm', format: 'LL:NN A'},
			{period: 'hh', format: 'LL:NN A'},
			{period: 'DD', format: 'MMM DD'},
			{period: 'WW', format: 'MMM DD'},
			{period: 'MM', format: 'MMM'},
			{period: 'YYYY', format: 'YYYY'}
		];

		function getNewChartId(){
			return chartIdPrefix + ++chartCnt;
		}

		function createSerialChart(chartId, chartData, graphDownloadFormat, use12HrFormat, stackValues){

			var valueAxis = {
				id: "v1",
				position: "left"
			};

			if(stackValues===true){
				valueAxis.stackType = "regular";
			}

			var chartConfig = {
				type: "serial",
				dataProvider: chartData,
				pathToImages: "js/vendor/amcharts/images/",
				categoryField: "timestamp",
				colors: colorArray,

				legend: {
					valueText: ""
				},

				categoryAxis: {
					parseDates: true,
					minPeriod: "ss",
					minorGridEnabled: true,
					dateFormats: (use12HrFormat ? categoryAxis12Hr:categoryAxis24Hr)
				},

				valueAxes: [
					valueAxis
				],

				chartCursor: {
					cursorAlpha: 1,
					zoomable: true,
					cursorColor: "#4682b4",
					categoryBalloonEnabled: true,
					categoryBalloonDateFormat: "D-MMM-YYYY " + (use12HrFormat ? "LL:NN:SS A":"JJ:NN:SS")

				},

				chartScrollbar: {
					"color": "#4682b4",
					"autoGridCount": true,
					"scrollbarHeight": 40
				},

				exportConfig: {
					menuItems: [
						{
							icon: 'js/vendor/amcharts/images/export.png',
							iconTitle: 'Download graph',
							fileName: "TimeNomeGraph",
							format: graphDownloadFormat
						}
					]
				}
			};

			var chart = AmCharts.makeChart(chartId, chartConfig);

			return chart;
		}

		function getamchartCreator(chartType){
			if(angular.isDefined(chartType) && angular.isString(chartType) && angular.isDefined(chartTypeMap[chartType])){
				return chartTypeMap[chartType];
			}
			else{
				return chartTypeMap[LINE_CHART];
			}
		}

		return {
			restrict: "A",
			templateUrl: "js/modules/amchart/templates/directives/amchart.html",
			scope: {},
			link: function(scope, element, attrs){
				var chartId = getNewChartId();
				element.children().first().attr("id", chartId);

				SettingsService.getUserSetting(USE_12_HOUR_FORMAT, function(userWants12HrFormat){
					var use12HrFormat = userWants12HrFormat;

					SettingsService.getUserSetting(GRAPH_DOWNLOAD_FORMAT, function(graphDownloadFormat){
						var chartConfig = scope.$parent[attrs.chartData];

						var amchartCreator = getamchartCreator(attrs.chartType);
						var chart = amchartCreator.createChart(chartId, chartConfig.dataArr, graphDownloadFormat, use12HrFormat, amchartCreator.stackValues);

						scope.archiveGraphMap = {};

						//Add Watch on chart-update
						if(angular.isDefined(attrs.onUpdateWatch)){
							scope.$watch('$parent.' + attrs.onUpdateWatch, function(newVal, oldVal){

								var archiveSet = chartConfig.selectedArchives;
								var newArchiveGraphMap = {};
								var archiveGraphMapChanged = false;

								for(var i = 0; i < archiveSet.length; i++){
									if(angular.isDefined(scope.archiveGraphMap[archiveSet[i]])){
										newArchiveGraphMap[archiveSet[i]] = scope.archiveGraphMap[archiveSet[i]];
										delete scope.archiveGraphMap[archiveSet[i]];
									}
									else{
										var graph = getamchartCreator(attrs.chartType).createGraph(archiveSet[i], "v1", archiveSet[i]);
										chart.addGraph(graph);

										newArchiveGraphMap[archiveSet[i]] = graph;
										archiveGraphMapChanged = true;
									}
								}

								for(var archiveId in scope.archiveGraphMap){
									if(scope.archiveGraphMap.hasOwnProperty(archiveId)){
										chart.removeGraph(scope.archiveGraphMap[archiveId]);
										archiveGraphMapChanged = true;
									}
								}

								scope.archiveGraphMap = newArchiveGraphMap;
								chart.dataProvider = chartConfig.dataArr;
								chart.exportConfig.menuItems[0].fileName = chartConfig.title;
								if(archiveGraphMapChanged){
									chart.validateNow();
									chart.validateData();
								}
								else{
									chart.validateData();
								}
							});
						}
					});
				});
			}
		};

	}]
);
