var timenomeUtil = angular.module('timenomeUtil', []);

timenomeUtil.directive(
	'breadcrumb',
	[function(){

		return {
			restrict: "A",
			templateUrl: "js/modules/timenomeUtil/templates/directives/breadcrumb.html",
			scope: {},
			link: function(scope, element, attrs){
				scope.homeUrl = "#/";
				scope.homeUrlText = "Home";
				scope.linkStack = [];

				scope.maxSelCheckValue = 0;

				/*=======================================================================
				 The view compares the selCheckValue for each link against the
				 maxSelCheckVal. The link for which the value matches is selected and
				 rest all remain	unselected.
				 =========================================================================*/
				scope.setSelected = function(breadCrumbLink){
					scope.maxSelCheckValue++;//no upper limit
					breadCrumbLink.selCheckValue = scope.maxSelCheckValue;
				};

				//If directive tag defines them and the values are defined somewhere in ancestor scope, change the above defaults
				if(attrs.homeUrl && scope.$parent[attrs.homeUrl]){
					scope.homeUrl = scope.$parent[attrs.homeUrl];
					scope.homeUrlText = scope.$parent[attrs.homeUrlText];
				}

				if(attrs.linkStack && scope.$parent[attrs.linkStack]){
					scope.linkStack = scope.$parent[attrs.linkStack];

					//By default select the last link in the link Stack
					linkStackLengthChanged(scope.linkStack);
				}

				scope.homeClicked = function(){
					scope.linkStack.length = 0;
				};

				/*=======================================================================
				 Check if linkStack is changed
				 =========================================================================*/
				scope.$watch('linkStack.length', function(newLength, oldLength){
					if(newLength){
						linkStackLengthChanged(scope.linkStack);
					}
				});

				function linkStackLengthChanged(linkStack){
					var linkStackLength = linkStack.length;
					if(linkStackLength > 0){
						scope.setSelected(linkStack[linkStackLength - 1]);
					}
				}
			}
		};

		/*=======================================================================
		 Usage

		 //In HTML
		 <div id="applicationsBreadCrumb" breadcrumb home-url="breadcrumbsHomeUrl" link-stack="breadcrumbsStack"></div>

		 //In JavaScript for the example HTML written above
		 var propObject = {
		 breadcrumbsHomeUrl:  "<url-pointing-to-home-page>",
		 breadcrumbsStack: [
		 {
		 text: "text for the link",
		 href: "#/actions"
		 },
		 {...},{...}
		 ]
		 };

		 //For dynamically updating links change data array.
		 //If updated using angular constructs breadcrumb will update directly
		 //else you may have to call $scope.$apply() to propagate changes
		 =========================================================================*/
	}]
);

timenomeUtil.filter(
	'TrimFilter', [function(){
		var MAX_LENGTH = 25;

		return function(input, maxLen){
			var filteredStr = input;

			if(angular.isUndefined(maxLen)){
				maxLen = MAX_LENGTH;
			}

			if(filteredStr.length > maxLen){
				filteredStr = filteredStr.substring(0, maxLen) + '...';
			}

			return filteredStr;
		};
	}]);

timenomeUtil.filter(
	'MillisecondsToStringFilter', [function(){

		function getTimeToFilteredString(count, unit){
			var retVal = '';

			if(count === 0){
				return '';
			}
			else if(count === 1){
				retVal = 1 + " " + unit;
			}
			else{
				retVal = count + " " + unit + 's';
			}

			return retVal + ", ";
		}

		return function(input){
			if(angular.isNumber(input)){
				//Find milliseconds
				var milliseconds = input % 1000;
				input = (input - milliseconds) / 1000;

				var seconds = input % 60;
				input = (input - seconds) / 60;

				var minutes = input % 60;
				input = (input - minutes) / 60;

				var hours = input % 24;
				input = (input - hours) / 24;

				var days = input % 30;
				input = (input - days) / 30;

				var months = input % 12;
				input = (input - months) / 12;

				var years = input;

				var retStr = getTimeToFilteredString(years, "Year");
				retStr = retStr + getTimeToFilteredString(months, "Month");
				retStr = retStr + getTimeToFilteredString(days, "Day");
				retStr = retStr + getTimeToFilteredString(hours, "Hour");
				retStr = retStr + getTimeToFilteredString(minutes, "Minute");
				retStr = retStr + getTimeToFilteredString(seconds, "Second");
				retStr = retStr + getTimeToFilteredString(milliseconds, "MilliSecond");

				return retStr.substring(0, retStr.length - 2);
			}
			else{
				return input;
			}
		};
	}]);

timenomeUtil.factory(
	'UtilService',
	[function(){
		return{
			scopeApply: safeApply,
			getEmptyDateObj: function(){
				return {
					getTime: function(){
						return 0;
					}
				};
			},
			traverseObject: function(dataObj, traversePatternArray){
				return traversePatternArray.reduce(traverseObject, dataObj);
			},
			getAttributeObject: function(attrValue, parentObj){
				var attrSplit = attrValue.split(".");

				var attrObj = parentObj[attrSplit[0]];

				if(attrSplit.length > 0){
					attrSplit.splice(0, 1);
					attrObj = this.traverseObject(attrObj, attrSplit);
				}
				return attrObj;

			},
			getMillisecondsTillMidnight: function(date){
				return Math.floor(date.getTime() / (MILLISECONDS_IN_DAY)) * MILLISECONDS_IN_DAY;
			}
		};

		function traverseObject(obj, index){
			if(angular.isUndefined(obj)){
				return;
			}

			return obj[index];
		}

		function safeApply(scope, fn){
			if(!scope.$$phase && !scope.$root.$$phase){
				if(angular.isDefined(fn)){
					scope.$apply(fn);
				}
				else{
					scope.$apply();
				}
			}
			else{
				if(angular.isDefined(fn)){
					fn();
				}
			}
		}
	}]
);