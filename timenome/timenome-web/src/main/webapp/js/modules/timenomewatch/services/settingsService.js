timeNomeWatch.factory(
	'SettingsService',
	['HttpService', function(HttpService){
		var userSettings = {};
		var systemSettings = {};

		//Default settings
		userSettings[USE_12_HOUR_FORMAT] = false;
		userSettings[GRAPH_DOWNLOAD_FORMAT] = 'png';

		//System settings
		systemSettings[CHART_TYPES] = [
			{name: "Line Chart", type: LINE_CHART},
			{name: "Column Chart", type: COLUMN_CHART},
			{name: "Stacked Column Chart", type: STACKED_COLUMN_CHART}
		];

		var userSettingsSyncedWithServer = false;

		HttpService.getUserPreferences(function(data){
				loadSetting(USE_12_HOUR_FORMAT, data[USE_12_HOUR_FORMAT]);
				loadSetting(GRAPH_DOWNLOAD_FORMAT, data[GRAPH_DOWNLOAD_FORMAT]);

				notifyCallbacksForUserSettings();
			}, function(er){
				//notify with default
				notifyCallbacksForUserSettings();
			}
		);

		function notifyCallbacksForUserSettings(){
			userSettingsSyncedWithServer = true;

			for(var settingName in userSettingCallbackList){
				if(userSettingCallbackList.hasOwnProperty(settingName)){
					for(var i=0;i<userSettingCallbackList[settingName].length;i++){
						userSettingCallbackList[settingName][i](userSettings[settingName]);
					}

					userSettingCallbackList[settingName].length = 0;
				}

			}
		}

		function loadSetting(settingName, settingValue){
			if(angular.isString(settingValue)){
				userSettings[settingName] = angular.fromJson(settingValue);
			}
		}

		var userSettingCallbackList = {};
		userSettingCallbackList[USE_12_HOUR_FORMAT] = [];
		userSettingCallbackList[GRAPH_DOWNLOAD_FORMAT] = [];

		return{
			getUserSetting: function(settingName, callback){
				if(userSettingsSyncedWithServer){
					if(angular.isFunction(callback)){
						callback(userSettings[settingName]);
					}
				}
				else{
					userSettingCallbackList[settingName].push(callback);
				}
			},

			getSystemSetting: function(settingName){
				return systemSettings[settingName];
			},

			setUserSettings: function(settingName, value){
				userSettings[settingName] = value;

				var userPrefToBeSaved = {};
				userPrefToBeSaved[KEY] = settingName;
				userPrefToBeSaved[VALUE] = angular.toJson(value);

				HttpService.setUserPreferences([userPrefToBeSaved], function(){
				}, function(er){
					console.log(er);
				});
			}
		};

	}]
);