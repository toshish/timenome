timeNomeWatch.controller(
	'ArchiveListCtrl',
	['$scope', '$routeParams', 'PageUrlService', 'DataFetchService','UtilService',
		function($scope, $routeParams, PageUrlService, DataFetchService, UtilService){
			$scope.allArchives = {};

			$scope.showConfirmDeleteArchiveModal = false;
			$scope.showNotificationModal = false;
			$scope.showCustomTimePeriodEditor=false;

			$scope.timeUnitMillisecondsMap = {
				'Seconds': MILLISECONDS_IN_SECOND/1000,
				'Minutes': MILLISECONDS_IN_MIN/1000,
				'Hours': MILLISECONDS_IN_HOUR/1000,
				'Days': MILLISECONDS_IN_DAY/1000,
				'Weeks': MILLISECONDS_IN_WEEK/1000,
				'Months (30 day)': MILLISECONDS_IN_DAY/1000 * 30,
				'Years (12 Months)': MILLISECONDS_IN_DAY/1000 * 30 * 12
			};

			$scope.search = {
				filterText: ''
			};

			$scope.toBeCreatedArchive = {
				'archiveName': '',
				'archiveKey': ''
			};

			$scope.storagePrecision = {};
			$scope.calculateStoragePrecision = function(storagePrecisionUnitCnt, storagePrecisionUnit){
				var storagePrecisionUnitValue = parseInt(storagePrecisionUnit);
				if(angular.isNumber(storagePrecisionUnitCnt)){
					$scope.toBeCreatedArchive.storagePrecision = storagePrecisionUnitCnt * storagePrecisionUnitValue;
				}else{
					$scope.toBeCreatedArchive.storagePrecision = '';
				}
			};

			function initStoragePrecision(){
				$scope.storagePrecision.unitCnt = 10;
				$scope.storagePrecision['unit'] = $scope.timeUnitMillisecondsMap['Hours'];

				$scope.calculateStoragePrecision($scope.storagePrecision.unitCnt,$scope.storagePrecision['unit']);
			}
			initStoragePrecision();

			$scope.aggregationFuncIndexToName = DataFetchService.getAggregationFuncIndexToName();

			$scope.$watch('toBeCreatedArchive.archiveName', function(){
				$scope.toBeCreatedArchive.archiveKey = DataFetchService.createArchiveKey($scope.selectedDataStoreName, $scope.toBeCreatedArchive.archiveName);
			});

			$scope.isNumber = function(number){
				return angular.isNumber(number);
			};

			$scope.onCreateArchiveClicked = function(){
				$scope.toBeCreatedArchive[ARCHIVE_NAME] = '';
				$scope.toBeCreatedArchive['aggregationFunction'] = '2'; //'2' is for AVERAGE
				$scope.toBeCreatedArchive['isCounterBased'] = false;
				$scope.toBeCreatedArchive['counterSize'] = 50;

				initStoragePrecision();

				$scope.showCreateArchiveModal = true;
			};

			$scope.onCreateArchiveCancelClicked = function(){
				$scope.showCreateArchiveModal = false;
			};

			$scope.onDeleteClicked = function(archiveKey){
				$scope.toBeDeletedEntityType = "Archive";
				$scope.toBeDeletedEntityId = $scope.allArchives[archiveKey][ARCHIVE_NAME];
				$scope.showConfirmDeleteArchiveModal = true;
			};

			$scope.onConfirmDeleteCancelClicked = function(){
				$scope.showConfirmDeleteArchiveModal = false;
			};

			$scope.onNotificationOkClicked = function(){
				$scope.showNotificationModal = false;
			};

			$scope.deleteEntity = function(archiveName){
				DataFetchService.deleteArchive($scope.selectedDataStoreName, archiveName, function(){
					$scope.showNotificationModal = true;
					$scope.notificationTitle = "Delete Successful";
					$scope.notificationMessage = "Successfully deleted Archive \"" + archiveName + "\".";
				}, function(exception){
					$scope.showNotificationModal = true;
					$scope.notificationTitle = "Delete Failed";
					$scope.notificationMessage = exception.exceptionMessage;
				});
			};

			$scope.createArchive = function(){
				var archive = angular.copy($scope.toBeCreatedArchive);
				archive.aggregationFunction = parseInt(archive.aggregationFunction);
				archive.storagePrecision = archive.storagePrecision * 1000;
				delete archive.archiveKey;

				DataFetchService.createArchive($scope.selectedDataStoreName,archive,
					function(){
						$scope.showNotificationModal = true;
						$scope.notificationTitle = "Create Successful";
						$scope.notificationMessage = "Successfully created Archive \"" + $scope.toBeCreatedArchive[ARCHIVE_NAME] + "\".";
					}, function(exception){
						$scope.showNotificationModal = true;
						$scope.notificationTitle = "Create Failed";
						$scope.notificationMessage = exception.exceptionMessage;
					});
			};


			$scope.$watch('search.filterText', filterArchives);

			function filterArchives(){
				var filteredArchives = DataFetchService.getAllArchivesTrie().search($scope.selectedDataStoreName + " " + $scope.search.filterText);

				var filteredArchiveKeys = filteredArchives.keySet();

				$scope.filteredArchiveKeys = [];
				for(var archiveKey in filteredArchiveKeys){
					if(filteredArchiveKeys.hasOwnProperty(archiveKey)){
						var archive = $scope.allArchives[archiveKey];
						if(angular.isDefined(archive)&& (archive[DATA_STORE_NAME]===$scope.selectedDataStoreName)){
							$scope.filteredArchiveKeys.push(archiveKey);
						}
					}
				}

				$scope.archiveCountSequence =  new Array(Math.ceil($scope.filteredArchiveKeys.length / 3));
			}

			DataFetchService.registerGetArchivesCallback('ArchiveListCtrl',function(archives){
				$scope.allArchives = archives;
				filterArchives();
				UtilService.scopeApply($scope);
			});

			$scope.$on('$destroy', function(){
				DataFetchService.unRegisterGetArchivesCallback('ArchiveListCtrl');
			});
		}]
);
