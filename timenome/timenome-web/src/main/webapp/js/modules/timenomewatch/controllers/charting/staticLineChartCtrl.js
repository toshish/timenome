/**
 * User: anirudh
 * Date: 26/11/13
 */

timeNomeWatch.controller(
	'StaticLineChartCtrl',
	['$scope', '$filter', 'DataFetchService', 'UtilService', 'HttpService', 'SettingsService',
		function($scope, $filter, DataFetchService, UtilService, HttpService, SettingsService){
			$scope.chartData = {
				updateCnt: 0,
				dataArr: [],
				title: '',
				selectedArchives: [],
				updated: function(){
					this.updateCnt++;
				}
			};

			var use12HrFormat = true;

			SettingsService.getUserSetting(USE_12_HOUR_FORMAT, function(userWants12HrFormat){
				use12HrFormat = userWants12HrFormat;
			});

			function reCalculateStartAndEndTimestampStrings(){
				var dateFilterFormat = 'dd MMM yyyy ';
				if(use12HrFormat){
					dateFilterFormat += 'hh:mm a';
				}else{
					dateFilterFormat += 'HH:mm';
				}

				$scope.graphStartTimestamp =  $filter('date')(new Date($scope.viewConfig.updatePolicy.startTimestamp), dateFilterFormat);
				$scope.graphEndTimestamp =  $filter('date')(new Date($scope.viewConfig.updatePolicy.endTimestamp), dateFilterFormat);
			}

			$scope.allArchives = {};

			//To make sure static chart isn't created twice the first time UI loads
			var renderChart = function(){
				renderChart = viewConfigUpdated;
				$scope.$watch("viewConfig.isUpdated", function(newVal, oldVal){
					if(angular.isDefined(newVal)){
						viewConfigUpdated();
					}
				});
			};

			function viewConfigUpdated(){
				$scope.chartData.selectedArchives = [];
				$scope.chartData.title = $scope.viewConfig.title;
				$scope.chartData.selectedArchives = [];

				var getDataRequest = {
					dataQualifiers: [],
					startTimestamp: $scope.viewConfig.updatePolicy.startTimestamp,
					endTimestamp: $scope.viewConfig.updatePolicy.endTimestamp
				};

				for(var i = 0; i < $scope.viewConfig.selectedArchives.length; i++){
					if(angular.isDefined($scope.allArchives[$scope.viewConfig.selectedArchives[i]])){
						$scope.chartData.selectedArchives.push($scope.viewConfig.selectedArchives[i]);
						getDataRequest.dataQualifiers.push({archiveKey: $scope.viewConfig.selectedArchives[i]});
					}
				}

				DataFetchService.getAmChartData(getDataRequest, function(data){
					if(data.success === true){
						var amChartData = data.amChartData;

						for(var i = 0; i < amChartData.length; i++){
							amChartData[i].timestamp = new Date(amChartData[i].timestamp);
						}

						$scope.chartData.dataArr = amChartData;
						$scope.chartData.updated();

						reCalculateStartAndEndTimestampStrings();
						UtilService.scopeApply($scope);

					}
				}, function(error){
					console.log(error);
					$scope.chartData.dataArr = [];
					$scope.chartData.updated();
					reCalculateStartAndEndTimestampStrings();
					UtilService.scopeApply($scope);
				});

				if($scope.chartData.selectedArchives.length === 0){
					$scope.chartData.updated();
				}
			}

			DataFetchService.registerGetArchivesCallback('StaticLineChartCtrl' + $scope.$id, function(archives){
				$scope.allArchives = archives;
				renderChart();
				UtilService.scopeApply($scope);
			});

			$scope.$on('$destroy', function(){
				DataFetchService.unRegisterGetArchivesCallback('StaticLineChartCtrl' + $scope.$id);
			});

			$scope.chartData.updated();
		}]
);
