timeNomeWatch.controller(
	'PortletViewConfigCtrl',
	['$scope', '$route' , '$filter', 'LoggerService', 'SettingsService', 'PageUrlService', 'PortletConfigService', 'PortletViewConfigPageInvocationService', 'DataFetchService', 'UtilService',
		function($scope, $route, $filter, LoggerService, SettingsService, PageUrlService, PortletConfigService, PortletViewConfigPageInvocationService, DataFetchService, UtilService){

			$scope.fromTimestamp = {};
			$scope.toTimestamp = {};
			$scope.isChartStatic = true;

			$scope.refreshArchiveSearchBox = 0;
			$scope.hideArchiveSearchDropdown = 0;

			$scope.dateTimeRange = {
				showDateTimeRangeSelector: false
			};

			function getChartTemplateName(updatePolicy, chartType){
				if(updatePolicy===STATIC_CHART){
					switch(chartType){
						case "lineChart": return STATIC_LINE_CHART;
						case "columnChart": return STATIC_COLUMN_CHART;
						case "stackedColumnChart": return STATIC_STACKED_COLUMN_CHART;
						default: return STATIC_LINE_CHART;
					}
				}else{
					switch(chartType){
						case "lineChart": return DYNAMIC_LINE_CHART;
						case "columnChart": return DYNAMIC_COLUMN_CHART;
						case "stackedColumnChart": return DYNAMIC_STACKED_COLUMN_CHART;
						default: return DYNAMIC_LINE_CHART;
					}
				}
			}

			$scope.dateTimeRangeBtnClicked = function($event){
				$event.stopPropagation();
				$scope.dateTimeRange.showDateTimeRangeSelector = !$scope.dateTimeRange.showDateTimeRangeSelector;
				$scope.hideArchiveSearchDropdown++;
			};

			$scope.archiveSearchBoxClicked = function($event){
				$scope.dateTimeRange.showDateTimeRangeSelector = false;
			};

			$scope.onStaticChartSelected = function(){
				$scope.viewConfig.updatePolicy.chartUpdateType = STATIC_CHART;
			};

			$scope.onDynamicChartSelected = function(){
				$scope.viewConfig.updatePolicy.chartUpdateType = DYNAMIC_CHART;
			};

			$scope.onOkClicked = function(){
				angular.copy($scope.viewConfig, $scope.origViewConfig);
				$scope.origViewConfig.templateUrl = PageUrlService.getPageUrlDetails(getChartTemplateName($scope.viewConfig.updatePolicy.chartUpdateType, $scope.viewConfig.chartType)).templateUrl;
				$scope.origViewConfig.updatePolicy[START_TIMESTAMP] = angular.copy($scope.fromTimestamp.timestamp);
				$scope.origViewConfig.updatePolicy[END_TIMESTAMP] = angular.copy($scope.toTimestamp.timestamp);
				$scope.origViewConfig.update();
			};

			$scope.chartTypes = SettingsService.getSystemSetting(CHART_TYPES);

			$scope.getDateTimeString = function(timestampModel){
				if(angular.isUndefined(timestampModel.date) || timestampModel.date.getTime() === 0){
					return "---";
				}
				else{
					return $filter('date')(new Date(timestampModel.timestamp), 'dd MMM yyyy') + ' ' + timestampModel.timeString;
				}
			};

			PortletViewConfigPageInvocationService.registerCallbackForInvocation(function(viewConfig){
				$scope.viewConfig = angular.copy(viewConfig);
				$scope.selectedArchives = $scope.viewConfig.selectedArchives;

				if($scope.viewConfig.templateUrl === PageUrlService.getPageUrlDetails(CHART_LOAD_PAGE).templateUrl){
					if($scope.viewConfig.chartType === LINE_CHART){
						$scope.viewConfig.templateUrl = PageUrlService.getPageUrlDetails(STATIC_LINE_CHART).templateUrl;
					}
				}else if($scope.viewConfig.templateUrl===PageUrlService.getPageUrlDetails(CHART_LOAD_PAGE_WITH_ARCHIVE_DELETED_MSG).templateUrl){
					$scope.viewConfig.templateUrl = PageUrlService.getPageUrlDetails(getChartTemplateName($scope.viewConfig.updatePolicy.chartUpdateType, $scope.viewConfig.chartType)).templateUrl;
				}

				if($scope.viewConfig.updatePolicy.chartUpdateType === STATIC_CHART){
					$scope.fromTimestamp.loadTimestamp(new Date($scope.viewConfig.updatePolicy.startTimestamp));
					$scope.toTimestamp.loadTimestamp(new Date($scope.viewConfig.updatePolicy.endTimestamp));
				}

				$scope.origViewConfig = viewConfig;

				$scope.refreshArchiveSearchBox++;

				//Sorry guys for using DOM Access code in controller
				angular.element('#portletViewConfig').modal({
					'keyboard': false,
					'show': true,
					'backdrop': 'static'
				});
			});
		}
	]
);
