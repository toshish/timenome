timeNomeWatch.directive('blur',
	[function(){

		return {
			restrict: "A", //The directive can only be used as an attribute
			link: function(scope, element, attrs){
				scope.$watch(attrs.blur, function(newValue){
					if(newValue === true){
						element.blur();
					}
				});
			}
		};
	}
	]);