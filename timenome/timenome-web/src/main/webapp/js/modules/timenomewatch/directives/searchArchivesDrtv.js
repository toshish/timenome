timeNomeWatch.directive('searchArchives',
	['DataFetchService', 'UtilService', function(DataFetchService, UtilService){

		var ARCHIVE_PAGE_SIZE = 10;
		var ARCHIVE_LOAD_MORE_SIZE = 10;

		return {
			restrict: "A",
			scope: {},
			templateUrl: "js/modules/timenomewatch/templates/views/util/searchArchives.html",
			link: function(scope, element, attrs){

				scope.$watch("$parent." + attrs.hideArchiveSearchDropdownOn, function(){
					scope.search.showArchiveSelectDropdown = false;
				});

				scope.$watch("$parent." + attrs.listenForRefreshOn, function(){
					scope.search.showArchiveSelectDropdown = false;
					scope.search.filterText = '';
					scope.search.blur = true;

					scope.selectedArchives = scope.$parent.selectedArchives;

					scope.selectedArchivesSet = {};
					if(angular.isArray(scope.selectedArchives)){
						for(var i = 0; i < scope.selectedArchives.length; i++){
							scope.selectedArchivesSet[scope.selectedArchives[i]] = scope.selectedArchives[i];
						}
					}
				});

			},
			controller: function($scope, $element, $attrs){
				$scope.search = {
					showArchiveSelectDropdown: false,
					filterText: '',
					blur: true
				};

				$scope.searchBoxClicked = function($event){
					$event.stopPropagation();
					$scope.search.showArchiveSelectDropdown = !$scope.search.showArchiveSelectDropdown;
					if($scope.search.showArchiveSelectDropdown === false){
						$scope.search.blur = true;
					}
					else{
						$scope.search.blur = false;
					}

					if(angular.isFunction($scope.$parent[$attrs.onSearchBoxClicked])){
						$scope.$parent[$attrs.onSearchBoxClicked]();
					}
				};

				$scope.removeArchiveFromSelectedList = function($event, index){
					$event.stopPropagation();

					delete $scope.selectedArchivesSet[$scope.selectedArchives[index]];

					$scope.selectedArchives.splice(index, 1);
				};

				$scope.addArchive = function($event, archiveKey){
					$event.stopPropagation();
					if(angular.isUndefined($scope.selectedArchivesSet[archiveKey])){
						$scope.selectedArchives.push(archiveKey);
						$scope.selectedArchivesSet[archiveKey] = archiveKey;
					}
				};

				$scope.loadMoreArchives = function(){
					if($scope.archiveCountSequence.length < $scope.filteredArchiveKeys.length){
						var remainingElements = $scope.filteredArchiveKeys.length - $scope.archiveCountSequence.length;
						var newLength = $scope.archiveCountSequence.length + ((ARCHIVE_LOAD_MORE_SIZE < remainingElements) ? ARCHIVE_LOAD_MORE_SIZE:remainingElements);
						$scope.archiveCountSequence = new Array(newLength);
					}
				};

				$scope.$watch('search.filterText', filterArchives);

				function filterArchives(){
					var filteredArchives = DataFetchService.getAllArchivesTrie().search($scope.search.filterText);

					$scope.filteredArchiveKeys = filteredArchives.values();
					$scope.archiveCountSequence = new Array(($scope.filteredArchiveKeys.length > ARCHIVE_PAGE_SIZE) ? ARCHIVE_PAGE_SIZE:$scope.filteredArchiveKeys.length);
				}

				DataFetchService.registerGetArchivesCallback('SearchArchivesDrtvCtrl' + $scope.$id, function(archives){
					$scope.allArchives = archives;
					filterArchives();
					UtilService.scopeApply($scope);
				});

				$scope.$on('$destroy', function(){
					DataFetchService.unRegisterGetArchivesCallback('SearchArchivesDrtvCtrl' + $scope.$id);
				});
			}
		};
	}
	]);
