timeNomeWatch.directive(
	'fullSizePortletPage',
	['$routeParams','$route', '$window',
		function($routeParams,$route, $window){

		return {
			restrict: "A", //The directive can only be used as an attribute
			link: function(scope, element, attrs){

				scope.viewportHeight = getViewportHeight();

				angular.element($window).bind('resize',function() {
					scope.viewportHeight = getViewportHeight();
					scope.$apply();
				});
			},
			controller: function($scope, $element, $attrs, $routeParams, $route){
				if(angular.isDefined($routeParams.portletId)  && angular.isDefined($route.current.onLoadCallback)){
					$scope[$route.current.onLoadCallback]($routeParams.portletId);
				}

				$scope.viewConfig = $scope.portletViewConfig;
			}
		};

		//Viewport height is window height minus navigation bar height
		function getViewportHeight(){
			return (angular.element($window).height() - angular.element("#navigationBar").outerHeight()) + "px";
		}
	}]
);