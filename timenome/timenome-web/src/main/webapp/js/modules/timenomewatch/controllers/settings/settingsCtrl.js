/**
 * User: anirudh
 * Date: 26/11/13
 */

timeNomeWatch.controller(
	'SettingsCtrl',
	['$scope', 'HttpService','LoggerService', 'SettingsService',
		function($scope, HttpService, LoggerService, SettingsService){

            HttpService.getApiCredentials(function(apiCredentials){
                $scope.apiCredentials = apiCredentials;
            }, function(){
                LoggerService.error("Failed to fetch API credentials");
            });

			SettingsService.getUserSetting(GRAPH_DOWNLOAD_FORMAT, function(format){
				$scope[GRAPH_DOWNLOAD_FORMAT] = format;
			});

			SettingsService.getUserSetting(USE_12_HOUR_FORMAT, function(format){
				$scope[USE_12_HOUR_FORMAT] = format;
				if($scope[USE_12_HOUR_FORMAT]){
					$scope.timeFormat = '12hr';
				}else{
					$scope.timeFormat = '24hr';
				}
			});

			$scope.onTimeFormatChanged = function(){
				$scope[USE_12_HOUR_FORMAT] = $scope.timeFormat==='12hr'? true: false;
				$scope.onSettingChanged(USE_12_HOUR_FORMAT);
			};

			$scope.onSettingChanged = function(settingName){
				SettingsService.setUserSettings(settingName, $scope[settingName]);
			};

			$scope.refreshPage = function(){
				window.location.reload();
			};
		}]
);
