var PORTLET_ID = "portletId";
var PAGE_NAME = "pageName";
var ON_PORTLET_LOADED = "onPortletLoaded";
var LOADING_PAGE = "loadingPage";
var CHART_LOAD_PAGE = "chartLoadPage";
var CHART_LOAD_PAGE_WITH_ARCHIVE_DELETED_MSG = "chartLoadPageWithArchiveDeletedMessage";

var KEY = "key";
var VALUE = "value";

var DATA_STORE = "dataStore";

var MONITOR_PAGE = "monitor";
var MANAGE_PAGE = "manage";
var DATA_STORE_LIST_PAGE = "dataStoreList";
var ARCHIVE_LIST_PAGE = "archiveList";
var RULES_PAGE = "rules";
var ALERT_DETAILS_PAGE = "alertDetails";
var SETTINGS_PAGE = "settings";
var FULL_SIZE_PORTLET_PAGE = "fullSizePortlet";

var UPDATE_POLICY = "updatePolicy";
var CHART_UPDATE_TYPE = "chartUpdateTypes";
var STATIC_CHART = "staticChart";
var STATIC_LINE_CHART = "staticLineChart";
var STATIC_COLUMN_CHART = "staticColumnChart";
var STATIC_STACKED_COLUMN_CHART = "staticStackedColumnChart";

var DYNAMIC_CHART = "dynamicChart";
var DYNAMIC_LINE_CHART = "dynamicLineChart";
var DYNAMIC_COLUMN_CHART = "dynamicColumnChart";
var DYNAMIC_STACKED_COLUMN_CHART = "dynamicColumnChart";

var CHART_TYPES = "chartTypes";
var CHART_TYPE = "chartType";
var LINE_CHART = "lineChart";
var COLUMN_CHART = "columnChart";
var STACKED_COLUMN_CHART = "stackedColumnChart";
var TREE_MAP = "treeMap";

var USE_12_HOUR_FORMAT = "use12HourFormat";
var USE_PRESETS = "usePresets";
var TIME_RANGE_PRESETS = "timeRangePresets";
var GRAPH_DOWNLOAD_FORMAT = "graphDownloadFormat";

var ARCHIVE_NAME = "archiveName";
var DATA_STORE_NAME = "dataStoreName";
var TIME_RANGE = "timeRange";

var RULE_NAME = "ruleName";

var ATTRIBUTES = "attributes";

var START_TIMESTAMP = "startTimestamp";
var END_TIMESTAMP = "endTimestamp";

//Time Constants
var MILLISECONDS_IN_SECOND = 1000;
var MILLISECONDS_IN_MIN = 60 * MILLISECONDS_IN_SECOND;
var MILLISECONDS_IN_HOUR = 60 * MILLISECONDS_IN_MIN;
var MILLISECONDS_IN_DAY = 24 * MILLISECONDS_IN_HOUR;
var MILLISECONDS_IN_WEEK = 7 * MILLISECONDS_IN_DAY;

var NVD3_CHART_UPDATE_TRANSITION_DURATION = 300;