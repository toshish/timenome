timeNomeWatch.directive(
	'navigationBar',
	['$location', function($location){

		return {
			restrict: "A", //The directive can only be used as an attribute
			link: function(scope, element, attrs){
				var anchorTag = element.find(".nav [header-link] a");
				var navBarCollapseBtn = element.find("#navbarCollapseBtn");

				anchorTag.on('click', function(){
					if(navBarCollapseBtn.is(':visible') === true){
						navBarCollapseBtn.click();
					}
				});
			}
		};
	}]
);