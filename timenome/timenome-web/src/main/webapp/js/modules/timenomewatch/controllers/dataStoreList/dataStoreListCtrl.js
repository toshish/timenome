timeNomeWatch.controller(
	'DataStoreListCtrl',
	['$scope', 'PageUrlService', 'DataFetchService', 'UtilService',
		function($scope, PageUrlService, DataFetchService, UtilService){

			$scope.showConfirmDeleteDsModal = false;
			$scope.showCreateDataStoreModal = false;
			$scope.showNotificationModal = false;

			$scope.allDataStores = {};

			$scope.search = {
				filterText: ''
			};

			$scope.showDataStoreList = function(){
				$scope.manageScreenName = DATA_STORE_LIST_PAGE;
				$scope.selectedDataStoreName = "";
			};

			$scope.showDataStoreList();

			$scope.toBeCreatedDataStore = {};

			$scope.$watch('search.filterText', filterDataStores);

			$scope.showArchiveList = function(dataStoreName){
				$scope.manageScreenName = ARCHIVE_LIST_PAGE;
				$scope.selectedDataStoreName = dataStoreName;
			};

			$scope.onDeleteClicked = function(index){
				$scope.toBeDeletedEntityType = "Data Store";
				$scope.toBeDeletedEntityId = $scope.filteredDataStoreNames[index];
				$scope.showConfirmDeleteDsModal = true;
			};

			$scope.onCreateDataStoreClicked = function(){
				$scope.toBeCreatedDataStore[DATA_STORE_NAME] = '';
				$scope.toBeCreatedDataStore[ATTRIBUTES] = [];

				$scope.showCreateDataStoreModal = true;
			};

			$scope.onNotificationOkClicked = function(){
				$scope.showNotificationModal = false;
			};

			$scope.onConfirmDeleteCancelClicked = function(){
				$scope.showConfirmDeleteDsModal = false;
			};

			$scope.onCreateDataStoreCancelClicked = function(){
				$scope.showCreateDataStoreModal = false;
			};

			$scope.createDataStore = function(){
				var dataStoreName = $scope.toBeCreatedDataStore[DATA_STORE_NAME];
				var attributes = $scope.toBeCreatedDataStore[ATTRIBUTES];
				for(var i=0; i<attributes.length; i++){
					delete attributes[i]['$$hashKey'];
				}
				DataFetchService.createDataStore(dataStoreName,attributes,
					function(){
						$scope.showNotificationModal = true;
						$scope.notificationTitle = "Create Successful";
						$scope.notificationMessage = "Successfully created Data Store \"" + dataStoreName + "\".";
					}, function(exception){
						$scope.showNotificationModal = true;
						$scope.notificationTitle = "Create Failed";
						$scope.notificationMessage = exception.exceptionMessage;
					});
			};

			$scope.deleteEntity = function(dataStoreName){
				DataFetchService.deleteDataStore(dataStoreName, function(){
					$scope.showNotificationModal = true;
					$scope.notificationTitle = "Delete Successful";
					$scope.notificationMessage = "Successfully deleted Data Store \"" + dataStoreName + "\".";
				}, function(exception){
					$scope.showNotificationModal = true;
					$scope.notificationTitle = "Delete Failed";
					$scope.notificationMessage = exception.exceptionMessage;
				});
			};

			$scope.createDataStoreDetailsPageUrl = function(index){
				var dataStoreName = $scope.filteredDataStoreNames[index];

				if(angular.isDefined(dataStoreName)){
					return PageUrlService.createDataStoreDetailsPageLink(dataStoreName);
				}

				return "#/";
			};

			function filterDataStores(){
				var filteredDataStores = DataFetchService.getAllDataStoresTrie().search($scope.search.filterText);

				$scope.filteredDataStoreNames = filteredDataStores.values();
				$scope.dataStoreCountSequence = new Array(Math.ceil($scope.filteredDataStoreNames.length / 3));// > ARCHIVE_PAGE_SIZE) ? ARCHIVE_PAGE_SIZE:$scope.filteredDataStoreNames.length);
			}

			DataFetchService.registerGetDataStoresCallback('DataStoreListCtrl', function(dataStores){
				$scope.allDataStores = dataStores;
				filterDataStores();
				UtilService.scopeApply($scope);
			});

			$scope.$on('$destroy', function(){
				DataFetchService.unRegisterGetDataStoresCallback('DataStoreListCtrl');
			});
		}]
);
