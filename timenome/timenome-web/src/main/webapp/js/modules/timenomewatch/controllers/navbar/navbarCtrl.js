timeNomeWatch.controller(
	'NavbarCtrl',
	['$scope','HttpService',
		function($scope, HttpService){

		HttpService.getUserinfo(function(username){
			$scope.loggedInUsername = username;
		});

		$scope.logoutClicked = function(){
			HttpService.logout(function(){
				window.location.href = 'login.html';
			});
		}
	}]
);
