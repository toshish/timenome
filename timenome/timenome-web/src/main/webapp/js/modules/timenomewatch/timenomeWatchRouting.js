timeNomeWatch.config(['$routeProvider', 'PageUrlServiceProvider',
	function($routeProvider, PageUrlServiceProvider){

		var monitorRoute = {
			templateUrl: PageUrlServiceProvider.getPageUrlDetails(MONITOR_PAGE).templateUrl,
			controller: 'MonitorCtrl'
		};
		monitorRoute[PAGE_NAME] = MONITOR_PAGE;

		var manageRoute = {
			templateUrl: PageUrlServiceProvider.getPageUrlDetails(MANAGE_PAGE).templateUrl,
			controller: 'DataStoreListCtrl'
		};
		manageRoute[PAGE_NAME] = MANAGE_PAGE;

		var rulesRoute = {
			templateUrl: PageUrlServiceProvider.getPageUrlDetails(RULES_PAGE).templateUrl,
			controller: 'RulesCtrl'
		};
		rulesRoute[PAGE_NAME] = RULES_PAGE;

		var settingsRoute = {
			templateUrl: PageUrlServiceProvider.getPageUrlDetails(SETTINGS_PAGE).templateUrl,
			controller: 'SettingsCtrl'
		};
		settingsRoute[PAGE_NAME] = SETTINGS_PAGE;

		var alertDetailsRoute = {
			templateUrl: PageUrlServiceProvider.getPageUrlDetails(ALERT_DETAILS_PAGE).templateUrl,
			controller: 'AlertDetailsCtrl'
		};
		alertDetailsRoute[PAGE_NAME] = ALERT_DETAILS_PAGE;

		//Route Config
		$routeProvider
			.when(PageUrlServiceProvider.getPageUrlDetails(MONITOR_PAGE).routePath, monitorRoute)

			.when(PageUrlServiceProvider.getPageUrlDetails(MANAGE_PAGE).routePath, manageRoute)

			.when(PageUrlServiceProvider.getPageUrlDetails(RULES_PAGE).routePath, rulesRoute)

			.when(PageUrlServiceProvider.getPageUrlDetails(SETTINGS_PAGE).routePath, settingsRoute)

			.when(PageUrlServiceProvider.getPageUrlDetails(ALERT_DETAILS_PAGE).routePath, alertDetailsRoute)

			.when(PageUrlServiceProvider.getPageUrlDetails(FULL_SIZE_PORTLET_PAGE).routePath, {
				templateUrl: PageUrlServiceProvider.getPageUrlDetails(FULL_SIZE_PORTLET_PAGE).templateUrl,
				controller: 'PortletViewCtrl',
				onLoadCallback: "onPortletLoaded"
			})

			.otherwise({
				redirectTo: PageUrlServiceProvider.getPageUrlDetails(MONITOR_PAGE).routePath
			});
	}
]);