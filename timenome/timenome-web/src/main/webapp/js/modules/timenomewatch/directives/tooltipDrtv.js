timeNomeWatch.directive(
	'tooltip',
	[function(){

			return {
				restrict: "A", //The directive can only be used as an attribute
				link: function(scope, element, attrs){
					element.attr('title', attrs['tooltip']);
					element.tooltip();
				}
			};
		}]
);