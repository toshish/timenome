timeNomeWatch.controller(
	'PortletViewCtrl',
	['$scope', '$route' , 'LoggerService', 'PageUrlService', 'PortletViewConfigPageInvocationService', 'PortletConfigService','DataFetchService','UtilService',
		function($scope, $route, LoggerService, PageUrlService, PortletViewConfigPageInvocationService, PortletConfigService, DataFetchService, UtilService){

			$scope.portletViewConfig = {
				templateUrl: PageUrlService.getTemplateUrl(LOADING_PAGE)
			};

			$scope[ON_PORTLET_LOADED] = function(portletId){

				DataFetchService.registerGetArchivesCallback('PortletViewCtrl' + $scope.$id, function(archives){
					$scope.allArchives = archives;

					PortletConfigService.registerOnPortletConfigLoaded(portletId, function(portletConfig){

						var availableArchives = [];
						for(var i = 0; i < portletConfig.selectedArchives.length; i++){
							if(angular.isDefined($scope.allArchives[portletConfig.selectedArchives[i]])){
								availableArchives.push(portletConfig.selectedArchives[i]);
							}
						}

						if(availableArchives.length !== portletConfig.selectedArchives.length){

							portletConfig.selectedArchives = availableArchives;

							if(availableArchives.length===0){
								portletConfig.templateUrl = PageUrlService.getTemplateUrl(CHART_LOAD_PAGE_WITH_ARCHIVE_DELETED_MSG);
							}

							portletConfig.update();
						}

						$scope.portletViewConfig = portletConfig;
                        UtilService.scopeApply($scope);
					});
				});
			};

			$scope.configureView = function(chartType){
				if(angular.isDefined(chartType) && angular.isString(chartType)){
					$scope.portletViewConfig[CHART_TYPE] = chartType;
				}

				PortletViewConfigPageInvocationService.openPortletViewConfigPage($scope.portletViewConfig);
			};

			$scope.$on('$destroy', function(){
				DataFetchService.unRegisterGetArchivesCallback('PortletViewCtrl' + $scope.$id);
			});
		}
	]
);
