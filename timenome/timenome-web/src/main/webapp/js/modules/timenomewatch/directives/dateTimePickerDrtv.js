timeNomeWatch.directive(
	'dateTimePicker',
	['SettingsService', 'UtilService',
		function(SettingsService, UtilService){

			var use12HrFormat = false;
			var linkFunctionParamsQueue = [];
			var use12HrFormatSyncedWithServer = false;

			var dateTimePickerMap = {};
			var idCnt = 0;

			function generateNewId(){
				return "datePicker" + idCnt++;
			}

			function getTimeInMilliseconds(timeStr){
				var splitHour_MinMeridian = timeStr.split(":");
				var hours = splitHour_MinMeridian[0];

				var splitMin_Meridian = splitHour_MinMeridian[1].split(' ');
				var mins = splitMin_Meridian[0];
				var meridian = splitMin_Meridian[1];

				if(meridian === "AM" && hours == 12){
					hours = 0;
				}

				return hours * MILLISECONDS_IN_HOUR + mins * MILLISECONDS_IN_MIN + (("PM" === meridian && hours != 12) ? 12 * 60:0) * MILLISECONDS_IN_MIN;
			}

			function createTimeString(timeInMillisec){
				var meridian = '';
				var hours = Math.floor(timeInMillisec / MILLISECONDS_IN_HOUR);
				var minutes = Math.floor((timeInMillisec - hours * MILLISECONDS_IN_HOUR) / MILLISECONDS_IN_MIN);
				if(use12HrFormat){
					if(hours >= 12){
						meridian = "PM";
						hours = hours > 12 ? hours - 12:hours;
					}
					else{
						meridian = "AM";
					}
				}

				return hours + ':' + minutes + ' ' + meridian;
			}

			function initDatePicker(scope, dateTimePickerObj, curDate){
				//Initialize datepicker
				dateTimePickerObj.datePicker.datepicker({ autoclose: true, todayHighlight: true, format: "dd-M-yyyy" });

				//Date Change Listener
				dateTimePickerObj.datePicker.on('changeDate', function(newDateEvent){
					var datePicker = dateTimePickerObj.datePicker;
					var dependentDateTimePicker = dateTimePickerObj.dependentDateTimePicker;

					if(angular.isDefined(dependentDateTimePicker)){
						var dependentDatePicker = dependentDateTimePicker.datePicker;

						if(angular.isUndefined(newDateEvent.date) || newDateEvent.date > dependentDatePicker.datepicker('getDate')){
							dependentDatePicker.datepicker('setDate', null);
						}

						dependentDatePicker.datepicker('setStartDate', newDateEvent.date);
					}

					scope.timestampModel.date = angular.isDefined(newDateEvent.date) ? newDateEvent.date:UtilService.getEmptyDateObj();
					scope.timestampModel.timestamp = scope.timestampModel.date.getTime() + (angular.isDefined(scope.timestampModel.time) ? scope.timestampModel.time:0);

					UtilService.scopeApply(scope);
				});

				//Set initial date
				var initialDate = angular.isUndefined(scope.timestampModel.date) ? curDate:scope.timestampModel.date;
				setDate(scope, dateTimePickerObj.datePicker, initialDate);
			}

			function setDate(scope, datePicker, date){
				scope.timestampModel.date = date;
				datePicker.datepicker('setDate', scope.timestampModel.date);
				datePicker.datepicker('update');
			}

			function initTimePicker(scope, dateTimePickerObj, curDate){
				//Initialize timepicker
				dateTimePickerObj.timePicker.timepicker({minuteStep: 1, showMeridian: use12HrFormat});

				//TimeChange Listener
				dateTimePickerObj.timePicker.on('changeTime.timepicker', function(newTimeEvent){
					scope.timestampModel.timeString = newTimeEvent.time.value;
					scope.timestampModel.time = getTimeInMilliseconds(scope.timestampModel.timeString);
					scope.timestampModel.timestamp = scope.timestampModel.date.getTime() + scope.timestampModel.time;
					UtilService.scopeApply(scope);
				});

				//Set initial time
				var initialTime = angular.isUndefined(scope.timestampModel.time) ? (curDate.getHours() * MILLISECONDS_IN_HOUR + curDate.getMinutes() * MILLISECONDS_IN_MIN):scope.timestampModel.time;
				setTime(scope, dateTimePickerObj.timePicker, initialTime);
			}

			function setTime(scope, timePicker, time){
				scope.timestampModel.time = time;
				scope.timestampModel.timeString = createTimeString(scope.timestampModel.time);
				timePicker.timepicker('setTime', scope.timestampModel.timeString);
			}

			function validateTimeRange(dependent, dependsOn){
				//DependsOn Timestamp should always be less than dependent
				//Since datepicker has built in validation, this situation can occur only with timepicker

				var dependsOnDate = dependsOn.datePicker.datepicker('getDate');
				var dependentDate = dependent.datePicker.datepicker('getDate');

				if(angular.isDefined(dependentDate) && angular.isDefined(dependsOnDate)){
					if(angular.equals(dependentDate, dependsOnDate)){ //In case of unequal dependent is always bigger which we need
						var dependsOnTime = dependsOn.timePicker.data("timepicker").getTime();
						var dependentTime = dependent.timePicker.data("timepicker").getTime();

						var dependsOnTimeInMs = getTimeInMilliseconds(dependsOnTime);
						var dependentTimeInMs = getTimeInMilliseconds(dependentTime);

						if(dependentTimeInMs < dependsOnTimeInMs){
							dependent.timePicker.timepicker('setTime', dependsOnTime);
						}
					}
				}
			}

			function linkFunction(scope, element, attrs){
				scope.timestampModel = scope.$parent[attrs.ngModel];

				var id = attrs.id;
				if(angular.isUndefined(id)){
					id = generateNewId();
				}

				if(angular.isDefined(attrs.label)){
					element.find("label").text(attrs.label);
				}
				else{
					element.find("label").remove();
				}

				dateTimePickerMap[id] = {
					datePicker: element.find("[datepicker]"),
					timePicker: element.find("[timepicker] > input")
				};

				//For date range
				var dependsOn = attrs.dependsOn;
				var isDependentDateTimePicker = angular.isDefined(dependsOn) && angular.isString(dependsOn) && dependsOn.length > 0 && angular.isDefined(dateTimePickerMap[dependsOn]);
				if(isDependentDateTimePicker){
					dependsOn = dateTimePickerMap[dependsOn];

					dependsOn.dependentDateTimePicker = dateTimePickerMap[id];
					dateTimePickerMap[id].dependsOnDateTimePicker = dependsOn;
				}

				var curDate = new Date();

				initDatePicker(scope, dateTimePickerMap[id], curDate);
				initTimePicker(scope, dateTimePickerMap[id], curDate);

				if(isDependentDateTimePicker){
					dateTimePickerMap[id].datePicker.datepicker('setStartDate', dateTimePickerMap[id].dependsOnDateTimePicker.datePicker.datepicker('getDate'));
				}

				//Keep a watch on timestamp
				scope.$watch('timestampModel.timestamp', function(){
					if(angular.isDefined(dateTimePickerMap[id].dependentDateTimePicker)){
						var dependent = dateTimePickerMap[id].dependentDateTimePicker;
						var dependsOn = dateTimePickerMap[id];

						validateTimeRange(dependent, dependsOn);

					}
					else if(angular.isDefined(dateTimePickerMap[id].dependsOnDateTimePicker)){
						var dependent = dateTimePickerMap[id];
						var dependsOn = dateTimePickerMap[id].dependsOnDateTimePicker;

						validateTimeRange(dependent, dependsOn);
					}//else don't care
				});

				scope.timestampModel.loadTimestamp = function(date){
					setDate(scope, dateTimePickerMap[id].datePicker, date);
					setTime(scope, dateTimePickerMap[id].timePicker, date.getHours() * MILLISECONDS_IN_HOUR + date.getMinutes() * MILLISECONDS_IN_MIN);
				};
			}

			SettingsService.getUserSetting(USE_12_HOUR_FORMAT, function(userWants12HrFormat){
				use12HrFormat = userWants12HrFormat;
				use12HrFormatSyncedWithServer = true;

				for(var i = 0; i < linkFunctionParamsQueue.length; i++){
					linkFunction(linkFunctionParamsQueue[i].scope, linkFunctionParamsQueue[i].element, linkFunctionParamsQueue[i].attrs);
				}

				linkFunctionParamsQueue.length = 0;
			});

			return {
				restrict: "A", //The directive can only be used as an attribute
				templateUrl: "js/modules/timenomewatch/templates/views/util/dateTimePicker.html",
				require: 'ngModel',
				scope: {},
				link: function(scope, element, attrs){
					if(use12HrFormatSyncedWithServer){
						linkFunction(scope, element, attrs);
					}
					else{
						linkFunctionParamsQueue.push({scope: scope, element: element, attrs: attrs});
					}
				}
			};
		}]
)