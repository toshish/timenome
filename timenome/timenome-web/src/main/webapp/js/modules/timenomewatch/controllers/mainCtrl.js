/**
 * User: anirudh
 * Date: 26/11/13
 */

timeNomeWatch.controller(
	'MainCtrl',
	['$scope','WebSocketService', 'PageUrlService',
		function($scope, WebSocketService, PageUrlService){
		$scope.isFullSizePortletPage = false;
		$scope.bodyStyle = {};

		$scope.showNoWebSocketSupportModal = !WebSocketService.IS_WEBSOCKET_SUPPORTED;
		$scope.notificationTitle = "Dashboard will not work on this browser.";
		$scope.notificationMessage = "We are sorry. We do not support browsers without WebSocket yet.";

		$scope.onNotificationOkClicked = function(){
			$scope.showNoWebSocketSupportModal = false;
		};

		$scope.$on('$routeChangeSuccess', function(event, curRoute){
			$scope.isFullSizePortletPage = angular.equals(curRoute.loadedTemplateUrl, PageUrlService.getTemplateUrl(FULL_SIZE_PORTLET_PAGE)) ? true:false;

			if($scope.isFullSizePortletPage){
				$scope.bodyStyle['padding-top'] = "0px";
			}else{
				$scope.bodyStyle['padding-top'] = "50px";
			}
		});
	}]
);
