timeNomeWatch.factory(
	'DataFetchService',
	['TrieService', '$filter', 'LoggerService', 'HttpService', 'WebSocketService',
		function(TrieService, $filter, LoggerService, HttpService, WebSocketService){

			var getAmChartDataBuffer = [];
			var streamAmChartDataBuffer = [];

			var archiveUpdateListenerList = {};
			var dataStoreUpdateListenerList = {};

			var allArchives = {};
			var allDataStores = {};
			var allArchivesTrie;
			var allDataStoresTrie;

			var fetchIncomplete = true;

			var userApiCredentials = {};

			resetDataStoreMap();
			resetArchiveMap();

			var returnObj = {
				registerGetDataStoresCallback: function(callbackId, dataStoreUpdateListener){
					if(angular.isFunction(dataStoreUpdateListener)){
						dataStoreUpdateListenerList[callbackId] = dataStoreUpdateListener;
						if(fetchIncomplete === false){
							dataStoreUpdateListener(allDataStores);
						}
					}
				},

				registerGetArchivesCallback: function(callbackId, archiveUpdateListener){
					if(angular.isFunction(archiveUpdateListener)){
						archiveUpdateListenerList[callbackId] = archiveUpdateListener;
						if(fetchIncomplete === false){
							archiveUpdateListener(allArchives);
						}
					}
				},

				unRegisterGetDataStoresCallback: function(callbackId){
					delete dataStoreUpdateListenerList[callbackId];
				},

				unRegisterGetArchivesCallback: function(callbackId){
					delete archiveUpdateListenerList[callbackId];
				},

				//Add this to your scope and keep a watch on
				archiveUpdateFlag: archiveUpdateFlag,

				getAmChartData: function(getDataRequest, successCallback, errorCallback){
					getAmChartDataBuffer.push({getDataRequest: getDataRequest, successCallback: successCallback, errorCallback: errorCallback});
				},

				streamAmChartData: function(streamDataRequest, streamOpenCallback, onDataCallback, errorCallback){
					streamAmChartDataBuffer.push({
						streamDataRequest: streamDataRequest,
						streamOpenCallback: streamOpenCallback,
						onDataCallback: onDataCallback,
						errorCallback: errorCallback
					});
				},

				createDataStore: createDataStore,
				deleteDataStore: deleteDataStore,
				createArchiveKey: createArchiveKey,
				createArchive: createArchive,
				deleteArchive: deleteArchive,
				getAggregationFuncIndexToName: function(){
					return aggregationFuncIndexToName;
				},

				getAllArchivesTrie: function(){
					return allArchivesTrie;
				},

				getAllDataStoresTrie: function(){
					return allDataStoresTrie;
				},

				getUserAPICredentials: function(){
					//I might make it async method later
					return userApiCredentials;
				}
			};

			var timeNomeAuthenticatedConnection = null;

			var aggregationFuncIndexToName = {
				0: "Max",
				1: "Min",
				2: "Average",
				3: "Sum",
				4: "Last",
				5: "First",
				6: "Range",
				7: "Value"//No_Operation
			};

			var archiveUpdateFlag = {
				updateCnt: 0
			};
			var dataStoreFetchCount = 0;

			function resetDataStoreMap(){
				allDataStores = {};
				allDataStoresTrie = TrieService.createNewTrie();
			}

			function resetArchiveMap(){
				allArchives = {};
				allArchivesTrie = TrieService.createNewTrie();
			}

			HttpService.getApiCredentials(function(apiCredentials){
				userApiCredentials = apiCredentials;
				timeNome.createConnection(WebSocketService.WEB_SOCKET_URL, new timeNome.AuthenticationObj(apiCredentials),
					function(connectionObj){
						timeNomeAuthenticatedConnection = connectionObj;

						fetchAllDataStoresAndArchives();
					}, function(errorObj){
						LoggerService.error("Failed to authenticate API credentials", errorObj);
					});
			}, function(){
				LoggerService.error("Failed to fetch API credentials");
			});

			function fetchAllDataStoresAndArchives(){

				fetchIncomplete = true;

				//First fetch all data store names
				timeNomeAuthenticatedConnection.listAllDataStoreNames(new timeNomeAuthenticatedConnection.ListAllDataStoreNamesRequest(),
					function(response){
						var dataStoreNames = response.dataStoreName;

						resetDataStoreMap();
						resetArchiveMap();

						//Fetch detail of each data store
						dataStoreFetchCount = dataStoreNames.length;
						if(dataStoreFetchCount > 0){
							for(var i = 0; i < dataStoreNames.length; i++){
								fetchDataStore(dataStoreNames[i]);
							}
						}
						else{
							fetchIncomplete = false;
							onAllDataStoresFetched();
							onAllArchivesLoaded();
						}
					},
					function(){
						LoggerService.error("Failed to fetch data store names");
						fetchIncomplete = false;
					});
			}

			function fetchDataStore(dataStoreName){
				timeNomeAuthenticatedConnection.getDataStore(new timeNomeAuthenticatedConnection.GetDataStoreRequest({'dataStoreName': dataStoreName}),
					function(response){
						var dataStore = response.dataStore;
						registerDatastore(dataStore.dataStoreName, angular.isArray(dataStore.attribute)?dataStore.attribute: []);
						registerArchives(dataStore.dataStoreName, dataStore.archive);

						if(--dataStoreFetchCount === 0){
							fetchIncomplete = false;
							onAllDataStoresFetched();
							onAllArchivesLoaded();
						}

					}, function(){
						LoggerService.error("Failed to get data store details for " + dataStoreName);
					});

			}

			function registerDatastore(dataStoreName, attributes){
				allDataStores[dataStoreName] = {
					'dataStoreName': dataStoreName,
					'attributes': attributes
				};

				var dsNameSplit = dataStoreName.split(" ");

				for(var i = 0; i < dsNameSplit.length; i++){
					//add dataStoreName in trie
					allDataStoresTrie.addWord(dsNameSplit[i], dataStoreName);
				}

				for(i = 0; i < attributes.length; i++){
					var keySplit = attributes[i].key.split(" ");
					var valueSplit = attributes[i].value.split(" ");

					for(var j = 0; j < keySplit.length; j++){
						allDataStoresTrie.addWord(keySplit[j], dataStoreName);
					}

					for(var j = 0; j < valueSplit.length; j++){
						allDataStoresTrie.addWord(valueSplit[j], dataStoreName);
					}
				}
			}

			function registerArchives(dataStoreName, archiveList){
				var dsNameSplit = dataStoreName.split(" ");
				for(var i = 0; i < archiveList.length; i++){
					var archive = archiveList[i];

					var archiveKey = createArchiveKey(dataStoreName, archive.archiveName);
					allArchives[archiveKey] = archive;

					var archiveNameSplit = archive.archiveName.split(" ");
					var j = 0;

					for(j = 0; j < archiveNameSplit.length; j++){
						//add archiveName in trie
						allArchivesTrie.addWord(archiveNameSplit[j], archiveKey);
					}

					for(j = 0; j < dsNameSplit.length; j++){
						//add dataStoreName in trie
						allArchivesTrie.addWord(dsNameSplit[j], archiveKey);
					}

					archive.aggregationFunction = aggregationFuncIndexToName[archive.aggregationFunction];
					archive.dataStoreName = dataStoreName;
					if(archive.isCounterBased === false){
						archive.storagePrecision = archive.storagePrecision.toNumber();
						archive.storagePrecisionString = $filter('MillisecondsToStringFilter')(archive.storagePrecision);

						var storagePrecisionSplit = archive.storagePrecisionString.split(" ");
						for(j = 0; j < storagePrecisionSplit.length; j++){
							allArchivesTrie.addWord(storagePrecisionSplit[j], archiveKey);
						}
					}

					//add aggregation name in trie
					allArchivesTrie.addWord(archive.aggregationFunction, archiveKey);
				}
			}

			function onAllDataStoresFetched(){
				for(var callbackId in dataStoreUpdateListenerList){
					if(dataStoreUpdateListenerList.hasOwnProperty(callbackId)){
						dataStoreUpdateListenerList[callbackId](allDataStores);
					}
				}
			}

			function onAllArchivesLoaded(){
				returnObj.getAmChartData = getAmChartData;
				returnObj.streamAmChartData = streamAmChartData;

				for(var callbackId in archiveUpdateListenerList){
					if(archiveUpdateListenerList.hasOwnProperty(callbackId)){
						archiveUpdateListenerList[callbackId](allArchives);
					}
				}

				for(var i = 0; i < getAmChartDataBuffer.length; i++){
					var params = getAmChartDataBuffer[i];

					getAmChartData(params.getDataRequest, params.successCallback, params.errorCallback);
				}

				getAmChartDataBuffer.length = 0;

				for(var i = 0; i < streamAmChartDataBuffer.length; i++){
					var params = streamAmChartDataBuffer[i];

					streamAmChartData(params.streamDataRequest, params.streamOpenCallback, params.onDataCallback, params.errorCallback);
				}

				streamAmChartDataBuffer.length = 0;
			}

			function createArchiveKey(dataStoreName, archiveName){
				return dataStoreName + " - " + archiveName;
			}

			function createDataStore(dataStoreName, attributes, successCallback, errorCallback){
				timeNomeAuthenticatedConnection.createDataStore(new timeNomeAuthenticatedConnection.CreateDataStoreRequest({
					'dataStore': new timeNomeAuthenticatedConnection.DataStore({
						'dataStoreName': dataStoreName,
						'attribute': attributes
					})
				}), function(){
					if(angular.isFunction(successCallback)){
						successCallback();
					}
					fetchAllDataStoresAndArchives();
				}, errorCallback);
			}

			function deleteDataStore(dataStoreName, successCallback, errorCallback){
				timeNomeAuthenticatedConnection.deleteDataStore(new timeNomeAuthenticatedConnection.DeleteDataStoreRequest({
					'dataStoreName': dataStoreName
				}), function(){
					if(angular.isFunction(successCallback)){
						successCallback();
					}
					fetchAllDataStoresAndArchives();
				}, errorCallback);
			}

			function deleteArchive(dataStoreName, archiveName, successCallback, errorCallback){
				timeNomeAuthenticatedConnection.deleteArchive(new timeNomeAuthenticatedConnection.DeleteArchiveRequest({
					'dataStoreName': dataStoreName,
					'archiveName': archiveName
				}), function(){
					if(angular.isFunction(successCallback)){
						successCallback();
					}
					fetchAllDataStoresAndArchives();
				}, errorCallback);
			}

			function createArchive(dataStoreName, archiveDetails, successCallback, errorCallback){
				timeNomeAuthenticatedConnection.createArchive(new timeNomeAuthenticatedConnection.CreateArchiveRequest({
					'dataStoreName': dataStoreName,
					'archive': new timeNomeAuthenticatedConnection.Archive(archiveDetails)
				}), function(){
					if(angular.isFunction(successCallback)){
						successCallback();
					}
					fetchAllDataStoresAndArchives();
				}, errorCallback);
			}

			function getAmChartData(getDataRequest, successCallback, errorCallback){
				if(angular.isFunction(successCallback)){
					var dataQualifiers = getDataRequest.dataQualifiers;

					getDataRequest.apiKey = userApiCredentials.key;
					getDataRequest.apiSecret = userApiCredentials.secret;

					for(var i = 0; i < dataQualifiers.length; i++){
						var archive = allArchives[dataQualifiers[i].archiveKey];
						if(angular.isDefined(archive)){
							dataQualifiers[i].dataStoreName = archive.dataStoreName;
							dataQualifiers[i].archiveName = archive.archiveName;
						}
					}

					HttpService.getAmChartData(getDataRequest, successCallback, errorCallback);
				}
			}

			function streamAmChartData(streamDataRequest, streamOpenCallback, onDataCallback, errorCallback){
				if(angular.isFunction(onDataCallback)){
					var dataQualifiers = streamDataRequest.dataQualifiers;

					streamDataRequest.apiKey = userApiCredentials.key;
					streamDataRequest.apiSecret = userApiCredentials.secret;

					for(var i = 0; i < dataQualifiers.length; i++){
						var archive = allArchives[dataQualifiers[i].archiveKey];
						if(angular.isDefined(archive)){
							dataQualifiers[i].dataStoreName = archive.dataStoreName;
							dataQualifiers[i].archiveName = archive.archiveName;
							dataQualifiers[i].storagePrecision = archive.storagePrecision;
						}
					}

					WebSocketService.streamAmChartData(streamDataRequest, streamOpenCallback, onDataCallback, errorCallback);
				}
			}

			return returnObj;
		}]
);