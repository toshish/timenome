timeNomeWatch.factory(
	'PortletViewConfigPageInvocationService',
	['$timeout', 'PageUrlService',
		function($timeout, PageUrlService){

			var openPorletViewConfigPageCallback = null;

			var returnObj = {
				"openPortletViewConfigPage": openPortletViewConfigPage,
				"registerCallbackForInvocation": function(callback){
					openPorletViewConfigPageCallback = callback;
				}
			};

			function openPortletViewConfigPage(viewConfig){
				if(angular.isDefined(openPorletViewConfigPageCallback) && openPorletViewConfigPageCallback!==null){
					openPorletViewConfigPageCallback(viewConfig);
				}
			}

			return returnObj;
		}]
);