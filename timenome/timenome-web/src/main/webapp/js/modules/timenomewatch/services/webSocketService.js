timeNomeWatch.factory(
	'WebSocketService',
	[function(){

		var wsUrl = "ws://" + window.location.host + "/" + window.location.pathname.split("/")[1] + "/";
		var webSocketSupported = 'WebSocket' in window || 'MozWebSocket' in window;

		var ENDPOINT_URL = {
			'AM_CHART_STREAMING': 'amchartstreamingservice'
		};

		//Websock connection object
		function WebSockConn(endpointUrl, websockCallbacks){
			endpointUrl = wsUrl + endpointUrl;
			if('WebSocket' in window){
				this.websocket = new WebSocket(endpointUrl);
			}
			else if('MozWebSocket' in window){
				this.websocket = new MozWebSocket(endpointUrl);
			}
			else{
				throw {message: "WebSocket is not supported on this browser"};
			}

			this.websocket.binaryType = "arraybuffer";//protobuf doesn't work otherwise
			this.setCallbacks(websockCallbacks);
		}

		WebSockConn.prototype.setCallbacks = function(websockCallbacks){
			if(typeof websockCallbacks == "object"){
				if(typeof websockCallbacks.onOpen === "function"){
					this.websocket.onopen = websockCallbacks.onOpen;
				}

				if(typeof websockCallbacks.onMessage === "function"){
					this.websocket.onmessage = websockCallbacks.onMessage;
				}

				if(typeof websockCallbacks.onClose === "function"){
					this.websocket.onclose = websockCallbacks.onClose;
				}

				if(typeof websockCallbacks.onError === "function"){
					this.websocket.onerror = websockCallbacks.onError;
				}
			}
		};

		WebSockConn.prototype.sendMessage = function(message){
			try{
				this.websocket.send(message);
			}
			catch(e){
				console.log(e);
			}
		};

		WebSockConn.prototype.disconnect = function(){
			this.websocket.close();
		};

		function streamAmChartData(streamDataRequest, streamOpenCallback, onDataCallback, errorCallback){
			var webSockConn = new WebSockConn(ENDPOINT_URL.AM_CHART_STREAMING, {
				onOpen: function(){
					if(angular.isFunction(streamOpenCallback)){
						streamOpenCallback(webSockConn);
						webSockConn.sendMessage(angular.toJson(streamDataRequest));
					}
				},
				onMessage: function(event){
					if(angular.isFunction(onDataCallback)){
						onDataCallback(angular.fromJson(event.data));
					}
				},
				onError: function(event){
					console.log(event);
				},
				onClose: function(event){
					console.log(event);
				}

			});
		}

		return {
			WEB_SOCKET_URL: wsUrl,
			IS_WEBSOCKET_SUPPORTED: webSocketSupported,

			'streamAmChartData': streamAmChartData
		};
	}]
);
