/**
 * User: anirudh
 * Date: 26/11/13
 */

timeNomeWatch.controller(
	'RulesCtrl',
	['$scope', 'DataFetchService', 'UtilService', 'RulesService',
		function($scope, DataFetchService, UtilService, RulesService){

			var RULE_SCREEN_INFO = 1;
			var RULE_SCREEN_CONDITION = 2;
			var RULE_SCREEN_ACTION = 3;

			$scope.operatorList = [
				{
					title: 'is Greater than',
					value: 'greaterThan'
				},
				{
					title: 'is Lesser than',
					value: 'lesserThan'
				},
				{
					title: 'is Greater than or Equal to',
					value: 'greaterThanOrEqual'
				},
				{
					title: 'is Lesser than or Equal to',
					value: 'lesserThanOrEqual'
				},
				{
					title: 'is Equal To',
					value: 'equalTo'
				},
				{
					title: 'is Not Equal to',
					value: 'notEqualTo'
				}
			];

			$scope.rulescreen = RULE_SCREEN_INFO;

			$scope.showConfirmDeleteRuleModal = false;
			$scope.showCreateRuleModal = false;
			$scope.showNotificationModal = false;

			$scope.allRules = {};
			$scope.hideArchiveSearchDropdownCnt = 0;
			$scope.refreshArchiveSearchBox = 0;

			$scope.condition1 = {};
			$scope.condition2 = {};

			$scope.search = {
				filterText: ''
			};

			$scope.emailBox = {
				emailId: ''
			};

			$scope.smsBox = {
				sms: ''
			};

			$scope.toBeCreatedRule = {};

			$scope.$watch('search.filterText', filterRules);

			$scope.$watch('emailBox.emailId', function(newVal, oldVal){
				if(angular.isString(newVal) && newVal.length > 0 && newVal.charAt(newVal.length - 1) === ' '){
					var mailId = newVal.substring(0, newVal.length - 1).trim();
					if(mailId.length > 0){
						$scope.toBeCreatedRule.alert.emailList.push(mailId);
						$scope.emailBox.emailId = '';
					}
				}
			});

			$scope.$watch('smsBox.sms', function(newVal, oldVal){
				if(angular.isString(newVal) && newVal.length > 0 && newVal.charAt(newVal.length - 1) === ' '){
					var phone = newVal.substring(0, newVal.length - 1).trim();
					if(phone.length > 0){
						$scope.toBeCreatedRule.alert.smsList.push(phone);
						$scope.smsBox.sms = '';
					}
				}
			});

			$scope.onDeleteClicked = function(index){
				$scope.toBeDeletedEntityType = "Rule";
				$scope.toBeDeletedEntityId = $scope.filteredDataStoreNames[index];
				$scope.showConfirmDeleteRuleModal = true;
			};

			function initializeSingleCondition(condition){
				condition.type = 'single';
				condition.operator = $scope.operatorList[0].value;
				condition.value = 0;
			};

			$scope.removeCondition1 = function(){
				$scope.condition1 = angular.copy($scope.condition2);
				$scope.toBeCreatedRule.condition = $scope.condition1;

				initializeSingleCondition($scope.condition2);
			};

			$scope.removeCondition2 = function(){
				$scope.toBeCreatedRule.condition = $scope.condition1;
				initializeSingleCondition($scope.condition2);
			};

			$scope.addAnotherConditionClicked = function(){
				$scope.toBeCreatedRule.condition = {
					type: 'composite',
					operator: 'AND',
					operand1: $scope.condition1,
					operand2: $scope.condition2
				};
			};

			$scope.setCompositeConditionOperator = function(operatorName){
				$scope.toBeCreatedRule.condition.operator = operatorName;
			};

			$scope.onCreateRuleClicked = function(){
				$scope.toBeCreatedRule[RULE_NAME] = '';
				$scope.toBeCreatedRule.archives = [];
				$scope.toBeCreatedRule.alert = {
					frequency: 'once',
					alertTypes: {
						'email': false,
						'sms': false,
						'dashboardnotification': true
					},
					emailList: [],
					smsList: [],
					sendClearAlert: true
				};

				initializeSingleCondition($scope.condition1);
				initializeSingleCondition($scope.condition2);

				$scope.toBeCreatedRule.condition = $scope.condition1;

				$scope.selectedArchives = $scope.toBeCreatedRule.archives;
				$scope.rulescreen = RULE_SCREEN_INFO;

				$scope.refreshArchiveSearchBox++;
				$scope.showCreateRuleModal = true;
			};

			$scope.backClicked = function(){
				$scope.rulescreen--;
			};

			$scope.nextClicked = function(){
				$scope.rulescreen++;
			};

			$scope.hideArchiveSearchDropdown = function(){
				$scope.hideArchiveSearchDropdownCnt++;
			};

			$scope.onNotificationOkClicked = function(){
				$scope.showNotificationModal = false;
			};

			$scope.onConfirmDeleteCancelClicked = function(){
				$scope.showConfirmDeleteRuleModal = false;
			};

			$scope.onCreateRuleCancelClicked = function(){
				$scope.showCreateRuleModal = false;
			};

			$scope.removeEmailFromSelectedList = function($event, index){
				$event.stopPropagation();
				$scope.toBeCreatedRule.alert.emailList.splice(index, 1);
			};

			$scope.removeSmsFromSelectedList = function($event, index){
				$event.stopPropagation();
				$scope.toBeCreatedRule.alert.smsList.splice(index, 1);
			};

////		$scope.deleteEntity = function(dataStoreName){
//			DataFetchService.deleteDataStore(dataStoreName, function(){
//				$scope.showNotificationModal = true;
//				$scope.notificationTitle = "Delete Successful";
//				$scope.notificationMessage = "Successfully deleted Data Store \"" + dataStoreName + "\".";
//			}, function(exception){
//				$scope.showNotificationModal = true;
//				$scope.notificationTitle = "Delete Failed";
//				$scope.notificationMessage = exception.exceptionMessage;
//			});
//		};
//		$scope.createDataStore = function(){
//			var dataStoreName = $scope.toBeCreatedDataStore[DATA_STORE_NAME];
//			DataFetchService.createDataStore(dataStoreName,
//				function(){
//					$scope.showNotificationModal = true;
//					$scope.notificationTitle = "Create Successful";
//					$scope.notificationMessage = "Successfully created Data Store \"" + dataStoreName + "\".";
//				}, function(exception){
//					$scope.showNotificationModal = true;
//					$scope.notificationTitle = "Create Failed";
//					$scope.notificationMessage = exception.exceptionMessage;
//				});
//		};
//
//		$scope.deleteEntity = function(dataStoreName){
//			DataFetchService.deleteDataStore(dataStoreName, function(){
//				$scope.showNotificationModal = true;
//				$scope.notificationTitle = "Delete Successful";
//				$scope.notificationMessage = "Successfully deleted Data Store \"" + dataStoreName + "\".";
//			}, function(exception){
//				$scope.showNotificationModal = true;
//				$scope.notificationTitle = "Delete Failed";
//				$scope.notificationMessage = exception.exceptionMessage;
//			});
//		};
//
//		$scope.createDataStoreDetailsPageUrl = function(index){
//			var dataStoreName = $scope.filteredDataStoreNames[index];
//
//			if(angular.isDefined(dataStoreName)){
//				return PageUrlService.createDataStoreDetailsPageLink(dataStoreName);
//			}
//
//			return "#/";
//		};
//
			function filterRules(){
				var filteredRules = RulesService.getAllRulesTrie().search($scope.search.filterText);

				$scope.filteredRuleNames = filteredRules.values();
				$scope.ruleCountSequence = new Array(Math.ceil($scope.filteredRuleNames.length / 3));// > ARCHIVE_PAGE_SIZE) ? ARCHIVE_PAGE_SIZE:$scope.filteredDataStoreNames.length);
			}

			RulesService.registerGetRulesCallback('RulesCtrl', function(rules){
				$scope.allRules = rules;
				filterRules();
				UtilService.scopeApply($scope);
			});

			$scope.$on('$destroy', function(){
				RulesService.unRegisterGetRulesCallback('RulesCtrl');
			});
		}
	]
);

