timeNomeWatch.factory(
	'PortletConfigService',
	['$timeout', 'PageUrlService', 'DataFetchService', 'HttpService',
		function($timeout, PageUrlService, DataFetchService, HttpService){

			var MONITOR_PORTLET_1 = "monitorPortlet1";
			var MONITOR_PORTLET_2 = "monitorPortlet2";
			var MONITOR_PORTLET_3 = "monitorPortlet3";
			var MONITOR_PORTLET_4 = "monitorPortlet4";

			var onLoadCallbackMap = {};
			var portletConfigMap = {};

			var returnObj = {
				registerOnPortletConfigLoaded: registerOnPortletConfigLoaded
			};

			var defaultConfig = {id: MONITOR_PORTLET_1,
				title: "",
				chartType: LINE_CHART,
				updatePolicy: {
					chartUpdateType: STATIC_CHART,
					startTimestamp: new Date().getTime(),
					endTimestamp: new Date().getTime()
				},
				selectedArchives: [],
				templateUrl: PageUrlService.getTemplateUrl(CHART_LOAD_PAGE),
				isUpdated: 0,
				update: function(){
					this.isUpdated++;
					savePortletConfig(this.id, portletConfigMap[this.id]);
				}
			};

			portletConfigMap[MONITOR_PORTLET_1] = angular.copy(defaultConfig);
			portletConfigMap[MONITOR_PORTLET_1].id = MONITOR_PORTLET_1;

			portletConfigMap[MONITOR_PORTLET_2] = angular.copy(defaultConfig);
			portletConfigMap[MONITOR_PORTLET_2].id = MONITOR_PORTLET_2;

			portletConfigMap[MONITOR_PORTLET_3] = angular.copy(defaultConfig);
			portletConfigMap[MONITOR_PORTLET_3].id = MONITOR_PORTLET_3;

			portletConfigMap[MONITOR_PORTLET_4] = angular.copy(defaultConfig);
			portletConfigMap[MONITOR_PORTLET_4].id = MONITOR_PORTLET_4;

            function registerOnPortletConfigLoaded(portletId, callback){
                if(angular.isFunction(callback)){
                    onLoadCallbackMap[portletId] = callback;

                    if(angular.isDefined(portletConfigMap[portletId])){
                        callback(portletConfigMap[portletId]);
                    }
                }
            }


            //Load user preferences
			HttpService.getUserPreferences(function(data){
					loadPortletConfig(MONITOR_PORTLET_1, data[MONITOR_PORTLET_1]);
					loadPortletConfig(MONITOR_PORTLET_2, data[MONITOR_PORTLET_2]);
					loadPortletConfig(MONITOR_PORTLET_3, data[MONITOR_PORTLET_3]);
					loadPortletConfig(MONITOR_PORTLET_4, data[MONITOR_PORTLET_4]);
				}, function(er){
					//Load default
					loadPortletConfig(MONITOR_PORTLET_1);
					loadPortletConfig(MONITOR_PORTLET_2);
					loadPortletConfig(MONITOR_PORTLET_3);
					loadPortletConfig(MONITOR_PORTLET_4);
				}
			);

			function loadPortletConfig(portletId, config){
				if(angular.isString(config)){
					portletConfigMap[portletId] = angular.fromJson(config);
					portletConfigMap[portletId].update = function(){
						this.isUpdated++;
						savePortletConfig(this.id, portletConfigMap[this.id]);
					};
				}

				if(angular.isFunction(onLoadCallbackMap[portletId])){
					onLoadCallbackMap[portletId](portletConfigMap[portletId]);
				}
			}

			function savePortletConfig(id, config){
				var portletConfigPreference = {};
				portletConfigPreference[KEY] = id;
				portletConfigPreference[VALUE] = angular.toJson(config);
				HttpService.setUserPreferences([portletConfigPreference], function(){
				}, function(er){
					console.log(er);
				});
			}

			return returnObj;
		}]
);