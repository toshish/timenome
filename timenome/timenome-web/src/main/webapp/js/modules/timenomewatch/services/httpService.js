timeNomeWatch.factory(
	'HttpService',
	['$http', function($http){

		var REQUEST_URL = {
			'SET_USER_PREFERENCES': 'rest/userservice/set/preferences',
			'GET_USER_PREFERENCES': 'rest/userservice/get/preferences',
			'GET_API_CREDENTIALS': 'rest/userservice/get/apicredentials',
			'GET_USERINFO': 'rest/userservice/get/userinfo',
			'GET_AM_CHART_DATA': 'rest/amchartdataservice/get/data',
			'LOGOUT': 'rest/userservice/logout'
		};

		var HTTP_METHOD = {
			GET: "GET",
			POST: "POST"
		};

		function sendHttpGetRequest(params, successCallback, failureCallback){
			if(!angular.isObject(params)){
				params = {};
			}

			params['data'] = angular.toJson(params.data);
			params['method'] = HTTP_METHOD.GET;
			$http(params).success(successCallback).error(failureCallback);
		}

		function sendHttpPostRequest(params, successCallback, failureCallback){
			if(!angular.isObject(params)){
				params = {};
			}

			if(angular.isUndefined(params.data) || !angular.isObject(params.data)){
				params['data'] = {};
			}

			params['data'] = angular.toJson(params.data);
			params['method'] = HTTP_METHOD.POST;
			$http(params).success(successCallback).error(failureCallback);
		}

		function setUserPreferences(preferencesArray, successCallback, failureCallback){
			if(angular.isArray(preferencesArray)){
				sendHttpPostRequest({url: REQUEST_URL.SET_USER_PREFERENCES, data: preferencesArray}, successCallback, failureCallback);
			}else{
				failureCallback("Preferences not an array");
			}
		}

		function getUserPreferences(successCallback, failureCallback){
			sendHttpGetRequest({url: REQUEST_URL.GET_USER_PREFERENCES}, successCallback, failureCallback);
		}

		function getUserinfo(successCallback, failureCallback){
			sendHttpGetRequest({url: REQUEST_URL.GET_USERINFO}, function(data){
				successCallback(data);
			}, function(){
			});
		}

		function getAmChartData(dataRequest, successCallback, failureCallback){
			sendHttpPostRequest({url: REQUEST_URL.GET_AM_CHART_DATA, data: dataRequest}, function(data){
				successCallback(data);
			}, function(){
			});
		}

		function logout(successCallback, failureCallback){
			sendHttpPostRequest({url: REQUEST_URL.LOGOUT}, successCallback, failureCallback);
		}

		function getApiCredentials(successCallback, failureCallback){
			sendHttpGetRequest({url: REQUEST_URL.GET_API_CREDENTIALS}, function(data){
				successCallback(angular.fromJson(data));
			}, function(){
				failureCallback();
			});
		}

		return {
			'setUserPreferences': setUserPreferences,
			'getUserPreferences': getUserPreferences,
			'getApiCredentials': getApiCredentials,
			'getUserinfo': getUserinfo,
			 'getAmChartData': getAmChartData,
			'logout': logout
		};
	}]
);
