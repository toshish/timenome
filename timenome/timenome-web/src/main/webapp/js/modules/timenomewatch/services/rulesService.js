timeNomeWatch.factory(
	'RulesService',
	['TrieService', '$filter', 'LoggerService', 'HttpService', 'WebSocketService',
		function(TrieService, $filter, LoggerService, HttpService, WebSocketService){

			var ruleUpdateListenerList = {};

			var fetchIncomplete = true;

			var allRules = {};
			var allRulesTrie;

			resetRulesMap();

			var returnObj = {
				registerGetRulesCallback: function(callbackId, ruleUpdateListener){
					if(angular.isFunction(ruleUpdateListener)){
						ruleUpdateListenerList[callbackId] = ruleUpdateListener;
						if(fetchIncomplete === false){
							ruleUpdateListener(allRules);
						}
					}
				},

				unRegisterGetRulesCallback: function(callbackId){
					delete ruleUpdateListenerList[callbackId];
				},

				getAllRulesTrie: function(){
					return allRulesTrie;
				}
			};

			function resetRulesMap(){
				allRules = {};
				allRulesTrie = TrieService.createNewTrie();
			}

			var timeNomeAuthenticatedConnection = null;

			HttpService.getApiCredentials(function(apiCredentials){
				timeNome.createConnection(WebSocketService.WEB_SOCKET_URL, new timeNome.AuthenticationObj(apiCredentials),
					function(connectionObj){
						timeNomeAuthenticatedConnection = connectionObj;

						fetchAllRules();
					}, function(errorObj){
						LoggerService.error("Failed to authenticate API credentials", errorObj);
					});
			}, function(){
				LoggerService.error("Failed to fetch API credentials");
			});


			function fetchAllRules(){
				//Do something with data fetch service

				fetchIncomplete = false;
				//name, datastore, archive, action, test condition
			}

			return returnObj;
		}]
);