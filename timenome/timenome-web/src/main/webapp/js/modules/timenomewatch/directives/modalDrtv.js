timeNomeWatch.directive(
	'modal',
	['UtilService',function(UtilService){

		return {
			restrict: "A",
			link: function(scope, element, attrs){
				var modalElement = element.find(".modal");

				modalElement.on('hidden.bs.modal', function (e) {
					if(angular.isDefined(attrs.onCancelClickHandler) && angular.isFunction(scope[attrs.onCancelClickHandler])){
						scope[attrs.onCancelClickHandler]();
						UtilService.scopeApply(scope);
					}
				});

				if(angular.isDefined(attrs.modalTrigger)){
					scope.$watch(attrs.modalTrigger, function(showModal){
						if(showModal===true){
							modalElement.modal({
								'keyboard': false,
								'show': true,
								'backdrop': 'static'
							});
						}else{
							modalElement.modal({
								'show': false
							});
						}
					});
				}
			}
		};
	}]
);