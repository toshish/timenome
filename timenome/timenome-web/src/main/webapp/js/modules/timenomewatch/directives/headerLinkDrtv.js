timeNomeWatch.directive(
	'headerLink',
		['$location','PageUrlService',
			function($location, PageUrlService){

			return {
				restrict: "A", //The directive can only be used as an attribute
				scope: {},
				link: function(scope, element, attrs){
					var pageId = attrs.pageId;

					if(angular.isDefined(pageId)){
						var pageUrlDetails = PageUrlService.getPageUrlDetails(pageId);

						if(pageUrlDetails !== null){
							var anchorTag = element.find("a");
							anchorTag.attr("href", "#" + pageUrlDetails.routePath);
							scope.location = pageUrlDetails.routePath;

							if(scope.location === $location.path()){
								element.addClass("active");
							}
							else{
								element.removeClass("active")
							}

							scope.$on('$locationChangeSuccess', function(){
								if(scope.location === $location.path()){
									element.addClass("active");
								}
								else{
									element.removeClass("active");
								}
							});
						}
					}
				}
			};
		}]
);