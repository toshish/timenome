/**
 * User: anirudh
 * Date: 26/11/13
 */

timeNomeWatch.controller(
	'DynamicLineChartCtrl',
	['$scope', 'DataFetchService', 'UtilService', 'HashSetService',
		function($scope, DataFetchService, UtilService, HashSetService){
			var MAX_LENGTH_CHART_DATA_ARRAY = 50;

			var currentTimestamps = HashSetService.newHashSet();

			var streamHandle;

			$scope.chartData = {
				updateCnt: 0,
				dataArr: [],
				title: '',
				selectedArchives: [], //array of objects, each having archiveKey and streamHandler
				updated: function(){
					this.updateCnt++;
				}
			};

			$scope.allArchives = {};

			function killStreams(){
				if(angular.isDefined(streamHandle)){
					streamHandle.disconnect();
				}
			}

			//To make sure dynamic chart isn't created twice the first time UI loads
			var renderChart = function(){
				renderChart = viewConfigUpdated;
				$scope.$watch("viewConfig.isUpdated", function(newVal, oldVal){
					if(angular.isDefined(newVal)){
						viewConfigUpdated();
					}
				});
			};

			var addDataToChart = overwriteChartData;

			function mergeWithExistingData(amChartData){
				for(var i = 0; i < amChartData.length; i++){
					if(amChartData[i].timestamp > $scope.chartData.dataArr[$scope.chartData.dataArr.length - 1].timestamp.getTime()){
						currentTimestamps.add(amChartData[i].timestamp);
						amChartData[i].timestamp = new Date(amChartData[i].timestamp);
						$scope.chartData.dataArr.push(amChartData[i]);
					}

					else if(currentTimestamps.containsKey(amChartData[i].timestamp)){
						//Merge
						var index = binarySearch(amChartData[i].timestamp, $scope.chartData.dataArr, 0, $scope.chartData.dataArr.length - 1);
						var dataTuple = $scope.chartData.dataArr[index];
						amChartData[i].timestamp = new Date(amChartData[i].timestamp);

						for(key in amChartData[i]){
							if(amChartData[i].hasOwnProperty(key)){
								dataTuple[key] = amChartData[i][key];
							}
						}
					}
					else if(amChartData[i].timestamp > $scope.chartData.dataArr[0].timestamp.getTime()){
						var index = binarySearch(amChartData[i].timestamp, $scope.chartData.dataArr, 0, $scope.chartData.dataArr.length - 1);
						amChartData[i].timestamp = new Date(amChartData[i].timestamp);

						currentTimestamps.add(amChartData[i].timestamp);
						$scope.chartData.dataArr.splice(index + 1, 0, amChartData[i]);
					}
				}

				//Trim chart if more entries appear than needed
				if($scope.chartData.dataArr.length > MAX_LENGTH_CHART_DATA_ARRAY){
					var numElementsToBeRemoved = $scope.chartData.dataArr.length - MAX_LENGTH_CHART_DATA_ARRAY;
					for(i = 0; i < numElementsToBeRemoved; i++){
						currentTimestamps.remove($scope.chartData.dataArr[i].timestamp.getTime());
					}
					$scope.chartData.dataArr.splice(0, numElementsToBeRemoved);
				}
			}

			function overwriteChartData(amChartData){

				if(amChartData.length > 0){
					addDataToChart = mergeWithExistingData;
				}

				for(var i = 0; i < amChartData.length; i++){
					currentTimestamps.add(amChartData[i].timestamp);
					amChartData[i].timestamp = new Date(amChartData[i].timestamp);
				}

				$scope.chartData.dataArr = amChartData;
			};

			function binarySearch(key, array, startIndex, endIndex){
				var midIndex = Math.floor((endIndex - startIndex) / 2) + startIndex;

				var midTime = array[midIndex].timestamp.getTime();

				if(key > midTime){
					if(midIndex + 1> endIndex){
						return endIndex;
					}
					return binarySearch(key, array, midIndex + 1, endIndex);
				}
				else if(key < midTime){
					if(startIndex > midIndex - 1){
						return midIndex - 1;
					}
					return binarySearch(key, array, startIndex, midIndex - 1);
				}
				else{
					return midIndex;
				}
			}

			function viewConfigUpdated(){
				//Kill older streams--optimize by not restarting common streams later
				killStreams();

				currentTimestamps.clear();
				addDataToChart = overwriteChartData;

				$scope.chartData.dataArr = [];
				$scope.chartData.title = $scope.viewConfig.title;
				$scope.chartData.selectedArchives = [];

				var streamDataRequest = {
					dataQualifiers: [],
					maxInitialDataEntries: MAX_LENGTH_CHART_DATA_ARRAY
				};

				for(var i = 0; i < $scope.viewConfig.selectedArchives.length; i++){
					if(angular.isDefined($scope.allArchives[$scope.viewConfig.selectedArchives[i]])){
						$scope.chartData.selectedArchives.push($scope.viewConfig.selectedArchives[i]);
						streamDataRequest.dataQualifiers.push({archiveKey: $scope.viewConfig.selectedArchives[i]});
					}
				}

				DataFetchService.streamAmChartData(streamDataRequest, function(streamHandleObj){
					streamHandle = streamHandleObj;
					UtilService.scopeApply($scope);
				}, function(data){
					if(data.success === true){
						var amChartData = data.amChartData;

						addDataToChart(amChartData);
						$scope.chartData.updated();
						UtilService.scopeApply($scope);
					}
				}, function(error){
					console.log(error);
					$scope.chartData.dataArr = [];
					$scope.chartData.updated();
					UtilService.scopeApply($scope);
				});

				if($scope.chartData.selectedArchives.length === 0){
					$scope.chartData.updated();
				}
			}

			DataFetchService.registerGetArchivesCallback('DynamicLineChartCtrl' + $scope.$id, function(archives){
				$scope.allArchives = archives;
				renderChart();
				UtilService.scopeApply($scope);
			});

			$scope.$on('$destroy', function(){
				killStreams();
				DataFetchService.unRegisterGetArchivesCallback('DynamicLineChartCtrl' + $scope.$id);
			});

			$scope.chartData.updated();
		}]
);
