timeNomeWatch.provider(
	'PageUrlService',
	[function(){

		var pageUrlMap = {};

		this.$get = function(){
			return{
				"getPageUrlDetails": getPageUrlDetails,
				"getTemplateUrl": function(templateName){
					return pageUrlMap[templateName].templateUrl;
				},
				"createFullSizePortletPageLink": function(pageName, portletId){
					return "#/fullSize/" + PAGE_NAME + "/" + pageName + "/" + PORTLET_ID + "/" + portletId;
				},
				"createDataStoreDetailsPageLink": function(dataStoreName){
					return "#/dataStoreList/" + DATA_STORE + "/" + dataStoreName;
				}
			};
		};

		//Monitor
		pageUrlMap[MONITOR_PAGE] = {
			routePath: "/monitor",
			templateUrl: 'js/modules/timenomewatch/templates/views/monitor/monitor.html'
		};

		//Data Store List
		pageUrlMap[DATA_STORE_LIST_PAGE] = {
			routePath: "/dataStoreList",
			templateUrl: 'js/modules/timenomewatch/templates/views/dataStoreList/dataStoreList.html'
		};

		//Data Store List
		pageUrlMap[MANAGE_PAGE] = {
			routePath: "/manage",
			templateUrl: 'js/modules/timenomewatch/templates/views/manage/manage.html'
		};

		//Rules
		pageUrlMap[RULES_PAGE] = {
			routePath: "/rules",
			templateUrl: 'js/modules/timenomewatch/templates/views/rules/rules.html'
		};

		//Settings
		pageUrlMap[SETTINGS_PAGE] = {
			routePath: "/settings",
			templateUrl: 'js/modules/timenomewatch/templates/views/settings/settings.html'
		};

		//Alert Details
		pageUrlMap[ALERT_DETAILS_PAGE] = {
			routePath: "/alertDetails",
			templateUrl: 'js/modules/timenomewatch/templates/views/alertDetails/alertDetails.html'
		};

		//Full page Portlet-view
		pageUrlMap[FULL_SIZE_PORTLET_PAGE] = {
			routePath: "/fullSize/" + PAGE_NAME + "/:" + PAGE_NAME + "/" + PORTLET_ID + "/:" + PORTLET_ID,
			templateUrl: 'js/modules/timenomewatch/templates/views/util/fullSizePortlet.html'
		};

		//Loading page
		pageUrlMap[LOADING_PAGE] = {
			templateUrl: 'js/modules/timenomewatch/templates/views/util/loading.html'
		};

		//Chart Load Page
		pageUrlMap[CHART_LOAD_PAGE] = {
			templateUrl: 'js/modules/timenomewatch/templates/views/charting/chartLoad.html'
		};

		//Chart Load Page With Archive deleted Message
		pageUrlMap[CHART_LOAD_PAGE_WITH_ARCHIVE_DELETED_MSG] = {
			templateUrl: 'js/modules/timenomewatch/templates/views/charting/chartLoadWithArchiveDeletedMessage.html'
		};

		pageUrlMap[STATIC_LINE_CHART] = {
			templateUrl: "js/modules/timenomewatch/templates/views/charting/staticLineChart.html"
		};

		pageUrlMap[STATIC_COLUMN_CHART] = {
			templateUrl: "js/modules/timenomewatch/templates/views/charting/staticColumnChart.html"
		};

		pageUrlMap[STATIC_STACKED_COLUMN_CHART] = {
			templateUrl: "js/modules/timenomewatch/templates/views/charting/staticStackedColumnChart.html"
		};

		pageUrlMap[DYNAMIC_LINE_CHART] = {
			templateUrl: "js/modules/timenomewatch/templates/views/charting/dynamicLineChart.html"
		};

		pageUrlMap[DYNAMIC_COLUMN_CHART] = {
			templateUrl: "js/modules/timenomewatch/templates/views/charting/dynamicColumnChart.html"
		};

		pageUrlMap[DYNAMIC_STACKED_COLUMN_CHART] = {
			templateUrl: "js/modules/timenomewatch/templates/views/charting/dynamicStackedColumnChart.html"
		};

		function getPageUrlDetails(pageName){
			if(angular.isDefined(pageUrlMap[pageName])){
				return pageUrlMap[pageName];
			}

			return null;
		}

		this.getPageUrlDetails = getPageUrlDetails;

	}]
);
