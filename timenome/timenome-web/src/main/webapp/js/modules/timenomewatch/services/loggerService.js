timeNomeWatch.factory(
	'LoggerService',
	['$log', function($log){

		var LogLevel = {
			DEBUG: 1,
			INFO: 2,
			WARN: 3,
			ERROR: 4,
			OFF: 5
		};

		var logLevel = LogLevel.INFO;

		return {
			debug: function(message, data){
				if(logLevel === LogLevel.DEBUG){
					$log.debug('[DEBUG] ' + message);
					if(angular.isDefined(data)){
						$log.info(data);
					}
				}
			},
			info: function(message, data){
				if(logLevel <= LogLevel.INFO){
					$log.info('[INFO] ' + message);
					if(angular.isDefined(data)){
						$log.info(data);
					}
				}
			},
			warn: function(message, data){
				if(logLevel <= LogLevel.WARN){
					$log.warn('[WARNING] ' + message);
					if(angular.isDefined(data)){
						$log.info(data);
					}
				}
			},
			error: function(message, data){
				if(logLevel <= LogLevel.ERROR){
					$log.error('[ERROR] ' + message);
					if(angular.isDefined(data)){
						$log.info(data);
					}
				}
			}
		};
	}]
);
