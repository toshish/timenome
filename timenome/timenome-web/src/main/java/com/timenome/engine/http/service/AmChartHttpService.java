/**
 * 
 */
package com.timenome.engine.http.service;

import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.timenome.api.core.User;
import com.timenome.api.exceptions.TimeOutOfRangeException;
import com.timenome.api.exceptions.UserDoesNotExists;
import com.timenome.api.logger.TimeNomeLogger;
import com.timenome.engine.amchartdataservice.AmChartDataRequest;
import com.timenome.engine.amchartdataservice.DataQualifier;
import com.timenome.engine.authentication.AuthenticationManager;
import com.timenome.engine.dashboardendpoint.AmChartDataResponse;
import com.timenome.engine.endpoint.utils.DataFetcher;
import com.timenome.proto.TimeNomeProtos.DataEntry;
import com.timenome.proto.TimeNomeProtos.GetDataRequest;
import com.timenome.proto.TimeNomeProtos.GetDataResponse;

@Path("/amchartdataservice")
public class AmChartHttpService {
	@Context
	ServletContext servletContext;
	@Context
	HttpServletRequest httpServletRequest;

	private static TimeNomeLogger logger = TimeNomeLogger.getTimeNomeLogger(AmChartHttpService.class);
	private static int SAMPLE_COUNT = 10;

	private Map<Long, Map<String, Double>> amChartData = new ConcurrentSkipListMap<>(new Comparator<Long>() {

		@Override
		public int compare(Long timestamp1, Long timestamp2) {
			return (int) (timestamp1 - timestamp2);
		}
	});

	// Map of series name and axis they should be shown on. Series with similar
	// values will share an axis. Access to this map is synchronized
	private Map<String, List<String>> seriesValueAxisMap = new HashMap<>();

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/get/data")
	public AmChartDataResponse getData(AmChartDataRequest amChartDataRequest) {
		String apiKey = amChartDataRequest.getApiKey();
		String apiSecret = amChartDataRequest.getApiSecret();

		long startTimestamp = amChartDataRequest.getStartTimestamp();
		long endTimestamp = amChartDataRequest.getEndTimestamp();

		logger.info("Fetching data b/w " + new Date(startTimestamp) + " and " + new Date(endTimestamp));

		ExecutorService dataFetchTaskExecutorService = Executors.newCachedThreadPool();

		User user = AuthenticationManager.getInstance().authenticateAPI(apiKey, apiSecret);
		String userName = null;
		if (user != null) {
			userName = user.getName();

			List<DataQualifier> dataQualifiers = amChartDataRequest.getDataQualifiers();

			for (final DataQualifier dataQualifier : dataQualifiers) {
				dataFetchTaskExecutorService.execute(new DataFetchTask(userName, dataQualifier, startTimestamp,
						endTimestamp));
			}

			dataFetchTaskExecutorService.shutdown();
			try {
				dataFetchTaskExecutorService.awaitTermination(10, TimeUnit.MINUTES);
			} catch (InterruptedException e) {
				logger.severe(e.getLocalizedMessage());
			}
			logger.info("Fetching data b/w " + new Date(startTimestamp) + " and " + new Date(endTimestamp) + " complete." );
			return new AmChartDataResponse(true, "", amChartData.values(), seriesValueAxisMap);
		}
		return new AmChartDataResponse(false, "API authentication failed", null);
	}

	private class DataFetchTask implements Runnable {

		private long startTimestamp, endTimestamp;
		private DataQualifier dataQualifier;
		private String userName;

		DataFetchTask(String userName, DataQualifier dataQualifier, long startTimestamp, long endTimestamp) {
			this.userName = userName;
			this.dataQualifier = dataQualifier;
			this.startTimestamp = startTimestamp;
			this.endTimestamp = endTimestamp;
		}

		@Override
		public void run() {
			GetDataRequest.Builder getDataRequest = GetDataRequest.newBuilder();
			getDataRequest.setDataStoreName(dataQualifier.getDataStoreName());
			getDataRequest.setArchiveName(dataQualifier.getArchiveName());
			getDataRequest.setStartTimestamp(startTimestamp);
			getDataRequest.setEndTimestamp(endTimestamp);

			String archiveKey = dataQualifier.getArchiveKey();
			long timestamp;
			double value;
			try {
				GetDataResponse data = DataFetcher.getInstance().getData(userName, getDataRequest.build());
				List<DataEntry> dataEntryList = data.getDataEntryList();

				for (DataEntry dataEntry : dataEntryList) {
					timestamp = dataEntry.getTimestamp();
					value = dataEntry.getValue();

					if (amChartData.containsKey(timestamp)) {
						amChartData.get(timestamp).put(archiveKey, value);
					} else {
						Map<String, Double> tuple = new ConcurrentHashMap<String, Double>();
						amChartData.put(timestamp, tuple);
						tuple.put("timestamp", (double) timestamp);
						tuple.put(archiveKey, value);
					}
				}

				setSeriesValueAxisId(archiveKey, dataEntryList);

			} catch (UserDoesNotExists e) {
				e.printStackTrace();
			} catch (TimeOutOfRangeException e) {
				e.printStackTrace();
			}
		}

		private void setSeriesValueAxisId(String archiveKey, List<DataEntry> dataEntryList) {
			int totalDataEntries = dataEntryList.size();
			int sampleCount = SAMPLE_COUNT;
			int offsetCnt = (int) Math.floor(totalDataEntries / sampleCount);

			
			if (offsetCnt == 0) {
				offsetCnt = 1;// collect all samples
				sampleCount = totalDataEntries;
			}

			double sampleSum = 0;
			for (int i = 0; i < totalDataEntries; i += offsetCnt) {
				sampleSum += dataEntryList.get(i).getValue();
			}

			double sampleAverage = sampleSum / sampleCount;

			if (sampleAverage == 0) {
				addSeriesToAxis(archiveKey, "p0");
			} else {
				boolean negative = sampleAverage < 0;
				if (negative) {
					sampleAverage = -sampleAverage;
				}

				int axisId = (int) Math.ceil(Math.log10(sampleAverage));
				if (axisId == 0) {
					addSeriesToAxis(archiveKey, "p0");
				} else {
					String prefix = negative ? "n" : "p";
					addSeriesToAxis(archiveKey, prefix + axisId);
				}
			}
		}
		
		private synchronized void addSeriesToAxis(String seriesId, String axisId){
			if(seriesValueAxisMap.containsKey(axisId)){
				seriesValueAxisMap.get(axisId).add(seriesId);
			}else{
				seriesValueAxisMap.put(axisId, new LinkedList<String>());
				seriesValueAxisMap.get(axisId).add(seriesId);
			}
		}
	}
}
