package com.timenome.engine.http.service.verification;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.timenome.api.logger.TimeNomeLogger;


@Path("/")
public class VerificationService {

	private static TimeNomeLogger logger = TimeNomeLogger.getTimeNomeLogger(VerificationService.class);
	
	@Context HttpServletRequest httpServletRequest;
	@Context HttpServletResponse httpServletResponse;
	
	public VerificationService() {
		// TODO Auto-generated constructor stub
	}

	@GET
	@Path("/{random}")
	@Produces(MediaType.APPLICATION_JSON)
	public boolean verify(@PathParam("random") String randomUrl) {
		VerificationURLHandler verificationURLHandler = VerificationURLHandler.getVerificationURLHandler();
		boolean isVerified = false;
		if(verificationURLHandler.containsUrl(randomUrl)) {
			String email = verificationURLHandler.emailOfUrl(randomUrl);
			logger.info("Verification request received for email : " + email);
			try {
				httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/login.html");
				logger.info("Verified successfully. Redirecting to " + httpServletRequest.getContextPath() + "/login.html");
				isVerified = true;
				verificationURLHandler.removeURL(randomUrl);
			} catch (IOException e) {
				logger.severe("Couldn't redirect to " + httpServletRequest.getContextPath() + "/login.html");
			}
		}
		else {
			try {
				httpServletResponse.sendError(404);
			} catch (IOException e1) {
				logger.severe("Error while redirecting to error 404.");
			}
		}
		return isVerified;
	}
	
}
