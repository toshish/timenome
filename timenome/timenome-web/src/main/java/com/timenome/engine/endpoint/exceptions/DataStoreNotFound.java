package com.timenome.engine.endpoint.exceptions;

public class DataStoreNotFound extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public DataStoreNotFound(String message){
		super(message);
	}

}
