/**
 * 
 */
package com.timenome.engine.endpoint.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;

import javax.websocket.Session;

import com.google.protobuf.GeneratedMessage;
import com.google.protobuf.InvalidProtocolBufferException;
import com.timenome.api.core.User;
import com.timenome.api.exceptions.AuthenticationException;
import com.timenome.api.exceptions.MessageParsingException;
import com.timenome.api.logger.TimeNomeLogger;
import com.timenome.engine.authentication.AuthenticationManager;
import com.timenome.engine.endpoint.enumeration.MessageEnum;
import com.timenome.proto.TimeNomeProtos.Authentication;
import com.timenome.proto.TimeNomeProtos.Error;
import com.timenome.proto.TimeNomeProtos.ExecuteRequest;
import com.timenome.proto.TimeNomeProtos.ExecutionResponse;
import com.timenome.proto.TimeNomeProtos.StatusResponse;

/**
 * Utility class for Endpoints.
 * 
 * @author toshish
 *
 */
public final class EndpointUtils {

	private static TimeNomeLogger logger = TimeNomeLogger.getTimeNomeLogger(EndpointUtils.class);

	public static Error getErrorInstance(int errorCode, String ErrorText) {
		Error error = Error.newBuilder()
				.setErrorCode(errorCode)
				.setErrorText(ErrorText)
				.build();
		return error;
	}

	public static StatusResponse getStatusResponse(boolean status) {
		return EndpointUtils.getStatusResponse(status, null);
	}

	public static StatusResponse getStatusResponse(boolean status, Error error) {
		StatusResponse.Builder statusResponseBuilder = StatusResponse.newBuilder();
		statusResponseBuilder.setIsSuccess(status);
		if(error != null) {
			statusResponseBuilder.setErrorObj(error);
		}
		return statusResponseBuilder.build();
	}

	@SuppressWarnings("unchecked")
	public static <T extends GeneratedMessage> T getEnclosingMessage(
			ByteBuffer byteBuf, MessageEnum messageEnum) throws MessageParsingException {
		ExecuteRequest execRequest = null;
		Class<?> protoBufClass = messageEnum.getProtoBufClass();
		Method getMethod = null;
		try {
			logger.info("get" + protoBufClass.getSimpleName());
			getMethod = ExecuteRequest.class.getMethod("get" + protoBufClass.getSimpleName());
		} catch (NoSuchMethodException | SecurityException e1) {
			throw new MessageParsingException("Error while finding type " + messageEnum.toString(), e1);
		}
		T genMessage = null;
		try {
			execRequest = ExecuteRequest.parseFrom(byteBuf.array());
			try {
				genMessage = (T) getMethod.invoke(execRequest);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				throw new MessageParsingException("Error while executing getter for message.", e);
			}
		} catch (InvalidProtocolBufferException e) {
			throw new MessageParsingException(e);
		}
		return genMessage;
	}

	
	/**
	 * 
	 * @deprecated Use authenticateAPI instead of this.
	 * 
	 * @param byteBuf
	 * @return
	 * @throws MessageParsingException
	 * @throws AuthenticationException
	 */
	@Deprecated
	public static User authenticateUser(ByteBuffer byteBuf) throws MessageParsingException, AuthenticationException {
		ExecuteRequest execRequest;
		try {
			execRequest = ExecuteRequest.parseFrom(byteBuf.array());
		} catch (InvalidProtocolBufferException e) {
			throw new MessageParsingException(e);
		}
		Authentication authentication = execRequest.getAuthentication();
		User user = AuthenticationManager.getInstance().authenticateUser(authentication.getKey(), authentication.getSecret());
		if(user == null) {
			throw new AuthenticationException("Authentication failed for User " + authentication.getKey() + ".");
		}
		return user;
	}

	public static User authenticateAPI(ByteBuffer byteBuf) throws MessageParsingException, AuthenticationException {
		ExecuteRequest execRequest;
		try {
			execRequest = ExecuteRequest.parseFrom(byteBuf.array());
		} catch (InvalidProtocolBufferException e) {
			throw new MessageParsingException(e);
		}
		Authentication authentication = execRequest.getAuthentication();
		User user = AuthenticationManager.getInstance().authenticateAPI(authentication.getKey(), authentication.getSecret());
		if(user == null) {
			throw new AuthenticationException("Authentication failed for User " + authentication.getKey() + ".");
		}
		return user;
	}

	public static void sendExecutionResponse(StatusResponse statusResponse,
			MessageEnum messageEnum, GeneratedMessage response, Session session) throws MessageParsingException {
		ExecutionResponse.Builder execResponseBuilder = ExecutionResponse.newBuilder();
		execResponseBuilder.setStatusResponse(statusResponse);
		if(response != null ) {
			Class<?> protoBufClass = messageEnum.getProtoBufClass();
			Method setMethod = null;
			try {
				setMethod = ExecutionResponse.Builder.class.getMethod("set" + protoBufClass.getSimpleName(), protoBufClass);
			} catch (NoSuchMethodException | SecurityException e1) {
				throw new MessageParsingException("Error while finding type " + messageEnum.toString(), e1);
			}
			try {
				setMethod.invoke(execResponseBuilder, protoBufClass.cast(response));
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				throw new MessageParsingException("Error while executing setter for message.", e);
			}
		}
		ExecutionResponse execResponse = execResponseBuilder.build();
		session.getAsyncRemote().sendBinary(ByteBuffer.wrap(execResponse.toByteArray()));
	}

	public static void sendStatusResponse(StatusResponse statusResponse, Session session) {
		if(statusResponse == null) {
			//			throw 
		}
		ExecutionResponse.Builder execResponseBuilder = ExecutionResponse.newBuilder();
		execResponseBuilder.setStatusResponse(statusResponse);
		ExecutionResponse execResponse = execResponseBuilder.build();
		session.getAsyncRemote().sendBinary(ByteBuffer.wrap(execResponse.toByteArray()));
	}
}
