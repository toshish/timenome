/**
 * 
 */
package com.timenome.engine.authentication.filters;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.timenome.api.logger.TimeNomeLogger;
import com.timenome.engine.context.TimeNomeConstants;

/**
 * @author toshish
 * 
 */
public class LoginFilter extends HtmlFilter {
	private static TimeNomeLogger logger = TimeNomeLogger.getTimeNomeLogger(LoginFilter.class);

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain filterChain) throws IOException,
			ServletException {
		super.doFilter(req, resp, filterChain);
		HttpServletRequest httpServletRequest = (HttpServletRequest) req;
		
		if (isAuthorized) {
			logger.info("Authorized request for login.html. Redirecting to index.html");
			((HttpServletResponse) resp).sendRedirect(httpServletRequest.getContextPath() + "/" + TimeNomeConstants.INDEX_PAGE);
		} else {
			logger.info("Unauthorized request for login.html. Letting it pass");
			filterChain.doFilter(req, resp);
		}
	}
}
