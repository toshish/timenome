package com.timenome.engine.context;

public class TimeNomeConstants {

	public static final String IS_AUTHORIZED = "isAuthorized";
	
	public static final String USERNAME = "username";
	public static final String PASSWORD = "password";
	public static final String USER_TOKEN = "userToken";
	
	//Pages
	public static final String LOGIN_PAGE = "login.html";
	public static final String INDEX_PAGE = "index.html";
}
