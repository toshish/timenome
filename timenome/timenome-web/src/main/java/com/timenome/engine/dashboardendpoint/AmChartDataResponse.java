package com.timenome.engine.dashboardendpoint;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public class AmChartDataResponse {
	private boolean success;
	private String errorMsg;
	private Collection<Map<String, Double>> amChartData;
	private Map<String, List<String>> seriesValueAxisMap;

	public AmChartDataResponse(boolean success, String errorMsg, Collection<Map<String, Double>> amChartData) {
		this(success, errorMsg, amChartData, null);
	}

	public AmChartDataResponse(boolean success, String errorMsg, Collection<Map<String, Double>> amChartData,
			Map<String, List<String>> seriesValueAxisMap) {
		super();
		this.success = success;
		this.errorMsg = errorMsg;
		this.amChartData = amChartData;
		this.seriesValueAxisMap = seriesValueAxisMap;
	}
	
	
	public Map<String, List<String>> getSeriesValueAxisMap() {
		return seriesValueAxisMap;
	}
	
	public void setSeriesValueAxisMap(Map<String, List<String>> seriesValueAxisMap) {
		this.seriesValueAxisMap = seriesValueAxisMap;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public Collection<Map<String, Double>> getAmChartData() {
		return amChartData;
	}

	public void setAmChartData(Collection<Map<String, Double>> amChartData) {
		this.amChartData = amChartData;
	}
}
