/**
 * 
 */
package com.timenome.engine.endpoint.enumeration;

import com.timenome.proto.TimeNomeProtos.CreateArchiveRequest;
import com.timenome.proto.TimeNomeProtos.CreateDataStoreRequest;
import com.timenome.proto.TimeNomeProtos.DeleteArchiveRequest;
import com.timenome.proto.TimeNomeProtos.DeleteDataStoreRequest;
import com.timenome.proto.TimeNomeProtos.GetDataRequest;
import com.timenome.proto.TimeNomeProtos.GetDataResponse;
import com.timenome.proto.TimeNomeProtos.GetDataStoreRequest;
import com.timenome.proto.TimeNomeProtos.GetDataStoreResponse;
import com.timenome.proto.TimeNomeProtos.ListAllDataStoreNamesRequest;
import com.timenome.proto.TimeNomeProtos.ListAllDataStoreNamesResponse;
import com.timenome.proto.TimeNomeProtos.StreamDataRequest;

/**
 * @author toshish
 *
 */
public enum MessageEnum {
	
	CREATE_DATASTORE_REQUEST(CreateDataStoreRequest.class),
	DELETE_DATASTORE_REQUEST(DeleteDataStoreRequest.class),
	CREATE_ARCHIVE_REQUEST(CreateArchiveRequest.class),
	DELETE_ARCHIVE_REQUEST(DeleteArchiveRequest.class),
	LIST_ALL_DATASTORE_NAMES_REQUEST(ListAllDataStoreNamesRequest.class),
	GET_DATASTORE_REQUEST(GetDataStoreRequest.class),
	GET_DATA_REQUEST(GetDataRequest.class),
	STREAM_DATA_REQUEST(StreamDataRequest.class),
	
	GET_DATA_RESPONSE(GetDataResponse.class),
	STREAM_DATA_RESPONSE(GetDataResponse.class),
	LIST_ALL_DATA_STORE_NAMES_RESPONSE(ListAllDataStoreNamesResponse.class),
	GET_DATA_STORE_RESPONSE(GetDataStoreResponse.class);

	private Class<?> protoBufClass;

	/**
	 * @param protoBufClass
	 */
	private MessageEnum(Class<?> protoBufClass) {
		this.setProtoBufClass(protoBufClass);
	}

	/**
	 * @return the protoBufClass
	 */
	public Class<?> getProtoBufClass() {
		return protoBufClass;
	}

	/**
	 * @param protoBufClass the protoBufClass to set
	 */
	private void setProtoBufClass(Class<?> protoBufClass) {
		this.protoBufClass = protoBufClass;
	}
}
