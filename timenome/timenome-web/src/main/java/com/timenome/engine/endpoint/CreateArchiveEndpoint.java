package com.timenome.engine.endpoint;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.logging.Level;

import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import com.timenome.api.BuiltInCDF;
import com.timenome.api.TimeUnit;
import com.timenome.api.core.DataStore;
import com.timenome.api.core.TimeRangeDef;
import com.timenome.api.core.User;
import com.timenome.api.exceptions.ArchiveException;
import com.timenome.api.exceptions.AuthenticationException;
import com.timenome.api.exceptions.MessageParsingException;
import com.timenome.api.exceptions.UserDoesNotExists;
import com.timenome.api.logger.TimeNomeLogger;
import com.timenome.engine.context.TimeNomeInits;
import com.timenome.engine.endpoint.enumeration.MessageEnum;
import com.timenome.engine.endpoint.utils.EndpointUtils;
import com.timenome.engine.error.ErrorCodes;
import com.timenome.proto.TimeNomeProtos.Archive;
import com.timenome.proto.TimeNomeProtos.CreateArchiveRequest;
import com.timenome.proto.TimeNomeProtos.StatusResponse;

@ServerEndpoint("/create/archive")
public class CreateArchiveEndpoint {
	private static TimeNomeLogger logger = TimeNomeLogger.getTimeNomeLogger(CreateArchiveEndpoint.class);

	@OnOpen
	public void onOpen(Session session) {

	}
	@OnError
	public void onError(Session session, Throwable t) {
		logger.log(Level.SEVERE, "", t);
		try {
			session.close();
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error while closing Session.", e);
		}
	}

	@OnMessage
	public void onMessage(Session session, ByteBuffer byteBuf, boolean last) {
		logger.entering("CreateArchiveEndpoint", "onMessage");
		CreateArchiveRequest createArchiveRequest = null;
		StatusResponse statusResponse = null;
		DataStore dataStore = null;
		try {
			User user = EndpointUtils.authenticateAPI(byteBuf);
			createArchiveRequest = EndpointUtils.getEnclosingMessage(byteBuf, MessageEnum.CREATE_ARCHIVE_REQUEST);
			dataStore = TimeNomeInits.getConnection().getDataStore(createArchiveRequest.getDataStoreName(), user.getName());
			if(dataStore != null) {
				// Create the archive now.
				Archive archive = createArchiveRequest.getArchive();
				String archiveName = archive.getArchiveName();
				TimeRangeDef storageRange = new TimeRangeDef(TimeUnit.MILLI_SECOND, 0l);
				TimeRangeDef storagePrecision = new TimeRangeDef(TimeUnit.MILLI_SECOND, archive.getStoragePrecision());
				BuiltInCDF cdf = BuiltInCDF.valueOf(archive.getAggregationFunction().toString());
				try {
					dataStore.addArchive(archiveName, storageRange, storagePrecision, cdf);
					statusResponse = EndpointUtils.getStatusResponse(true);
				} catch (ArchiveException e) {
					statusResponse = EndpointUtils.getStatusResponse(false, 
							EndpointUtils.getErrorInstance(ErrorCodes.ARCHIVE_ERROR, e.getLocalizedMessage()));
					logger.info(statusResponse.toString());
				}
			}
			else {
				// Didn't find the data store in which the archive is to be created.
				statusResponse = EndpointUtils.getStatusResponse(false, 
						EndpointUtils.getErrorInstance(ErrorCodes.ENTITY_DOES_NOT_EXIST, "DataStore " + 
				createArchiveRequest.getDataStoreName() + " does not exist."));
				logger.info(statusResponse.toString());
			}
			
		} catch (MessageParsingException | AuthenticationException e) {
			statusResponse = EndpointUtils.getStatusResponse(false, 
					EndpointUtils.getErrorInstance(ErrorCodes.INVALID_MESSAGE_ERROR, e.getLocalizedMessage()));
			logger.info(statusResponse.toString());
		} catch (UserDoesNotExists e) {
			statusResponse = EndpointUtils.getStatusResponse(false, 
					EndpointUtils.getErrorInstance(ErrorCodes.ENTITY_DOES_NOT_EXIST, e.getLocalizedMessage()));
			logger.info(statusResponse.toString());
		}
		logger.info(statusResponse.toString());
		
		EndpointUtils.sendStatusResponse(statusResponse, session);
		logger.exiting("CreateArchiveEndpoint", "onMessage");
	}
}
