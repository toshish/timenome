package com.timenome.engine.endpoint;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.logging.Level;

import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import com.timenome.proto.TimeNomeProtos.DeleteArchiveRequest;
import com.timenome.proto.TimeNomeProtos.StatusResponse;
import com.timenome.api.core.DataStore;
import com.timenome.api.core.User;
import com.timenome.api.exceptions.ArchiveException;
import com.timenome.api.exceptions.AuthenticationException;
import com.timenome.api.exceptions.MessageParsingException;
import com.timenome.api.exceptions.UserDoesNotExists;
import com.timenome.api.logger.TimeNomeLogger;
import com.timenome.engine.context.TimeNomeInits;
import com.timenome.engine.endpoint.enumeration.MessageEnum;
import com.timenome.engine.endpoint.utils.EndpointUtils;
import com.timenome.engine.error.ErrorCodes;

@ServerEndpoint("/delete/archive")
public class DeleteArchiveEndpoint {
	private static TimeNomeLogger logger = TimeNomeLogger.getTimeNomeLogger(DeleteArchiveEndpoint.class);
	@OnOpen
	public void onOpen(Session session) {

	}
	@OnError
	public void onError(Session session, Throwable t) {
		logger.log(Level.SEVERE, "", t);
		try {
			session.close();
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error while closing Session.", e);
		}
	}

	@OnMessage
	public void onMessage(Session session, ByteBuffer byteBuf, boolean last) {
		DeleteArchiveRequest deleteArchiveRequest = null;
		StatusResponse statusResponse = null;
		try {
			User user = EndpointUtils.authenticateAPI(byteBuf);
			deleteArchiveRequest = EndpointUtils.getEnclosingMessage(byteBuf, MessageEnum.DELETE_ARCHIVE_REQUEST);
			DataStore dataStore = TimeNomeInits.getConnection().getDataStore(deleteArchiveRequest.getDataStoreName(), user.getName());
			if(dataStore != null) {
				dataStore.removeArchive(deleteArchiveRequest.getArchiveName());
				statusResponse = EndpointUtils.getStatusResponse(true);
			}
			else {
				statusResponse = EndpointUtils.getStatusResponse(false, 
						EndpointUtils.getErrorInstance(ErrorCodes.ENTITY_DOES_NOT_EXIST, "DataStore " + deleteArchiveRequest.getDataStoreName() + " doesn't exist."));
			}
		} catch(UserDoesNotExists e2) {
			statusResponse = EndpointUtils.getStatusResponse(false, 
					EndpointUtils.getErrorInstance(ErrorCodes.ENTITY_DOES_NOT_EXIST, e2.getLocalizedMessage()));
		} catch(MessageParsingException e) {
			statusResponse = EndpointUtils.getStatusResponse(false, 
					EndpointUtils.getErrorInstance(ErrorCodes.INVALID_MESSAGE_ERROR, e.getLocalizedMessage()));
		} catch(AuthenticationException e) {
			statusResponse = EndpointUtils.getStatusResponse(false, 
					EndpointUtils.getErrorInstance(ErrorCodes.INVALID_AUTHENTICATION_CREDENTIALS, e.getLocalizedMessage()));
		} catch(ArchiveException e) {
			statusResponse = EndpointUtils.getStatusResponse(false, 
					EndpointUtils.getErrorInstance(ErrorCodes.ARCHIVE_ERROR, e.getLocalizedMessage()));
		}
		logger.info(statusResponse.toString());
		EndpointUtils.sendStatusResponse(statusResponse, session);
	}
}
