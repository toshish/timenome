/**
 * 
 */
package com.timenome.engine.http.service;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.timenome.api.core.User;
import com.timenome.api.exceptions.UserAlreadyExists;
import com.timenome.api.logger.TimeNomeLogger;
import com.timenome.engine.authentication.AuthenticationManager;
import com.timenome.engine.context.TimeNomeConstants;
import com.timenome.engine.error.ErrorCodes;
import com.timenome.engine.userservice.ApiCredentialType;
import com.timenome.engine.userservice.ErrorType;
import com.timenome.engine.userservice.SignupRequestType;
import com.timenome.engine.userservice.SignupResponseType;
import com.timenome.engine.userservice.UserInfo;
import com.timenome.engine.userservice.UserPreferenceTuple;

/**
 * @author toshish
 * 
 */
@Path("/userservice")
public class UserService {
	@Context
	ServletContext servletContext;
	@Context
	HttpServletRequest httpServletRequest;

	private AuthenticationManager authenticationManager = AuthenticationManager.getInstance();
	private static TimeNomeLogger logger = TimeNomeLogger.getTimeNomeLogger(UserService.class);

	private static Map<String, UserInfo> userTokenMap = new ConcurrentHashMap<>();

	private String createNewUserToken() {
		return new BigInteger(20, new SecureRandom()).toString(16);
	}

	@POST
	@Path("/set/preferences")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean setUserPreferences(List<UserPreferenceTuple> userPreferences) {
		String userToken = (String) httpServletRequest.getSession().getAttribute(TimeNomeConstants.USER_TOKEN);
		if (userTokenMap.containsKey(userToken)) {
			String email = userTokenMap.get(userToken).getUserName();
			String password = userTokenMap.get(userToken).getPassword();
			return authenticationManager.setUserPreferences(email, password, userPreferences);
		} else {
			return false;//TODO: throw exception instead
		}
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/get/preferences")
	public Map<String, String> getUserPreferences() {
		String userToken = (String) httpServletRequest.getSession().getAttribute(TimeNomeConstants.USER_TOKEN);
		if (userTokenMap.containsKey(userToken)) {
			String email = userTokenMap.get(userToken).getUserName();
			String password = userTokenMap.get(userToken).getPassword();

			HashMap<String, String> preferences = authenticationManager.getUserPreferences(email, password);

			return preferences;
		} else {
			return null;//TODO: throw exception instead
		}
	}

	@GET
	@Path("/get/apicredentials")
	@Produces(MediaType.APPLICATION_JSON)
	public ApiCredentialType getApiCredentials() {
		String userToken = (String) httpServletRequest.getSession().getAttribute(TimeNomeConstants.USER_TOKEN);
		return userTokenMap.get(userToken).getApiCrendential();
	}

	@GET
	@Path("/get/userinfo")
	@Produces(MediaType.APPLICATION_JSON)
	public String getUserName() {
		String userToken = (String) httpServletRequest.getSession().getAttribute(TimeNomeConstants.USER_TOKEN);
		return userTokenMap.get(userToken).getUserName();
	}

	@POST
	@Path("/login")
	@Produces(MediaType.APPLICATION_JSON)
	public boolean login() {
		String email = (String) httpServletRequest.getAttribute(TimeNomeConstants.USERNAME);
		String password = (String) httpServletRequest.getAttribute(TimeNomeConstants.PASSWORD);
		boolean isAuthorized = (boolean) httpServletRequest.getSession().getAttribute(TimeNomeConstants.IS_AUTHORIZED);

		if (!isAuthorized) {
			return false;
		} else {
			if (email != null && password != null
					&& httpServletRequest.getSession().getAttribute(TimeNomeConstants.USER_TOKEN) == null) {
				String userToken = createNewUserToken();
				httpServletRequest.getSession().setAttribute(TimeNomeConstants.USER_TOKEN, userToken);
				UserInfo userInfo = new UserInfo();
				userInfo.setUserName(email);
				userInfo.setPassword(password);
				userInfo.setApiCrendential(getApiCredentials(email, password));
				userTokenMap.put(userToken, userInfo);
			}
			return true;
		}
	}

	@POST
	@Path("/logout")
	@Produces(MediaType.APPLICATION_JSON)
	public boolean logout() {
		Object userToken = httpServletRequest.getSession().getAttribute(TimeNomeConstants.USER_TOKEN);
		if (userToken != null) {
			String userTokenStr = (String) userToken;
			userTokenMap.remove(userTokenStr);
			httpServletRequest.getSession().invalidate();
		}
		return true;
	}

	@POST
	@Path("/signup")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
	@Produces(MediaType.APPLICATION_JSON)
	public SignupResponseType signup(SignupRequestType signupRequest) {
		String email = (String) httpServletRequest.getAttribute(TimeNomeConstants.USERNAME);
		String password = (String) httpServletRequest.getAttribute(TimeNomeConstants.PASSWORD);
		boolean isAuthorized = (boolean) httpServletRequest.getSession().getAttribute(TimeNomeConstants.IS_AUTHORIZED);

		if (isAuthorized) {
			logger.info("Existing user authenticated : " + email);
		}

		String name = signupRequest.getName();
		SignupResponseType signupResponse = new SignupResponseType();
		try {
			User user = AuthenticationManager.getInstance().createUser(email, password, name);
			if (user != null) {
				logger.info("User with email id '" + email + "' created.");
				signupResponse.setApiCredential(getApiCredentials(email, password));
				VerificationMailSender verificationMailSender = new VerificationMailSender("http://" + httpServletRequest.getLocalName() + ":"
						+ httpServletRequest.getLocalPort() + "/timenome/verify/", email);
				verificationMailSender.start();
			}
		} catch (UserAlreadyExists e) {
			ErrorType error = new ErrorType();
			error.setErrorCode(ErrorCodes.ENTITY_ALREADY_EXIST);
			error.setErrorText("User with email id '" + email + "' already exists.");
			signupResponse.setError(error);
		}

		return signupResponse;
	}

	

	private ApiCredentialType getApiCredentials(String email, String password) {
		ApiCredentialType apiCredentials = new ApiCredentialType();

		String[] apiCreds = AuthenticationManager.getAPICredentials(email, password);

		if (apiCreds.length == 2 && apiCreds[0] != null && apiCreds[1] != null) {
			apiCredentials.setKey(apiCreds[0]);
			apiCredentials.setSecret(apiCreds[1]);
		} else {
			logger.severe("Couldn't fetch API credentials for user: " + email);
		}
		return apiCredentials;
	}
}
