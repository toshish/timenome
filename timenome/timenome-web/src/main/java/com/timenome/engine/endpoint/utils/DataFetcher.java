package com.timenome.engine.endpoint.utils;

import java.util.Iterator;
import java.util.List;

import com.timenome.api.Entry;
import com.timenome.api.TimeUnit;
import com.timenome.api.core.Archive;
import com.timenome.api.core.DataStore;
import com.timenome.api.core.TimeRangeDef;
import com.timenome.api.exceptions.TimeOutOfRangeException;
import com.timenome.api.exceptions.UserDoesNotExists;
import com.timenome.engine.context.TimeNomeInits;
import com.timenome.proto.TimeNomeProtos.DataEntry;
import com.timenome.proto.TimeNomeProtos.GetDataRequest;
import com.timenome.proto.TimeNomeProtos.GetDataResponse;

public class DataFetcher {
	
	private static DataFetcher dataFetcher = new DataFetcher();
	
	public static DataFetcher getInstance(){
		return dataFetcher;
	}
	
	private DataFetcher(){
		
	}
	
	public GetDataResponse getData(String userName, GetDataRequest getDataRequest) throws UserDoesNotExists, TimeOutOfRangeException{
		GetDataResponse.Builder responseBuilder = GetDataResponse.newBuilder();
		
		long startTimeStamp = getDataRequest.getStartTimestamp();
		long endTimeStamp = getDataRequest.getEndTimestamp();

		String dataStoreName = getDataRequest.getDataStoreName();
		String archiveName = getDataRequest.getArchiveName();

		DataStore dataStore = null;
		dataStore = TimeNomeInits.getConnection().getDataStore(dataStoreName, userName);
		TimeRangeDef startTime = new TimeRangeDef(TimeUnit.MILLI_SECOND, startTimeStamp);
		TimeRangeDef endTime = new TimeRangeDef(TimeUnit.MILLI_SECOND, endTimeStamp);

		Archive archive = dataStore.getArchive(archiveName);

		List<Entry<? extends Number>> entries = null;
		entries = archive.getData(startTime, endTime);

		if (entries != null) {
			Iterator<Entry<? extends Number>> iter = entries.iterator();
			while (iter.hasNext()) {
				DataEntry.Builder dataEntryBuilder = DataEntry.newBuilder();
				Entry<? extends Number> entry = iter.next();
				dataEntryBuilder.setTimestamp(entry.getTimeStamp());
				if (entry.getValue() != null) {
					dataEntryBuilder.setValue((Double) entry.getValue());
				}
				responseBuilder.addDataEntry(dataEntryBuilder.build());
			}
		}
		responseBuilder.setDataStoreName(dataStoreName);
		responseBuilder.setArchiveName(archiveName);
		
		return responseBuilder.build();
	}

}
