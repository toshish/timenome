/**
 * 
 */
package com.timenome.engine.authentication.utils;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.timenome.api.logger.TimeNomeLogger;

/**
 * @author toshish
 *
 */
public class AuthenticationUtils {
	private static TimeNomeLogger logger = TimeNomeLogger.getTimeNomeLogger(AuthenticationUtils.class);
	
	public static String getSHA1HashOf(String string) {
		MessageDigest cript = null;
		try {
			cript = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException e1) {
			logger.severe("SHA-1 is not supported as message digest algorithm.\n" + e1.getLocalizedMessage());
		}
        cript.reset();
        try {
			cript.update(string.getBytes("utf8"));
		} catch (UnsupportedEncodingException e) {
			logger.severe("utf8 is not supported as encoding format." + e.getLocalizedMessage());
		}
        return new BigInteger(1, cript.digest()).toString(16);
	}
}
