/**
 * 
 */
package com.timenome.engine.authentication.filters;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.timenome.api.logger.TimeNomeLogger;
import com.timenome.engine.context.TimeNomeConstants;

/**
 * @author toshish
 * 
 */
public class IndexFilter extends HtmlFilter {

	private static TimeNomeLogger logger = TimeNomeLogger.getTimeNomeLogger(IndexFilter.class);

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain filterChain) throws IOException,
			ServletException {
		super.doFilter(req, resp, filterChain);

		HttpServletRequest httpServletRequest = (HttpServletRequest) req;

		if (isAuthorized) {
			logger.info("Authorized request for index.html. Letting it pass");

			filterChain.doFilter(req, resp);
		} else {
			logger.info("Unauthorized request for index.html. Redirecting to login");
			((HttpServletResponse) resp).sendRedirect(httpServletRequest.getContextPath() + "/"
					+ TimeNomeConstants.LOGIN_PAGE);
		}
	}
}
