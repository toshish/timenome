package com.timenome.engine.endpoint;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.logging.Level;

import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import com.timenome.api.core.User;
import com.timenome.api.exceptions.AuthenticationException;
import com.timenome.api.exceptions.DataStoreException;
import com.timenome.api.exceptions.MessageParsingException;
import com.timenome.api.exceptions.UserDoesNotExists;
import com.timenome.api.logger.TimeNomeLogger;
import com.timenome.engine.context.TimeNomeInits;
import com.timenome.engine.endpoint.enumeration.MessageEnum;
import com.timenome.engine.endpoint.utils.EndpointUtils;
import com.timenome.engine.error.ErrorCodes;
import com.timenome.proto.TimeNomeProtos.DeleteDataStoreRequest;
import com.timenome.proto.TimeNomeProtos.StatusResponse;

@ServerEndpoint("/delete/datastore")
public class DeleteDataStoreEndpoint {
	private static TimeNomeLogger logger = TimeNomeLogger.getTimeNomeLogger(DeleteDataStoreEndpoint.class);
	@OnOpen
	public void onOpen(Session session) {
		
	}
	@OnError
	public void onError(Session session, Throwable t) {
		logger.log(Level.SEVERE, "", t);
		try {
			session.close();
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error while closing Session.", e);
		}
	}
	
	@OnMessage
	public void onMessage(Session session, ByteBuffer byteBuf, boolean last) {
		DeleteDataStoreRequest deleteDS = null;
		StatusResponse statusResponse = null;
		
		try {
			User user = EndpointUtils.authenticateAPI(byteBuf);
			deleteDS = EndpointUtils.getEnclosingMessage(byteBuf, MessageEnum.DELETE_DATASTORE_REQUEST);
			boolean isDeleted = TimeNomeInits.getConnection().deleteDataStore(deleteDS.getDataStoreName(), user.getName());
			if(isDeleted) {
				statusResponse = EndpointUtils.getStatusResponse(true);
			}
			else {
				statusResponse = EndpointUtils.getStatusResponse(false, 
						EndpointUtils.getErrorInstance(ErrorCodes.UNKNOWN_ERROR, "Unknown error in deleting Data Store."));
			}
		}  catch (DataStoreException e) {
			statusResponse = EndpointUtils.getStatusResponse(false, 
					EndpointUtils.getErrorInstance(ErrorCodes.DATA_STORE_ERROR, e.getLocalizedMessage()));
		} catch (UserDoesNotExists e) {
			statusResponse = EndpointUtils.getStatusResponse(false, 
					EndpointUtils.getErrorInstance(ErrorCodes.ENTITY_DOES_NOT_EXIST, e.getLocalizedMessage()));
		} catch (MessageParsingException e) {
			statusResponse = EndpointUtils.getStatusResponse(false, 
					EndpointUtils.getErrorInstance(ErrorCodes.INVALID_MESSAGE_ERROR, e.getLocalizedMessage()));
		} catch (AuthenticationException e) {
			statusResponse = EndpointUtils.getStatusResponse(false, 
					EndpointUtils.getErrorInstance(ErrorCodes.INVALID_AUTHENTICATION_CREDENTIALS, e.getLocalizedMessage()));
		}
		logger.info(statusResponse.toString());
		
		EndpointUtils.sendStatusResponse(statusResponse, session);
		
	}
}
