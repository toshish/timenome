package com.timenome.engine.endpoint;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.logging.Level;

import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import com.google.protobuf.InvalidProtocolBufferException;
import com.timenome.api.exceptions.UserDoesNotExists;
import com.timenome.api.logger.TimeNomeLogger;
import com.timenome.engine.authentication.AuthenticationManager;
import com.timenome.engine.error.ErrorCodes;
import com.timenome.proto.TimeNomeProtos.Authentication;
import com.timenome.proto.TimeNomeProtos.Error;
import com.timenome.proto.TimeNomeProtos.StatusResponse;

@ServerEndpoint("/delete/user")
public class DeleteUserEndpoint {
	private static TimeNomeLogger logger = TimeNomeLogger.getTimeNomeLogger(DeleteUserEndpoint.class);
	
	@OnOpen
	public void onOpen(Session session) {
		
	}
	
	@OnError
	public void onError(Session session, Throwable t) {
		logger.log(Level.SEVERE, "", t);
		try {
			session.close();
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error while closing Session.", e);
		}
	}
	
	@OnMessage
	public void onMessage(Session session, ByteBuffer byteBuf, boolean last) {
		logger.entering("DeleteUserEndpoint", "onMessage");
		Authentication authentication = null;
		try {
			authentication = Authentication.parseFrom(byteBuf.array());
		} catch (InvalidProtocolBufferException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String username = authentication.getKey();
		String authToken = authentication.getSecret();
		StatusResponse.Builder aRBuilder = StatusResponse.newBuilder();
		boolean isUserDeleted = false;
		try {
			AuthenticationManager authenticationManager = AuthenticationManager.getInstance();
			isUserDeleted = authenticationManager.deleteUser(username, authToken);
			
			if(isUserDeleted) {
				aRBuilder.setIsSuccess(true);
			}
			else {
				logger.severe("Unexepected situation occured. null instance of user should not be retured.");
				aRBuilder.setIsSuccess(false);
				aRBuilder.setErrorObj(Error.newBuilder()
											.setErrorCode(ErrorCodes.UNKNOWN_ERROR)
											.setErrorText("Unexepected error occured while creating new user.")
											.build());
			}
		} catch (UserDoesNotExists e) {
			logger.warning(e.getLocalizedMessage());
			aRBuilder.setIsSuccess(false);
			aRBuilder.setErrorObj(Error.newBuilder()
										.setErrorCode(ErrorCodes.ENTITY_DOES_NOT_EXIST)
										.setErrorText(e.getLocalizedMessage())
										.build());
		}
		StatusResponse aR = aRBuilder.build();
		session.getAsyncRemote().sendBinary(ByteBuffer.wrap(aR.toByteArray()));
		logger.exiting("DeleteUserEndpoint", "onMessage");
	}
}
