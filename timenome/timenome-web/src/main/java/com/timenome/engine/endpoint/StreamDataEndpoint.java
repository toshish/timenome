package com.timenome.engine.endpoint;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.logging.Level;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import com.timenome.api.core.User;
import com.timenome.api.exceptions.AuthenticationException;
import com.timenome.api.exceptions.MessageParsingException;
import com.timenome.api.exceptions.UserDoesNotExists;
import com.timenome.api.logger.TimeNomeLogger;
import com.timenome.engine.endpoint.enumeration.MessageEnum;
import com.timenome.engine.endpoint.exceptions.ArchiveNotFound;
import com.timenome.engine.endpoint.exceptions.DataStoreNotFound;
import com.timenome.engine.endpoint.utils.ArchiveStreamManager;
import com.timenome.engine.endpoint.utils.EndpointUtils;
import com.timenome.engine.endpoint.utils.StreamReceiver;
import com.timenome.engine.error.ErrorCodes;
import com.timenome.proto.TimeNomeProtos.DataEntry;
import com.timenome.proto.TimeNomeProtos.GetDataResponse;
import com.timenome.proto.TimeNomeProtos.StatusResponse;
import com.timenome.proto.TimeNomeProtos.StreamDataRequest;

@ServerEndpoint("/stream/data")
public class StreamDataEndpoint {
	private static TimeNomeLogger logger = TimeNomeLogger.getTimeNomeLogger(StreamDataEndpoint.class);
	private String streamId = null;

	@OnOpen
	public void onOpen(Session session) {
	}

	@OnError
	public void onError(Session session, Throwable t) {
		logger.log(Level.SEVERE, "", t);
		try {
			session.close();
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error while closing Session.", e);
		}

		if (streamId != null) {
			try {
				ArchiveStreamManager.getInstance().stopStreaming(streamId);
			} catch (UserDoesNotExists | ArchiveNotFound | DataStoreNotFound e) {
			}
		}
	}

	@OnClose
	public void onClose() {
		if (streamId != null) {
			try {
				ArchiveStreamManager.getInstance().stopStreaming(streamId);
			} catch (UserDoesNotExists | ArchiveNotFound | DataStoreNotFound e) {
			}
		}
	}

	@OnMessage
	public void onMessage(final Session session, ByteBuffer byteBuf, boolean last) {
		StreamDataRequest streamDataRequest = null;
		StatusResponse statusResponse = null;

		User user = null;
		try {
			user = EndpointUtils.authenticateAPI(byteBuf);
			streamDataRequest = EndpointUtils.getEnclosingMessage(byteBuf, MessageEnum.STREAM_DATA_REQUEST);
		} catch (MessageParsingException e) {
			statusResponse = EndpointUtils.getStatusResponse(false,
					EndpointUtils.getErrorInstance(ErrorCodes.INVALID_MESSAGE_ERROR, e.getLocalizedMessage()));
		} catch (AuthenticationException e) {
			statusResponse = EndpointUtils.getStatusResponse(false,
					EndpointUtils.getErrorInstance(ErrorCodes.INVALID_MESSAGE_ERROR, e.getLocalizedMessage()));
		}

		String dataStoreName = streamDataRequest.getDataStoreName();
		String archiveName = streamDataRequest.getArchiveName();
		String userName = user.getName();

		try {
			streamId = ArchiveStreamManager.getInstance().startStreaming(dataStoreName, archiveName, userName,
					new StreamReceiver() {

						@Override
						public void onStreamReceived(String dataStoreName, String archiveName,
								Collection<DataEntry> dataEntries) {
							GetDataResponse.Builder getDataResponseBuilder = GetDataResponse.newBuilder();
							getDataResponseBuilder.setDataStoreName(dataStoreName);
							getDataResponseBuilder.setArchiveName(archiveName);
							getDataResponseBuilder.addAllDataEntry(dataEntries);

							StatusResponse statusResponse = EndpointUtils.getStatusResponse(true);

							try {
								EndpointUtils.sendExecutionResponse(statusResponse, MessageEnum.STREAM_DATA_RESPONSE,
										getDataResponseBuilder.build(), session);
							} catch (MessageParsingException e) {
								e.printStackTrace();
							}
						}
					});

			statusResponse = EndpointUtils.getStatusResponse(true);
		} catch (UserDoesNotExists e) {
			statusResponse = EndpointUtils.getStatusResponse(false,
					EndpointUtils.getErrorInstance(ErrorCodes.ENTITY_DOES_NOT_EXIST, e.getLocalizedMessage()));
		} catch (ArchiveNotFound e) {
			statusResponse = EndpointUtils.getStatusResponse(
					false,
					EndpointUtils.getErrorInstance(ErrorCodes.ENTITY_DOES_NOT_EXIST, "Archive" + " with name "
							+ archiveName + " does not exist in DataStore " + dataStoreName + "."));
		} catch (DataStoreNotFound e) {
			statusResponse = EndpointUtils.getStatusResponse(
					false,
					EndpointUtils.getErrorInstance(ErrorCodes.ENTITY_DOES_NOT_EXIST, "DataStore" + " with name "
							+ dataStoreName + " does not exist."));
		}

		logger.info(statusResponse.toString());
		try {
			EndpointUtils.sendExecutionResponse(statusResponse, MessageEnum.STREAM_DATA_RESPONSE, null, session);
		} catch (MessageParsingException e) {
			logger.severe(e.getLocalizedMessage());
		}
	}
}