package com.timenome.engine.endpoint.utils;

import java.util.Collection;

import com.timenome.proto.TimeNomeProtos.DataEntry;

public interface StreamReceiver {
	void onStreamReceived(String dataStoreName, String archiveName, Collection<DataEntry> dataEntries);
}
