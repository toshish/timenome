package com.timenome.engine.error;

public final class ErrorCodes {
	
	public static int UNKNOWN_ERROR = 1;
	
	public static final int TIME_OUT_OF_RANGE_ERROR = 11;
	
	public static int ENTITY_ALREADY_EXIST = 31;
	public static int ENTITY_DOES_NOT_EXIST = 32;
	public static int ENTITY_CREATION_ERROR = 33;
	public static int ENTITY_DELETION_ERROR = 34;
	
	public static int DATA_STORE_ERROR = 41;
	public static int ARCHIVE_ERROR = 42;
	
	public static final int ENTRY_ADDITION_ERROR = 51;

	public static final int SESSION_NOT_AUTHENTICATED = 61;
	public static final int INVALID_AUTHENTICATION_CREDENTIALS = 62;

	public static final int INVALID_MESSAGE_ERROR = 21;
	
	public static final int PROTO_ENCODING_ERROR = 71;
	public static final int PROTO_DECODING_ERROR = 72;
	
	public static final int CONNECTION_ERROR = 100;
	
}
