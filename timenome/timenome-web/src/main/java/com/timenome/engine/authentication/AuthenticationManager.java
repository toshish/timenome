package com.timenome.engine.authentication;

import java.util.HashMap;
import java.util.List;

import com.timenome.api.core.User;
import com.timenome.api.exceptions.UserAlreadyExists;
import com.timenome.api.exceptions.UserDoesNotExists;
import com.timenome.api.logger.TimeNomeLogger;
import com.timenome.engine.authentication.utils.AuthenticationUtils;
import com.timenome.engine.context.TimeNomeInits;
import com.timenome.engine.userservice.UserPreferenceTuple;

/**
 * @author toshish
 *
 */
public class AuthenticationManager {
	
	private static AuthenticationManager authenticationManager;
	private HashMap<String, User> userMap = new HashMap<>();
	private static TimeNomeLogger logger = TimeNomeLogger.getTimeNomeLogger(AuthenticationManager.class);
	
	private AuthenticationManager() {
		
	}
	
	public static AuthenticationManager getInstance() {
		if(authenticationManager == null) {
			authenticationManager = new AuthenticationManager();
		}
		return authenticationManager;
	}
	
	public User authenticateUser(String username, String password) {
		return TimeNomeInits.getUserBase().getUser(username, AuthenticationUtils.getSHA1HashOf(password));
	}

	public User createUser(String username, String password, String name) throws UserAlreadyExists {
		logger.finest("entering createUser");
		User user = TimeNomeInits.getUserBase().addUser(username, AuthenticationUtils.getSHA1HashOf(password), name);
		userMap.put(username, user);
		logger.finest("exiting createUser");
		return user;
	}

	public boolean deleteUser(String username, String password) throws UserDoesNotExists {
		logger.finest("entering deleteUser");
		boolean isUserDeleted = TimeNomeInits.getUserBase().removeUser(username, AuthenticationUtils.getSHA1HashOf(password));
		if(isUserDeleted) {
			userMap.remove(username);
		}
		logger.finest("exiting deleteUser");
		return isUserDeleted;
	}
	
	public boolean setUserPreferences(String username, String password, List<UserPreferenceTuple> list) {
		HashMap<String, String> attribMap = new HashMap<String, String>();
		for(UserPreferenceTuple prefTuple : list) {
			attribMap.put(prefTuple.getKey(), prefTuple.getValue());
		}
		return TimeNomeInits.getUserBase().setPreferences(username, AuthenticationUtils.getSHA1HashOf(password), attribMap);
	}

	public HashMap<String, String> getUserPreferences(String username, String password) {
		return TimeNomeInits.getUserBase().getPreferences(username, AuthenticationUtils.getSHA1HashOf(password));
	}

	public User authenticateAPI(String key, String secret) {
		return TimeNomeInits.getUserBase().getUserByAPICredentials(key, secret);
	}
	
	/**
	 * @param username
	 * @param password
	 * @return Key and secret for given user name
	 */
	public static String[] getAPICredentials(String username, String password) {
		return TimeNomeInits.getUserBase().getAPICredentials(username, AuthenticationUtils.getSHA1HashOf(password));
	}
	
}
