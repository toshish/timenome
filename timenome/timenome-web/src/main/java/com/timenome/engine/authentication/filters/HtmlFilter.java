package com.timenome.engine.authentication.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.timenome.api.logger.TimeNomeLogger;
import com.timenome.engine.context.TimeNomeConstants;

public class HtmlFilter implements Filter{

	protected boolean isAuthorized = false;
	
	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain arg2) throws IOException,
			ServletException {

		HttpServletRequest httpServletRequest = (HttpServletRequest) req;
		HttpServletResponse httpServletResponse = (HttpServletResponse) resp;
		
		httpServletResponse.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
		httpServletResponse.setHeader("Pragma", "no-cache"); // HTTP 1.0.
		httpServletResponse.setDateHeader("Expires", 0); // Proxies.

		TimeNomeLogger.getTimeNomeLogger(HtmlFilter.class).info(httpServletRequest.getSession().getId());
		
		Object isAuthorizedObj = httpServletRequest.getSession().getAttribute(TimeNomeConstants.IS_AUTHORIZED);
		isAuthorized = isAuthorizedObj == null ? false : (boolean) isAuthorizedObj;
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}
}
