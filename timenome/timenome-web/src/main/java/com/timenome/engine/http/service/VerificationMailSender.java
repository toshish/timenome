/**
 * 
 */
package com.timenome.engine.http.service;

import java.math.BigInteger;
import java.security.SecureRandom;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

import com.timenome.api.logger.TimeNomeLogger;
import com.timenome.engine.http.service.verification.VerificationURLHandler;

/**
 * Thread that sends verification email on email address passed via constructor.
 * 
 * @author toshish
 *
 */
public class VerificationMailSender extends Thread {
	
	private TimeNomeLogger logger = TimeNomeLogger.getTimeNomeLogger(VerificationMailSender.class);
	private String emailId;
	private String verificationUrlContext;
	
	/**
	 * 
	 */
	public VerificationMailSender(String verificationUrlContext, String emailId) {
		this.emailId = emailId;
		this.verificationUrlContext = verificationUrlContext;
	}
	
	@Override
	public void run() {
		try {
			sendVerificationMail();
		} catch (EmailException e) {
			logger.throwing("VerificationMailSender", "run", e);
		}
	}

	private void sendVerificationMail() throws EmailException {
		SecureRandom random = new SecureRandom();
		String randomURL = new BigInteger(130, random).toString(32);

		if(!verificationUrlContext.endsWith("/")) {
			verificationUrlContext = verificationUrlContext + "/"; 
		}
		String verificationUrl = verificationUrlContext + randomURL;

		String emailBody = "<html>" + "<body>" + "<h>Welcome to TimeNome!</h>" + "</br>"
				+ "<p>Please click following url to verify your email id.</p>" + "</br>" + "<i>" + verificationUrl
				+ "</i>" + "</body>" + "</html>";

		HtmlEmail email = new HtmlEmail();
		email.setHostName("smtp.googlemail.com");
		email.setSmtpPort(465);
		DefaultAuthenticator auth = new DefaultAuthenticator("timenomeuser", "beyondsingularity");
		email.setAuthenticator(auth);
		email.setSSLOnConnect(true);
		email.setFrom("noreply@timenome.com");
		email.setSubject("Welcome to TimeNome! - Please verify your email id.");
		email.setHtmlMsg(emailBody);
		email.setMsg(emailBody);
		email.addTo(emailId);
		email.send();

		VerificationURLHandler.getVerificationURLHandler().addURL(randomURL, emailId);
		
		logger.info("Verification email sent to emailId : " + emailId);
	}

}
