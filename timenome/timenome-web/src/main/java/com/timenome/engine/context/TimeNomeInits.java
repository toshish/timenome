package com.timenome.engine.context;

import com.timenome.api.core.Connection;
import com.timenome.api.execution.TimeNomeEngine;
import com.timenome.api.user.TimeNomeUserBase;

public final class TimeNomeInits {
	private static Connection connection;
	private static TimeNomeEngine engine;
	private static TimeNomeUserBase userBase;
	
	public static Connection getConnection() {
		return connection;
	}

	static void setConnection(Connection connection) {
		TimeNomeInits.connection = connection;
	}
	
	public static TimeNomeEngine getEngine() {
		return engine;
	}
	
	static void setEngine(TimeNomeEngine engine) {
		TimeNomeInits.engine = engine;
	}

	/**
	 * @return the userBase
	 */
	public static TimeNomeUserBase getUserBase() {
		return userBase;
	}

	/**
	 * @param userBase the userBase to set
	 */
	static void setUserBase(TimeNomeUserBase userBase) {
		TimeNomeInits.userBase = userBase;
	}
	
	
}

