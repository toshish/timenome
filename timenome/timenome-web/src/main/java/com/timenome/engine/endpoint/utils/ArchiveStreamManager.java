package com.timenome.engine.endpoint.utils;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.timenome.api.Entry;
import com.timenome.api.core.Archive;
import com.timenome.api.core.DataStore;
import com.timenome.api.exceptions.UserDoesNotExists;
import com.timenome.api.handler.StreamDataHandler;
import com.timenome.engine.context.TimeNomeInits;
import com.timenome.engine.endpoint.exceptions.ArchiveNotFound;
import com.timenome.engine.endpoint.exceptions.DataStoreNotFound;
import com.timenome.proto.TimeNomeProtos.DataEntry;

public class ArchiveStreamManager {

	private static ArchiveStreamManager archiveStreamManager = new ArchiveStreamManager();

	private String streamIdPrefix = ArchiveStreamManager.class.getName();
	private long streamCnt = 0;

	private synchronized String generateStreamId() {
		return streamIdPrefix + streamCnt++;
	}

	// Array contains userName, dataStoreName and archiveName
	private Map<String, String[]> streamIdMap;

	public static ArchiveStreamManager getInstance() {
		return archiveStreamManager;
	}

	private ArchiveStreamManager() {
		streamIdMap = new ConcurrentHashMap<>();
	}

	public String startStreaming(String dataStoreName, String archiveName, String userName,
			StreamReceiver streamReceiver) throws UserDoesNotExists, ArchiveNotFound, DataStoreNotFound {
		String streamId = null;

		DataStore dataStore = TimeNomeInits.getConnection().getDataStore(dataStoreName, userName);

		if (dataStore != null) {
			Archive archive = dataStore.getArchive(archiveName);
			if (archive == null) {
				throw new ArchiveNotFound(archiveName + " not found in system");
			} else {
				streamId = generateStreamId();
				streamIdMap.put(streamId, new String[] { userName, dataStoreName, archiveName });

				archive.addStreamDataHandler(streamId, new ArchiveEntryReceiver(dataStoreName, archiveName,
						streamReceiver));
			}
		} else {
			throw new DataStoreNotFound(dataStoreName + " not found in system");
		}

		return streamId;
	}

	public void stopStreaming(String streamId) throws UserDoesNotExists, ArchiveNotFound, DataStoreNotFound {

		if (streamIdMap.containsKey(streamId)) {
			String[] streamQualifiers = streamIdMap.remove(streamId);
			String userName = streamQualifiers[0];
			String dataStoreName = streamQualifiers[1];
			String archiveName = streamQualifiers[2];
			DataStore dataStore = TimeNomeInits.getConnection().getDataStore(dataStoreName, userName);

			if (dataStore != null) {
				Archive archive = dataStore.getArchive(archiveName);
				if (archive == null) {
					throw new ArchiveNotFound(archiveName + " not found in system");
				} else {
					archive.removeStreamDataHandler(streamId);
				}
			} else {
				throw new DataStoreNotFound(dataStoreName + " not found in system");
			}
		}
	}

	private class ArchiveEntryReceiver implements StreamDataHandler {
		private final StreamReceiver streamReceiver;
		private String archiveName;
		private String dataStoreName;

		private ArchiveEntryReceiver(String dataStoreName, String archiveName, StreamReceiver streamReceiver) {
			this.dataStoreName = dataStoreName;
			this.archiveName = archiveName;
			this.streamReceiver = streamReceiver;
		}

		@Override
		public void onDataAggregated(List<Entry<? extends Number>> dataEntries) {
			List<DataEntry> protoDataEntries = new LinkedList<>();

			if (dataEntries != null) {
				for (Entry<? extends Number> entry : dataEntries) {
					DataEntry.Builder dataEntryBuilder = DataEntry.newBuilder();
					dataEntryBuilder.setTimestamp(entry.getTimeStamp());
					if (entry.getValue() != null) {
						dataEntryBuilder.setValue((Double) entry.getValue());
					}
					DataEntry dataEntry = dataEntryBuilder.build();
					protoDataEntries.add(dataEntry);
				}
			}

			streamReceiver.onStreamReceived(dataStoreName, archiveName, protoDataEntries);
		}
	}
}
