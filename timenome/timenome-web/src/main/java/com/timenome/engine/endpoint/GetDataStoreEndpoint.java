package com.timenome.engine.endpoint;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;

import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import com.timenome.api.core.DataStore;
import com.timenome.api.core.User;
import com.timenome.api.exceptions.AuthenticationException;
import com.timenome.api.exceptions.MessageParsingException;
import com.timenome.api.exceptions.UserDoesNotExists;
import com.timenome.api.logger.TimeNomeLogger;
import com.timenome.engine.context.TimeNomeInits;
import com.timenome.engine.endpoint.enumeration.MessageEnum;
import com.timenome.engine.endpoint.utils.EndpointUtils;
import com.timenome.engine.error.ErrorCodes;
import com.timenome.proto.TimeNomeProtos.Archive;
import com.timenome.proto.TimeNomeProtos.Attribute;
import com.timenome.proto.TimeNomeProtos.BuiltInAggregationFunction;
import com.timenome.proto.TimeNomeProtos.GetDataStoreRequest;
import com.timenome.proto.TimeNomeProtos.GetDataStoreResponse;
import com.timenome.proto.TimeNomeProtos.StatusResponse;

@ServerEndpoint("/get/datastore")
public class GetDataStoreEndpoint {
	private static TimeNomeLogger logger = TimeNomeLogger.getTimeNomeLogger(GetDataStoreEndpoint.class);
	@OnOpen
	public void onOpen(Session session) {
		
	}

	@OnError
	public void onError(Session session, Throwable t) {
		logger.log(Level.SEVERE, "", t);
		try {
			session.close();
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error while closing Session.", e);
		}
	}

	@OnClose
	public void onClose(Session session, CloseReason reason) {
		
	}

	@OnMessage
	public void onMessage(Session session, ByteBuffer byteBuf, boolean last) {
		GetDataStoreRequest getDataStoreRequest = null;
		StatusResponse statusResponse = null;
		GetDataStoreResponse getDataStoreResponse = null;
		
		try {
			User user = EndpointUtils.authenticateAPI(byteBuf);
			getDataStoreRequest = EndpointUtils.getEnclosingMessage(byteBuf, MessageEnum.GET_DATASTORE_REQUEST);
			String dataStoreName = getDataStoreRequest.getDataStoreName();
			DataStore dataStore = TimeNomeInits.getConnection().getDataStore(dataStoreName, user.getName());
			if(dataStore != null) {
				GetDataStoreResponse.Builder getDataStoreResponseBuilder = GetDataStoreResponse.newBuilder();
				com.timenome.proto.TimeNomeProtos.DataStore.Builder protoDataStoreBuilder =
													com.timenome.proto.TimeNomeProtos.DataStore.newBuilder();
				
				protoDataStoreBuilder.setDataStoreName(dataStore.getDataStoreName());
				Set<Entry<String, com.timenome.api.core.Archive>> archives = dataStore.getArchives();
				
				for(Entry<String, com.timenome.api.core.Archive> archiveEntry : archives) {
					com.timenome.api.core.Archive archive = archiveEntry.getValue(); 
					Archive.Builder archiveBuilder = Archive.newBuilder();
					archiveBuilder.setArchiveName(archive.getArchiveName());
					archiveBuilder.setAggregationFunction(BuiltInAggregationFunction.valueOf(archive.getConsolidationFunction().getCDFValue().toString()));
					archiveBuilder.setStoragePrecision(archive.getStoragePrecision().getTimeInMillseconds());
					archiveBuilder.setIsCounterBased(false);
					protoDataStoreBuilder.addArchive(archiveBuilder.build());
				}
				
				Map<String, String> attributeMap = dataStore.getAttributes();
				
				for(String key : attributeMap.keySet()) {
					Attribute.Builder attribBuilder = Attribute.newBuilder();
					attribBuilder.setKey(key);
					attribBuilder.setValue(attributeMap.get(key));
					protoDataStoreBuilder.addAttribute(attribBuilder);
				}
				
				getDataStoreResponseBuilder.setDataStore(protoDataStoreBuilder.build());
				getDataStoreResponse = getDataStoreResponseBuilder.build();
				statusResponse = EndpointUtils.getStatusResponse(true);
			}
			else {
				statusResponse = EndpointUtils.getStatusResponse(false, 
						EndpointUtils.getErrorInstance(ErrorCodes.ENTITY_DOES_NOT_EXIST, "DataStore"
								+ " with name " + dataStoreName + " does not exist."));
			}
			
		} catch (UserDoesNotExists e) {
			statusResponse = EndpointUtils.getStatusResponse(false, 
					EndpointUtils.getErrorInstance(ErrorCodes.ENTITY_DOES_NOT_EXIST, e.getLocalizedMessage()));
		} catch (MessageParsingException e) {
			statusResponse = EndpointUtils.getStatusResponse(false, 
					EndpointUtils.getErrorInstance(ErrorCodes.INVALID_MESSAGE_ERROR, e.getLocalizedMessage()));
		} catch (AuthenticationException e) {
			statusResponse = EndpointUtils.getStatusResponse(false, 
					EndpointUtils.getErrorInstance(ErrorCodes.INVALID_AUTHENTICATION_CREDENTIALS, e.getLocalizedMessage()));
		}
		logger.info(statusResponse.toString());
		try {
			EndpointUtils.sendExecutionResponse(statusResponse, MessageEnum.GET_DATA_STORE_RESPONSE, 
					getDataStoreResponse, session);
		} catch (MessageParsingException e) {
			logger.severe(e.getLocalizedMessage());
		}
	}
}