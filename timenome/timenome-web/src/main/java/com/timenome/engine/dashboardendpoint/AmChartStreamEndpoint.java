package com.timenome.engine.dashboardendpoint;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.timenome.api.core.User;
import com.timenome.api.exceptions.UserDoesNotExists;
import com.timenome.api.logger.TimeNomeLogger;
import com.timenome.engine.amchartdataservice.AmChartDataRequest;
import com.timenome.engine.amchartdataservice.AmChartStreamRequest;
import com.timenome.engine.amchartdataservice.DataQualifier;
import com.timenome.engine.authentication.AuthenticationManager;
import com.timenome.engine.endpoint.exceptions.ArchiveNotFound;
import com.timenome.engine.endpoint.exceptions.DataStoreNotFound;
import com.timenome.engine.endpoint.utils.ArchiveStreamManager;
import com.timenome.engine.endpoint.utils.StreamReceiver;
import com.timenome.engine.http.service.AmChartHttpService;
import com.timenome.proto.TimeNomeProtos.DataEntry;

@ServerEndpoint("/amchartstreamingservice")
public class AmChartStreamEndpoint {
	private static TimeNomeLogger logger = TimeNomeLogger.getTimeNomeLogger(AmChartStreamEndpoint.class);
	private List<String> streamIdList = new LinkedList<>();

	@OnOpen
	public void onOpen(Session session) {
		logger.entering(AmChartStreamEndpoint.class.getName(), "onOpen");
	}

	@OnError
	public void onError(Session session, Throwable t) {
		logger.log(Level.SEVERE, "", t);
		try {
			session.close();
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error while closing Session.", e);
		}
		
		closeAllStreams();
	}

	@OnClose
	public void onClose(Session session) {
		logger.log(Level.INFO, "amchart socket closed");
		closeAllStreams();
	}
	
	private void closeAllStreams(){
		for(String streamId: streamIdList){
			try {
				ArchiveStreamManager.getInstance().stopStreaming(streamId);
			} catch (UserDoesNotExists | ArchiveNotFound | DataStoreNotFound e) {
				//Do nothing
				logger.log(Level.WARNING, e.getMessage(), e);
			}
		}
		
		streamIdList.clear();
	}

	@OnMessage
	public void onMessage(final Session session, String request) {
		logger.entering(AmChartStreamEndpoint.class.getName(), "onMessage");

		if (request != null) {

			try {
				AmChartStreamRequest amChartStreamRequest = new ObjectMapper().readValue(request,
						AmChartStreamRequest.class);

				// Get static data so that something shows up on UI, UI will
				// merge automatically from all sources
				AmChartDataResponse staticData = getStaticData(amChartStreamRequest);

				StringWriter stringWriter = new StringWriter();
				new ObjectMapper().writeValue(stringWriter, staticData);
				session.getAsyncRemote().sendText(stringWriter.toString());

				//Failure only takes place when user is null, in that case streaming will fail too
				if (staticData.isSuccess()) {
					startStreaming(session, amChartStreamRequest);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void startStreaming(final Session session, AmChartStreamRequest amChartStreamRequest) {
		String apiKey = amChartStreamRequest.getApiKey();
		String apiSecret = amChartStreamRequest.getApiSecret();
		User user = AuthenticationManager.getInstance().authenticateAPI(apiKey, apiSecret);

		String userName = null;
		if (user != null) {
			userName = user.getName();

			// Start streaming for archives
			List<DataQualifier> dataQualifiers = amChartStreamRequest.getDataQualifiers();
			for (DataQualifier dataQualifier : dataQualifiers) {
				String dataStoreName = dataQualifier.getDataStoreName();
				String archiveName = dataQualifier.getArchiveName();
				final String archiveKey = dataQualifier.getArchiveKey();

				try {
					streamIdList.add(ArchiveStreamManager.getInstance().startStreaming(dataStoreName, archiveName, userName,
							new AmChartArchiveStreamReceiver(session, archiveKey)));
				} catch (UserDoesNotExists | ArchiveNotFound | DataStoreNotFound e) {
					//Don't do anything
					logger.log(Level.WARNING, e.getMessage(), e);
				}
			}
		}//else don't do anything, failure for user authentication is already sent in static call failure
	}

	private class AmChartArchiveStreamReceiver implements StreamReceiver {
		private final Session session;
		private final String archiveKey;

		private AmChartArchiveStreamReceiver(Session session, String archiveKey) {
			this.session = session;
			this.archiveKey = archiveKey;
		}

		@Override
		public void onStreamReceived(String dataStoreName, String archiveName, Collection<DataEntry> dataEntries) {
			List<Map<String, Double>> tupleList = new LinkedList<>();
			
			for (DataEntry dataEntry : dataEntries) {
				long timestamp = dataEntry.getTimestamp();
				double value = dataEntry.getValue();

				HashMap<String, Double> tuple = new HashMap<String, Double>();
				tuple.put("timestamp", (double) timestamp);
				tuple.put(archiveKey, value);
				tupleList.add(tuple);
			}

			AmChartDataResponse amChartDataResponse = new AmChartDataResponse(true, "", tupleList);
			StringWriter stringWriter = new StringWriter();
			try {
				new ObjectMapper().writeValue(stringWriter, amChartDataResponse);
				String dataJson = stringWriter.toString();
				logger.info(dataJson);
				session.getAsyncRemote().sendText(dataJson);
			} catch (IOException e) {
				//Don't do anything
				logger.log(Level.WARNING, e.getMessage(), e);
			}			
		}
	}

	private AmChartDataResponse getStaticData(AmChartStreamRequest amChartStreamRequest)
			throws JsonGenerationException, JsonMappingException, IOException {
		long leastCalibration = Long.MAX_VALUE;
		long endTimestamp = new Date().getTime();

		AmChartDataRequest amChartDataRequest = new AmChartDataRequest();
		List<DataQualifier> getReqDataQualifiers = amChartDataRequest.getDataQualifiers();

		for (DataQualifier dataQualifier : amChartStreamRequest.getDataQualifiers()) {
			if (dataQualifier.getStoragePrecision() < leastCalibration) {
				leastCalibration = dataQualifier.getStoragePrecision();
			}

			getReqDataQualifiers.add(dataQualifier);
		}

		long startTimestamp = endTimestamp - leastCalibration * amChartStreamRequest.getMaxInitialDataEntries();

		amChartDataRequest.setApiKey(amChartStreamRequest.getApiKey());
		amChartDataRequest.setApiSecret(amChartStreamRequest.getApiSecret());
		amChartDataRequest.setStartTimestamp(startTimestamp);
		amChartDataRequest.setEndTimestamp(endTimestamp);

		AmChartDataResponse data = new AmChartHttpService().getData(amChartDataRequest);
		return data;
	}
}
