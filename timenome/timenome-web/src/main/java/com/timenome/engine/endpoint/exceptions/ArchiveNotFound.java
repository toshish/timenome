package com.timenome.engine.endpoint.exceptions;

public class ArchiveNotFound extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ArchiveNotFound(String message){
		super(message);
	}

}
