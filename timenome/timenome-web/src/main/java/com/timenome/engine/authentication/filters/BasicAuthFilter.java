/**
 * 
 */
package com.timenome.engine.authentication.filters;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response.Status;
import javax.xml.bind.DatatypeConverter;

import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import com.timenome.api.core.User;
import com.timenome.api.logger.TimeNomeLogger;
import com.timenome.engine.authentication.AuthenticationManager;
import com.timenome.engine.context.TimeNomeConstants;

/**
 * @author toshish
 * 
 */
public class BasicAuthFilter implements ContainerRequestFilter {
	
	@Context
	HttpServletRequest servletRequest;
	@Context
	ServletContext servletContext;

	private static TimeNomeLogger logger = TimeNomeLogger.getTimeNomeLogger(BasicAuthFilter.class);
	private static final String AUTHORIZATION_HEADER_PARAM = "authorization";

	public BasicAuthFilter() {
		logger.info("Initializing BasicAuthFilter");
	}

	@Override
	public ContainerRequest filter(ContainerRequest request) {
		logger.entering("BasicAuthFilter", "filter");

		Object isAuthorizedObj = servletRequest.getSession().getAttribute(TimeNomeConstants.IS_AUTHORIZED);
		boolean isAuthorized = isAuthorizedObj == null ? false : (boolean) isAuthorizedObj;

		if (!isAuthorized) {
			String authorizationHeaderParam = request.getHeaderValue(AUTHORIZATION_HEADER_PARAM);

			// If the user does not have the right (does not provide any HTTP
			// Basic Auth)
			if (authorizationHeaderParam == null) {
				throw new WebApplicationException(Status.UNAUTHORIZED);
			}

			String[] logingAndPasswordArr = decodeAuthHeader(authorizationHeaderParam);

			// If login or password fail
			if (logingAndPasswordArr == null || logingAndPasswordArr.length != 2) {
				throw new WebApplicationException(Status.UNAUTHORIZED);
			}

			AuthenticationManager authenticationManager = AuthenticationManager.getInstance();
			User user = authenticationManager.authenticateUser(logingAndPasswordArr[0], logingAndPasswordArr[1]);

			// Our system refused login and password
			if (user == null) {
				logger.warning("authorization doesn't exist.");
			} else { 
				isAuthorized = true;
			}
			
			servletRequest.setAttribute(TimeNomeConstants.USERNAME, logingAndPasswordArr[0]);
			servletRequest.setAttribute(TimeNomeConstants.PASSWORD, logingAndPasswordArr[1]);
			servletRequest.getSession().setAttribute(TimeNomeConstants.IS_AUTHORIZED, isAuthorized);
			logger.info(servletRequest.getSession().getId());
		}

		logger.exiting("BasicAuthFilter", "filter");
		return request;
	}

	/**
	 * Decode the basic auth and convert it to array login/password
	 * 
	 * @param auth
	 *            The string encoded authentication
	 * @return The login (case 0), the password (case 1)
	 */
	public static String[] decodeAuthHeader(String auth) {
		// Replacing "Basic THE_BASE_64" to "THE_BASE_64" directly
		auth = auth.replaceFirst("[B|b]asic ", "");

		// Decode the Base64 into byte[]
		byte[] decodedBytes = DatatypeConverter.parseBase64Binary(auth);

		// If the decode fails in any case
		if (decodedBytes == null || decodedBytes.length == 0) {
			return null;
		}

		// Now we can convert the byte[] into a splitted array :
		// - the first one is login,
		// - the second one password
		return new String(decodedBytes).split(":", 2);
	}

}
