package com.timenome.engine.context;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.xml.bind.JAXBException;

import com.timenome.api.core.Registry;
import com.timenome.api.exceptions.ConnectionException;
import com.timenome.api.exceptions.UserbaseInitializationException;
import com.timenome.api.execution.TimeNomeEngine;
import com.timenome.api.user.TimeNomeUserBase;

public class TimeNomeServletContextListener implements ServletContextListener {
	private static Logger logger = Logger.getLogger(TimeNomeServletContextListener.class.getName());

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		logger.info("Closing Timewarp connection.");
		TimeNomeInits.getConnection().close();
		logger.info("Closed Timewarp connection.");
	}

	@Override
	public void contextInitialized(ServletContextEvent servContextEvent) {
		logger.log(Level.FINE, "servlet context initializing...");
		
		Registry registry = Registry.getRegistry();
		HashMap<String, String> connectionParams = new HashMap<String, String>();
		connectionParams.put("name", "neo4jStorage");
		try {
			if(TimeNomeInits.getConnection() == null) {
				logger.log(Level.INFO, "Neo4jConnection is being initialized.");
				TimeNomeInits.setConnection(registry.connect(connectionParams));
				Runtime.getRuntime().addShutdownHook(new Thread(){
					@Override
					public void run() {
						logger.info("Closing TimeNome connection. via shutdownhook");
						TimeNomeInits.getConnection().close();
						logger.info("Closed TimeNome connection. via shutdownhook");
					}
				});
				logger.log(Level.INFO, "Neo4jConnection initialized.");
			}
			TimeNomeInits.setEngine(TimeNomeEngine.getTimeNomeEngine(TimeNomeInits.getConnection()));
		} catch (JAXBException e) {
			logger.log(Level.SEVERE, "", e);
		} catch (ClassNotFoundException e) {
			logger.log(Level.SEVERE, "", e);
		} catch (ConnectionException e) {
			logger.log(Level.SEVERE, "", e);
		} catch (InstantiationException e) {
			logger.log(Level.SEVERE, "", e);
		} catch (IllegalAccessException e) {
			logger.log(Level.SEVERE, "", e);
		}
		try {
			if(TimeNomeInits.getUserBase() == null) {
				logger.info("Initializing TimeNome User base.");
				TimeNomeInits.setUserBase(TimeNomeUserBase.getTimeNomeUserBase(TimeNomeInits.getEngine()));
				logger.info("Initialization of TimeNome User base completed.");
			}
		} catch (UserbaseInitializationException e) {
			logger.log(Level.SEVERE, "", e);
		}
	}

}
