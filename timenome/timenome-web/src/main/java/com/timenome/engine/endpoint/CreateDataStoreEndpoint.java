package com.timenome.engine.endpoint;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import com.timenome.api.BuiltInCDF;
import com.timenome.api.TimeUnit;
import com.timenome.api.core.DataStore;
import com.timenome.api.core.TimeRangeDef;
import com.timenome.api.core.User;
import com.timenome.api.exceptions.ArchiveException;
import com.timenome.api.exceptions.AuthenticationException;
import com.timenome.api.exceptions.DataStoreException;
import com.timenome.api.exceptions.MessageParsingException;
import com.timenome.api.exceptions.UserDoesNotExists;
import com.timenome.api.logger.TimeNomeLogger;
import com.timenome.engine.context.TimeNomeInits;
import com.timenome.engine.endpoint.enumeration.MessageEnum;
import com.timenome.engine.endpoint.utils.EndpointUtils;
import com.timenome.engine.error.ErrorCodes;
import com.timenome.proto.TimeNomeProtos.Archive;
import com.timenome.proto.TimeNomeProtos.Attribute;
import com.timenome.proto.TimeNomeProtos.CreateDataStoreRequest;
import com.timenome.proto.TimeNomeProtos.StatusResponse;

@ServerEndpoint("/create/datastore")
public class CreateDataStoreEndpoint {
	private static TimeNomeLogger logger = TimeNomeLogger.getTimeNomeLogger(CreateDataStoreEndpoint.class);
	private List<com.timenome.api.core.Archive> createdArchivesList = new ArrayList<>();

	@OnOpen
	public void onOpen(Session session) {

	}
	@OnError
	public void onError(Session session, Throwable t) {
		logger.log(Level.SEVERE, "", t);
		try {
			session.close();
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error while closing Session.", e);
		}
	}

	@OnMessage
	public void onMessage(Session session, ByteBuffer byteBuf, boolean last) {
		logger.entering("CreateDataStoreEndpoint", "onMessage");
		CreateDataStoreRequest createDataStoreRequest = null;

		DataStore dataStore = null;
		StatusResponse statusResponse = null;

		try {
			User user = EndpointUtils.authenticateAPI(byteBuf);
			createDataStoreRequest = EndpointUtils.getEnclosingMessage(byteBuf, MessageEnum.CREATE_DATASTORE_REQUEST);
			dataStore = TimeNomeInits.getConnection().createDataStore(createDataStoreRequest.getDataStore().getDataStoreName(), user.getName());
			if(dataStore != null) {
				List<Archive> createArchiveRequestList = createDataStoreRequest.getDataStore().getArchiveList();
				List<Attribute> dataStoreAttributeList = createDataStoreRequest.getDataStore().getAttributeList();
				try {
					addArchives(createArchiveRequestList, dataStore);
					setAttributes(dataStoreAttributeList, dataStore);
					statusResponse = EndpointUtils.getStatusResponse(true);
				} catch (ArchiveException e) {
					statusResponse = EndpointUtils.getStatusResponse(false, 
							EndpointUtils.getErrorInstance(ErrorCodes.ARCHIVE_ERROR, e.getLocalizedMessage()));
					logger.info(statusResponse.toString());
					
					logger.info("Archive creation has failed. Attempting to roll back to previous state.");
					
					boolean isDataStoreDeleted = TimeNomeInits.getConnection().deleteDataStore(createDataStoreRequest.getDataStore().getDataStoreName(), user.getName());
					
					logger.warning("Deletion for DataStore " + createDataStoreRequest.getDataStore().getDataStoreName() + 
							" for user " + user.getName() + ": " + isDataStoreDeleted);
				}
			}
			else {
				statusResponse = EndpointUtils.getStatusResponse(false, 
						EndpointUtils.getErrorInstance(ErrorCodes.UNKNOWN_ERROR, "Unknown error in creating Data Store."));
			}
		} catch (MessageParsingException | AuthenticationException e) {
			statusResponse = EndpointUtils.getStatusResponse(false, 
					EndpointUtils.getErrorInstance(ErrorCodes.INVALID_MESSAGE_ERROR, e.getLocalizedMessage()));
		} catch (DataStoreException e) {
			statusResponse = EndpointUtils.getStatusResponse(false, 
					EndpointUtils.getErrorInstance(ErrorCodes.DATA_STORE_ERROR, e.getLocalizedMessage()));
		} catch (UserDoesNotExists e) {
			statusResponse = EndpointUtils.getStatusResponse(false, 
					EndpointUtils.getErrorInstance(ErrorCodes.ENTITY_DOES_NOT_EXIST, e.getLocalizedMessage()));
		}
		logger.info(statusResponse.toString());

		EndpointUtils.sendStatusResponse(statusResponse, session);

	}

	private void addArchives(List<Archive> createArchiveRequestList, DataStore dataStore) throws ArchiveException {
		for(Archive archive : createArchiveRequestList) {
			String archiveName = archive.getArchiveName();
			TimeRangeDef storageRange = new TimeRangeDef(TimeUnit.MILLI_SECOND, 0l);
			TimeRangeDef storagePrecision = new TimeRangeDef(TimeUnit.MILLI_SECOND, archive.getStoragePrecision());
			BuiltInCDF cdf = BuiltInCDF.valueOf(archive.getAggregationFunction().toString());

			com.timenome.api.core.Archive createdArchive = dataStore.addArchive(archiveName, storageRange, storagePrecision, cdf);
			if(createdArchive != null) {
				createdArchivesList.add(createdArchive);
			}
			else {
				throw new ArchiveException("Couldn't add archive " + archiveName + " to DataStore " + dataStore.getDataStoreName());
			}
		}
	}
	
	private void setAttributes(List<Attribute> dataStoreAttributeList, DataStore dataStore) {
		for(Attribute attribute : dataStoreAttributeList) {
			String key = attribute.getKey();
			String value = attribute.getValue();
			if(!dataStore.setAttribute(key, value)) {
				logger.warning("Attribute " + key + " did not set.");
			}
		}
	}

}
