package com.timenome.engine.endpoint;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Map;
import java.util.logging.Level;

import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import com.timenome.proto.TimeNomeProtos.ListAllDataStoreNamesRequest;
import com.timenome.proto.TimeNomeProtos.ListAllDataStoreNamesResponse;
import com.timenome.proto.TimeNomeProtos.StatusResponse;
import com.timenome.api.core.DataStore;
import com.timenome.api.core.User;
import com.timenome.api.exceptions.AuthenticationException;
import com.timenome.api.exceptions.MessageParsingException;
import com.timenome.api.exceptions.UserDoesNotExists;
import com.timenome.api.logger.TimeNomeLogger;
import com.timenome.engine.context.TimeNomeInits;
import com.timenome.engine.endpoint.enumeration.MessageEnum;
import com.timenome.engine.endpoint.utils.EndpointUtils;
import com.timenome.engine.error.ErrorCodes;

@ServerEndpoint("/list/datastorenames")
public class ListAllDataStoreNamesEndpoint {
	private static TimeNomeLogger logger = TimeNomeLogger.getTimeNomeLogger(ListAllDataStoreNamesEndpoint.class);
	@OnOpen
	public void onOpen(Session session) {
		
	}

	@OnError
	public void onError(Session session, Throwable t) {
		logger.log(Level.SEVERE, "", t);
		try {
			session.close();
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error while closing Session.", e);
		}
	}

	@OnClose
	public void onClose(Session session, CloseReason reason) {
		
	}

	@OnMessage
	public void onMessage(Session session, ByteBuffer byteBuf, boolean last) {
		ListAllDataStoreNamesRequest listAllDataStoreRequest = null;
		StatusResponse statusResponse = null;
		ListAllDataStoreNamesResponse listAllDataStoreNamesResponse = null;
		try {
			User user = EndpointUtils.authenticateAPI(byteBuf);
			listAllDataStoreRequest = EndpointUtils.getEnclosingMessage(byteBuf, MessageEnum.LIST_ALL_DATASTORE_NAMES_REQUEST);
			ListAllDataStoreNamesResponse.Builder listAllDataStoreNamesResponseBuilder = ListAllDataStoreNamesResponse.newBuilder();
			
			if(listAllDataStoreRequest != null) {
				Map<String, DataStore> dataStores = TimeNomeInits.getConnection().getAllDataStores(user.getName());
				listAllDataStoreNamesResponseBuilder.addAllDataStoreName(dataStores.keySet());
			}
			
			listAllDataStoreNamesResponse = listAllDataStoreNamesResponseBuilder.build();
			statusResponse = EndpointUtils.getStatusResponse(true);
		} catch (UserDoesNotExists e) {
			statusResponse = EndpointUtils.getStatusResponse(false, 
					EndpointUtils.getErrorInstance(ErrorCodes.ENTITY_DOES_NOT_EXIST, e.getLocalizedMessage()));
		} catch (MessageParsingException e) {
			statusResponse = EndpointUtils.getStatusResponse(false, 
					EndpointUtils.getErrorInstance(ErrorCodes.INVALID_MESSAGE_ERROR, e.getLocalizedMessage()));
		} catch (AuthenticationException e) {
			statusResponse = EndpointUtils.getStatusResponse(false, 
					EndpointUtils.getErrorInstance(ErrorCodes.INVALID_AUTHENTICATION_CREDENTIALS, e.getLocalizedMessage()));
		}
		logger.info(statusResponse.toString());
		try {
			EndpointUtils.sendExecutionResponse(statusResponse, MessageEnum.LIST_ALL_DATA_STORE_NAMES_RESPONSE, 
					listAllDataStoreNamesResponse, session);
		} catch (MessageParsingException e) {
			logger.severe(e.getLocalizedMessage());
		}
		
		
	}
}