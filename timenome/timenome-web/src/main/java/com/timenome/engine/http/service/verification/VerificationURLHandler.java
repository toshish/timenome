package com.timenome.engine.http.service.verification;

import java.util.HashMap;

public class VerificationURLHandler {
	
	private static VerificationURLHandler verificationURLHandler;
	
	private HashMap<String, String> urlMap = new HashMap<>();
	
	private VerificationURLHandler() {
		// TODO Auto-generated constructor stub
	}
	
	public static VerificationURLHandler getVerificationURLHandler() {
		if(verificationURLHandler == null) {
			verificationURLHandler = new VerificationURLHandler();
		}
		return verificationURLHandler;
	}
	
	public void addURL(String url, String email) {
		urlMap.put(url, email);
	}
	
	public void removeURL(String url) {
		urlMap.remove(url);
	}
	
	public boolean containsUrl(String url) {
		return urlMap.containsKey(url);
	}
	
	public boolean containsUrlForEmail(String email) {
		return urlMap.containsValue(email);
	}

	public String emailOfUrl(String url) {
		return urlMap.get(url);
	}
}
