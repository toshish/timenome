package com.timenome.engine.endpoint;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.logging.Level;

import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import com.timenome.api.core.User;
import com.timenome.api.exceptions.AuthenticationException;
import com.timenome.api.exceptions.MessageParsingException;
import com.timenome.api.exceptions.TimeOutOfRangeException;
import com.timenome.api.exceptions.UserDoesNotExists;
import com.timenome.api.logger.TimeNomeLogger;
import com.timenome.engine.endpoint.enumeration.MessageEnum;
import com.timenome.engine.endpoint.utils.DataFetcher;
import com.timenome.engine.endpoint.utils.EndpointUtils;
import com.timenome.engine.error.ErrorCodes;
import com.timenome.proto.TimeNomeProtos.GetDataRequest;
import com.timenome.proto.TimeNomeProtos.GetDataResponse;
import com.timenome.proto.TimeNomeProtos.StatusResponse;

@ServerEndpoint("/get/data")
public class GetDataEndpoint {
	private static TimeNomeLogger logger = TimeNomeLogger.getTimeNomeLogger(GetDataEndpoint.class);

	@OnOpen
	public void onOpen(Session session) {

	}

	@OnError
	public void onError(Session session, Throwable t) {
		logger.log(Level.SEVERE, "", t);
		try {
			session.close();
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error while closing Session.", e);
		}
	}

	@OnMessage
	public void onMessage(Session session, ByteBuffer byteBuf, boolean last) {
		GetDataRequest getDataRequest = null;
		User user;
		StatusResponse statusResponse = null;
		GetDataResponse getDataResponse = null;
		try {
			user = EndpointUtils.authenticateAPI(byteBuf);
			getDataRequest = EndpointUtils.getEnclosingMessage(byteBuf, MessageEnum.GET_DATA_REQUEST);
			
			getDataResponse = DataFetcher.getInstance().getData(user.getName(), getDataRequest);
			
			statusResponse = EndpointUtils.getStatusResponse(true);
		} catch (MessageParsingException e2) {
			statusResponse = EndpointUtils.getStatusResponse(false,
					EndpointUtils.getErrorInstance(ErrorCodes.INVALID_MESSAGE_ERROR, e2.getLocalizedMessage()));
		} catch (AuthenticationException e2) {
			statusResponse = EndpointUtils.getStatusResponse(
					false,
					EndpointUtils.getErrorInstance(ErrorCodes.INVALID_AUTHENTICATION_CREDENTIALS,
							e2.getLocalizedMessage()));
		} catch (UserDoesNotExists e1) {
			statusResponse = EndpointUtils.getStatusResponse(false,
					EndpointUtils.getErrorInstance(ErrorCodes.ENTITY_DOES_NOT_EXIST, e1.getLocalizedMessage()));
		} catch (TimeOutOfRangeException e) {
			statusResponse = EndpointUtils.getStatusResponse(false,
					EndpointUtils.getErrorInstance(ErrorCodes.TIME_OUT_OF_RANGE_ERROR, e.getLocalizedMessage()));
		}
		logger.info(statusResponse.toString());
		try {
			EndpointUtils.sendExecutionResponse(statusResponse, MessageEnum.GET_DATA_RESPONSE, getDataResponse,
					session);
		} catch (MessageParsingException e) {
			logger.severe(e.getLocalizedMessage());
		}
	}
}
