package com.timenome.engine.endpoint;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.logging.Level;

import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import com.google.protobuf.InvalidProtocolBufferException;
import com.timenome.api.EngineEntry;
import com.timenome.api.Entry;
import com.timenome.api.core.User;
import com.timenome.api.exceptions.AuthenticationException;
import com.timenome.api.exceptions.MessageParsingException;
import com.timenome.api.execution.TimeNomeEngine;
import com.timenome.api.logger.TimeNomeLogger;
import com.timenome.engine.context.TimeNomeInits;
import com.timenome.engine.endpoint.utils.EndpointUtils;
import com.timenome.engine.error.ErrorCodes;
import com.timenome.proto.TimeNomeProtos.DataEntry;
import com.timenome.proto.TimeNomeProtos.Error;
import com.timenome.proto.TimeNomeProtos.ExecutionResponse;
import com.timenome.proto.TimeNomeProtos.PutDataRequest;
import com.timenome.proto.TimeNomeProtos.PutDataRequestInternal;
import com.timenome.proto.TimeNomeProtos.PutDataResponse;
import com.timenome.proto.TimeNomeProtos.StatusResponse;

@ServerEndpoint("/put/data")
public class PutDataEndpoint {
	private static TimeNomeLogger logger = TimeNomeLogger.getTimeNomeLogger(AuthenticationEndpoint.class);
	
	private interface PutProcessor{
		void processPutRequest(ByteBuffer bytebuffer, Session session);
	}
	
	private PutProcessor putProcessor = new PutAuthenticationProcessor();
	private String userName = null;
	
	@OnOpen
	public void onOpen(Session session) {
		System.out.println("Session open: " + session.getRequestURI());
	}

	@OnError
	public void onError(Session session, Throwable t) {
		logger.log(Level.SEVERE, "", t);
		try {
			session.close();
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error while closing Session.", e);
		}
	}
	

	@OnMessage
	public void onMessage(Session session, ByteBuffer byteBuf, boolean last) {
		putProcessor.processPutRequest(byteBuf, session);
	}
	
	private class PutAuthenticationProcessor implements PutProcessor{

		@Override
		public void processPutRequest(ByteBuffer bytebuffer, Session session) {
			StatusResponse statusResponse = null;
			
			try {
				User user = EndpointUtils.authenticateAPI(bytebuffer);
				if(user != null) {
					statusResponse = EndpointUtils.getStatusResponse(true);
					userName = user.getName();
					putProcessor = new PutMessageProcessor();
				}
				else {
					statusResponse = EndpointUtils.getStatusResponse(false, 
							EndpointUtils.getErrorInstance(ErrorCodes.INVALID_AUTHENTICATION_CREDENTIALS, "Invalid Authentication."));
				}
			} catch (MessageParsingException | AuthenticationException e) {
				statusResponse = EndpointUtils.getStatusResponse(false, 
						EndpointUtils.getErrorInstance(ErrorCodes.INVALID_MESSAGE_ERROR, e.getLocalizedMessage()));
			} 
			logger.info(statusResponse.toString());
			EndpointUtils.sendStatusResponse(statusResponse, session);
		}

	}
	
	private class PutMessageProcessor implements PutProcessor{
		
		@Override
		public void processPutRequest(ByteBuffer bytebuffer, Session session) {
			PutDataRequestInternal putDataRequestInternal = null;
			try {
				putDataRequestInternal = PutDataRequestInternal.parseFrom(bytebuffer.array());
			} catch (InvalidProtocolBufferException e) {
				logger.log(Level.SEVERE, "", e);
			}
			PutDataRequest putData = putDataRequestInternal.getPutDataRequest();
			TimeNomeEngine timeNomeEngine = TimeNomeInits.getEngine();
			String dsName = putData.getDataStoreName();
			StatusResponse.Builder statusBuilder = StatusResponse.newBuilder();
			List<DataEntry> entryList = putData.getDataEntryList();
			for(DataEntry dataEntry : entryList) {
				Entry<Double> entry = new Entry<Double>(dataEntry.getTimestamp(), dataEntry.getValue());
				EngineEntry engineEntry = new EngineEntry(userName, dsName, entry);
				try {
					timeNomeEngine.putEntry(engineEntry);
//					System.out.println("Entry added.");
				} catch (InterruptedException e) {
					// TODO This is a serious problem.
					// Do rollback here if any data added already.
					logger.log(Level.SEVERE, "", e);
					statusBuilder.setIsSuccess(false);
					Error error = Error.newBuilder()
							.setErrorCode(ErrorCodes.ENTRY_ADDITION_ERROR)
							.setErrorText(e.getLocalizedMessage())
							.build();
					statusBuilder.setErrorObj(error);
				}
			}
//			System.out.println("Done adding.");
			
			PutDataResponse.Builder putResponseBuilder = PutDataResponse.newBuilder();
			putResponseBuilder.setHandback(putDataRequestInternal.getHandback());
			statusBuilder.setIsSuccess(true);
			ExecutionResponse.Builder executionResponseBuilder = ExecutionResponse.newBuilder();
			executionResponseBuilder.setStatusResponse(statusBuilder);
			executionResponseBuilder.setPutDataResponse(putResponseBuilder);
			
			session.getAsyncRemote().sendBinary(ByteBuffer.wrap(executionResponseBuilder.build().toByteArray()));
//			logger.info(executionResponseBuilder.build().toString());
		}
	}
}
