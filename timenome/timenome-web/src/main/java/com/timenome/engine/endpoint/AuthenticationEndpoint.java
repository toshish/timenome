package com.timenome.engine.endpoint;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.logging.Level;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import com.timenome.api.core.User;
import com.timenome.api.exceptions.AuthenticationException;
import com.timenome.api.exceptions.MessageParsingException;
import com.timenome.api.logger.TimeNomeLogger;
import com.timenome.engine.endpoint.utils.EndpointUtils;
import com.timenome.engine.error.ErrorCodes;
import com.timenome.proto.TimeNomeProtos.StatusResponse;

@ServerEndpoint("/authenticate")
public class AuthenticationEndpoint {
	private static TimeNomeLogger logger = TimeNomeLogger.getTimeNomeLogger(AuthenticationEndpoint.class);
	
	@OnOpen
	public void onOpen(Session session) {
	}
	
	@OnError
	public void onError(Session session, Throwable t) {
		logger.log(Level.SEVERE, "", t);
		try {
			session.close();
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error while closing Session.", e);
		}
	}
	
	@OnClose
	public void onClose() {
	}
	
	
	@OnMessage
	public void onMessage(Session session, ByteBuffer byteBuf, boolean last) {
		logger.entering("AuthenticationEndpoint", "onMessage");
		
		StatusResponse statusResponse = null;
		
		try {
			User user = EndpointUtils.authenticateAPI(byteBuf);
			if(user != null) {
				statusResponse = EndpointUtils.getStatusResponse(true);
			}
			else {
				statusResponse = EndpointUtils.getStatusResponse(false, 
						EndpointUtils.getErrorInstance(ErrorCodes.INVALID_AUTHENTICATION_CREDENTIALS, "Invalid Authentication."));
			}
		} catch (MessageParsingException | AuthenticationException e) {
			statusResponse = EndpointUtils.getStatusResponse(false, 
					EndpointUtils.getErrorInstance(ErrorCodes.INVALID_MESSAGE_ERROR, e.getLocalizedMessage()));
		} 
		logger.info(statusResponse.toString());
		EndpointUtils.sendStatusResponse(statusResponse, session);
	}
}
