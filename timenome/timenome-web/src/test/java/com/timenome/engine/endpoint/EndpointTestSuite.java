package com.timenome.engine.endpoint;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({EndpointTest.class})
public class EndpointTestSuite {

}
