/**
 * 
 */
package com.timenome.engine.endpoint;

import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.protobuf.GeneratedMessage;
import com.timenome.api.core.User;
import com.timenome.api.exceptions.AuthenticationException;
import com.timenome.api.exceptions.MessageParsingException;
import com.timenome.engine.endpoint.enumeration.MessageEnum;
import com.timenome.engine.endpoint.utils.EndpointUtils;
import com.timenome.proto.TimeNomeProtos.Authentication;
import com.timenome.proto.TimeNomeProtos.ExecuteRequest;
import com.timenome.proto.TimeNomeProtos.ExecutionResponse;
import com.timenome.proto.TimeNomeProtos.ExecuteRequest.Builder;

/**
 * @author toshish
 *
 */
public class EndpointTest {



	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

//	/**
//	 * This test case will throw MessageParsingException on failure.
//	 * 
//	 * @throws MessageParsingException
//	 * @throws AuthenticationException
//	 */
//	@Test(expected=AuthenticationException.class)
//	public void testAuthenticationUtil() throws MessageParsingException, AuthenticationException {
//		Builder execReqBuilder = ExecuteRequest.newBuilder();
//		com.timenome.proto.TimeNomeProtos.Authentication.Builder authBuilder = Authentication.newBuilder();
//		authBuilder.setKey("testKey");
//		authBuilder.setSecret("testSecret");
//		execReqBuilder.setAuthentication(authBuilder.build());
//		User user = EndpointUtils.authenticateAPI(ByteBuffer.wrap(execReqBuilder.build().toByteArray()));
//		// Code should never reach here. And if it does, user should never be null.
//		assertNotNull(user);
//	}

//	@Test
//	public void testGetEnclosingMessageUtil() throws MessageParsingException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
//		for(MessageEnum messageEnum : MessageEnum.values()) {
//			if(messageEnum.toString().toLowerCase().contains("request")) {
//				Builder execReqBuilder = ExecuteRequest.newBuilder();
//				callSetterReq(execReqBuilder, messageEnum);
//				GeneratedMessage genMessage = EndpointUtils.getEnclosingMessage(ByteBuffer.wrap(execReqBuilder.build().toByteArray()), messageEnum);
//				assertNotNull(genMessage);
//				assertTrue(messageEnum.getProtoBufClass().isInstance(genMessage));
//			}
//			else {
//				callSetterResp(ExecutionResponse.newBuilder(), messageEnum);
//			}
//		}
//	}

	@Test
	public void testToStringMethod() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, InstantiationException {
		for(MessageEnum messageEnum : MessageEnum.values()) {
			Method method = messageEnum.getProtoBufClass().getMethod("getUnknownFields");
			assertNotNull(method);
//			String str = (String) method.invoke(messageEnum.getProtoBufClass().newInstance());
//			assertNotNull(str);
		}
	}

//	private void callSetterReq(Builder execBuilder, MessageEnum messageEnum) throws MessageParsingException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
//		Method setMethod = ExecuteRequest.Builder.class.getMethod("set" + messageEnum.getProtoBufClass().getSimpleName(), messageEnum.getProtoBufClass());
//		setMethod.invoke(execBuilder,  (messageEnum.getProtoBufClass().getMethod("newBuilder").invoke(null)) );
//		
//	}
//
//	private void callSetterResp(ExecutionResponse.Builder execBuilder, MessageEnum messageEnum) throws MessageParsingException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
//		Method setMethod = ExecutionResponse.Builder.class.getMethod("set" + messageEnum.getProtoBufClass().getSimpleName(), messageEnum.getProtoBufClass());
//		setMethod.invoke(execBuilder, messageEnum.getProtoBufClass().newInstance());
//		
//	}

}
