var endPointURL = "ws://" + window.location.host + "/" + window.location.pathname.split("/")[1] + "/get";

var websockConn = null;

var ProtoBuf = dcodeIO.ProtoBuf;
var timeWarpProto = ProtoBuf.protoFromFile("protofiles/timewarp.proto").build().timewarp;
var GetDataRequest =  timeWarpProto.GetDataRequest;
var DataStore = timeWarpProto.GetDataRequest.DataStoreRequest;

function connect() {
	websockConn = new WebSocket(endPointURL);
	websockConn.binaryType = "arraybuffer";//doesnt work with blob
	
	websockConn.onmessage = function(event) {		
		var result = timeWarpProto.GetDataResponse.decode(event.data);
		alert(result.dataStoreResponse[0].dataStoreName);
	};
	
	websockConn.onopen = function(event){
		console.log("websocket open");
		var getReq = new GetDataRequest();
		getReq.dataStore = new DataStore({dataStoreName: 'dummy-ds'});
		
		getReq.startTimestamp = new Date(2013, 10, 14, 1, 1, 1, 0);
		getReq.endTimestamp = new Date(2013, 11, 14, 1, 1, 1, 0);
		
		getReq.dataStore.archiveName.push('dummy-archive-7');
		
		sendMessage(getReq);
	};
	
	websockConn.onerror = function(event){
		console.log("websocket closed\n" + event.data);
	};
}

function disconnect() {
	websockConn.close();
}

function sendMessage(message) {
	websockConn.send(message.encode().toArrayBuffer());
}
