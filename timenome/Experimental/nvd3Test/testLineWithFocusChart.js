var time = new Date().getTime();

var data = [];

/*=======================================================================
month series
=========================================================================*/
var monthlySeries = {
	key: "Monthly",
	values: []
};
var msecInMonth = 31*24*60*60*1000;
for(var i=0; i<12;i++){
	monthlySeries.values.push([time + i*msecInMonth, Math.random()]);
}
data.push(monthlySeries);

/*=======================================================================
daily series
=========================================================================*/
var dailySeries = {
	key: "Daily",
	values: []
};
var msecInDay = 24*60*60*1000;
for(var i=0; i<12*31;i++){
if(i>100 && i<120 && i!==105){
dailySeries.values.push([time + i*msecInDay, null]);
continue;
}
	dailySeries.values.push([time + i*msecInDay, Math.random()*100]);
}
data.push(dailySeries);

/*=======================================================================
hourly series
=========================================================================*/
var hourlySeries = {
	key: "Hourly",
	values: []
};
var msecInHour = 60*60*1000;
// for(var i=0; i<12*31*24;i++){
// 	hourlySeries.values.push([time + i*msecInHour, Math.random()*100]);
// }

for(var i=0; i<12;i++){
	hourlySeries.values.push([time + i*msecInMonth, Math.random()*10000]);
}

data.push(hourlySeries);


/*=======================================================================
graph code
=========================================================================*/
function loadGraph(){
	nv.addGraph(function() {
		var chart = nv.models.lineWithFocusChart().x(function(d) { 
			return d[0]; 
		}).y(function(d) {
			 return d[1]; //adjusting, 100% is 1.00, not 100 as it is in the data
			}).color(d3.scale.category10().range()).yDomain([0,200,10000]).
		yRange([300,200,0]);
		// ;

		// chart.xAxis
		// .tickFormat(d3.format(',f'));
		chart.xAxis
		.tickFormat(function(d) {
			return d3.time.format('%X')(new Date(d));
		});

		chart.x2Axis
		.tickFormat(function(d) {
			return d3.time.format('%x')(new Date(d));
		});

		chart.yAxis
		.tickFormat(d3.format(',.1f'));

		chart.y2Axis
		.tickFormat(d3.format(',.1f'));

		d3.select('#testchart svg')
		.datum(data)
		.transition().duration(500)
		.call(chart);

		nv.utils.windowResize(chart.update);

		return chart;
	});
}
