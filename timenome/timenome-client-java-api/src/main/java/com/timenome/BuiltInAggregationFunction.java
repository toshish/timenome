package com.timenome;

/**
 * @author anirudh
 * 
 */
public enum BuiltInAggregationFunction {
	/**
	 * Stores maximum value from set of values collected over Storage Precision
	 * / Counter Size.
	 */
	MAX,
	/**
	 * Stores minimum value from set of values collected over Storage Precision
	 * / Counter Size.
	 */
	MIN,
	/**
	 * Stores average/mean of the set of values collected over Storage Precision
	 * / Counter Size.
	 */
	AVERAGE,
	/**
	 * Stores sum of the set of values collected over Storage Precision /
	 * Counter Size.
	 */
	SUM,
	/**
	 * Stores last value from the set of values collected over Storage Precision
	 * / Counter Size.
	 */
	LAST,
	/**
	 * Stores last value from the set of values collected over Storage Precision
	 * / Counter Size.
	 */
	FIRST,
	/**
	 * Stores the difference between maximum and minimum of the values from the
	 * set of values collected over Storage Precision / Counter Size.
	 */
	RANGE,
	/**
	 * Values are stored as they are.
	 */
	NO_OPERATION;
}
