package com.timenome;

/**
 * TimeUnit represents various units of time such as seconds, weeks, years etc.
 * 
 * @author anirudh
 * 
 */
public enum TimeUnit {
	MILLI_SECOND(1), SECOND(MILLI_SECOND.timeInMilliseconds * 1000), MINUTE(SECOND.timeInMilliseconds * 60), HOUR(
			MINUTE.timeInMilliseconds * 60), DAY(HOUR.timeInMilliseconds * 24), WEEK(DAY.timeInMilliseconds * 7), MONTH(
			DAY.timeInMilliseconds * 30), // 28, 30,31
	QUARTER(MONTH.timeInMilliseconds * 4), YEAR(MONTH.timeInMilliseconds * 12);

	long timeInMilliseconds;

	TimeUnit(long val) {
		this.timeInMilliseconds = val;
	}

	public long getTimeInMilliseconds() {
		return timeInMilliseconds;
	}
}
