package com.timenome;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.protobuf.InvalidProtocolBufferException;
import com.timenome.enumeration.MessageEnum;
import com.timenome.exception.AuthenticationException;
import com.timenome.exception.ConnectivityException;
import com.timenome.exception.DataStoreException;
import com.timenome.exception.EntityCreationException;
import com.timenome.exception.EntityRemovalException;
import com.timenome.exception.MessageParsingException;
import com.timenome.exception.TimeNomeException;
import com.timenome.factory.DataStoreFactory;
import com.timenome.handler.PartialMessageHandler;
import com.timenome.handler.TimeNomePartialMessageHandler;
import com.timenome.proto.TimeNomeProtos.CreateDataStoreRequest;
import com.timenome.proto.TimeNomeProtos.DataEntry;
import com.timenome.proto.TimeNomeProtos.DeleteDataStoreRequest;
import com.timenome.proto.TimeNomeProtos.ExecuteRequest;
import com.timenome.proto.TimeNomeProtos.ExecutionResponse;
import com.timenome.proto.TimeNomeProtos.GetDataRequest;
import com.timenome.proto.TimeNomeProtos.GetDataResponse;
import com.timenome.proto.TimeNomeProtos.StatusResponse;
import com.timenome.stream.DataEntryInputStream;
import com.timenome.stream.DataStreamReceiver;
import com.timenome.util.MessagingUtil;
import com.timenome.websocket.ExecuteCommandNonPersistentConnection;

public class WebsocketTimeNomeConnection extends AbstractTimeNomeConnection {
	private ExecuteCommandNonPersistentConnection timeNomeNonPersistentConnection;
	private DataStoreFactory dataStoreFactory;
	private PartialMessageHandler messageHandler = new TimeNomePartialMessageHandler();

	public WebsocketTimeNomeConnection(String host, int port, String key, String secret) throws ConnectivityException,
			MessageParsingException, AuthenticationException {
		super(host, port);
		timeNomeNonPersistentConnection = new ExecuteCommandNonPersistentConnection(host, port, key, secret);
		dataStoreFactory = DataStoreFactory.getDataStoreFactory(timeNomeNonPersistentConnection);
		if (!authenticate()) {
			throw new AuthenticationException("Invalid authentication.");
		}
	}

	private boolean authenticate() throws ConnectivityException, MessageParsingException {
		ExecuteRequest execRequest = MessagingUtil.createEmptyExecuteRequest(timeNomeNonPersistentConnection);
		ByteBuffer byteBuffer = timeNomeNonPersistentConnection.sendAsync("/authenticate",
				ByteBuffer.wrap(execRequest.toByteArray()), messageHandler);
		ExecutionResponse execResponse;
		try {
			execResponse = ExecutionResponse.parseFrom(byteBuffer.array());
		} catch (InvalidProtocolBufferException e) {
			throw new MessageParsingException(e);
		}
		return execResponse.getStatusResponse().getIsSuccess();
	}

	@Override
	public DataStore createDataStore(String dataStoreName) throws ConnectivityException, EntityCreationException,
			MessageParsingException, DataStoreException {
		DataStore dataStore = null;
		CreateDataStoreRequest.Builder cdSB = CreateDataStoreRequest.newBuilder();
		com.timenome.proto.TimeNomeProtos.DataStore.Builder dsb = com.timenome.proto.TimeNomeProtos.DataStore
				.newBuilder();
		dsb.setDataStoreName(dataStoreName);
		cdSB.setDataStore(dsb);
		CreateDataStoreRequest createDataStoreRequest = cdSB.build();

		ExecuteRequest execRequest = MessagingUtil.createExecuteRequest(timeNomeNonPersistentConnection,
				createDataStoreRequest, MessageEnum.CREATE_DATASTORE_REQUEST);
		ByteBuffer byteBuffer = timeNomeNonPersistentConnection.sendAsync("/create/datastore",
				ByteBuffer.wrap(execRequest.toByteArray()), messageHandler);

		ExecutionResponse execResponse = null;
		try {
			execResponse = ExecutionResponse.parseFrom(byteBuffer.array());
		} catch (InvalidProtocolBufferException e) {
			throw new MessageParsingException(e);
		}
		StatusResponse statusResponse = execResponse.getStatusResponse();

		if (statusResponse.getErrorObj() != null) {
			if (statusResponse.getErrorObj().getErrorCode() == 31 || statusResponse.getErrorObj().getErrorCode() == 41) {
				// This data store already exists.
				// Throw this error.
				throw new EntityCreationException("DataStore " + dataStoreName + " already exists.");

			} else if (statusResponse.getErrorObj().getErrorCode() == 33) {
				throw new EntityCreationException("Error detected while creating DataStore " + dataStoreName + ".");
			}
		}

		dataStore = dataStoreFactory.getDataStoreInstance(dataStoreName);
		return dataStore;
	}

	@Override
	public DataStore getDataStore(String dataStoreName) throws ConnectivityException, MessageParsingException,
			DataStoreException {
		return dataStoreFactory.getDataStoreInstance(dataStoreName);
	}

	@Override
	public List<DataStore> getDataStores() throws ConnectivityException {
		return dataStoreFactory.getAllDataStores();
	}

	@Override
	public boolean deleteDataStore(String dataStoreName) throws ConnectivityException, EntityRemovalException,
			MessageParsingException {

		DeleteDataStoreRequest.Builder delDSB = DeleteDataStoreRequest.newBuilder();
		delDSB.setDataStoreName(dataStoreName);
		DeleteDataStoreRequest deleleteDataStoreRequest = delDSB.build();

		ExecuteRequest execRequest = MessagingUtil.createExecuteRequest(timeNomeNonPersistentConnection,
				deleleteDataStoreRequest, MessageEnum.DELETE_DATASTORE_REQUEST);
		ByteBuffer byteBuffer = timeNomeNonPersistentConnection.sendAsync("/delete/datastore",
				ByteBuffer.wrap(execRequest.toByteArray()), messageHandler);

		ExecutionResponse execResponse = null;
		try {
			execResponse = ExecutionResponse.parseFrom(byteBuffer.array());
		} catch (InvalidProtocolBufferException e) {
			throw new MessageParsingException(e);
		}

		StatusResponse statusResponse = execResponse.getStatusResponse();

		if (statusResponse.getErrorObj().hasErrorCode()) {
			if (statusResponse.getErrorObj().getErrorCode() == 32) {
				// This data store already exists.
				// Throw this error.
				throw new EntityRemovalException("DataStore " + dataStoreName + " does not exists.");

			} else if (statusResponse.getErrorObj().getErrorCode() == 34) {
				throw new EntityRemovalException("Error detected while deleting DataStore " + dataStoreName + ".");
			}
		}
		return statusResponse.getIsSuccess();
	}

	@Override
	public void streamData(String dataStoreName, String archiveName, DataStreamReceiver streamReciver)
			throws ConnectivityException, MessageParsingException {
		ExecuteCommandNonPersistentConnection connection = this.getTimeNomeNonPersistentConnection();
		if (streamReciver != null) {
			new DataEntryInputStream(connection, streamReciver).sendStreamRequest(dataStoreName, archiveName);
		}
	}
	
	@Override
	public List<DataTuple<Double>> getData(String dataStoreName, String archiveName, Date startTimeStamp, Date endTimeStamp)
			throws ConnectivityException, MessageParsingException, TimeNomeException {
		
		GetDataRequest.Builder getDataRequestBuilder = GetDataRequest.newBuilder();
		
		getDataRequestBuilder.setStartTimestamp(startTimeStamp.getTime());
		getDataRequestBuilder.setEndTimestamp(endTimeStamp.getTime());
		getDataRequestBuilder.setArchiveName(archiveName);
		getDataRequestBuilder.setDataStoreName(dataStoreName);
		
		ExecuteRequest execRequest = MessagingUtil.createExecuteRequest(timeNomeNonPersistentConnection, getDataRequestBuilder.build(), MessageEnum.GET_DATA_REQUEST);
		ByteBuffer byteBuffer = timeNomeNonPersistentConnection.sendAsync("/get/data", ByteBuffer.wrap(execRequest.toByteArray()), messageHandler);
		
		GetDataResponse getDataResponse = MessagingUtil.getEnclosingMessageFromExecutionResponse(byteBuffer, MessageEnum.GET_DATA_RESPONSE);
		
		List<DataEntry> dataEntryList = getDataResponse.getDataEntryList();
		
		List<DataTuple<Double>> listEntries = new ArrayList<DataTuple<Double>>();
		for(DataEntry dataEntry : dataEntryList) {
			listEntries.add(new DataTuple<Double>(dataEntry.getTimestamp(), dataEntry.getValue()));
		}
		
		return listEntries;
	}

	@Override
	public ExecuteCommandNonPersistentConnection getTimeNomeNonPersistentConnection() {
		return timeNomeNonPersistentConnection;
	}

	@Override
	public void close() {
	}
}
