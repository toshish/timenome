package com.timenome;

import java.util.List;

import com.timenome.exception.ConnectivityException;
import com.timenome.stream.DataEntryInputStream;
import com.timenome.stream.DataStreamReceiver;
import com.timenome.websocket.ExecuteCommandNonPersistentConnection;
import com.timenome.websocket.TimeNomeStreamConnection;

public class ArchiveBasic implements Archive {
	private String archiveName;
	private TimeRange storagePrecision;
	private BuiltInAggregationFunction builtInCdf;
	private TimeNomeStreamConnection streamConnection;
	
	public ArchiveBasic(String archiveName, 
			TimeRange storagePrecision, BuiltInAggregationFunction builtInCdf) {
		super();
		this.archiveName = archiveName;
		this.storagePrecision = storagePrecision;
		this.builtInCdf = builtInCdf;
	}
	
	public ArchiveBasic(String archiveName, 
			long storagePrecision, BuiltInAggregationFunction builtInCdf) {
		this(archiveName, new TimeRange(TimeUnit.MILLI_SECOND, storagePrecision), builtInCdf);
	}
	
	public ArchiveBasic(String archiveName, 
			long storagePrecision, String builtInCdf) {
		this(archiveName, new TimeRange(TimeUnit.MILLI_SECOND, storagePrecision), BuiltInAggregationFunction.valueOf(builtInCdf));
	}
	
	public ArchiveBasic(String archiveName, 
			TimeRange storagePrecision, String builtInCdf) {
		this(archiveName, storagePrecision, BuiltInAggregationFunction.valueOf(builtInCdf));
	}
	
	@Override
	public List<DataTuple<? extends Number>> getData() {
		return null;
	}
	
	@Override
	public String getArchiveName() {
		return archiveName;
	}
	
	
	@Override
	public long getStoragePrecisionInMillis() {
		return storagePrecision.getTimeRangeInMillseconds();
	}

	@Override
	public BuiltInAggregationFunction getBuiltInAggregationFunction() {
		return builtInCdf;
	}
	
	@Override
	public String toString() {
		return "[" + archiveName + ", " + storagePrecision.getTimeRangeInMillseconds() + ", " + builtInCdf.toString() + "]";
	}
}
