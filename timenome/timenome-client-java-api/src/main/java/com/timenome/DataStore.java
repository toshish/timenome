package com.timenome;

import java.util.List;
import java.util.Map;

import com.timenome.exception.ConnectivityException;
import com.timenome.exception.EntityCreationException;
import com.timenome.exception.EntityRemovalException;
import com.timenome.exception.MessageParsingException;
import com.timenome.exception.TimeNomeException;

/**
 * Data Store represents a collection of storage archives where data is physically stored. 
 *
 */
public interface DataStore {
	
	public String getDataStoreName();
	
	/**
	 * Returns a map of all archives present in the data store.
	 * @return Map of archive name and archive object.
	 * @throws MessageParsingException
	 * @throws ConnectivityException
	 * @throws TimeNomeException
	 */
	public Map<String, Archive> getArchives() throws MessageParsingException, ConnectivityException, TimeNomeException;
	
	public Archive getArchive(String archiveName) throws ConnectivityException, MessageParsingException, TimeNomeException;
	
	public Archive addArchive(String archiveName, TimeRange storagePrecision, BuiltInAggregationFunction builtInCdf) throws ConnectivityException, EntityCreationException, MessageParsingException;

	public boolean deleteArchive(String archiveName) throws ConnectivityException, EntityRemovalException;
	
	/**
	 * Add a data point to the data store's archive
	 * @param entry
	 * @return
	 * @throws ConnectivityException
	 */
	public boolean putData(DataTuple<? extends Number> entry) throws ConnectivityException;
	
	/**
	 * Utility method to add multiple data points at once to data store's archives
	 * @param entries
	 * @return
	 * @throws ConnectivityException
	 */
	public boolean putData(List<DataTuple<? extends Number>> entries) throws ConnectivityException;
	
	/**
	 * Add a data point to the data store's archive
	 * @param timeStamp
	 * @param value
	 * @return
	 * @throws ConnectivityException
	 */
	public boolean putData(long timeStamp, Number value) throws ConnectivityException;
	
}