package com.timenome.factory;

import com.timenome.WebsocketTimeNomeConnection;
import com.timenome.TimeNomeConnection;
import com.timenome.exception.AuthenticationException;
import com.timenome.exception.ConnectivityException;
import com.timenome.exception.MessageParsingException;

public final class TimeNomeConnectionFactory {
	/**
	 * @param host The ipaddress/hostname of the machine where TimeNome engine is running
	 * @param port The port number on which TimeNome engine is running
	 * @param key Key of the user to be authenticated.
	 * @param secret Secret of the user to be authenticated.
	 * @return
	 * @throws ConnectivityException
	 * @throws MessageParsingException
	 * @throws AuthenticationException
	 */
	public static TimeNomeConnection createConnection(String host, int port, String key, String secret) throws ConnectivityException, MessageParsingException, AuthenticationException {
		return new WebsocketTimeNomeConnection(host, port, key, secret);
	}
}
