package com.timenome.factory;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.timenome.DataStore;
import com.timenome.DataStoreBasic;
import com.timenome.enumeration.MessageEnum;
import com.timenome.exception.ConnectivityException;
import com.timenome.exception.DataStoreException;
import com.timenome.exception.MessageParsingException;
import com.timenome.exception.TimeNomeException;
import com.timenome.handler.PartialMessageHandler;
import com.timenome.handler.TimeNomePartialMessageHandler;
import com.timenome.proto.TimeNomeProtos.ExecuteRequest;
import com.timenome.proto.TimeNomeProtos.GetDataStoreRequest;
import com.timenome.proto.TimeNomeProtos.GetDataStoreResponse;
import com.timenome.util.MessagingUtil;
import com.timenome.websocket.ExecuteCommandNonPersistentConnection;

public final class DataStoreFactory {
	private static Map<ExecuteCommandNonPersistentConnection, DataStoreFactory> dataStoreFactory = new HashMap<>();
	private ExecuteCommandNonPersistentConnection nonPersistentConnection;
//	private TimeNomePersistentConnection putConnection;
	private PartialMessageHandler messageHandler = new TimeNomePartialMessageHandler();

	private DataStoreFactory(ExecuteCommandNonPersistentConnection connection) throws ConnectivityException {
		this.nonPersistentConnection = connection;
//		try {
//			this.putConnection = new TimeNomePersistentConnection(connection.getHost(),
//														   connection.getPort(),
//														   connection.getKey(),
//														   connection.getSecret());
//		} catch (MessageParsingException e) {
//			throw new ConnectivityException(e);
//		}
//		System.out.println("Constructor - DataStoreFactory " + this.putConnection);
	}

	public static DataStoreFactory getDataStoreFactory(ExecuteCommandNonPersistentConnection connection) throws ConnectivityException {
		if (dataStoreFactory.get(connection) == null) {
			dataStoreFactory.put(connection, new DataStoreFactory(connection));
		}
		return dataStoreFactory.get(connection);
	}

	public DataStore getDataStoreInstance(String dataStoreName) throws MessageParsingException, ConnectivityException,
			DataStoreException {
		DataStore dataStore = null;

		GetDataStoreRequest.Builder getDataStoreRequestBuilder = GetDataStoreRequest.newBuilder();
		getDataStoreRequestBuilder.setDataStoreName(dataStoreName);

		ExecuteRequest execRequest = MessagingUtil.createExecuteRequest(nonPersistentConnection,
				getDataStoreRequestBuilder.build(), MessageEnum.GET_DATASTORE_REQUEST);

		ByteBuffer byteBuffer = nonPersistentConnection.sendAsync("/get/datastore", ByteBuffer.wrap(execRequest.toByteArray()), messageHandler);
		
		GetDataStoreResponse getDataStoreResponse;
		try {
			getDataStoreResponse = MessagingUtil.getEnclosingMessageFromExecutionResponse(byteBuffer, MessageEnum.GET_DATA_STORE_RESPONSE);
		} catch (TimeNomeException e) {
			throw new DataStoreException(e);
		}
		
		dataStore = new DataStoreBasic(getDataStoreResponse.getDataStore().getDataStoreName(), nonPersistentConnection);
		return dataStore;
	}

	public List<DataStore> getAllDataStores() throws ConnectivityException {
		updateDataStoreMap();
		ArrayList<DataStore> dsList = new ArrayList<>();
		// Set<String> keySet = dataStoreMap.keySet();
		// for(String dsName : keySet) {
		// dsList.add(dataStoreMap.get(dsName));
		// }
		return dsList;
	}

	private void updateDataStoreMap() throws ConnectivityException {
		// retrive from engine
		// ByteBuffer byteBuffer = null;
		// PartialMessageHandler messageHandler = new
		// TimeNomePartialMessageHandler();
		// ListDataStoresRequest listDSR =
		// ListDataStoresRequest.newBuilder().build();
		// byteBuffer = connection.sendAsync("/list/datastore",
		// ByteBuffer.wrap(listDSR.toByteArray()), messageHandler);
		// ListDataStoresResponse ldsnR;
		// try {
		// ldsnR = ListDataStoresResponse.parseFrom(byteBuffer.array());
		// } catch (InvalidProtocolBufferException e) {
		// // TODO Auto-generated catch block
		// throw new ConnectivityException("Invalid message received.", e);
		// }
		//
		// List<String> dsList = ldsnR.getDataStoreNameList();
		// for(String dsName : dsList) {
		// if(!dataStoreMap.containsKey(dsName)) {
		// dataStoreMap.put(dsName, new DataStoreBasic(dsName, connection,
		// putConnection));
		// }
		// }
	}

}
