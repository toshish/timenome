/**
 * 
 */
package com.timenome.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;

import com.google.protobuf.GeneratedMessage;
import com.google.protobuf.InvalidProtocolBufferException;
import com.timenome.enumeration.MessageEnum;
import com.timenome.exception.MessageParsingException;
import com.timenome.exception.TimeNomeException;
import com.timenome.proto.TimeNomeProtos.Authentication;
import com.timenome.proto.TimeNomeProtos.ExecuteRequest;
import com.timenome.proto.TimeNomeProtos.ExecutionResponse;
import com.timenome.proto.TimeNomeProtos.StatusResponse;
import com.timenome.websocket.ExecuteCommandNonPersistentConnection;

/**
 * @author toshish
 *
 */
public final class MessagingUtil {

	public static ExecuteRequest createExecuteRequest(ExecuteCommandNonPersistentConnection connection,
			GeneratedMessage message, MessageEnum messageEnum) throws MessageParsingException {
		ExecuteRequest.Builder execRequestBuilder = ExecuteRequest.newBuilder();
		Class<?> protoBufClass = messageEnum.getProtoBufClass();
		execRequestBuilder.setAuthentication(createAuthenticationMessage(connection));
		Method setMethod = null;
		try {
			setMethod = ExecuteRequest.Builder.class.getMethod("set" + protoBufClass.getSimpleName(), protoBufClass);
		} catch (NoSuchMethodException | SecurityException e1) {
			throw new MessageParsingException("Error while finding type " + messageEnum.toString(), e1);
		}

		try {

			setMethod.invoke(execRequestBuilder, message);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new MessageParsingException("Error while executing getter for message.", e);
		}
		return execRequestBuilder.build();
	}

	private static Authentication createAuthenticationMessage(ExecuteCommandNonPersistentConnection connection) {
		Authentication.Builder authBuilder = Authentication.newBuilder();
		authBuilder.setKey(connection.getKey());
		authBuilder.setSecret(connection.getSecret());
		return authBuilder.build();
	}

	public static ExecuteRequest createEmptyExecuteRequest(ExecuteCommandNonPersistentConnection connection) {
		ExecuteRequest.Builder execRequestBuilder = ExecuteRequest.newBuilder();
		execRequestBuilder.setAuthentication(createAuthenticationMessage(connection));
		return execRequestBuilder.build();
	}

	@SuppressWarnings("unchecked")
	public static <T extends GeneratedMessage> T getEnclosingMessageFromExecutionResponse(ByteBuffer byteBuf, MessageEnum messageEnum) 
											throws MessageParsingException, TimeNomeException {
		
		ExecutionResponse execResponse;
		try {
			execResponse = ExecutionResponse.parseFrom(byteBuf.array());
		} catch (InvalidProtocolBufferException e1) {
			throw new MessageParsingException("Error while parsing message.", e1);
		}
		
		StatusResponse statusResponse = execResponse.getStatusResponse();
		
		if(!statusResponse.getIsSuccess()) {
			throw new TimeNomeException(statusResponse.getErrorObj().getErrorText());
		}
		
		Class<?> protoBufClass = messageEnum.getProtoBufClass();
		Method getMethod = null;
		try {
			getMethod = ExecutionResponse.class.getMethod("get" + protoBufClass.getSimpleName());
		} catch (NoSuchMethodException | SecurityException e1) {
			throw new MessageParsingException("Error while finding type " + messageEnum.toString(), e1);
		}
		
		T genMessage = null;
		
		try {
			genMessage = (T) getMethod.invoke(execResponse);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new MessageParsingException("Error while executing getter for message.", e);
		}
		return genMessage;
	}
	
	public static StatusResponse getStatusResponseFromExecutionResponse(ByteBuffer byteBuf) throws MessageParsingException {
		ExecutionResponse execResponse;
		try {
			execResponse = ExecutionResponse.parseFrom(byteBuf.array());
		} catch (InvalidProtocolBufferException e1) {
			throw new MessageParsingException("Error while parsing message", e1);
		}
		return execResponse.getStatusResponse();
	}

}
