/**
 * 
 */
package com.timenome.stream;

import java.nio.ByteBuffer;

/**
 * @author toshish
 *
 */
public interface ByteBufferStreamReceiver{
	public void onReceived(ByteBuffer byteBuffer);
}
