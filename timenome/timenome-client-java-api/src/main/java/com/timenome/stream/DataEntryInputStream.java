/**
 * 
 */
package com.timenome.stream;

import java.io.Closeable;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import com.google.protobuf.InvalidProtocolBufferException;
import com.timenome.DataTuple;
import com.timenome.exception.ConnectivityException;
import com.timenome.exception.MessageParsingException;
import com.timenome.exception.TimeNomeException;
import com.timenome.proto.TimeNomeProtos.DataEntry;
import com.timenome.proto.TimeNomeProtos.ExecutionResponse;
import com.timenome.proto.TimeNomeProtos.GetDataResponse;
import com.timenome.websocket.ExecuteCommandNonPersistentConnection;
import com.timenome.websocket.TimeNomeStreamConnection;

/**
 * @author toshish
 *
 */
public class DataEntryInputStream extends Observable implements Closeable, Runnable  {

	private TimeNomeStreamConnection streamConnection;
//	private boolean isOpen = false;
//	private DataEntryStreamReciever dataEntryStreamReciver;
	private Thread selfThread;

	public DataEntryInputStream(ExecuteCommandNonPersistentConnection nonPersistentConnection, final DataStreamReceiver dataEntryStreamReciver) throws ConnectivityException {
		
		selfThread = new Thread(this);
		if(streamConnection != null) {
			selfThread.start();
		}
		
		ByteBufferStreamReceiver streamReceiver = new ByteBufferStreamReceiver() {

			@Override
			public void onReceived(ByteBuffer byteBuffer) {
				try {
					List<DataTuple<Double>> entryList = read(byteBuffer);
					if(entryList.size() > 0) {
						dataEntryStreamReciver.onReceived(entryList);
					}
				} catch (InvalidProtocolBufferException e) {
					selfThread.interrupt();
					e.printStackTrace();
				} catch (TimeNomeException e) {
					selfThread.interrupt();
					e.printStackTrace();
				}
			}

		};
		
		try {
			streamConnection = new TimeNomeStreamConnection(nonPersistentConnection.getHost(), nonPersistentConnection.getPort(),
					nonPersistentConnection.getKey(), nonPersistentConnection.getSecret(), streamReceiver);
		} catch (MessageParsingException e) {
			throw new ConnectivityException(e);
		}
		
		
	}

	public List<DataTuple<Double>> read(ByteBuffer byteBuffer) throws TimeNomeException, InvalidProtocolBufferException {
		List<DataTuple<Double>> dataEntryList = new ArrayList<>();
		//		if(dataEntryList == null) {
		//			throw new TimeNomeException("List cannot be null.");
		//		}

		ExecutionResponse execResp = ExecutionResponse.parseFrom(byteBuffer.array());
		if(execResp.getStatusResponse().getIsSuccess()) {
			GetDataResponse getDataResponse = execResp.getGetDataResponse();
			List<DataEntry> listOfDE = getDataResponse.getDataEntryList();
			for(DataEntry de : listOfDE) {
				dataEntryList.add(new DataTuple<>(de.getTimestamp(), de.getValue()));
			}
		}
		else {
			throw new TimeNomeException(execResp.getStatusResponse().getErrorObj().toString());
		}
		return dataEntryList;

	}

	/* (non-Javadoc)
	 * @see java.io.Closeable#close()
	 */
	@Override
	public void close() throws IOException {
//		isOpen = false;
		selfThread.interrupt();
	}

	public void sendStreamRequest(String dataStoreName, String archiveName) throws ConnectivityException, MessageParsingException {
		streamConnection.sendStreamRequest(dataStoreName, archiveName);
	}

	@Override
	public void run() {
		System.out.println("Thread started.");
		try {
			wait();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
