/**
 * 
 */
package com.timenome.stream;

import java.util.List;

import com.timenome.DataTuple;


/**
 * @author toshish
 *
 */
public interface DataStreamReceiver{
	public void onReceived(List<DataTuple<Double>> listOfEntries);
}
