package com.timenome;

import java.util.List;

public interface Archive {
	List<DataTuple<? extends Number>> getData();

	String getArchiveName();

	long getStoragePrecisionInMillis();

	BuiltInAggregationFunction getBuiltInAggregationFunction();
}
