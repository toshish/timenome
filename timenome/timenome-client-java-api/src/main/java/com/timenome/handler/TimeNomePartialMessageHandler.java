package com.timenome.handler;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

public class TimeNomePartialMessageHandler implements PartialMessageHandler {
	final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

	private ByteBuffer byteBuffer;
	public TimeNomePartialMessageHandler() {
		
	}
	
	@Override
	public void onMessage(byte[] message, boolean isLast) {
		try {
			outputStream.write(message);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(isLast) {
			byteBuffer = ByteBuffer.wrap(outputStream.toByteArray());
			outputStream.reset();
		}
	}

	@Override
	public ByteBuffer getByteBuffer() {
		return this.byteBuffer;
	}

	@Override
	public void readFromStream(InputStream is) throws IOException {
		byte[] buffer = new byte[8192];
	    int bytesRead;
	    ByteArrayOutputStream output = new ByteArrayOutputStream();
	    while ((bytesRead = is.read(buffer)) != -1)
	    {
	        output.write(buffer, 0, bytesRead);
	        System.out.println(bytesRead);
	    }
	    
	    byteBuffer = ByteBuffer.wrap(output.toByteArray());
	}

}
