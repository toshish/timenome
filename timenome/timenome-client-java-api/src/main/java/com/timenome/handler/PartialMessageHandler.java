package com.timenome.handler;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import javax.websocket.MessageHandler;

public interface PartialMessageHandler extends MessageHandler.Partial<byte[]> {
	public ByteBuffer getByteBuffer();

	public void readFromStream(InputStream is) throws IOException;
}
