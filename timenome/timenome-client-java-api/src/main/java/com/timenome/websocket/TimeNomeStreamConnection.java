package com.timenome.websocket;

import java.nio.ByteBuffer;

import com.google.protobuf.InvalidProtocolBufferException;
import com.timenome.exception.ConnectivityException;
import com.timenome.exception.MessageParsingException;
import com.timenome.proto.TimeNomeProtos.Authentication;
import com.timenome.proto.TimeNomeProtos.ExecuteRequest;
import com.timenome.proto.TimeNomeProtos.ExecutionResponse;
import com.timenome.proto.TimeNomeProtos.StatusResponse;
import com.timenome.proto.TimeNomeProtos.StreamDataRequest;
import com.timenome.stream.ByteBufferStreamReceiver;

public class TimeNomeStreamConnection extends ExecuteCommandPersistentConnection {
	
	public TimeNomeStreamConnection(String host, int port, String key, String secret, ByteBufferStreamReceiver streamReceiver)
			throws ConnectivityException, MessageParsingException {
		super(host, port, key, secret, ExecuteCommandPersistentConnection.ConnectionOperation.STREAM, streamReceiver);
	}
	
	public void sendStreamRequest(String dataStoreName, String archiveName) throws ConnectivityException, MessageParsingException {
		Authentication.Builder authBuilder = Authentication.newBuilder();
		
		authBuilder.setKey(key);
		authBuilder.setSecret(secret);
		
		ExecuteRequest.Builder execRequestBuilder = ExecuteRequest.newBuilder();
		execRequestBuilder.setAuthentication(authBuilder.build());
		
		
		StreamDataRequest.Builder streamDataRequestBuilder = StreamDataRequest.newBuilder();
		
		streamDataRequestBuilder.setDataStoreName(dataStoreName);
		streamDataRequestBuilder.setArchiveName(archiveName);
		execRequestBuilder.setStreamDataRequest(streamDataRequestBuilder);
		
		ByteBuffer authRespByteBuf = sendAsync(ByteBuffer.wrap(execRequestBuilder.build().toByteArray()));
		
		try {
			ExecutionResponse execResponse = ExecutionResponse.parseFrom(authRespByteBuf.array());
			StatusResponse statusResponse = execResponse.getStatusResponse();
//			System.out.println(execResponse);
			if(!statusResponse.getIsSuccess()) {
				throw new ConnectivityException(statusResponse.getErrorObj().getErrorText());
			}
		} catch (InvalidProtocolBufferException e) {
			throw new MessageParsingException(e);
		}
	}
	
}
