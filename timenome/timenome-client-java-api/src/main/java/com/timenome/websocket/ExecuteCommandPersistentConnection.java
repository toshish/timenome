package com.timenome.websocket;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;

import javax.websocket.DeploymentException;

import com.timenome.exception.ConnectivityException;
import com.timenome.exception.MessageParsingException;
import com.timenome.handler.TimeNomePartialMessageHandler;
import com.timenome.stream.ByteBufferStreamReceiver;

public abstract class ExecuteCommandPersistentConnection extends ExecuteCommandConnection {

	public enum ConnectionOperation {
		PUT("/timenome/put/data"), STREAM("/timenome/stream/data");

		private String uri;

		private ConnectionOperation(String uri) {
			this.uri = uri;
		}

		public String getURI() {
			return uri;
		}
	}

	protected TimeNomeWebsocketClientEndPoint timenomeClient;

	private ConnectionOperation conOperation; 

	public ExecuteCommandPersistentConnection(String host, int port, String key, String secret, ConnectionOperation operation) throws ConnectivityException, MessageParsingException {
		super(host, port, key, secret);
		this.conOperation = operation;
		try {
			timenomeClient = new TimeNomeWebsocketClientEndPoint(new URI("ws://"+ host + ":" + port + conOperation.getURI()), new TimeNomePartialMessageHandler());
		} catch (DeploymentException e) {
			// TODO Add log entry
			throw new ConnectivityException("Deployment of a client endpoint failed for URL : " + "ws://"+ host + ":" + port + conOperation.getURI(), e);
		} catch (IOException e) {
			// TODO Add log entry
			throw new ConnectivityException("Failed while connecting to host " + host + " on port " + port + ".", e);
		} catch (URISyntaxException e) {
			// TODO Add log entry
			throw new ConnectivityException("Internal error due to invalid URI.", e);
		}
	}
	
	public ExecuteCommandPersistentConnection(String host, int port, String key, String secret, ConnectionOperation operation, ByteBufferStreamReceiver streamReceiver) throws ConnectivityException, MessageParsingException {
		this(host, port, key, secret, operation);
		timenomeClient.setStreamReceiver(streamReceiver);
	}

	public ByteBuffer sendAsync(ByteBuffer byteBuffer) throws ConnectivityException {
		ByteBuffer byteBufferResponse = null;
		// TODO Log the retry attempt message
		try {
			timenomeClient.send(byteBuffer.array());

			while(timenomeClient.getByteBuffer() == null) {
				Thread.sleep(10);
			}
			byteBufferResponse = timenomeClient.getByteBuffer();
			//			System.out.println("send of put completed of putconnection : " + this);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new ConnectivityException("Connectivity problem while sending request.", e);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			throw new ConnectivityException("Transmission interrupted.", e);
		}
		return byteBufferResponse;
	}



}
