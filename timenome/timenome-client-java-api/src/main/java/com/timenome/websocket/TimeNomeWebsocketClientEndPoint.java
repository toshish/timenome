package com.timenome.websocket;

import java.io.IOException;
import java.net.URI;
import java.nio.ByteBuffer;
import java.util.Observable;
import java.util.concurrent.Future;

import javax.websocket.ClientEndpoint;
import javax.websocket.CloseReason;
import javax.websocket.ContainerProvider;
import javax.websocket.DeploymentException;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

import com.timenome.handler.PartialMessageHandler;
import com.timenome.stream.ByteBufferStreamReceiver;

@ClientEndpoint
public class TimeNomeWebsocketClientEndPoint extends Observable {
	private Session session = null;
	private PartialMessageHandler messageHandler;
	private ByteBuffer byteBuffer;
	private ByteBufferStreamReceiver streamReceiver;
	
	public TimeNomeWebsocketClientEndPoint(URI endPointURI, PartialMessageHandler messageHandler) throws DeploymentException, IOException {
		WebSocketContainer container = ContainerProvider.getWebSocketContainer();
//		container.setDefaultMaxBinaryMessageBufferSize(1024 * 10);
		this.session = container.connectToServer(this, endPointURI);
		this.messageHandler = messageHandler;
	}
	
	public TimeNomeWebsocketClientEndPoint(URI endPointURI, PartialMessageHandler messageHandler, ByteBufferStreamReceiver streamReceiver) throws DeploymentException, IOException {
		this(endPointURI, messageHandler);
		this.streamReceiver = streamReceiver;
	}
	
	@OnOpen
	public void onOpen(Session userSession) {
		
	}
	
	@OnClose
	public void onClose(Session userSession, CloseReason reason) {
		
	}

	@OnMessage
	public void onMessage(ByteBuffer byteBuf, boolean isLast) {
		if(this.messageHandler != null) {
			this.messageHandler.onMessage(byteBuf.array(), isLast);
			if(isLast) {
//				notifyObservers(messageHandler.getByteBuffer());
				if(streamReceiver != null) {
					streamReceiver.onReceived(messageHandler.getByteBuffer());
				}
				setByteBuffer(messageHandler.getByteBuffer());
			}
		}
	}
	
//	@OnMessage
//	public void onMessage(InputStream is) throws IOException {
//		if(this.messageHandler != null) {
//			
//			this.messageHandler.readFromStream(is);
//			setByteBuffer(messageHandler.getByteBuffer());
//		}
//	}
	
	@OnError
	public void onError(Throwable t) {
		
	}
	
	public void closeSession() throws IOException {
		this.session.close();
	}
	
	public Future<Void> sendAsync(byte[] message) {
		byteBuffer = null;
		return this.session.getAsyncRemote().sendBinary(ByteBuffer.wrap(message));
		
	}
	
	public void send(byte[] message) throws IOException {
		byteBuffer = null;
		this.session.getBasicRemote().sendBinary(ByteBuffer.wrap(message));
	}

	public ByteBuffer getByteBuffer() {
		return byteBuffer;
	}

	private void setByteBuffer(ByteBuffer byteBuffer) {
		this.byteBuffer = byteBuffer;
	}
	
	public ByteBufferStreamReceiver getStreamReceiver() {
		return streamReceiver;
	}

	public void setStreamReceiver(ByteBufferStreamReceiver streamReceiver) {
		this.streamReceiver = streamReceiver;
	}
}
