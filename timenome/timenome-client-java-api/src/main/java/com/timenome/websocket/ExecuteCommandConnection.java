/**
 * 
 */
package com.timenome.websocket;


/**
 * @author toshish
 *
 */
public abstract class ExecuteCommandConnection {
	
	protected String host;
	protected int port;
	protected String key;
	protected String secret;
	
	public ExecuteCommandConnection(String host, int port, String key, String secret) {
		this.setHost(host);
		this.setPort(port);
		this.setKey(key);
		this.setSecret(secret);
	}
	public String getHost() {
		return host;
	}
	private void setHost(String host) {
		this.host = host;
	}
	public int getPort() {
		return port;
	}
	private void setPort(int port) {
		this.port = port;
	}

	
	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}
	/**
	 * @param key the key to set
	 */
	private void setKey(String key) {
		this.key = key;
	}
	/**
	 * @return the secret
	 */
	public String getSecret() {
		return secret;
	}
	/**
	 * @param secret the secret to set
	 */
	private void setSecret(String secret) {
		this.secret = secret;
	}
}
