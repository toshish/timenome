package com.timenome.websocket;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;

import javax.websocket.DeploymentException;

import com.timenome.exception.ConnectivityException;
import com.timenome.handler.PartialMessageHandler;

public class ExecuteCommandNonPersistentConnection extends ExecuteCommandConnection{
	
	public ExecuteCommandNonPersistentConnection(String host, int port, String key, String secret) {
		super(host, port, key, secret);
	}
	
	public ByteBuffer sendAsync(String url, ByteBuffer byteBuffer, PartialMessageHandler messageHandler) throws ConnectivityException {
		ByteBuffer byteBufferResponse = null;
		TimeNomeWebsocketClientEndPoint timenomeClient;
		try {
			timenomeClient = new TimeNomeWebsocketClientEndPoint(new URI("ws://"+ host + ":" + port + "/timenome" + url), messageHandler);
		} catch (DeploymentException e) {
			// TODO Add log entry
			throw new ConnectivityException("Deployment of a client endpoint failed for URL : " + "ws://"+ host + ":" + port + url, e);
		} catch (IOException e) {
			// TODO Add log entry
			throw new ConnectivityException("Failed while connecting to host " + host + " on port " + port + ".", e);
		} catch (URISyntaxException e) {
			// TODO Add log entry
			throw new ConnectivityException("Internal error due to invalid URI.", e);
		}

		try {
			timenomeClient.send(byteBuffer.array());
//			while(retryCount < TimeNomeClientAPIConstants.RETRY_ATTEMPTS) {
//				byteBufferResponse = timewarpClient.getByteBuffer();
//				
//				if(byteBufferResponse == null) {
//					// TODO Log that couldn't receive response in 30 sec. and retrying.
//					System.out.println("couldn't receive response; retrying.");
//					Thread.sleep(3000);
//					retryCount++;
//					continue;
//				}
//				else {
//					System.out.println(byteBufferResponse.array().length);
//					//					byteBufferResponse = ByteBuffer.wrap(byteBuffer.array());
//					timewarpClient.closeSession();
//					break;
//				}
//			}
			
//			wait(60000);
			while(timenomeClient.getByteBuffer() == null) {
				Thread.sleep(50);
			}
			byteBufferResponse = timenomeClient.getByteBuffer();
			timenomeClient.closeSession();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new ConnectivityException("Connectivity problem while sending request.", e);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			throw new ConnectivityException("Transmission interrupted.", e);
		}
		
		try {
			timenomeClient.closeSession();
		} catch (IOException e) {
			// TODO Log this event as warning
			new ConnectivityException("Error while closing connection.", e).printStackTrace();
		}
		return byteBufferResponse;
	}

}
