package com.timenome;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.protobuf.InvalidProtocolBufferException;
import com.timenome.enumeration.MessageEnum;
import com.timenome.exception.ConnectivityException;
import com.timenome.exception.EntityCreationException;
import com.timenome.exception.EntityRemovalException;
import com.timenome.exception.InvalidHandBack;
import com.timenome.exception.MessageParsingException;
import com.timenome.exception.TimeNomeException;
import com.timenome.handler.PartialMessageHandler;
import com.timenome.handler.TimeNomePartialMessageHandler;
import com.timenome.proto.TimeNomeProtos.CreateArchiveRequest;
import com.timenome.proto.TimeNomeProtos.DataEntry;
import com.timenome.proto.TimeNomeProtos.DeleteArchiveRequest;
import com.timenome.proto.TimeNomeProtos.ExecuteRequest;
import com.timenome.proto.TimeNomeProtos.ExecutionResponse;
import com.timenome.proto.TimeNomeProtos.GetDataStoreRequest;
import com.timenome.proto.TimeNomeProtos.GetDataStoreResponse;
import com.timenome.proto.TimeNomeProtos.PutDataRequest;
import com.timenome.proto.TimeNomeProtos.PutDataRequestInternal;
import com.timenome.proto.TimeNomeProtos.StatusResponse;
import com.timenome.security.HandbackGenerator;
import com.timenome.util.MessagingUtil;
import com.timenome.websocket.ExecuteCommandNonPersistentConnection;
import com.timenome.websocket.ExecuteCommandPersistentConnection;
import com.timenome.websocket.TimeNomePutConnection;

public class DataStoreBasic implements DataStore {
	private String dataStoreName;
	private ExecuteCommandNonPersistentConnection connection;
	private ExecuteCommandPersistentConnection putConnection;
	private Map<String, Archive> archiveMap = new HashMap<>();
	private HandbackGenerator handbackGenerator = new HandbackGenerator();
	private PartialMessageHandler messageHandler = new TimeNomePartialMessageHandler();
	private ArrayList<String> handbackList = new ArrayList<>();

	public DataStoreBasic(String dataStoreName, ExecuteCommandNonPersistentConnection connection) {
		this.setDataStoreName(dataStoreName);
		this.connection = connection;
	}

	@Override
	public Archive getArchive(String archiveName) throws ConnectivityException, MessageParsingException, TimeNomeException {
		return getArchives().get(archiveName);
	}

	@Override
	public Map<String, Archive> getArchives() throws MessageParsingException, ConnectivityException, TimeNomeException {
		Map<String, Archive> listOfArchives = new HashMap<String, Archive>();

		GetDataStoreRequest.Builder getDataStoreRequestBuilder = GetDataStoreRequest.newBuilder();
		getDataStoreRequestBuilder.setDataStoreName(dataStoreName);
		GetDataStoreRequest getDataStoreRequest = getDataStoreRequestBuilder.build();
		ExecuteRequest execRequest = MessagingUtil.createExecuteRequest(connection, getDataStoreRequest, MessageEnum.GET_DATASTORE_REQUEST);
		ByteBuffer responseByteBuf = connection.sendAsync("/get/datastore", ByteBuffer.wrap(execRequest.toByteArray()), messageHandler);

		GetDataStoreResponse getDataStoreResponse = MessagingUtil.getEnclosingMessageFromExecutionResponse(responseByteBuf, MessageEnum.GET_DATA_STORE_RESPONSE);

		com.timenome.proto.TimeNomeProtos.DataStore dataStore = getDataStoreResponse.getDataStore();
		List<com.timenome.proto.TimeNomeProtos.Archive> archiveList = dataStore.getArchiveList();



		for(com.timenome.proto.TimeNomeProtos.Archive archiveProto : archiveList) {
			Archive archive = new ArchiveBasic(archiveProto.getArchiveName(), archiveProto.getStoragePrecision(), archiveProto.getAggregationFunction().toString());
			listOfArchives.put(archiveProto.getArchiveName(), archive);
		}

		return listOfArchives;
	}

//	@Override
//	public List<Archive> getArchivesHavingCDF(BuiltInAggregationFunction cdf) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public List<Archive> getArchivesHavingGranularity(TimeRangeDef granularity) {
//		// TODO Auto-generated method stub
//		return null;
//	}

	/**
	 * @return the dataStoreName
	 */
	@Override
	public String getDataStoreName() {
		return dataStoreName;
	}
	/**
	 * @param dataStoreName the dataStoreName to set
	 */
	private void setDataStoreName(String dataStoreName) {
		this.dataStoreName = dataStoreName;
	}

	@Override
	public Archive addArchive(String archiveName, TimeRange storagePrecision, BuiltInAggregationFunction builtInCdf) throws ConnectivityException, EntityCreationException, MessageParsingException {
		Archive archive = new ArchiveBasic(archiveName, storagePrecision, builtInCdf);
		if(addArchive(archive)) {
			return archive;
		}
		return null;
	}

	private boolean addArchive(Archive archive) throws ConnectivityException, EntityCreationException, MessageParsingException {
		PartialMessageHandler messageHandler = new TimeNomePartialMessageHandler();

		CreateArchiveRequest.Builder crArchB = CreateArchiveRequest.newBuilder();
		com.timenome.proto.TimeNomeProtos.Archive.Builder archiveProtoBuilder = com.timenome.proto.TimeNomeProtos.Archive.newBuilder(); 
		crArchB.setDataStoreName(dataStoreName);
		archiveProtoBuilder.setArchiveName(archive.getArchiveName());
		archiveProtoBuilder.setStoragePrecision(archive.getStoragePrecisionInMillis());
		archiveProtoBuilder.setAggregationFunction(com.timenome.proto.TimeNomeProtos.BuiltInAggregationFunction.valueOf(archive.getBuiltInAggregationFunction().toString()));
		archiveProtoBuilder.setIsCounterBased(false);
		crArchB.setArchive(archiveProtoBuilder.build());
		CreateArchiveRequest createArchiveRequest = crArchB.build();

		ExecuteRequest execRequest = MessagingUtil.createExecuteRequest(connection, createArchiveRequest, MessageEnum.CREATE_ARCHIVE_REQUEST);
		ByteBuffer byteBuffer = connection.sendAsync("/create/archive", ByteBuffer.wrap(execRequest.toByteArray()), messageHandler);

		ExecutionResponse execResponse;
		try {
			execResponse = ExecutionResponse.parseFrom(byteBuffer.array());
		} catch (InvalidProtocolBufferException e) {
			// TODO Auto-generated catch block
			throw new MessageParsingException("Invalid message received.", e);
		}

		StatusResponse statusResp = execResponse.getStatusResponse();
		return statusResp.getIsSuccess();
	}

	@Override
	public boolean deleteArchive(String archiveName) throws ConnectivityException, EntityRemovalException {
		if(archiveMap.get(archiveName) == null) {
			throw new EntityRemovalException("Archive " + archiveName + " does not exists in DataStore " + dataStoreName);
		}
		DeleteArchiveRequest.Builder delArchB = DeleteArchiveRequest.newBuilder();
		delArchB.setDataStoreName(dataStoreName);
		delArchB.setArchiveName(archiveName);
		DeleteArchiveRequest delArch = delArchB.build();
		ByteBuffer byteBuffer = null;
		PartialMessageHandler messageHandler = new TimeNomePartialMessageHandler();
		byteBuffer = connection.sendAsync("/delete/archive", ByteBuffer.wrap(delArch.toByteArray()), messageHandler);
		StatusResponse statusResp;
		try {
			statusResp = StatusResponse.parseFrom(byteBuffer.array());
		} catch (InvalidProtocolBufferException e) {
			// TODO Auto-generated catch block
			throw new ConnectivityException("Invalid message received.", e);
		}
		return statusResp.getIsSuccess();
	}

	@Override
	public boolean putData(long timeStamp, Number value)
			throws ConnectivityException {
		return putData(new DataTuple<>(timeStamp, value));
	}

	@Override
	public boolean putData(DataTuple<? extends Number> entry)
			throws ConnectivityException {
		ArrayList<DataTuple<? extends Number>> singleEntryList = new ArrayList<>(1);
		if(singleEntryList.add(entry)) {
			return putData(singleEntryList);
		}
		return false;
	}

	@Override
	public boolean putData(List<DataTuple<? extends Number>> entries)
			throws ConnectivityException {
		if(this.putConnection == null) {
			try {
				putConnection = new TimeNomePutConnection(connection.getHost(), connection.getPort(),
						connection.getKey(), connection.getSecret());
			} catch (MessageParsingException e) {
				throw new ConnectivityException(e);
			}
		}

		PutDataRequest.Builder putDataRequestBuilder = PutDataRequest.newBuilder();
		putDataRequestBuilder.setDataStoreName(dataStoreName);

		for(DataTuple<? extends Number> entry : entries) {
			double value = 0l;
			if(entry.getValue() != null) {
				value = entry.getValue().doubleValue();
			}
			DataEntry dataEntry = DataEntry.newBuilder()
					.setTimestamp(entry.getTimeStamp())
					.setValue(value)
					.build();
			putDataRequestBuilder.addDataEntry(dataEntry);
		}

		PutDataRequestInternal.Builder putDataRequestInternalBuilder = PutDataRequestInternal.newBuilder();
		String handback = handbackGenerator.nextSessionId();
		putDataRequestInternalBuilder.setHandback(handback);
		putDataRequestInternalBuilder.setPutDataRequest(putDataRequestBuilder);

		ByteBuffer byteBuffer = null;
		if(handbackList.add(handback)) {
			byteBuffer = putConnection.sendAsync(ByteBuffer.wrap(putDataRequestInternalBuilder.build().toByteArray()));
		}
		StatusResponse statusResp;
		try {
			ExecutionResponse executionResp = ExecutionResponse.parseFrom(byteBuffer.array());
			statusResp = executionResp.getStatusResponse();
			if(!handbackList.contains(executionResp.getPutDataResponse().getHandback())) {
				throw new InvalidHandBack("Invalid handback received : '" + executionResp.getPutDataResponse().getHandback() + "'.");
			}
			else {
				handbackList.remove(executionResp.getPutDataResponse().getHandback());
			}
		} catch (InvalidProtocolBufferException e) {
			// TODO Auto-generated catch block
			throw new ConnectivityException("Invalid message received.", e);
		} catch (InvalidHandBack e) {
			// TODO Auto-generated catch block
			throw new ConnectivityException(e);
		}
		return statusResp.getIsSuccess();
	}

	
	@Override
	public String toString() {
		return "DataStore [dataStoreName=" + dataStoreName + "]";
	}
}
