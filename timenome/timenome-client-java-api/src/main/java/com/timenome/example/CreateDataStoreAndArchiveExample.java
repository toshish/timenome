package com.timenome.example;

import java.util.Map;

import com.timenome.Archive;
import com.timenome.BuiltInAggregationFunction;
import com.timenome.DataStore;
import com.timenome.TimeNomeConnection;
import com.timenome.TimeRange;
import com.timenome.TimeUnit;
import com.timenome.exception.AuthenticationException;
import com.timenome.exception.ConnectivityException;
import com.timenome.exception.DataStoreException;
import com.timenome.exception.EntityCreationException;
import com.timenome.exception.MessageParsingException;
import com.timenome.exception.TimeNomeException;
import com.timenome.factory.TimeNomeConnectionFactory;

public class CreateDataStoreAndArchiveExample {

	public static void main(String[] args) throws ConnectivityException, EntityCreationException, MessageParsingException, AuthenticationException, DataStoreException, TimeNomeException {
		TimeNomeConnection timeNomeService = TimeNomeConnectionFactory.createConnection("localhost", 8080, 
				"p7r5g5ganfb1oe6hgjetosqgki", "936h7600h28dd0cmajmqb9aqn2");
//		DataStore dataStore = timeNomeService.createDataStore("dummy-ds");
//		if(dataStore == null) {
//			System.err.println("Something's wrong.");
//			System.exit(1);
//		}
//		dataStore.addArchive("dummy-arch-per-sec-avg",
//				new TimeRangeDef(TimeUnit.SECOND, 1),
//				BuiltInAggregationFunction.AVERAGE);
//		
//		dataStore.addArchive("dummy-arch-per-min-range",
//				new TimeRangeDef(TimeUnit.SECOND, 1),
//				BuiltInAggregationFunction.RANGE);
//		
//		dataStore.addArchive("dummy-arch-per-half-min-sum",
//				new TimeRangeDef(TimeUnit.SECOND, 30),
//				BuiltInAggregationFunction.SUM);
		
		DataStore dataStoreFreeMem = timeNomeService.createDataStore("free-memory");
		if(dataStoreFreeMem == null) {
			System.err.println("Something's wrong.");
			System.exit(1);
		}
		dataStoreFreeMem.addArchive("free-memory-ten-sec-avg",
				new TimeRange(TimeUnit.SECOND, 10),
				BuiltInAggregationFunction.AVERAGE);
		
		dataStoreFreeMem.addArchive("free-memory-ten-sec-max",
				new TimeRange(TimeUnit.SECOND, 10),
				BuiltInAggregationFunction.MAX);
		
		dataStoreFreeMem.addArchive("free-memory-ten-sec-min",
				new TimeRange(TimeUnit.SECOND, 10),
				BuiltInAggregationFunction.MIN);
		
		printArchivesOfDataStore(dataStoreFreeMem);
		
		DataStore dataStoreActiveMem = timeNomeService.createDataStore("active-memory");
		dataStoreActiveMem.addArchive("active-memory-ten-sec-avg",
				new TimeRange(TimeUnit.SECOND, 10),
				BuiltInAggregationFunction.AVERAGE);
		
		dataStoreActiveMem.addArchive("active-memory-ten-sec-max",
				new TimeRange(TimeUnit.SECOND, 10),
				BuiltInAggregationFunction.MAX);
		
		dataStoreActiveMem.addArchive("active-memory-ten-sec-min",
				new TimeRange(TimeUnit.SECOND, 10),
				BuiltInAggregationFunction.MIN);
		
		printArchivesOfDataStore(dataStoreActiveMem);
		
		DataStore dataStoreInactiveMem = timeNomeService.createDataStore("inactive-memory");
		dataStoreInactiveMem.addArchive("inactive-memory-ten-sec-avg",
				new TimeRange(TimeUnit.SECOND, 10),
				BuiltInAggregationFunction.AVERAGE);
		
		dataStoreInactiveMem.addArchive("inactive-memory-ten-sec-max",
				new TimeRange(TimeUnit.SECOND, 10),
				BuiltInAggregationFunction.MAX);
		
		dataStoreInactiveMem.addArchive("inactive-memory-ten-sec-min",
				new TimeRange(TimeUnit.SECOND, 10),
				BuiltInAggregationFunction.MIN);
		
		printArchivesOfDataStore(dataStoreInactiveMem);
		
		DataStore dataStoreVirtualMem = timeNomeService.createDataStore("virtual-memory");
		dataStoreVirtualMem.addArchive("virtual-memory-ten-sec-avg",
				new TimeRange(TimeUnit.SECOND, 10),
				BuiltInAggregationFunction.AVERAGE);
		
		dataStoreVirtualMem.addArchive("virtual-memory-ten-sec-max",
				new TimeRange(TimeUnit.SECOND, 10),
				BuiltInAggregationFunction.MAX);
		
		dataStoreVirtualMem.addArchive("virtual-memory-ten-sec-min",
				new TimeRange(TimeUnit.SECOND, 10),
				BuiltInAggregationFunction.MIN);
		
		printArchivesOfDataStore(dataStoreVirtualMem);
	}
	
	private static void printArchivesOfDataStore(DataStore dataStore) throws MessageParsingException, ConnectivityException, TimeNomeException {
		Map<String, Archive> archives = dataStore.getArchives();
		for(String archiveName : archives.keySet()) {
			System.out.println(archives.get(archiveName));
		}
	}

}
