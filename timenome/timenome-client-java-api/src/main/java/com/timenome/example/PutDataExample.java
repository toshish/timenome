package com.timenome.example;

import java.util.Random;

import com.timenome.DataStore;
import com.timenome.DataTuple;
import com.timenome.TimeNomeConnection;
import com.timenome.exception.AuthenticationException;
import com.timenome.exception.ConnectivityException;
import com.timenome.exception.DataStoreException;
import com.timenome.exception.InvalidTimeRange;
import com.timenome.exception.MessageParsingException;
import com.timenome.factory.TimeNomeConnectionFactory;

public class PutDataExample {

	public PutDataExample() {

	}
	public static void main(String[] args) throws ConnectivityException, InvalidTimeRange, InterruptedException, MessageParsingException, AuthenticationException, DataStoreException {
		TimeNomeConnection timeNomeService = TimeNomeConnectionFactory.createConnection("localhost", 8080, 
				"ug74cgecra21cpt9b2qdra8tac", "tulptmtl1scu17k2tb49j1kno0");
		
		DataStore dsFro = timeNomeService.getDataStore("Alex Frommeyer");
		DataStore dsAlex = timeNomeService.getDataStore("Alex Curry");
		DataStore dsDan = timeNomeService.getDataStore("Dan Dykes");
		
		Random r = new Random();
		int i = 0;
		while(i < 100000) {
			DataTuple<? extends Number> entry = new DataTuple<Double>(r.nextDouble() * 10);
			if(!dsFro.putData(entry)) {
				System.err.println("Error adding entry : " + entry);
			}
			else {
				System.out.println("Added : " + entry);
			}
			Thread.sleep(500);
		}
	}
}
