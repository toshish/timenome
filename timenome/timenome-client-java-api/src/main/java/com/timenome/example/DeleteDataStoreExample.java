package com.timenome.example;

import com.timenome.DataStore;
import com.timenome.TimeNomeConnection;
import com.timenome.exception.AuthenticationException;
import com.timenome.exception.ConnectivityException;
import com.timenome.exception.DataStoreException;
import com.timenome.exception.EntityRemovalException;
import com.timenome.exception.MessageParsingException;
import com.timenome.factory.TimeNomeConnectionFactory;

public class DeleteDataStoreExample {

	public static void main(String[] args) throws ConnectivityException, MessageParsingException, AuthenticationException, EntityRemovalException, DataStoreException {
		TimeNomeConnection timeNomeConnection = TimeNomeConnectionFactory.createConnection("localhost", 8080, 
				"p7r5g5ganfb1oe6hgjetosqgki", "936h7600h28dd0cmajmqb9aqn2");

//		boolean isDeleted = timeNomeService.deleteDataStore("Bearing");
//		
//		System.out.println("DataStore deletion status : " + isDeleted);
//		
//		DataStore dataStore = timeNomeService.getDataStore("Bearing");
//		
//		if(dataStore == null) {
//			System.out.println("Deleted successfuly.");
//		}
		
		boolean isDeleted = timeNomeConnection.deleteDataStore("free-memory");
//		
		System.out.println("DataStore deletion status : " + isDeleted);
		
		
//		
		System.out.println("DataStore deletion status : " + timeNomeConnection.deleteDataStore("active-memory"));
		
		
//		
		System.out.println("DataStore deletion status : " + timeNomeConnection.deleteDataStore("inactive-memory"));
		
		
//		
		System.out.println("DataStore deletion status : " + timeNomeConnection.deleteDataStore("virtual-memory"));
//		
//		DataStore dataStore = timeNomeService.getDataStore("Bearing");
//		
//		if(dataStore == null) {
//			System.out.println("Deleted successfuly.");
//		}
		
	}

}
