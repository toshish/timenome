package com.timenome.example;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import com.timenome.DataStore;
import com.timenome.DataTuple;
import com.timenome.TimeNomeConnection;
import com.timenome.exception.AuthenticationException;
import com.timenome.exception.ConnectivityException;
import com.timenome.exception.DataStoreException;
import com.timenome.exception.InvalidTimeRange;
import com.timenome.exception.MessageParsingException;
import com.timenome.exception.TimeNomeException;
import com.timenome.factory.TimeNomeConnectionFactory;

public class GetDataExample {

	public static void main(String[] args) throws ConnectivityException, InvalidTimeRange, MessageParsingException, AuthenticationException, DataStoreException, TimeNomeException {
		TimeNomeConnection timeNomeService = TimeNomeConnectionFactory.createConnection("localhost", 8080, 
				"4rfe96m6kid1d2pntma3u94147", "onj869ocgb7joagrlkp5tg2u21");
		DataStore dummyDS = timeNomeService.getDataStore("dummy-ds");
		System.out.println(dummyDS);
		Calendar cal = GregorianCalendar.getInstance(TimeZone.getDefault());
		//		cal.set(Calendar.YEAR, 2013);
		//		cal.set(Calendar.MONTH, 11 - 1);
		//		cal.set(Calendar.DAY_OF_MONTH, 26);
		//		cal.set(Calendar.HOUR_OF_DAY, 04);
		//		cal.set(Calendar.MINUTE, 28);
		cal.setTimeInMillis(System.currentTimeMillis() - 60 * 60 * 1000);
		System.out.println(cal.getTime());

		Date startTime = cal.getTime();
		Calendar cal1 = GregorianCalendar.getInstance(TimeZone.getDefault());
		cal1.setTimeInMillis(System.currentTimeMillis());
		//		cal1.set(Calendar.YEAR, 2013);
		//		cal1.set(Calendar.MONTH, 11 - 1);
		//		cal1.set(Calendar.DAY_OF_MONTH, 30);
		//		cal1.set(Calendar.HOUR_OF_DAY, 13);
		//		cal1.set(Calendar.MINUTE, 30);
		System.out.println(cal1.getTime());

		Date endTime = cal1.getTime();

		List<DataTuple<Double>> dataList = timeNomeService.getData(dummyDS.getDataStoreName(), "dummy-arch-per-half-min-sum", startTime, endTime);
		for(DataTuple<? extends Number> entry : dataList) {
			System.out.println("  " + entry.getTimeStamp() + " : " + entry.getValue());
		}

	}

}
