/**
 * 
 */
package com.timenome.example;

import java.util.List;

import com.timenome.Archive;
import com.timenome.DataStore;
import com.timenome.DataTuple;
import com.timenome.TimeNomeConnection;
import com.timenome.exception.AuthenticationException;
import com.timenome.exception.ConnectivityException;
import com.timenome.exception.DataStoreException;
import com.timenome.exception.MessageParsingException;
import com.timenome.exception.TimeNomeException;
import com.timenome.factory.TimeNomeConnectionFactory;
import com.timenome.stream.DataStreamReceiver;

/**
 * @author toshish
 *
 */
public class StreamDataExample {

	/**
	 * @param args
	 * @throws AuthenticationException 
	 * @throws MessageParsingException 
	 * @throws ConnectivityException 
	 * @throws DataStoreException 
	 * @throws TimeNomeException 
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws ConnectivityException, MessageParsingException, AuthenticationException, DataStoreException, TimeNomeException, InterruptedException {
		TimeNomeConnection timeNomeService = TimeNomeConnectionFactory.createConnection("localhost", 8080, 
				"9dng2u0dm5d8rui3gdf7gl2tap", "36ingapqjjh6atucrisbde9tjg");

		DataStore dataStore = timeNomeService.getDataStore("dummy-ds");
		if(dataStore != null) {
			System.out.println(dataStore);
			String archiveName = "dummy-arch-per-sec-avg";
			Archive archive = dataStore.getArchive(archiveName);
			if(archive != null) {
				System.out.println(archive);
				DataStreamReceiver streamReceiver = new DataStreamReceiver() {

					@Override
					public void onReceived(List<DataTuple<Double>> listOfEntries) {
						for(DataTuple<Double> entry : listOfEntries) {
							System.out.println(entry);
						}
						
					}
				};
				
				timeNomeService.streamData(dataStore.getDataStoreName(), archiveName, streamReceiver);
				
				Thread.sleep(100000);
			}
			else {
				System.err.println("Something went wrong. Archive not found");
			}
		}
		else {
			System.err.println("Something went wrong. DataStore not found");
		}
	}

}
