package com.timenome;

import java.util.Date;
import java.util.List;

import com.timenome.exception.ConnectivityException;
import com.timenome.exception.DataStoreException;
import com.timenome.exception.EntityCreationException;
import com.timenome.exception.EntityRemovalException;
import com.timenome.exception.InvalidTimeRange;
import com.timenome.exception.MessageParsingException;
import com.timenome.exception.TimeNomeException;
import com.timenome.stream.DataStreamReceiver;

public interface TimeNomeConnection {
	
	/**
	 * Creates an empty data store with no Archives in it.
	 * @param dataStoreName This should be a unique name in the system among all of the data stores.
	 * @return
	 * @throws ConnectivityException
	 * @throws EntityCreationException
	 * @throws MessageParsingException
	 * @throws DataStoreException
	 */
	public DataStore createDataStore(String dataStoreName) throws ConnectivityException, EntityCreationException, MessageParsingException, DataStoreException;
	
	/**
	 * Deletes a data store along it with all of its archives and data stored in them.
	 * @param dataStoreName
	 * @return
	 * @throws ConnectivityException
	 * @throws EntityRemovalException
	 * @throws MessageParsingException
	 */
	public boolean deleteDataStore(String dataStoreName) throws ConnectivityException, EntityRemovalException, MessageParsingException;
	
	/**
	 * Returns details of a DataStore along with its Archives. Data present in the archives is not returned.
	 * @param dataStoreName
	 * @return {@link DataStore}
	 * @throws ConnectivityException
	 * @throws MessageParsingException
	 * @throws DataStoreException
	 * 
	 */
	public DataStore getDataStore(String dataStoreName) throws ConnectivityException, MessageParsingException, DataStoreException;
	
	/**
	 * Returns details of all DataStores present in the system along with their Archives. Data present in the archives is not returned.
	 * @return List of {@link DataStore}
	 * @throws ConnectivityException
	 */
	public List<DataStore> getDataStores() throws ConnectivityException;
	
	/**
	 * Starts streaming live data from an Archive.
	 * 
	 * @param dataStoreName
	 * @param archiveName
	 * @param streamReciver
	 * @throws ConnectivityException
	 * @throws MessageParsingException
	 */
	public void streamData(String dataStoreName, String archiveName, DataStreamReceiver streamReciver) throws ConnectivityException, MessageParsingException;
	
	/**
	 * Returns data from the specified archive. startTimestamp and endTimestamp specify the timestamp range for which the data is required.
	 * @param dataStoreName
	 * @param archiveName
	 * @param startTimestamp  
	 * @param endTimestamp
	 * @return
	 * @throws ConnectivityException
	 * @throws InvalidTimeRange
	 * @throws MessageParsingException
	 * @throws TimeNomeException
	 */
	public List<DataTuple<Double>> getData(String dataStoreName, String archiveName, Date startTimestamp, Date endTimestamp) throws ConnectivityException, InvalidTimeRange, MessageParsingException, TimeNomeException;
	
	
	/**
	 * Closes the TimeNomeConnection and stops all data streams.
	 */
	public void close();
}
