package com.timenome.exception;

public class InvalidTimeRange extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 653134176674773191L;

	public InvalidTimeRange() {
		// TODO Auto-generated constructor stub
	}

	public InvalidTimeRange(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvalidTimeRange(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidTimeRange(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidTimeRange(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
