package com.timenome.exception;

public class EntityRemovalException extends Exception {

	public EntityRemovalException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EntityRemovalException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public EntityRemovalException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public EntityRemovalException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public EntityRemovalException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -4249449730822589718L;

}
