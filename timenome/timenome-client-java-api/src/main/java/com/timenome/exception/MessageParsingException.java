/**
 * 
 */
package com.timenome.exception;

/**
 * @author toshish
 *
 */
public class MessageParsingException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8089289043226683724L;

	/**
	 * 
	 */
	public MessageParsingException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public MessageParsingException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public MessageParsingException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public MessageParsingException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public MessageParsingException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
