package com.timenome;

import com.timenome.websocket.ExecuteCommandNonPersistentConnection;

public abstract class AbstractTimeNomeConnection implements TimeNomeConnection {
	
	protected String host;
	protected int port;
	
	public AbstractTimeNomeConnection(String host, int port) {
		this.host = host;
		this.port = port;
	}
	
	public  abstract ExecuteCommandNonPersistentConnection getTimeNomeNonPersistentConnection();
	
}
