package com.timenome.testapp;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.carpalsoft.timewarp.schema.storageimpl.ObjectFactory;
import com.carpalsoft.timewarp.schema.storageimpl.Storage;


public class CreateXMLTest {
	public static void main(String[] args) throws JAXBException {
		ObjectFactory of = new ObjectFactory();
		Storage st = of.createStorage();
		Storage.StorageImpl sit = new Storage.StorageImpl();
		sit.setConnection("com.timenome.neo4j.Neo4jConnector");
		sit.setName("neo4jStorage");
		st.getStorageImpl().add(sit);
		JAXBContext jc = JAXBContext.newInstance(Storage.class);
		Marshaller mar = jc.createMarshaller();
		
		mar.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		 
//		mar.marshal(st, System.out);
		
		
		mar.marshal(st, System.out);
	}
}
