package com.timenome.testapp;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

import javax.websocket.ClientEndpoint;
import javax.websocket.CloseReason;
import javax.websocket.ContainerProvider;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

import com.google.protobuf.InvalidProtocolBufferException;
import com.timenome.proto.TimeNomeProtos.Archive;
import com.timenome.proto.TimeNomeProtos.Authentication;
import com.timenome.proto.TimeNomeProtos.BuiltInAggregationFunction;
import com.timenome.proto.TimeNomeProtos.CreateArchiveRequest;
import com.timenome.proto.TimeNomeProtos.CreateDataStoreRequest;
import com.timenome.proto.TimeNomeProtos.DataEntry;
import com.timenome.proto.TimeNomeProtos.DataStore;
import com.timenome.proto.TimeNomeProtos.DeleteArchiveRequest;
import com.timenome.proto.TimeNomeProtos.DeleteDataStoreRequest;
import com.timenome.proto.TimeNomeProtos.GetDataRequest;
import com.timenome.proto.TimeNomeProtos.GetDataResponse;
import com.timenome.proto.TimeNomeProtos.PutDataRequest;
import com.timenome.proto.TimeNomeProtos.StatusResponse;

@ClientEndpoint()
public class TimeWarpClientEP {
//	private static String host = "ec2-46-51-222-160.ap-southeast-1.compute.amazonaws.com";
	private static String host = "localhost";
	private static int port = 8080;
	private Session userSession;
	private Session mySession;
	private MessageHandler messageHandler;

	public TimeWarpClientEP(URI endpointURI) {
		try {
			WebSocketContainer container = ContainerProvider
					.getWebSocketContainer();
			System.out.println(container);
//			container.setDefaultMaxBinaryMessageBufferSize(1024 * 1024 * 10);
			mySession = container.connectToServer(this, endpointURI);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Callback hook for Connection open events.
	 * 
	 * @param userSession
	 *            the userSession which is opened.
	 */
	@OnOpen
	public void onOpen(Session userSession) {
		System.out.println("session opened");
		this.userSession = userSession;
	}

	/**
	 * Callback hook for Connection close events.
	 * 
	 * @param userSession
	 *            the userSession which is getting closed.
	 * @param reason
	 *            the reason for connection close
	 */
	@OnClose
	public void onClose(Session userSession, CloseReason reason) {
		System.out.println("closing session");
		this.userSession = null;
	}

	/**
	 * Callback hook for Message Events. This method will be invoked when a
	 * client send a message.
	 * 
	 * @param message
	 *            The text message
	 */
	@OnMessage
	public void onMessage(String message) {
		if (this.messageHandler != null)
			this.messageHandler.handleMessage(message);
	}

	@OnMessage
	public void onMessage(ByteBuffer byteBuf) {
		
		if(this.messageHandler != null) {
			this.messageHandler.handleMessage(byteBuf.array());
		}
		else {
			System.err.println("Null messageHandler");
		}
	}
	
//	@OnMessage
//	public void onMessage(ByteBuffer byteBuf, boolean last) {
//		
//		if(this.messageHandler != null) {
//			this.messageHandler.handleMessage(byteBuf.array(), last);
//		}
//		else {
//			System.err.println("Null messageHandler");
//		}
//	}
	
	@OnError
	public void onError(Throwable t) {
		t.printStackTrace();
	}
	/**
	 * register message handler
	 * 
	 * @param message
	 */
	public void addMessageHandler(MessageHandler msgHandler) {
		this.messageHandler = msgHandler;
	}
	
	public void closeMySession() throws IOException {
		this.mySession.close();
	}

	public void sendMessage(String message) {
		this.userSession.getAsyncRemote().sendText(message);
	}

	public void sendAsyncMessage(byte[] message) {
		this.userSession.getAsyncRemote().sendBinary(ByteBuffer.wrap(message));
	}

	public void sendMessage(byte[] message) throws IOException {
		this.userSession.getBasicRemote().sendBinary(ByteBuffer.wrap(message));
	}
	public static interface MessageHandler {
		public void handleMessage(String message);
		public void handleMessage(byte[] message, boolean last);
		public void handleMessage(byte[] message);
	}

	public static void main(String[] args) throws URISyntaxException, InterruptedException, IOException {
//		sendCreateDataStore("dummy-ds");
//		sendCreateArchive("dummy-ds", "dummy-archive-7", 1000);
//		sendCreateArchive("dummy-ds", "dummy-archive-5", 5000);
//		sendCreateArchive("dummy-ds", "dummy-archive-2", 30000);
//		sendCreateArchive("dummy-ds", "dummy-archive-1", 60000 * 5);
////		sendDeleteArchive("dummy-ds", "dummy-archive");
////		for(int i = 1; i < 8; i++) {
////			sendDeleteArchive("dummy-ds", "dummy-archive-" + i);
////		}
//		
//		sendPutData("dummy-ds");
		
//		sendGetData("dummy-ds", "dummy-archive-2", "dummy-archive-7");
		
		for(int i = 0; i < 10; i++) {
			sendCreateUser("saurabh","sau");
			sendDeleteUser("saurabh","sau");
		}
		
	}
	
	private static void sendDeleteArchive(String dsName, String archiveName) throws URISyntaxException, InterruptedException, IOException {
		final TimeWarpClientEP timewarpClient = new TimeWarpClientEP(new URI("ws://"+ host + ":" + port + "/timewarp/delete/" + dsName + "/archive"));
		DeleteArchiveRequest.Builder dAB = DeleteArchiveRequest.newBuilder();
		dAB.setArchiveName(archiveName);
		DeleteArchiveRequest dA = dAB.build();
		timewarpClient.addMessageHandler(new TimeWarpClientEP.MessageHandler() {
			public void handleMessage(String message) {
				
			}

			@Override
			public void handleMessage(byte[] message) {
				StatusResponse response;
				try {
					response = StatusResponse.parseFrom(message);
					System.out.println(response.toString());
				} catch (InvalidProtocolBufferException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@Override
			public void handleMessage(byte[] message, boolean last) {
				
				
			}
		});
		
		timewarpClient.sendMessage(dA.toByteArray());
		Thread.sleep(3000);
		timewarpClient.closeMySession();
		
	}

	private static void sendCreateDataStore(String dsName) throws URISyntaxException, InterruptedException, IOException {
		final TimeWarpClientEP timewarpClient = new TimeWarpClientEP(new URI("ws://"+ host + ":" + port + "/timewarp/create/datastore"));
		CreateDataStoreRequest.Builder cdSB = CreateDataStoreRequest.newBuilder();
		DataStore.Builder dsb = DataStore.newBuilder();
		dsb.setDataStoreName(dsName);
		cdSB.setDataStore(dsb);
		CreateDataStoreRequest cdS = cdSB.build();
		timewarpClient.addMessageHandler(new TimeWarpClientEP.MessageHandler() {
			public void handleMessage(String message) {
				
			}

			@Override
			public void handleMessage(byte[] message) {
				StatusResponse response;
				try {
					response = StatusResponse.parseFrom(message);
					System.out.println(response.toString());
				} catch (InvalidProtocolBufferException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@Override
			public void handleMessage(byte[] message, boolean last) {
				// TODO Auto-generated method stub
				
			}
		});
		
		timewarpClient.sendMessage(cdS.toByteArray());
		Thread.sleep(3000);
		timewarpClient.closeMySession();
	}
	
	private static void sendCreateUser(String userName, String password) throws URISyntaxException, InterruptedException, IOException {
		final TimeWarpClientEP timewarpClient = new TimeWarpClientEP(new URI("ws://"+ host + ":" + port + "/timenome/create/user"));
		Authentication.Builder cdSB = Authentication.newBuilder();
		
		cdSB.setKey(userName);
		cdSB.setSecret(password);
		Authentication auth = cdSB.build();
		
		timewarpClient.addMessageHandler(new TimeWarpClientEP.MessageHandler() {
			public void handleMessage(String message) {
				
			}

			@Override
			public void handleMessage(byte[] message) {
				StatusResponse response;
				try {
					response = StatusResponse.parseFrom(message);
					System.out.println(response.toString());
				} catch (InvalidProtocolBufferException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@Override
			public void handleMessage(byte[] message, boolean last) {
				// TODO Auto-generated method stub
				
			}
		});
		
		timewarpClient.sendMessage(auth.toByteArray());
		Thread.sleep(3000);
		timewarpClient.closeMySession();
	}
	
	private static void sendDeleteUser(String userName, String password) throws URISyntaxException, InterruptedException, IOException {
		final TimeWarpClientEP timewarpClient = new TimeWarpClientEP(new URI("ws://"+ host + ":" + port + "/timenome/delete/user"));
		Authentication.Builder cdSB = Authentication.newBuilder();
		
		cdSB.setKey(userName);
		cdSB.setSecret(password);
		Authentication auth = cdSB.build();
		
		timewarpClient.addMessageHandler(new TimeWarpClientEP.MessageHandler() {
			public void handleMessage(String message) {
				
			}

			@Override
			public void handleMessage(byte[] message) {
				StatusResponse response;
				try {
					response = StatusResponse.parseFrom(message);
					System.out.println(response.toString());
				} catch (InvalidProtocolBufferException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@Override
			public void handleMessage(byte[] message, boolean last) {
				// TODO Auto-generated method stub
				
			}
		});
		
		timewarpClient.sendMessage(auth.toByteArray());
		Thread.sleep(3000);
		timewarpClient.closeMySession();
	}
	
	private static void sendCreateArchive(String dsName, String archiveName, long precision) throws URISyntaxException, InterruptedException, IOException {
		final TimeWarpClientEP timewarpClient = new TimeWarpClientEP(new URI("ws://"+ host + ":" + port + "/timewarp/create/archive"));
		CreateArchiveRequest.Builder aB = CreateArchiveRequest.newBuilder();
		Archive.Builder archiveBuilder = Archive.newBuilder();
		archiveBuilder.setStoragePrecision(precision);
		archiveBuilder.setAggregationFunction(BuiltInAggregationFunction.AVERAGE);
		aB.setArchive(archiveBuilder.build());
		long stRange = 2592000000l;//1000 * 60 * 60 * 24 * 30;
		System.out.println(stRange);
		aB.setDataStoreName(dsName);
		final CreateArchiveRequest cA = aB.build();
		timewarpClient.addMessageHandler(new TimeWarpClientEP.MessageHandler() {
			public void handleMessage(String message) {
				
			}

			@Override
			public void handleMessage(byte[] message) {
				StatusResponse response;
				try {
					response = StatusResponse.parseFrom(message);
					System.out.println(response.toString());
				} catch (InvalidProtocolBufferException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@Override
			public void handleMessage(byte[] message, boolean last) {
				// TODO Auto-generated method stub
				
			}
		});
		
		timewarpClient.sendMessage(cA.toByteArray());
		Thread.sleep(3000);
		timewarpClient.closeMySession();
	}
	
	private static void sendPutData(String dsName) throws URISyntaxException, InterruptedException, IOException {
		final TimeWarpClientEP timewarpClient = new TimeWarpClientEP(new URI("ws://"+ host + ":" + port + "/timewarp/put/data"));
		Random r = new Random();
		
		
		
		timewarpClient.addMessageHandler(new TimeWarpClientEP.MessageHandler() {
			public void handleMessage(String message) {
				
			}

			@Override
			public void handleMessage(byte[] message) {
				StatusResponse response;
				try {
					response = StatusResponse.parseFrom(message);
					System.out.println(response.toString());
				} catch (InvalidProtocolBufferException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@Override
			public void handleMessage(byte[] message, boolean last) {
				// TODO Auto-generated method stub
				
			}
		});
		for(int i = 0; i < 1000000; i++) {
			PutDataRequest.Builder putDataBuilder = PutDataRequest.newBuilder();
			putDataBuilder.setDataStoreName(dsName);
			DataEntry.Builder deBuilder = DataEntry.newBuilder();
			deBuilder.setTimestamp(System.currentTimeMillis());
			deBuilder.setValue(r.nextDouble() * 100d);
			putDataBuilder.addDataEntry(deBuilder.build());
			PutDataRequest putData = putDataBuilder.build();
			timewarpClient.sendMessage(putData.toByteArray());
			Thread.sleep(175);
		}
		timewarpClient.closeMySession();
	}
	
	private static void sendGetData(String dsName, String archive) throws URISyntaxException, IOException, InterruptedException {
		final TimeWarpClientEP timewarpClient = new TimeWarpClientEP(new URI("ws://"+ host + ":" + port + "/timewarp/get"));
		final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		timewarpClient.addMessageHandler(new TimeWarpClientEP.MessageHandler() {
			public void handleMessage(String message) {
				System.out.println(message);
			}

			@Override
			public void handleMessage(byte[] message) {
				GetDataResponse response;
				try {
					response = GetDataResponse.parseFrom(message);
					System.out.println(response.toString());
				} catch (InvalidProtocolBufferException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@Override
			public void handleMessage(byte[] message, boolean last) {
				GetDataResponse response;
				System.out.println("yeah");
				try {
					outputStream.write(message);
					if(last) {
						response = GetDataResponse.parseFrom(outputStream.toByteArray());
						System.out.println(response.toString());
						outputStream.close();
					}
				} catch (InvalidProtocolBufferException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		GetDataRequest.Builder gdB = GetDataRequest.newBuilder();
		Calendar cal = GregorianCalendar.getInstance();
		cal.set(Calendar.YEAR, 2013);
		cal.set(Calendar.MONTH, 10 - 1);
		cal.set(Calendar.DAY_OF_MONTH, 15);
		cal.set(Calendar.HOUR, 14);
		cal.set(Calendar.MINUTE, 14);
		
		Date startTime = cal.getTime();
		Calendar cal1 = GregorianCalendar.getInstance();
		cal1.set(Calendar.YEAR, 2013);
		cal1.set(Calendar.MONTH, 11 - 1);
		cal1.set(Calendar.DAY_OF_MONTH, 27);
		cal1.set(Calendar.HOUR, 2);
		cal1.set(Calendar.MINUTE, 30);
		
		Date endTime = cal1.getTime();
		
		System.out.println(cal.getTime().toString());
		
		gdB.setStartTimestamp(startTime.getTime());
		gdB.setEndTimestamp(endTime.getTime());
		gdB.setArchiveName(archive);
		gdB.setDataStoreName(dsName);
		
		GetDataRequest gdR = gdB.build();
		
		timewarpClient.sendMessage(gdR.toByteArray());
		Thread.sleep(30000);
		timewarpClient.closeMySession();
	}
}






