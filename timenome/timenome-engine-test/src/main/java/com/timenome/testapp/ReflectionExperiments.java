package com.timenome.testapp;

import com.timenome.api.core.Archive;
import com.timenome.neo4j.core.Neo4jArchive;

public class ReflectionExperiments {
	public static void main(String[] args) {
		Class<?>[] classes = Archive.class.getClasses();
		System.out.println(Archive.class.getConstructors().length);
		System.out.println(Neo4jArchive.class.getSuperclass().getCanonicalName());
		for(Class<?> clazz : classes) {
			System.out.println(clazz.getCanonicalName());
		}
	}
}
