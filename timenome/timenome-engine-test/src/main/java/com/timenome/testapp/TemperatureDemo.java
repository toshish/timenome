package com.timenome.testapp;

import java.util.HashMap;

import javax.xml.bind.JAXBException;

import com.timenome.api.BuiltInCDF;
import com.timenome.api.TimeUnit;
import com.timenome.api.core.Archive;
import com.timenome.api.core.Connection;
import com.timenome.api.core.DataStore;
import com.timenome.api.core.Registry;
import com.timenome.api.core.TimeRangeDef;
import com.timenome.api.exceptions.ArchiveException;
import com.timenome.api.exceptions.ConnectionException;
import com.timenome.api.exceptions.DataStoreException;
import com.timenome.api.exceptions.UserDoesNotExists;
import com.timenome.neo4j.core.Neo4jArchive;


public class TemperatureDemo {
	public static void main(String[] args) {
		
		Registry registry = Registry.getRegistry();
		HashMap<String, String> connectionParams = new HashMap<String, String>();
		connectionParams.put("name", "neo4jStorage");
		Connection neo4jConnection = null;
		try {
			 neo4jConnection = registry.connect(connectionParams);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DataStore temperatureDS = null;
		try {
			temperatureDS = neo4jConnection.createDataStore("temperature-ds", "dummy-user");
		} catch (DataStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UserDoesNotExists e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Archive archiveTempPerMin = new Neo4jArchive("temperature-per-min", new TimeRangeDef(TimeUnit.MONTH, 1), new TimeRangeDef(TimeUnit.MINUTE, 1), BuiltInCDF.AVERAGE);
		Archive archiveTempPerHour = new Neo4jArchive("temperature-per-hour", new TimeRangeDef(TimeUnit.MONTH, 1), new TimeRangeDef(TimeUnit.HOUR, 1), BuiltInCDF.MAX);
		
		try {
			temperatureDS.addArchive(archiveTempPerMin);
			temperatureDS.addArchive(archiveTempPerHour);
		} catch (ArchiveException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
}
