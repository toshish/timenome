package com.timenome.testapp;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.cypher.javacompat.ExecutionResult;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.helpers.collection.IteratorUtil;

public class Neo4jDeleteSubTree {

	public Neo4jDeleteSubTree() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		GraphDatabaseService dbService = new GraphDatabaseFactory().newEmbeddedDatabase("/home/toshish/neo4j_db");
		ExecutionEngine engine = new ExecutionEngine(dbService);
		Set<Relationship> relSet = new HashSet<>();
		Set<Node> nodeSet = new HashSet<>();
		ExecutionResult result = engine.execute("start n=node(4)\n" + 
				"match p=(n)-[*]->(m)\n" + 
				"return p order by length(p) desc");
		Iterator<Path> p_column = result.columnAs("p");
		for(Path path : IteratorUtil.asIterable(p_column))
		{
			for(Relationship rel : path.relationships()) {
				relSet.add(rel);
			}
			
			for(Node node : path.nodes()) {
				nodeSet.add(node);
			}
			
//			
//			System.out.println(path.length() + " : " + path.toString());
//						for(Relationship rel : path.reverseRelationships()) {
//							doIt(dbService, rel);
//						}
		}
		
		
//		dbService.getNodeById(44108).delete();
		
		Transaction tx = dbService.beginTx();
		for(Relationship rel : relSet) {
			System.out.println("Deleting rel : " + rel.getId());
			rel.delete();
			
//			engine.execute("start r=rel(" + rel.getId() + ") delete r");
		}
		
		for(Node node : nodeSet) {
			if(node.getId() == 4) {
				Iterable<Relationship> rels = node.getRelationships(Direction.INCOMING);
				for(Relationship rel : rels) {
					System.out.println("Deleting rel : " + rel.getId());
					rel.delete();
				}
			}
			System.out.println("Deleting node : " + node.getId());
			node.delete();
			
//			engine.execute("start n=node(" + node.getId() + ") delete n");
		}
		
		tx.success();
		tx.finish();

	}

	private static void doIt(GraphDatabaseService dbService, Relationship rel) {
		Node endNode = rel.getEndNode();
		Iterable<Relationship> rels = endNode.getRelationships(Direction.INCOMING);
		Transaction tx = dbService.beginTx();
		try {
			
			for(Relationship relat : rels) {
				System.out.println("Deleting rel : " + relat);
				relat.delete();
			}
			System.out.println("deleting node: " + endNode);
			endNode.delete();
		
		} catch(IllegalStateException e) {
			System.err.println(e.getLocalizedMessage());
		}
		tx.finish();
		tx.success();
	}
}

