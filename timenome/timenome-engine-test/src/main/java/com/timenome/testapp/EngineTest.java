package com.timenome.testapp;

import java.util.HashMap;
import java.util.Random;

import javax.xml.bind.JAXBException;

import com.timenome.api.EngineEntry;
import com.timenome.api.Entry;
import com.timenome.api.core.Connection;
import com.timenome.api.core.Registry;
import com.timenome.api.exceptions.ConnectionException;
import com.timenome.api.execution.TimeNomeEngine;

public class EngineTest {

	public static void main(String[] args) throws InterruptedException {
		Registry registry = Registry.getRegistry();
		HashMap<String, String> connectionParams = new HashMap<String, String>();
		Connection connection = null;
		TimeNomeEngine engine = null;
		connectionParams.put("name", "neo4jStorage");
		try {
				connection = registry.connect(connectionParams);
			engine = TimeNomeEngine.getTimeNomeEngine(connection);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Random r = new Random();
		for(int i = 0; i < 30; i++) {
			Entry<Double> entry = new Entry<Double>(r.nextDouble() * 100);
			Thread.sleep(700);
			EngineEntry ee = new EngineEntry("dummy-user", "dummy-ds", entry);
			engine.putEntry(ee);
		}
		
	}

}
