package com.timenome.testapp;

import java.io.IOException;

import com.timenome.testapp.WSConnector.WSEventHandler;

public class Test {
	public static void main(String[] args) {
		WSConnector wsConn = new WSConnector();
		WSEventHandler wsEventHandler = new WSEventHandler() {
			
			@Override
			public void onStop() {
				System.out.println("Session stopped");
			}
			
			@Override
			public void onOpen() {
				System.out.println("Session Opened");
				
			}
			
			@Override
			public void onMessage(byte[] message) {
				System.out.println(new String(message));
				
			}
			
			@Override
			public void onMessage(String message) {
				System.out.println(message);
				
			}
			
			@Override
			public void onError(Exception e) {
				e.printStackTrace();
				
			}
			
			@Override
			public void onClose() {
				System.out.println("session closed.");
				
			}
		};
		try {
			wsConn.connect("ws", "localhost", 8080, "/timewarp/get", false, wsEventHandler);
			wsConn.send(new byte[2]);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}
