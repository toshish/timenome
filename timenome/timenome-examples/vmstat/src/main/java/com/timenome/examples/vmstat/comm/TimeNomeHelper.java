package com.timenome.examples.vmstat.comm;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.timenome.Archive;
import com.timenome.BuiltInAggregationFunction;
import com.timenome.DataStore;
import com.timenome.TimeNomeConnection;
import com.timenome.TimeRange;
import com.timenome.TimeUnit;
import com.timenome.examples.vmstat.utils.VMStatConstants;
import com.timenome.exception.ConnectivityException;
import com.timenome.exception.DataStoreException;
import com.timenome.exception.EntityCreationException;
import com.timenome.exception.EntityRemovalException;
import com.timenome.exception.MessageParsingException;
import com.timenome.exception.TimeNomeException;

public class TimeNomeHelper {

	private TimeNomeConnection TimeNomeConnection;

	Map<String, DataStore> dataStoreMap = new ConcurrentHashMap<String, DataStore>();

	public TimeNomeHelper(TimeNomeConnection TimeNomeConnection) throws ConnectivityException, MessageParsingException,
			DataStoreException {
		this.TimeNomeConnection = TimeNomeConnection;
	}

	public boolean push(Map<String, Long> stats) throws ConnectivityException {
		// System.out.println("Free: " + stats.get("free"));
		boolean isPushSuccess = dataStoreMap.get(VMStatConstants.FREE_MEMORY_DATASTORE).putData(new Date().getTime(),
				stats.get("free"));
		isPushSuccess = isPushSuccess
				&& dataStoreMap.get(VMStatConstants.ACTIVE_MEMORY_DATASTORE).putData(new Date().getTime(),
						stats.get("active"));
		isPushSuccess = isPushSuccess
				&& dataStoreMap.get(VMStatConstants.INACTIVE_MEMORY_DATASTORE).putData(new Date().getTime(),
						stats.get("inact"));
		isPushSuccess = isPushSuccess
				&& dataStoreMap.get(VMStatConstants.VIRTUAL_MEMORY_DATASTORE).putData(new Date().getTime(),
						stats.get("swpd"));

		return isPushSuccess;
	}

	public void createDataStores() throws ConnectivityException, EntityCreationException, MessageParsingException,
			DataStoreException, TimeNomeException {
		DataStore dataStoreFreeMem = TimeNomeConnection.createDataStore(VMStatConstants.FREE_MEMORY_DATASTORE);
		if (dataStoreFreeMem == null) {
			System.err.println("Something's wrong.");
			System.exit(1);
		}
		dataStoreFreeMem.addArchive("free-memory-ten-sec-avg", new TimeRange(TimeUnit.SECOND, 10),
				BuiltInAggregationFunction.AVERAGE);

		dataStoreFreeMem.addArchive("free-memory-ten-sec-max", new TimeRange(TimeUnit.SECOND, 10),
				BuiltInAggregationFunction.MAX);

		dataStoreFreeMem.addArchive("free-memory-ten-sec-min", new TimeRange(TimeUnit.SECOND, 10),
				BuiltInAggregationFunction.MIN);

		dataStoreMap.put(dataStoreFreeMem.getDataStoreName(), dataStoreFreeMem);

		printArchivesOfDataStore(dataStoreFreeMem);

		DataStore dataStoreActiveMem = TimeNomeConnection.createDataStore(VMStatConstants.ACTIVE_MEMORY_DATASTORE);
		dataStoreActiveMem.addArchive("active-memory-ten-sec-avg", new TimeRange(TimeUnit.SECOND, 10),
				BuiltInAggregationFunction.AVERAGE);

		dataStoreActiveMem.addArchive("active-memory-ten-sec-max", new TimeRange(TimeUnit.SECOND, 10),
				BuiltInAggregationFunction.MAX);

		dataStoreActiveMem.addArchive("active-memory-ten-sec-min", new TimeRange(TimeUnit.SECOND, 10),
				BuiltInAggregationFunction.MIN);

		dataStoreMap.put(dataStoreActiveMem.getDataStoreName(), dataStoreActiveMem);
		printArchivesOfDataStore(dataStoreActiveMem);

		DataStore dataStoreInactiveMem = TimeNomeConnection.createDataStore(VMStatConstants.INACTIVE_MEMORY_DATASTORE);
		dataStoreInactiveMem.addArchive("inactive-memory-ten-sec-avg", new TimeRange(TimeUnit.SECOND, 10),
				BuiltInAggregationFunction.AVERAGE);

		dataStoreInactiveMem.addArchive("inactive-memory-ten-sec-max", new TimeRange(TimeUnit.SECOND, 10),
				BuiltInAggregationFunction.MAX);

		dataStoreInactiveMem.addArchive("inactive-memory-ten-sec-min", new TimeRange(TimeUnit.SECOND, 10),
				BuiltInAggregationFunction.MIN);
		dataStoreMap.put(dataStoreInactiveMem.getDataStoreName(), dataStoreInactiveMem);
		printArchivesOfDataStore(dataStoreInactiveMem);

		DataStore dataStoreVirtualMem = TimeNomeConnection.createDataStore(VMStatConstants.VIRTUAL_MEMORY_DATASTORE);
		dataStoreVirtualMem.addArchive("virtual-memory-ten-sec-avg", new TimeRange(TimeUnit.SECOND, 10),
				BuiltInAggregationFunction.AVERAGE);

		dataStoreVirtualMem.addArchive("virtual-memory-ten-sec-max", new TimeRange(TimeUnit.SECOND, 10),
				BuiltInAggregationFunction.MAX);

		dataStoreVirtualMem.addArchive("virtual-memory-ten-sec-min", new TimeRange(TimeUnit.SECOND, 10),
				BuiltInAggregationFunction.MIN);
		dataStoreMap.put(dataStoreVirtualMem.getDataStoreName(), dataStoreVirtualMem);
		printArchivesOfDataStore(dataStoreVirtualMem);
	}

	private void printArchivesOfDataStore(DataStore dataStore) throws MessageParsingException, ConnectivityException,
			TimeNomeException {
		Map<String, Archive> archives = dataStore.getArchives();
		for (String archiveName : archives.keySet()) {
			System.out.println(archives.get(archiveName));
		}
	}

	public void deleteDataStores() throws ConnectivityException, EntityRemovalException, MessageParsingException {
		System.out.println("DataStore deletion status : "
				+ TimeNomeConnection.deleteDataStore(VMStatConstants.FREE_MEMORY_DATASTORE));
		System.out.println("DataStore deletion status : "
				+ TimeNomeConnection.deleteDataStore(VMStatConstants.ACTIVE_MEMORY_DATASTORE));
		System.out.println("DataStore deletion status : "
				+ TimeNomeConnection.deleteDataStore(VMStatConstants.INACTIVE_MEMORY_DATASTORE));
		System.out.println("DataStore deletion status : "
				+ TimeNomeConnection.deleteDataStore(VMStatConstants.VIRTUAL_MEMORY_DATASTORE));
	}

	public boolean areDataStoresCreated() throws ConnectivityException, MessageParsingException {
		boolean allCreated;
		DataStore dataStore = null;
		try {
			allCreated = (dataStore = TimeNomeConnection.getDataStore(VMStatConstants.FREE_MEMORY_DATASTORE)) != null;
			System.out.println(allCreated);
			dataStoreMap.put(dataStore.getDataStoreName(), dataStore);
			// System.out.println(TimeNomeConnection.getDataStore(VMStatConstants.FREE_MEMORY_DATASTORE));
			allCreated = allCreated
					&& (dataStore = TimeNomeConnection.getDataStore(VMStatConstants.ACTIVE_MEMORY_DATASTORE)) != null;
			System.out.println(allCreated);
			dataStoreMap.put(dataStore.getDataStoreName(), dataStore);
			allCreated = allCreated
					&& (dataStore = TimeNomeConnection.getDataStore(VMStatConstants.INACTIVE_MEMORY_DATASTORE)) != null;
			System.out.println(allCreated);
			dataStoreMap.put(dataStore.getDataStoreName(), dataStore);
			allCreated = allCreated
					&& (dataStore = TimeNomeConnection.getDataStore(VMStatConstants.VIRTUAL_MEMORY_DATASTORE)) != null;
			System.out.println(allCreated);
			dataStoreMap.put(dataStore.getDataStoreName(), dataStore);
		} catch (DataStoreException e) {
			allCreated = false;
		}
		return allCreated;

	}

}
