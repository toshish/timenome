package com.timenome.examples.vmstat.utils;

final public class VMStatConstants {
	public static String SPACE = " ";
	public static String NEW_LINE = "\n";
	
	public static String FREE_MEMORY_DATASTORE = "free-memory";
	public static String ACTIVE_MEMORY_DATASTORE = "active-memory";
	public static String INACTIVE_MEMORY_DATASTORE = "inactive-memory";
	public static String VIRTUAL_MEMORY_DATASTORE = "virtual-memory";
}
