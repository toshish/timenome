/**
 * 
 */
package com.timenome.examples.vmstat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.tomcat.util.codec.binary.Base64;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.timenome.TimeNomeConnection;
import com.timenome.examples.vmstat.comm.TimeNomeHelper;
import com.timenome.examples.vmstat.exception.CommandExecutionException;
import com.timenome.examples.vmstat.exception.CommandNotFoundException;
import com.timenome.examples.vmstat.exception.InvalidOutputException;
import com.timenome.examples.vmstat.exception.UnsupportedOperatingSystem;
import com.timenome.examples.vmstat.fetcher.VMStatFetcher;
import com.timenome.exception.AuthenticationException;
import com.timenome.exception.ConnectivityException;
import com.timenome.exception.DataStoreException;
import com.timenome.exception.EntityCreationException;
import com.timenome.exception.EntityRemovalException;
import com.timenome.exception.MessageParsingException;
import com.timenome.exception.TimeNomeException;
import com.timenome.factory.TimeNomeConnectionFactory;

/**
 * @author toshishj
 *
 */
final public class Main {

	/**
	 * @param args
	 * @throws CommandNotFoundException 
	 * @throws CommandExecutionException 
	 * @throws UnsupportedOperatingSystem 
	 * @throws InvalidOutputException 
	 * @throws AuthenticationException 
	 * @throws MessageParsingException 
	 * @throws ConnectivityException 
	 * @throws TimeNomeException 
	 * @throws DataStoreException 
	 * @throws EntityCreationException 
	 * @throws EntityRemovalException 
	 * @throws IOException 
	 * @throws ParseException 
	 */
	public static void main(String[] args) throws UnsupportedOperatingSystem, CommandExecutionException, CommandNotFoundException, InvalidOutputException, ConnectivityException, MessageParsingException, AuthenticationException, EntityCreationException, DataStoreException, TimeNomeException, EntityRemovalException, ParseException, IOException {
		if(args.length != 4 ) {
			System.err.println("Usage: <executable> <host> <port> <username> <password>");
			System.exit(1);
		}

		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String username = args[2];
		String password = args[3];

		String[] apiSecret = getAPICredentials(host, port, username, password);


		final VMStatFetcher vmStatFetcher = new VMStatFetcher();

		TimeNomeConnection timeNomeService = TimeNomeConnectionFactory.createConnection(host, port, 
				apiSecret[0], apiSecret[1]);

		final TimeNomeHelper timeNomeHelper = new TimeNomeHelper(timeNomeService);




		if(!timeNomeHelper.areDataStoresCreated()) {
			try {
				timeNomeHelper.deleteDataStores();
			} catch(EntityRemovalException e) {
				e.printStackTrace();
			}

			System.out.println("Creating data stores.");
			timeNomeHelper.createDataStores();
		}




		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				Map<String, Long> stats = null;
				try {
					stats = vmStatFetcher.getStats();
					timeNomeHelper.push(stats);
				} catch (CommandExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (CommandNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvalidOutputException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ConnectivityException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				//				for(String key : stats.keySet()) {
				//					System.out.println(key + " : " + stats.get(key));
				//				}
			}
		}, 1000, 1000);

	}

	private static String[] getAPICredentials(String host, int port, String username, String password) throws ParseException, IOException {
		CookieHandler.setDefault( new CookieManager( null, CookiePolicy.ACCEPT_ALL ) );
		String response = sendHTTPRequest("POST", "/timenome/rest/userservice/login", host, port, username, password, true);
		
		if(!response.contains("true")) {
			throw new RuntimeException("Login failed.");
		}
		
		response = sendHTTPRequest("GET", "/timenome/rest/userservice/get/apicredentials", host, port, username, password, false);

		JSONParser parser = new JSONParser();

		Object obj = parser.parse(response);

		JSONObject jsonObject = (JSONObject) obj;

		String[] keySecret = new String[] {(String) jsonObject.get("key"), (String) jsonObject.get("secret") };
		System.out.println("Obtained.");
		return keySecret;

	}

	private static String sendHTTPRequest(String method, String urlString, String host, int port, String username, String password, boolean addAuthorization) throws IOException {
		URL url = new URL("http://" + host + ":" + port + urlString);
		System.out.println(url.toExternalForm());
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod(method);
		conn.setRequestProperty("Origin", "http://" + host + ":" + port);
		conn.setRequestProperty("Accept", "application/json, text/plain, */*");
		//		conn.setRequestProperty("Referer", "http://" + host + ":" + port + "/timenome/index.html");
		conn.setRequestProperty("contentType", "application/json;charset=UTF-8");
		conn.setRequestProperty("Accept-Encoding", "gzip,deflate,sdch");
		if(addAuthorization) {
			byte[] base64Encoded = Base64.encodeBase64((username + ':' + password).getBytes());
			String base64EncodedString = new String(base64Encoded);
			conn.setRequestProperty("Authorization", "Basic " + base64EncodedString);
		}


		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ conn.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));

		String output;
		String response = "";
		System.out.println("Output from Server .... \n");
		while ((output = br.readLine()) != null) {
			System.out.println(output);
			response = response + output;
		}
		
		conn.disconnect();
		return response;
	}

}
