package com.timenome.examples.vmstat.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.timenome.examples.vmstat.exception.CommandExecutionException;
import com.timenome.examples.vmstat.exception.CommandNotFoundException;

final public class VMStatUtils {

	

	public static boolean isUnix() {
		String os = System.getProperty("os.name").toLowerCase();
		return (os.indexOf("nix") >= 0 || os.indexOf("nux") >= 0 || os.indexOf("aix") > 0 );
	}

	public static String getVMStatOutput(String ... args) throws CommandExecutionException, CommandNotFoundException {
		String commandOutput = null;
		try {
			commandOutput = executeCommand("vmstat", args);
		} catch (IOException e) {
			throw new CommandExecutionException(e);
		} catch (InterruptedException e) {
			throw new CommandExecutionException(e);
		}
		if(commandOutput != null) {
			if(commandOutput.toLowerCase().contains("command not found")) {
				throw new CommandNotFoundException(commandOutput);
			}
		}
		else {
			throw new CommandExecutionException("Null response received when expected not null.");
		}
		return commandOutput;
	}

	public static String executeCommand(String command, String ... args) throws IOException, InterruptedException {
		Runtime runtime = Runtime.getRuntime();
		for(String arg : args) {
			command = command + VMStatConstants.SPACE + arg;
		}
		Process process = runtime.exec(command);
		process.waitFor();
		BufferedReader b = null;
		String output = null;
		try {
			b = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String line = "";
			output = "";
			while ((line = b.readLine()) != null) {
				output = output + line + VMStatConstants.NEW_LINE;
			}
		}
		finally {
			b.close();
		}
		return output.trim();
	}

}
