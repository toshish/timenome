/**
 * 
 */
package com.timenome.examples.vmstat.exception;

/**
 * @author toshishj
 *
 */
public class UnsupportedOperatingSystem extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public UnsupportedOperatingSystem() {
		this("Non-Unix OS detected. Only Unix systems are supported.");
	}
	
	/**
	 * @param message
	 */
	public UnsupportedOperatingSystem(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public UnsupportedOperatingSystem(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public UnsupportedOperatingSystem(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public UnsupportedOperatingSystem(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
