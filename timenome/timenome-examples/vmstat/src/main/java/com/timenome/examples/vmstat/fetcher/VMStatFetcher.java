/**
 * 
 */
package com.timenome.examples.vmstat.fetcher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.timenome.examples.vmstat.exception.CommandExecutionException;
import com.timenome.examples.vmstat.exception.CommandNotFoundException;
import com.timenome.examples.vmstat.exception.InvalidOutputException;
import com.timenome.examples.vmstat.exception.UnsupportedOperatingSystem;
import com.timenome.examples.vmstat.utils.VMStatConstants;
import com.timenome.examples.vmstat.utils.VMStatUtils;

/**
 * @author toshishj
 *
 */
public class VMStatFetcher {

	/**
	 * @throws UnsupportedOperatingSystem 
	 * @throws CommandNotFoundException 
	 * @throws CommandExecutionException 
	 * 
	 */
	public VMStatFetcher() throws UnsupportedOperatingSystem, CommandExecutionException, CommandNotFoundException {
		if(!VMStatUtils.isUnix()) {
			throw new UnsupportedOperatingSystem();
		}
		String vmstatVersion = VMStatUtils.getVMStatOutput("-V");
		System.out.println("vmstatVersion : " + vmstatVersion);
	}

	public Map<String, Long> getStats() throws CommandExecutionException, CommandNotFoundException, InvalidOutputException {
		HashMap<String, Long> stats = new HashMap<String, Long>();
		String output = VMStatUtils.getVMStatOutput("-a");
		
		String[] outputLines = output.split("\\\n");
		
		if(outputLines.length != 3) {
			throw new InvalidOutputException("Output of 3 lines expected. Got : " + output);
		}
		
		String headerLine = outputLines[1];
		String valuesLine = outputLines[2];
		
		String[] headersTmp = headerLine.split(VMStatConstants.SPACE);
		String[] valuesTmp = valuesLine.split(VMStatConstants.SPACE);
		
		ArrayList<String> headers = new ArrayList<String>();
		ArrayList<Long> values = new ArrayList<Long>();
		for(String s : headersTmp) {
			if(s.length() > 0) {
				headers.add(s);
			}
		}
		for(String s : valuesTmp) {
			if(s.length() > 0) {
				values.add(Long.parseLong(s));
			}
		}
		
		if(headers.size() != values.size()) {
			for(String s : headers) {
				System.out.println(s);
			}
			for(Long l : values) {
				System.out.println(l);
			}
			throw new InvalidOutputException("Header cound and values cound does not match."
					+ " Header count : " + headers.size() + ", Values count : " + values.size());
		}
		
		for(int i = 0; i < headers.size(); i++) {
			stats.put(headers.get(i), values.get(i));
		}
		
		return stats;
	}
	
}
