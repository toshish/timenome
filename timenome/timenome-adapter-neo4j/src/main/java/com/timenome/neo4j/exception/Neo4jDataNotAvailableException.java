package com.timenome.neo4j.exception;

public class Neo4jDataNotAvailableException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4617493169771386617L;

	public Neo4jDataNotAvailableException() {
		// TODO Auto-generated constructor stub
	}

	public Neo4jDataNotAvailableException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public Neo4jDataNotAvailableException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public Neo4jDataNotAvailableException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public Neo4jDataNotAvailableException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
