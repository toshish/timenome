package com.timenome.neo4j.constants;

public final class Neo4jConstants {
	public static final String NEO4J_PROPERTIES_FILE = System.getenv("TIMENOME_PROPERTIES");;
	
	
	public static final String PROPERTY_NAME = "name";
	public static final String PROPERTY_DATA_STORE_ID = "data_store_id";
	public static final String PROPERTY_ROOT_DATA_STORE_ID = "root_data_store_id";
	public static final String PROPERTY_STORAGE_RANGE_TIME = "storage_range_time";
	public static final String PROPERTY_STORAGE_PRECISION_TIME = "storage_precision_time";
	public static final String PROPERTY_CDF = "cdf";
	public static final String PROPERTY_HOUR_NODE_ID = "hour_node_id";
	
	public static final String PROPERTY_STORAGE_RANGE_TIME_UNIT = "storage_range_time_unit";
	public static final String PROPERTY_STORAGE_PRECISION_TIME_UNIT = "storage_precision_time_unit";


	public static final String PROPERTY_TIME_VALUE = "time_value";
}
