package com.timenome.neo4j.core;

import java.util.Map;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;

import com.timenome.api.core.DataStore;
import com.timenome.api.core.User;
import com.timenome.api.exceptions.UserDoesNotExists;
import com.timenome.api.logger.TimeNomeLogger;
import com.timenome.neo4j.Neo4jConnection;
import com.timenome.neo4j.constants.Neo4jConstants;

public class Neo4jUser extends User {
	
	private Node userNode;
	private GraphDatabaseService dbService;
	private static TimeNomeLogger logger = TimeNomeLogger.getTimeNomeLogger(Neo4jUser.class);
	
	private Neo4jUser(String name) {
		super(name);
		if(dbService == null) {
			dbService = Neo4jConnection.getNeo4jConnection().getNeo4jDB();
		}
	}

	public Neo4jUser(Node userNode) {
		this((String) userNode.getProperty(Neo4jConstants.PROPERTY_NAME));
		this.userNode = userNode;
	}

	@Override
	protected void getMyDataStoresSpec() {
		try {
			Map<String, DataStore> dsMap = Neo4jConnection.getNeo4jConnection().getAllDataStores(getName());
			
			for(String key : dsMap.keySet()) {
				dataStoreMap.put(key, dsMap.get(key));
			}
			
		} catch (UserDoesNotExists e1) {
			logger.warning(getName() + " user does not exist");
		}
//		HashMap<String, Node> dataStoreNodeMap = null;
//		if(dbService == null) {
//			dbService = Neo4jConnection.getNeo4jConnection().getNeo4jDB();
//		}
//		
//	
//		try {
//			dataStoreNodeMap = Neo4jDatabaseUtils.getAllDataStoreNodes(dbService, getName());
//			for(String dsName : dataStoreNodeMap.keySet()) {
//				if(!dataStoreMap.containsKey(dsName)) {
//					DataStore dataStore = new Neo4jDataStore(dbService, dataStoreNodeMap.get(dsName));
//					dataStoreMap.put(dsName, dataStore);
//				}
//			}
//		} catch (UserDoesNotExists e) {
//			
//		}
	}

	public Node getNode() {
		return this.userNode;
	}

	

}
