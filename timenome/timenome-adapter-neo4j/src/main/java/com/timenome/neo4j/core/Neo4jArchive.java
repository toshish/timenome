package com.timenome.neo4j.core;

import java.util.List;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;

import com.timenome.api.BuiltInCDF;
import com.timenome.api.Entry;
import com.timenome.api.TimeUnit;
import com.timenome.api.core.Archive;
import com.timenome.api.core.TimeRangeDef;
import com.timenome.api.exceptions.TimeOutOfRangeException;
import com.timenome.api.shared.ConsolidationFunction;
import com.timenome.neo4j.Neo4jConnection;
import com.timenome.neo4j.constants.Neo4jConstants;
import com.timenome.neo4j.exception.Neo4jDataNotAvailableException;
import com.timenome.neo4j.exception.Neo4jUnexpectedSituationException;
import com.timenome.neo4j.utils.Neo4jDatabaseUtils;

public class Neo4jArchive extends Archive {
	
	private GraphDatabaseService dbService;
	private Node archiveNode;
	
	public Neo4jArchive(Node archiveNode) {
		super((String) archiveNode.getProperty(Neo4jConstants.PROPERTY_NAME)
				, new TimeRangeDef(TimeUnit.MILLI_SECOND, 
						(Long) archiveNode.getProperty(Neo4jConstants.PROPERTY_STORAGE_RANGE_TIME))
		, new TimeRangeDef(TimeUnit.MILLI_SECOND, 
				(Long) archiveNode.getProperty(Neo4jConstants.PROPERTY_STORAGE_PRECISION_TIME)), 
				BuiltInCDF.valueOf((String) archiveNode.getProperty(Neo4jConstants.PROPERTY_CDF)));
//		System.out.println("Const: " + archiveNode);
		this.archiveNode = archiveNode;
//		System.out.println("Constr: " + this.archiveNode);
		this.dbService = Neo4jConnection.getNeo4jConnection().getNeo4jDB();
	}
	
	public Neo4jArchive(String archiveName, TimeRangeDef storageRange, 
			TimeRangeDef storagePrecision, ConsolidationFunction consolidationFunction) {
			super(archiveName, storageRange, storagePrecision, consolidationFunction);
			this.dbService = Neo4jConnection.getNeo4jConnection().getNeo4jDB();
			if(archiveNode == null) {
				// Not called via direct Node object's constructor.
				// So construct here manually. This is case when adding new Archive.
				Transaction tx = dbService.beginTx();
				archiveNode = dbService.createNode();
				archiveNode.setProperty(Neo4jConstants.PROPERTY_NAME, archiveName);
				archiveNode.setProperty(Neo4jConstants.PROPERTY_STORAGE_RANGE_TIME, storageRange.getTimeInMillseconds());
				archiveNode.setProperty(Neo4jConstants.PROPERTY_STORAGE_PRECISION_TIME, storagePrecision.getTimeInMillseconds());
				archiveNode.setProperty(Neo4jConstants.PROPERTY_CDF, consolidationFunction.getCDFValue());
				tx.success();
				tx.finish();
			}
	}
	
	
	public Neo4jArchive(String archiveName, TimeRangeDef storageRange, 
			TimeRangeDef storagePrecision, BuiltInCDF builtInCDF) {
		super(archiveName, storageRange, storagePrecision, builtInCDF);
		this.dbService = Neo4jConnection.getNeo4jConnection().getNeo4jDB();
		if(archiveNode == null) {
			// Not called via direct Node object's constructor.
			// So construct here manually. This is case when adding new Archive.
			Transaction tx = dbService.beginTx();
			archiveNode = dbService.createNode();
			archiveNode.setProperty(Neo4jConstants.PROPERTY_NAME, archiveName);
			archiveNode.setProperty(Neo4jConstants.PROPERTY_STORAGE_RANGE_TIME, storageRange.getTimeInMillseconds());
			archiveNode.setProperty(Neo4jConstants.PROPERTY_STORAGE_PRECISION_TIME, storagePrecision.getTimeInMillseconds());
			archiveNode.setProperty(Neo4jConstants.PROPERTY_CDF, builtInCDF.toString());
			tx.success();
			tx.finish();
		}
	}

	@Override
	public Object getSpecificArchiveObject() {
		return archiveNode;
	}

	/**
	 * 
	 */
	@Override
	public boolean putSpec(Entry<? extends Number> entry) {
//		System.out.println("I am " + getArchiveName() + " on " + ++counter);
//		System.out.println("Adding entry: " + entry);
		try {
			return Neo4jDatabaseUtils.addEntryToArchive(dbService, archiveNode, entry);
		} catch (Neo4jUnexpectedSituationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	protected TimeRangeDef getArchiveStartTime() {
		long timestamp = -1;
		try {
			timestamp = Neo4jDatabaseUtils.getStartingTimestampOfArchive(dbService, archiveNode);
		} catch (Neo4jDataNotAvailableException
				| Neo4jUnexpectedSituationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		TimeRangeDef time = new TimeRangeDef(TimeUnit.MILLI_SECOND, timestamp);
		return time;
	}

	@Override
	protected List<Entry<? extends Number>> getDataSpec(TimeRangeDef startTimeStamp,
			TimeRangeDef endTimeStamp) throws TimeOutOfRangeException {
			return Neo4jDatabaseUtils.getDataFromArchive(dbService, archiveNode, 
					startTimeStamp.getTimeInMillseconds(), endTimeStamp.getTimeInMillseconds());
	}

}
