package com.timenome.neo4j.exception;

public class Neo4jNodeCreationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5440282386955437975L;

	public Neo4jNodeCreationException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Neo4jNodeCreationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public Neo4jNodeCreationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public Neo4jNodeCreationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
