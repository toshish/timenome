package com.timenome.neo4j;

import org.neo4j.graphdb.RelationshipType;

public enum Relationships implements RelationshipType {
	HAS_DATA_STORE,
	HAS_ARCHIVE,
	HAS_ARCHIVE_DATA,
	DATA_STORE, 
	CACHE,
	HAS_DATA,
	YEAR,
	MONTH,
	DAY,
	HOUR, 
	NEXT,
	LAST_HOUR, 
	USER
}
