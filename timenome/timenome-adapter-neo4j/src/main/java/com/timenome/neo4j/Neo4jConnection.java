package com.timenome.neo4j;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

import com.timenome.api.core.Connection;
import com.timenome.api.core.DataStore;
import com.timenome.api.core.User;
import com.timenome.api.exceptions.DataStoreException;
import com.timenome.api.exceptions.UserAlreadyExists;
import com.timenome.api.exceptions.UserDoesNotExists;
import com.timenome.neo4j.constants.Neo4jConstants;
import com.timenome.neo4j.core.Neo4jDataStore;
import com.timenome.neo4j.exception.Neo4jNodeCreationException;
import com.timenome.neo4j.exception.Neo4jPropertiesException;
import com.timenome.neo4j.utils.Neo4jDatabaseUtils;

/**
 * Connection instance class for Neo4j DB instance.
 * 
 * @author toshish
 * 
 */
public class Neo4jConnection implements Connection {
	private Properties properties;
	private static Neo4jConnection neo4jConnection;
	private GraphDatabaseService neo4jDB;
	private Map<String, Map<String, DataStore>> dataStoreMap = new HashMap<>();

	private GraphDatabaseFactory gdbFactory = new GraphDatabaseFactory();
	
 
	
	private Neo4jConnection(Properties properties) throws Neo4jPropertiesException {
		this.properties = properties;
		String neo4jDBPath = this.properties.getProperty("neo4j.db.dir");
		if(neo4jDBPath != null) {
			neo4jDB = gdbFactory.newEmbeddedDatabaseBuilder( neo4jDBPath )
				    			.loadPropertiesFromFile("neo4j.properties" )
				    			.newGraphDatabase();
		}
		else {
			throw new Neo4jPropertiesException("neo4j.db.dir property must be set.");
		}
		
//		// Check if this db already has a dataStore cache
//		Transaction tx = neo4jDB.beginTx();
//		if(!neo4jDB.getNodeById(0).getRelationships(Direction.OUTGOING, Relationships.DATA_STORE).iterator().hasNext()) {
//			// There is no DataStore root node.
//			// Create it.
//			rootDS = neo4jDB.createNode();
//			rootDS.setProperty(Neo4jConstants.PROPERTY_NAME, "DATA_STORE_ROOT");
//			neo4jDB.getNodeById(0).createRelationshipTo(rootDS, Relationships.DATA_STORE);
//		}
//		else {
//			// Root DataStore node exists.
//			// Use it.
//			rootDS = neo4jDB.getNodeById(0).getRelationships(Direction.OUTGOING, Relationships.DATA_STORE).iterator().next().getEndNode();
//		}
//		// Now rootDS is initialised.
//		// Neo4jConnection construction completes.
//		
//		tx.success();
//		tx.finish();
		
	}
	
	
	/**
	 * 
	 * @return a singleton instance of the Neo4j Connection. null if any error occurs, error is printed in form of stack trace.
	 */
	public static Neo4jConnection getNeo4jConnection() {
		if(neo4jConnection == null) {
			Properties properties = new Properties();
			try {
				properties.load(new FileInputStream(new File(Neo4jConstants.NEO4J_PROPERTIES_FILE)));
				neo4jConnection = new Neo4jConnection(properties);
				// Get all Data Stores and store in map.
				
				
				// Data stores are loaded.

				
			} catch (FileNotFoundException e) {
				Neo4jPropertiesException npe = new Neo4jPropertiesException("Properties file not found.", e);
				npe.printStackTrace();
			} catch (IOException e) {
				Neo4jPropertiesException npe = new Neo4jPropertiesException("I/O Error while accessing neo4jConnector.properties file.", e);
				npe.printStackTrace();
			} catch (Neo4jPropertiesException e) {
				e.printStackTrace();
			}
		}
		return neo4jConnection;
	}

	@Override
	public DataStore createDataStore(String dsName, String username) throws DataStoreException, UserDoesNotExists {
		Node newDSNode = null;
		try {
			newDSNode = Neo4jDatabaseUtils.createNewDataStore(neo4jDB, username, dsName);
		} catch (Neo4jNodeCreationException e) {
			throw new DataStoreException(e);
		}
		Neo4jDataStore dataStore = new Neo4jDataStore(neo4jDB, newDSNode);
		updateDataStoreMapFor(username);
		dataStoreMap.get(username).put(dataStore.getDataStoreName(), dataStore);
		return dataStore;
	}

	@Override
	public boolean deleteDataStore(String dsName, String username) throws DataStoreException, UserDoesNotExists {
		boolean isDSDeleted = false;
		try {
			isDSDeleted = Neo4jDatabaseUtils.deleteDataStore(neo4jDB, username, dsName);
			if(isDSDeleted) {
				DataStore ds = dataStoreMap.get(username).get(dsName);
				ds.stop();
				dataStoreMap.get(username).remove(dsName);
			}
		} catch (Neo4jNodeCreationException e2) {
			throw new DataStoreException(e2);
		}
		return isDSDeleted;
	}

	@Override
	public DataStore getDataStore(String dsName, String username) throws UserDoesNotExists {
		if(!dataStoreMap.containsKey(username)) {
			updateDataStoreMapFor(username);
		}
		return dataStoreMap.get(username).get(dsName);
	}
	
	public GraphDatabaseService getNeo4jDB() {
		return neo4jDB;
	}


	@Override
	public Map<String, DataStore> getAllDataStores(String username) throws UserDoesNotExists {
		if(!dataStoreMap.containsKey(username)) {
			updateDataStoreMapFor(username);
		}
		return dataStoreMap.get(username);
	}
	
	@Override
	public void close() {
		neo4jDB.shutdown();
	}

	private void updateDataStoreMapFor(String username) throws UserDoesNotExists {
		HashMap<String, Node> dsNodeMap = Neo4jDatabaseUtils.getAllDataStoreNodes(neo4jDB, Neo4jDatabaseUtils.getUserNode(neo4jDB, username));
		HashMap<String, DataStore> dsMap = new HashMap<>();
		Iterator<String> dsIter = dsNodeMap.keySet().iterator();
		while(dsIter.hasNext()) {
			String dataStoreName = dsIter.next();
			Node dataStoreNode = dsNodeMap.get(dataStoreName);
			if(dataStoreNode != null) {
				dsMap.put(dataStoreName, new Neo4jDataStore(neo4jConnection.getNeo4jDB(), dataStoreNode));
			}
		}
		if(dataStoreMap.containsKey(username)) {
			dataStoreMap.remove(username);
		}
		dataStoreMap.put(username, dsMap);
	}


	@Override
	public Map<String, User> getAllUsers() {
		return Neo4jDatabaseUtils.getAllUsers(neo4jDB);
	}

	@Override
	public User getUser(String username) throws UserDoesNotExists {
		return Neo4jDatabaseUtils.getUser(neo4jDB, username);
	}

	@Override
	public User createUser(String username) throws UserAlreadyExists {
		return Neo4jDatabaseUtils.addNewUser(neo4jDB, username);
	}


	@Override
	public boolean deleteUser(String username) throws UserDoesNotExists {
		return Neo4jDatabaseUtils.deleteUser(neo4jDB, username);
	}
}
