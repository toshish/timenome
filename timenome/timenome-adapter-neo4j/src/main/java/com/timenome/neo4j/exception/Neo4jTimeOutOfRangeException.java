package com.timenome.neo4j.exception;

import com.timenome.api.exceptions.TimeOutOfRangeException;

public class Neo4jTimeOutOfRangeException extends TimeOutOfRangeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2084570609875937540L;

	public Neo4jTimeOutOfRangeException() {
		// TODO Auto-generated constructor stub
	}

	public Neo4jTimeOutOfRangeException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public Neo4jTimeOutOfRangeException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public Neo4jTimeOutOfRangeException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public Neo4jTimeOutOfRangeException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
