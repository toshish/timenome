package com.timenome.neo4j.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;

import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.cypher.javacompat.ExecutionResult;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.NotFoundException;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Transaction;
import org.neo4j.helpers.collection.IteratorUtil;

import com.timenome.api.Entry;
import com.timenome.api.core.User;
import com.timenome.api.exceptions.DataStoreException;
import com.timenome.api.exceptions.UserAlreadyExists;
import com.timenome.api.exceptions.UserDoesNotExists;
import com.timenome.api.logger.TimeNomeLogger;
import com.timenome.neo4j.Relationships;
import com.timenome.neo4j.constants.Neo4jConstants;
import com.timenome.neo4j.core.Neo4jUser;
import com.timenome.neo4j.exception.Neo4jDataNotAvailableException;
import com.timenome.neo4j.exception.Neo4jNodeCreationException;
import com.timenome.neo4j.exception.Neo4jTimeOutOfRangeException;
import com.timenome.neo4j.exception.Neo4jUnexpectedSituationException;

public final class Neo4jDatabaseUtils {

	private static TimeNomeLogger logger = TimeNomeLogger.getTimeNomeLogger(Neo4jDatabaseUtils.class);

	/**
	 * Creates a node for new DataStore with relating to node '0' with relationship HAS_DATA_STORE.
	 * @param dbService
	 * @param dataStoreName
	 * @return newly created DataStore node.
	 * @throws Neo4jNodeCreationException 
	 * @throws UserDoesNotExists 
	 * @throws DataStoreException 
	 */
	public static Node createNewDataStore(GraphDatabaseService dbService, String username, String dataStoreName) 
			throws Neo4jNodeCreationException, UserDoesNotExists, DataStoreException {
		Node userNode = getUserNode(dbService, username);
		Node cacheNode = getDSCacheNode(dbService, userNode);
		Node dataStoreNode = getDataStoreNode(dbService, cacheNode, dataStoreName);
		if(dataStoreNode != null) {
			throw new DataStoreException("Data Store " + dataStoreName + " already exist.");
		}
		Transaction tx = dbService.beginTx();
		Node newDSNode = dbService.createNode();
		try {
			newDSNode.setProperty(Neo4jConstants.PROPERTY_NAME, dataStoreName);
			userNode.createRelationshipTo(newDSNode, Relationships.HAS_DATA_STORE);
			// Create cache entry for this DataStore
			if(!createCacheEntryForDS(dbService, newDSNode, userNode)) {
				throw new Neo4jNodeCreationException("Cache entry was not created for this DataStore node.");
			}
		}
		finally {
			tx.success();
			tx.finish();
		}
		return newDSNode;
	}
	/**
	 * Returns DataStore cache node. If node is not present, this function will create it.
	 * @param dbService
	 * @param userNode 
	 * @return DataStore cache node.
	 * @throws Neo4jNodeCreationException 
	 */
	public static Node getDSCacheNode(GraphDatabaseService dbService, Node userNode) throws Neo4jNodeCreationException {
		long userNodeId = userNode.getId();

		Iterator<Relationship> relIter = dbService.getNodeById(userNodeId).getRelationships(Direction.OUTGOING, Relationships.CACHE).iterator();
		if(relIter.hasNext()) {
			return relIter.next().getEndNode();
		}
		else {
			Iterator<Relationship> iter = dbService.getNodeById(userNodeId).getRelationships(Direction.OUTGOING, Relationships.DATA_STORE).iterator();
			Node dsRootNode = null;
			Transaction tx = dbService.beginTx();
			if(iter.hasNext()) {
				dsRootNode = iter.next().getEndNode();
			}
			else {
				// DataStore root doesn't exist. Create it.
				dsRootNode = dbService.createNode();
				dsRootNode.setProperty(Neo4jConstants.PROPERTY_NAME, "ROOT_DATA_STORE");
				//				throw new Neo4jNodeCreationException("DataStore Root Node doesn't exist. Cache cannot exist for non-existent DataStore hierarchy.");
			}

			Node newDSCacheNode = dbService.createNode();
			newDSCacheNode.setProperty(Neo4jConstants.PROPERTY_NAME, "DATA_STORE_CACHE");
			newDSCacheNode.setProperty(Neo4jConstants.PROPERTY_ROOT_DATA_STORE_ID, dsRootNode.getId());
			dbService.getNodeById(userNodeId).createRelationshipTo(newDSCacheNode, Relationships.CACHE);
			tx.success();
			tx.finish();
			return newDSCacheNode;
		}
	}

	private static boolean createCacheEntryForDS(GraphDatabaseService dbService, Node dsNode, Node userNode) throws Neo4jNodeCreationException {
		Transaction tx = dbService.beginTx();
		try {
			Node dsCacheNode = getDSCacheNode(dbService, userNode);
			dsCacheNode.setProperty((String)dsNode.getProperty(Neo4jConstants.PROPERTY_NAME), dsNode.getId());
		}
		finally {
			tx.success();
			tx.finish();
		}
		return true;
	}

	public static HashMap<String, Node> getAllDataStoreNodes(GraphDatabaseService dbService, Node userNode) {
		HashMap<String, Node> map = new HashMap<String, Node>();
		Node cacheNode = null;
		try {
			cacheNode = getDSCacheNode(dbService, userNode);
		} catch (Neo4jNodeCreationException e) {
			e.printStackTrace();
			return null;
		}

		Iterator<String> iterKeys = cacheNode.getPropertyKeys().iterator();
		while(iterKeys.hasNext()) {
			String property = iterKeys.next().trim();
			if(!property.equalsIgnoreCase(Neo4jConstants.PROPERTY_NAME) && !property.equalsIgnoreCase("ROOT_DATA_STORE_ID")) {
				Node dsNode = getDataStoreNode(dbService, cacheNode, property);
				map.put(property, dsNode);
			}

		}
		return map;

	}

	public static Node getDataStoreNode(GraphDatabaseService dbService, Node cacheNode, String dsName) {
		Node dsNode = null;
		logger.info("Getting dataStore node for : " + dsName);
		if(cacheNode.hasProperty(dsName)) {
			Object nodeId = cacheNode.getProperty(dsName);
			long dsNodeId = -1;
			if(nodeId instanceof Number) {
				dsNodeId = (Long) nodeId;
			}
			else {
				dsNodeId = Long.parseLong(((String) nodeId));
			}

			ExecutionEngine engine = new ExecutionEngine(dbService);
			Transaction tx = dbService.beginTx();
			ExecutionResult result = engine.execute("start n=node(" + dsNodeId + ") return n");
			Iterator<Node> n_column = result.columnAs("n");
			for(Node node : IteratorUtil.asIterable(n_column))
			{
				dsNode = node;
			}
			tx.success();
			tx.finish();
		}
		return dsNode;
	}

	public static boolean addEntryToArchive(GraphDatabaseService dbService, Node archiveNode, Entry<? extends Number> entry) throws Neo4jUnexpectedSituationException {
		//		System.out.println(archiveNode);
		//		System.out.println(archiveNode.getSingleRelationship(Relationships.HAS_DATA, Direction.OUTGOING));
		Node rootArchiveDataNode = archiveNode.getSingleRelationship(Relationships.HAS_DATA, Direction.OUTGOING).getEndNode();
		Node hourNode;
		try {
			hourNode = getHourNodeOfTimeStamp(dbService, archiveNode, entry.getTimeStamp(), true, true);
		} catch (Neo4jTimeOutOfRangeException e) {
			e.printStackTrace();
			// Impossible to occur this situation. As create operation is true.
			throw new Neo4jUnexpectedSituationException("Fetch operation failed during create operation. This is invalid situation."
					+ " This should have never occured. This issue needs to be fixed.");
		}
		Node lastHourNode = getLastHourNode(dbService,rootArchiveDataNode);
		Transaction tx = dbService.beginTx();
		if(lastHourNode != null) {
			// last hour's node exists
			//			long lastHourNodeId = (Long)lastHourNode.getProperty(Neo4jConstants.PROPERTY_HOUR_NODE_ID);
			if(lastHourNode.getId() != hourNode.getId()) {
				// Current hour node is not last hour node.
				// Next hour has started
				// create next relationship from last hour's node to current hour's node
				lastHourNode.createRelationshipTo(hourNode, Relationships.NEXT);
			}
		}
		// Update last hour's node
		updateLastHourNode(dbService, rootArchiveDataNode, hourNode);

		Relationship dataRel = hourNode.getSingleRelationship(Relationships.HAS_DATA, Direction.OUTGOING);
		Node dataNode = null;
		if(dataRel == null) {
			//Obviously data node doesn't exist
			//create it.
			dataNode = dbService.createNode();
			hourNode.createRelationshipTo(dataNode, Relationships.HAS_DATA);
		}
		else {
			dataNode = dataRel.getEndNode();
		}
		dataNode.setProperty(Long.toString(entry.getTimeStamp()), entry.getValue() == null ? "" : entry.getValue());
		logger.finest("Entry added : " + entry);
		tx.success();
		tx.finish();

		return true;
	}

	private static Node getNodeWithTimeValue(GraphDatabaseService dbService, int timeValue, Relationships rel, Node fromNode, boolean create, boolean isStart) throws Neo4jTimeOutOfRangeException {
		Iterator<Relationship> relIter = fromNode.getRelationships(Direction.OUTGOING, rel).iterator();
		Node node = null;
		TreeMap<Long, Node> list = new TreeMap<>();
		while(relIter.hasNext()) {
			Relationship reletionship = relIter.next();
			Node tempNode = reletionship.getEndNode();
			int value = (Integer) tempNode.getProperty(Neo4jConstants.PROPERTY_TIME_VALUE);
			list.put((long) value, tempNode);
			if(value == timeValue) {
				node = tempNode;
				break;
			}
		}
		if(node == null) {
			if(create) {
				Transaction tx = dbService.beginTx();
				node = dbService.createNode();
				node.setProperty(Neo4jConstants.PROPERTY_TIME_VALUE, timeValue);
				fromNode.createRelationshipTo(node, rel);
				tx.success();
				tx.finish();
			}
			else {
				long closestValue = (long) timeValue;
				if(isStart) {
					closestValue = closestLarge((long) timeValue, list.keySet());
				}
				else {
					closestValue = closestSmall((long) timeValue, list.keySet());
				}

				node = list.get(closestValue);

				if(node == null) {
					Iterable<Relationship> nextRels = null;
					if(!isStart) {
						closestValue = closestLarge(0l, list.keySet());
						node = list.get(closestValue);
						// Traverse backwards with Next relationships
						nextRels = node.getRelationships(Direction.INCOMING, Relationships.NEXT);
					}
					else {
						closestValue = closestSmall((long) timeValue, list.keySet());
						node = list.get(closestValue);
						// Traverse backwards with Next relationships
						nextRels = node.getRelationships(Direction.OUTGOING, Relationships.NEXT);
					}
					Iterator<Relationship> iter = nextRels.iterator();
					if(iter.hasNext()) {
						Relationship nextRel = iter.next();
						node = nextRel.getEndNode();
					}
					else {
						// There is no further data available before/after this point.
						return node;
					}

				}
			}
		}
		return node;
	}

	private static Node getLastHourNode(GraphDatabaseService dbService, Node archiveDataNode) {
		Transaction tx = dbService.beginTx();
		Iterator<Relationship> iter = archiveDataNode.getRelationships(Relationships.LAST_HOUR, Direction.OUTGOING).iterator();
		Node lastHourNode = null;
		long hourNodeId = -1;
		if(iter.hasNext()) { 
			Relationship rel = iter.next();
			hourNodeId = (Long) rel.getEndNode().getProperty(Neo4jConstants.PROPERTY_HOUR_NODE_ID);
		}
		if(hourNodeId > -1) {
			ExecutionEngine exEngine = new ExecutionEngine(dbService);
			String query = "start n=node(" + hourNodeId + ")" +
					"return n";
			ExecutionResult exResult = exEngine.execute(query);
			Iterator<Node> nColumn = exResult.columnAs("n");
			for(Node node : IteratorUtil.asIterable(nColumn))
			{
				lastHourNode = node;
			}
		}
		tx.success();
		tx.finish();
		return lastHourNode;
	}

	private static void updateLastHourNode(GraphDatabaseService dbService, Node archiveDataNode, Node hourNode) {
		Transaction tx = dbService.beginTx();
		Iterator<Relationship> iter = archiveDataNode.getRelationships(Relationships.LAST_HOUR, Direction.OUTGOING).iterator();
		Node lastHourNode = null;
		if(iter.hasNext()) { 
			Relationship rel = iter.next();
			lastHourNode = rel.getEndNode();
		}
		if(lastHourNode == null) {
			// There is no last hour's node yet
			// create it
			lastHourNode = dbService.createNode();
			archiveDataNode.createRelationshipTo(lastHourNode, Relationships.LAST_HOUR);
		}
		// Update lastHourNode
		lastHourNode.setProperty(Neo4jConstants.PROPERTY_HOUR_NODE_ID, hourNode.getId());
		tx.success();
		tx.finish();
	}

	public static long getStartingTimestampOfArchive(GraphDatabaseService dbService, Node archiveNode) throws Neo4jDataNotAvailableException, Neo4jUnexpectedSituationException {
		long firstTimeStamp = -1;
		Node rootArchiveDataNode = archiveNode.getSingleRelationship(Relationships.HAS_DATA, Direction.OUTGOING).getEndNode();
		Node smallestYear = getNodeWithSmallestValue(dbService, rootArchiveDataNode, Relationships.YEAR);
		Node smallestMonth = getNodeWithSmallestValue(dbService, smallestYear, Relationships.MONTH);
		Node smallestDay = getNodeWithSmallestValue(dbService, smallestMonth, Relationships.DAY);
		Node smallestHour = getNodeWithSmallestValue(dbService, smallestDay, Relationships.HOUR);
		Relationship dataNodeInHourRel = smallestHour.getSingleRelationship(Relationships.HAS_DATA, Direction.OUTGOING);
		if(dataNodeInHourRel == null) {
			throw new Neo4jDataNotAvailableException("Data in archive is not available.");
		}
		Node dataNodeInHour = dataNodeInHourRel.getEndNode();
		Iterator<String> iter = dataNodeInHour.getPropertyKeys().iterator();
		ArrayList<String> timeStampList = new ArrayList<String>();
		// This is bad practice.
		// TODO: To be changed in future soon
		while(iter.hasNext()) {
			timeStampList.add(iter.next());
		}
		if(timeStampList.size() > 0) {
			Collections.sort(timeStampList);
			firstTimeStamp = Long.parseLong(timeStampList.get(0));
		}
		else {
			throw new Neo4jUnexpectedSituationException("Hour node exists but no data in it's data node.");
		}
		return firstTimeStamp;
	}

	private static Node getNodeWithSmallestValue(GraphDatabaseService dbService, Node fromNode, RelationshipType rel) throws Neo4jDataNotAvailableException {
		if(fromNode == null) {
			throw new Neo4jDataNotAvailableException(rel.toString() + " data in archive is not available.");
		}
		long smallest = Long.MAX_VALUE;
		Node wantedNode = null;
		for(Relationship relationship : fromNode.getRelationships(rel, Direction.OUTGOING)) {
			Node node = relationship.getEndNode();
			long value = (Long) node.getProperty(Neo4jConstants.PROPERTY_TIME_VALUE);
			if(smallest > value) {
				smallest = value;
				wantedNode = node;
			}
		}
		return wantedNode;
	}

	public static List<Entry<? extends Number>> getDataFromArchive(GraphDatabaseService dbService,
			Node archiveNode, long startTime, long endTime) throws Neo4jTimeOutOfRangeException {
		ArrayList<Entry<? extends Number>> dataList = new ArrayList<>();
		try {
			Node startHourNode = getHourNodeOfTimeStamp(dbService, archiveNode, startTime, false, true);
			Node endHourNode = getHourNodeOfTimeStamp(dbService, archiveNode, endTime, false, false);
			boolean lastNodeDone = false;
			Node hourNode = startHourNode;
			do {
				Relationship rel = hourNode.getSingleRelationship(Relationships.HAS_DATA, Direction.OUTGOING);
				if(rel == null) {
					continue;
				}
				Node dataNode = rel.getEndNode();
				Iterator<String> iter = dataNode.getPropertyKeys().iterator();
				ArrayList<Long> thisHourTimeStampList = new ArrayList<Long>(); 
				// Bad practice
				// TODO replace this with efficient code
				while(iter.hasNext()) {
					thisHourTimeStampList.add(Long.parseLong(iter.next()));
				}
				Collections.sort(thisHourTimeStampList);
				int startIndex = 0;
				int endIndex = thisHourTimeStampList.size() - 1;

				if(hourNode.getId() == startHourNode.getId()) {
					startIndex = thisHourTimeStampList.indexOf(closest(startTime, thisHourTimeStampList));
				}
				if(hourNode.getId() == endHourNode.getId()) {
					endIndex = thisHourTimeStampList.indexOf(closest(endTime, thisHourTimeStampList));
					lastNodeDone = true;
				}
				dataList.addAll(getDataListForTimeStamps(thisHourTimeStampList, startIndex, endIndex, dataNode));
				Iterable<Relationship> nextRels = hourNode.getRelationships(Relationships.NEXT, Direction.OUTGOING);
				Iterator<Relationship> relIter = nextRels.iterator();
				Relationship nextRel;
				if(relIter.hasNext()) {
					nextRel = relIter.next();
					if(relIter.hasNext()) {
						logger.warning("Too many Next relationships found for node with id : " + hourNode.getId());
					}
				}
				else {
					break;
				}

				hourNode = nextRel.getEndNode();
			} while(!lastNodeDone);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return dataList;
	}

	private static List<Entry<? extends Number>> getDataListForTimeStamps(List<Long> list, int startIndex, int endIndex, Node dataNode) {
		ArrayList<Entry<? extends Number>> dataList = new ArrayList<>();
		for(int index = startIndex; index < endIndex; index++) {
			Double value = null;
			try {
				value = (Double) dataNode.getProperty(Long.toString(list.get(index)));
				//				System.out.println(value);
			}
			catch(ClassCastException e) {
				// Null value detected.
			}
			dataList.add(new Entry<Double>((Long)list.get(index), value));
		}
		return dataList;
	}

	private static long closest(long of, Collection<Long> in) {
		long min = Integer.MAX_VALUE;
		long closest = of;

		for (long v : in) {
			final long diff = Math.abs(v - of);

			if (diff < min) {
				min = diff;
				closest = v;
			}
		}
		return closest;
	}

	private static long closestLarge(long of, Collection<Long> in) {
		long min = Integer.MAX_VALUE;
		long closest = of;

		for (long v : in) {
			final long diff = Math.abs(v - of);

			if (diff < min && v > of) {
				min = diff;
				closest = v;
			}
		}
		return closest;
	}

	private static long closestSmall(long of, Collection<Long> in) {
		long min = Integer.MAX_VALUE;
		long closest = of;

		for (long v : in) {
			final long diff = Math.abs(v - of);

			if (diff < min && v < of) {
				min = diff;
				closest = v;
			}
		}
		return closest;
	}

	private static Node getHourNodeOfTimeStamp(GraphDatabaseService dbService,
			Node archiveNode, long timeStamp, boolean create, boolean isStart) throws Neo4jTimeOutOfRangeException {
		Node rootArchiveDataNode = archiveNode.getSingleRelationship(Relationships.HAS_DATA, Direction.OUTGOING).getEndNode();
		Calendar cal = GregorianCalendar.getInstance(TimeZone.getDefault()); 
		cal.setTimeInMillis(timeStamp);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;
		int day = cal.get(Calendar.DAY_OF_MONTH);
		int hour = cal.get(Calendar.HOUR_OF_DAY);

		Node yearNode = getNodeWithTimeValue(dbService, year, Relationships.YEAR, rootArchiveDataNode, create, isStart);
		if((Integer)yearNode.getProperty(Neo4jConstants.PROPERTY_TIME_VALUE) != year) {
			year = (Integer)yearNode.getProperty(Neo4jConstants.PROPERTY_TIME_VALUE);
			if(isStart) {
				month = 1;
				day = 1;
				hour = 0;
			}
			else {
				month = 12;
				day = 31;
				hour = 23;
			}
		}
		Node monthNode = getNodeWithTimeValue(dbService, month, Relationships.MONTH, yearNode, create, isStart);
		if((Integer)monthNode.getProperty(Neo4jConstants.PROPERTY_TIME_VALUE) != month) {
			month = (Integer)monthNode.getProperty(Neo4jConstants.PROPERTY_TIME_VALUE);
			if(isStart) {
				day = 1;
				hour = 0;
			}
			else {
				Calendar tempCal = new GregorianCalendar(year, month - 1, 1);
				day = tempCal.getActualMaximum(Calendar.DAY_OF_MONTH);
				hour = 23;
			}
		}
		Node dayNode = getNodeWithTimeValue(dbService, day, Relationships.DAY, monthNode, create, isStart);
		if((Integer)dayNode.getProperty(Neo4jConstants.PROPERTY_TIME_VALUE) != day) {
			day = (Integer)dayNode.getProperty(Neo4jConstants.PROPERTY_TIME_VALUE);
			if(isStart) {
				hour = 0;
			}
			else {
				hour = 23;
			}
		}
		Node hourNode = getNodeWithTimeValue(dbService, hour, Relationships.HOUR, dayNode, create, isStart);
		return hourNode;
	}

	public static Neo4jUser addNewUser(GraphDatabaseService dbService, String username) throws UserAlreadyExists {
		Node userCacheNode = getUserCacheNode(dbService);
		if(userCacheNode.hasProperty(username)) {
			throw new UserAlreadyExists(username + " user already exists.");
		}
		Transaction tx = dbService.beginTx();
		Node userNode = dbService.createNode();
		userNode.setProperty(Neo4jConstants.PROPERTY_NAME, username);
		dbService.getNodeById(0).createRelationshipTo(userNode, Relationships.USER);
		userCacheNode.setProperty(username, userNode.getId());
		tx.success();
		tx.finish();
		return new Neo4jUser(userNode);
	}

	public static Node getUserNode(GraphDatabaseService dbService, String username) throws UserDoesNotExists {
		Node userCacheNode = getUserCacheNode(dbService);
		if(!userCacheNode.hasProperty(username)) {
			throw new UserDoesNotExists(username + " user does not exist.");
		}
		long userNodeId = (long) userCacheNode.getProperty(username);
		Node userNode = dbService.getNodeById(userNodeId);
		return userNode;
	}

	private static Node getUserCacheNode(GraphDatabaseService dbService) {
		Relationship rel = dbService.getNodeById(0).getSingleRelationship(Relationships.CACHE, Direction.OUTGOING);
		if(rel != null && rel.getEndNode() != null) {
			return rel.getEndNode();
		}
		Transaction tx = dbService.beginTx();
		Node userCacheNode = dbService.createNode();
		userCacheNode.setProperty(Neo4jConstants.PROPERTY_NAME, "USER_CACHE_NODE");
		dbService.getNodeById(0).createRelationshipTo(userCacheNode, Relationships.CACHE);
		tx.success();
		tx.finish();
		return userCacheNode;
	}

	public static Map<String, User> getAllUsers(GraphDatabaseService dbService) {
		Iterable<Relationship> rels = dbService.getNodeById(0).getRelationships(Relationships.USER, Direction.OUTGOING);
		HashMap<String, User> userMap = new HashMap<String, User>();
		for(Relationship rel : rels) {
			Node userNode = rel.getEndNode();
			String username = (String) userNode.getProperty(Neo4jConstants.PROPERTY_NAME);
			userMap.put(username, new Neo4jUser(userNode));
		}
		return userMap;
	}
	public static HashMap<String, Node> getAllDataStoreNodes(GraphDatabaseService dbService, String username) throws UserDoesNotExists {
		return getAllDataStoreNodes(dbService, getUserNode(dbService, username));
	}

	public static boolean deleteSubGraph(GraphDatabaseService dbService, Node rootNode) {
		boolean isSubGraphDeleted = false;
		ExecutionEngine engine = new ExecutionEngine(dbService);
		Set<Relationship> relSet = new HashSet<>();
		Set<Node> nodeSet = new HashSet<>();
		ExecutionResult result = engine.execute("start n=node(" + rootNode.getId() + ")\n" + 
				"match p=(n)-[*]->(m)\n" + 
				"return p order by length(p) desc");
		Iterator<Path> p_column = result.columnAs("p");
		for(Path path : IteratorUtil.asIterable(p_column)) {
			for(Relationship rel : path.relationships()) {
				relSet.add(rel);
			}

			for(Node node : path.nodes()) {
				nodeSet.add(node);
			}
		}
		Transaction tx = dbService.beginTx();
		try {

			for(Relationship rel : relSet) {
				rel.delete();
			}
			for(Node node : nodeSet) {
				if(node.getId() == rootNode.getId()) {
					Iterable<Relationship> rels = node.getRelationships(Direction.INCOMING);
					for(Relationship rel : rels) {
						rel.delete();
					}
				}
				node.delete();
			}
			tx.success();
		}
		finally {
			tx.finish();
			isSubGraphDeleted = true;
		}
		return isSubGraphDeleted;
	}


	public static boolean deleteUser(GraphDatabaseService dbService, String username) throws UserDoesNotExists {
		Node userNode = getUserNode(dbService, username);
		boolean isUserDeleted = false;
		if(deleteSubGraph(dbService, userNode)) {
			Node userCacheNode = getUserCacheNode(dbService);
			Transaction tx = dbService.beginTx();
			userCacheNode.removeProperty(username);
			tx.success();
			tx.finish();
			isUserDeleted = true;
		}
		return isUserDeleted;
	}

	public static boolean deleteDataStore(GraphDatabaseService dbService, String username, String dataStoreName) throws UserDoesNotExists, Neo4jNodeCreationException {
		boolean isDSDeleted = false;
		Node userNode = getUserNode(dbService, username);
		Node dsCacheNode = getDSCacheNode(dbService, userNode);
		try {
			long dsNodeId = (long) dsCacheNode.getProperty(dataStoreName);
			if(deleteSubGraph(dbService, dbService.getNodeById(dsNodeId))) {
				Transaction tx = dbService.beginTx();
				dsCacheNode.removeProperty(dataStoreName);
				tx.success();
				tx.finish();
				isDSDeleted = true;
			}
		}
		catch(NotFoundException e) {
			throw new UserDoesNotExists(e);
		}

		return isDSDeleted;
	}

	public static User getUser(GraphDatabaseService neo4jDB, String username) throws UserDoesNotExists {
		Node userNode = Neo4jDatabaseUtils.getUserNode(neo4jDB, username);
		return new Neo4jUser(userNode);
	}

}

