package com.timenome.neo4j.exception;

public class Neo4jPropertiesException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6377021503186983403L;

	public Neo4jPropertiesException() {
		// TODO Auto-generated constructor stub
	}

	public Neo4jPropertiesException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public Neo4jPropertiesException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public Neo4jPropertiesException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
