package com.timenome.neo4j.exception;

public class Neo4jUnexpectedSituationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7504473905601590267L;

	public Neo4jUnexpectedSituationException() {
		// TODO Auto-generated constructor stub
	}

	public Neo4jUnexpectedSituationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public Neo4jUnexpectedSituationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public Neo4jUnexpectedSituationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public Neo4jUnexpectedSituationException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
