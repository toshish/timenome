package com.timenome.neo4j.core;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;

import com.timenome.api.BuiltInCDF;
import com.timenome.api.Entry;
import com.timenome.api.core.Archive;
import com.timenome.api.core.DataStore;
import com.timenome.api.core.TimeRangeDef;
import com.timenome.api.shared.ConsolidationFunction;
import com.timenome.neo4j.Relationships;
import com.timenome.neo4j.constants.Neo4jConstants;
import com.timenome.neo4j.utils.Neo4jDatabaseUtils;

public class Neo4jDataStore extends DataStore {

	private Node dsNode;
	private GraphDatabaseService dbService;

	public Neo4jDataStore(GraphDatabaseService dbService, Node dsNode) {
		this((String)dsNode.getProperty("name"));
		this.dsNode = dsNode;
		this.dbService = dbService;
		loadDataStore(archiveMap);
	}

	public Neo4jDataStore(String dataStoreName) {
		super(dataStoreName);
	}


	private void loadDataStore(Map<String, Archive> archiveMap) {
		Transaction tx = dbService.beginTx();
		// Get Archives of this DataStore and load them into map.
		Iterator<Relationship> relIter = dsNode.getRelationships(Direction.OUTGOING, Relationships.HAS_ARCHIVE).iterator();
		while(relIter.hasNext()) {
			Node archiveNode = relIter.next().getEndNode();
			String archiveName = (String) archiveNode.getProperty(Neo4jConstants.PROPERTY_NAME);
			Neo4jArchive neo4jArchive = new Neo4jArchive(archiveNode);
			archiveMap.put(archiveName, neo4jArchive);
		}
		tx.success();
		tx.finish();
	}
	/**
	 * Adds the archive to this Data Store by attaching archive node to this Data Store node by relationship {@link Relationships.HAS_ARCHIVE}.
	 * @param archive An instance of archive to be added
	 */
	@Override
	protected void addArchiveSpec(Archive archive) {
		Node archiveNode = (Node) archive.getSpecificArchiveObject();
		Transaction tx = dbService.beginTx();
		// Attach this node to this DataStore's node
		dsNode.createRelationshipTo(archiveNode, Relationships.HAS_ARCHIVE);
		Node archiveDataNode = dbService.createNode();
		archiveDataNode.setProperty(Neo4jConstants.PROPERTY_NAME, "ARCHIVE_DATA_ROOT");
		archiveNode.createRelationshipTo(archiveDataNode, Relationships.HAS_DATA);
		tx.success();
		tx.finish();
	}


	@Override
	public List<Entry<? extends Number>> get() {
		return null;
		// TODO Auto-generated method stub

	}

	@Override
	protected boolean removeArchiveSpec(String archiveName)	{
		Archive archive = archiveMap.get(archiveName);
		Node archiveNode = (Node) archive.getSpecificArchiveObject();
		boolean isDeleted = Neo4jDatabaseUtils.deleteSubGraph(dbService, archiveNode);
		if(isDeleted) {
			archive.stop();
		}
		return isDeleted;
		//		Transaction tx = dbService.beginTx();
		//		// Delete relationship connecting this node to Data store node
		//		Relationship reletionship = archiveNode.getSingleRelationship(Relationships.HAS_ARCHIVE, Direction.INCOMING);
		//		reletionship.delete();
		//		// Delete all data stored in this archive
		//		Iterator<Relationship> iter = archiveNode.getRelationships(Direction.OUTGOING, Relationships.HAS_ARCHIVE_DATA).iterator();
		//		while(iter.hasNext()) {
		//			Relationship rel = iter.next();
		//			Node dataNode = rel.getEndNode();
		//			dataNode.delete();
		//			rel.delete();
		//		}
		//		// Now delete actual archive node.
		//		archiveNode.delete();
		//		tx.success();
		//		tx.finish();
		//		return true;
	}

	@Override
	protected Archive getConcreteArchive(String archiveName,
			TimeRangeDef storageRange, TimeRangeDef storagePrecision,
			ConsolidationFunction consolidationFunction) {
		return new Neo4jArchive(archiveName, storageRange, storagePrecision, consolidationFunction);
	}

	@Override
	protected Archive getConcreteArchive(String archiveName,
			TimeRangeDef storageRange, TimeRangeDef storagePrecision,
			BuiltInCDF builtInCDF) {
		return new Neo4jArchive(archiveName, storageRange, storagePrecision, builtInCDF);
	}

	@Override
	protected boolean setAttributeSpec(String key, String value) {
		try {
			Transaction tx = dbService.beginTx();
			dsNode.setProperty(key, value);
			tx.success();
			attributeMap.put(key, value);
			tx.finish();
		} catch(Exception e) {
			return false;
		}
		return true;
	}

	@Override
	protected boolean removeAttributeSpec(String key) {
		try {
			Transaction tx = dbService.beginTx();
			dsNode.removeProperty(key);
			tx.success();
			attributeMap.remove(key);
			tx.finish();
		} catch(Exception e) {
			return false;
		}
		return true;
	}

}

