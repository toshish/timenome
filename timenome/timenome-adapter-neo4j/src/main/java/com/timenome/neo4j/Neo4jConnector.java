package com.timenome.neo4j;

import java.util.Map;

import com.timenome.api.core.Connection;
import com.timenome.api.core.Connector;
import com.timenome.api.exceptions.ConnectionException;

public class Neo4jConnector implements Connector {

	@Override
	public Connection connect(Map<String, String> connectionParams)
			throws ConnectionException {
		return Neo4jConnection.getNeo4jConnection();
	}

}
