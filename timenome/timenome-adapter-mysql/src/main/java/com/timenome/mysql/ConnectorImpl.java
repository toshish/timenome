package com.timenome.mysql;

import java.util.Map;

import com.timenome.api.core.Connection;
import com.timenome.api.core.Connector;
import com.timenome.api.core.DataStore;
import com.timenome.api.core.User;
import com.timenome.api.exceptions.ConnectionException;
import com.timenome.api.exceptions.DataStoreException;
import com.timenome.api.exceptions.UserDoesNotExists;

public class ConnectorImpl implements Connector, Connection{
	
	@Override
	public Connection connect(Map<String, String> connectionParams) throws ConnectionException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void close() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean deleteDataStore(String dataStoreName, String username)
			throws DataStoreException, UserDoesNotExists {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public DataStore createDataStore(String dsName, String username)
			throws DataStoreException, UserDoesNotExists {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DataStore getDataStore(String dsName, String username)
			throws UserDoesNotExists {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, DataStore> getAllDataStores(String username)
			throws UserDoesNotExists {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, User> getAllUsers() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User createUser(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteUser(String username) throws UserDoesNotExists {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public User getUser(String username) throws UserDoesNotExists {
		// TODO Auto-generated method stub
		return null;
	}
	
}
